SELECT idsa, SUM(mt) 
FROM 
	(SElECT tc.codigoproducto as prod, tc.cantidadproducto as cant, tc.idtransaccion as trans, ids as idsa 
	FROM 
		compra tc 
	INNER JOIN 
		(SELECT codigodebarras as cb, idsucursal as ids FROM PRODUCTOSUMINISTRADO) tcm 
	ON tc.codigoproducto = tcm.cb) A 
INNER JOIN 
	(SELECT fecha as fecha, monto as mt, id as id FROM TRANSACCIONCLIENTE WHERE fecha BETWEEN ? AND ?) B 
	ON a.trans = b.id 
	GROUP BY idsa