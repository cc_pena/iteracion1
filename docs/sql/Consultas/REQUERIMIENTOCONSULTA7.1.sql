SELECT tt.sucursal, 
       tb."dia", 
       tb."month", 
       tt.maxi 
FROM   (SELECT sucursal, 
               Max(ingremax) AS maxi 
        FROM   (SELECT sucursal, 
                       "month", 
                       "dia", 
                       Sum(monto) AS INGREMAX 
                FROM   (SELECT ps.idsucursal                AS Sucursal, 
                               Extract (day FROM tc.fecha)  AS "dia", 
                               Extract(month FROM tc.fecha) AS "month", 
                               cc.cantidadproducto          AS monto 
                        FROM   producto p, 
                               productosuministrado ps, 
                               transaccioncliente tc, 
                               compra cc 
                        WHERE  p.codigodebarras = ps.codigodebarras 
                               AND tc.id = cc.idtransaccion 
                               AND cc.codigoproducto = ps.codigodebarras 
                               AND p.tipo = 'Carnes' 
                               AND Extract(year FROM tc.fecha) = 2018) 
                GROUP  BY sucursal, 
                          "month", 
                          "dia") 
        GROUP  BY sucursal) tt, 
       (SELECT sucursal, 
               "month", 
               "dia", 
               Sum(monto) AS "Demanda máxima" 
        FROM   (SELECT ps.idsucursal                AS Sucursal, 
                       Extract (day FROM tc.fecha)  AS "dia", 
                       Extract(month FROM tc.fecha) AS "month", 
                       cc.cantidadproducto          AS monto 
                FROM   producto p, 
                       productosuministrado ps, 
                       transaccioncliente tc, 
                       compra cc 
                WHERE  p.codigodebarras = ps.codigodebarras 
                       AND tc.id = cc.idtransaccion 
                       AND cc.codigoproducto = ps.codigodebarras 
                       AND p.tipo = 'Carnes' 
                       AND Extract(year FROM tc.fecha) = 2018) 
        GROUP  BY sucursal, 
                  "month", 
                  "dia") tb 
WHERE  tt.maxi = tb."demanda m�xima"; 