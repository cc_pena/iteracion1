    SELECT cod, mes FROM (SELECT ip.codigoproducto AS cod , EXTRACT(month from op.fechaentregareal) AS mes , COUNT(*) AS contador
    FROM ordenpedido op INNER JOIN infoproducto ip ON op.id = ip.idorden
    GROUP BY ip.codigoproducto , fechaentregareal 
    ORDER BY cod, fechaentregareal )
    WHERE contador > 0;