SELECT *
FROM producto
WHERE ? BETWEEN ? AND ? ;

SELECT *
FROM producto
WHERE ? = ?;

SELECT *
FROM productosuministrado
WHERE ? = ?;

SELECT *
FROM productosdelproveedor
WHERE nitproveedor = ?;

--Cuanto ha vendido los productos en dinero----
SELECT co.codigoproducto, SUM(trc.monto)
FROM compra co
INNER JOIN transaccioncliente trc ON co.idtransaccion = trc.id
GROUP BY co.codigoproducto;
--Productos que han vendido más de x unidades.----
SELECT co.codigoproducto , SUM(co.cantidadproducto) AS Suma
FROM compra co
GROUP BY co.codigoproducto HAVING suma > ? ;