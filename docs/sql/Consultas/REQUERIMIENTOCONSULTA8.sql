SELECT idcliente 
FROM   (SELECT idcliente, 
               Count(*) AS mesesConsecutivosComprando 
        FROM   (SELECT idcliente, 
                       mes, 
                       anio, 
                       Count(*) compras 
                FROM   (SELECT tc.idcliente, 
                               Extract (month FROM tc.fecha) AS mes, 
                               Extract (year FROM tc.fecha)  AS anio 
                        FROM   transaccioncliente tc, 
                               compra cc, 
                               productosuministrado ps 
                        WHERE  tc.id = cc.idtransaccion 
                               AND cc.codigoproducto = ps.codigodebarras 
                               AND ps.idsucursal = 2) 
                GROUP  BY mes, 
                          anio, 
                          idcliente) 
        WHERE  compras >= 2 
        GROUP  BY idcliente) 
WHERE  mesesconsecutivoscomprando >= ( ( (SELECT Extract(year FROM sysdate) 
                                          FROM   dual) - (SELECT Extract( 
                                                         year FROM fechamenor) 
                                                          FROM   (SELECT 
                                         Min(fecha) AS fechaMenor 
                                                  FROM 
                                         transaccioncliente)) ) * 12 ) 
                                            + 
                                                          ( (SELECT Extract( 
                                                            month FROM sysdate) 
                                                             FROM   dual) - 
                                     (SELECT 
                                     Extract(month FROM 
                                             fechamenor) 
                                                      FROM 
                                     (SELECT Min(fecha) AS 
                                             fechaMenor 
                                      FROM   transaccioncliente)) 
                                                          ) 

