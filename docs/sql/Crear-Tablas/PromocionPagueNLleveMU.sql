CREATE TABLE PROMOCIONPAGUENLLEVEMUNIDADES 
(
  UNIDADESPAGADAS NUMBER NOT NULL 
, UNIDADESOFRECIDAS NUMBER NOT NULL 
, ID NUMBER NOT NULL 
, CONSTRAINT PROMOCIONPAGUENLLEVEMUNIDA_PK PRIMARY KEY 
  (
    ID 
  )
  USING INDEX 
  (
      CREATE UNIQUE INDEX PROMOCIONPAGUENLLEVEMUNIDA_PK ON PROMOCIONPAGUENLLEVEMUNIDADES (ID ASC) 
      NOLOGGING 
      TABLESPACE TBSPROD 
      PCTFREE 10 
      INITRANS 2 
      STORAGE 
      ( 
        BUFFER_POOL DEFAULT 
      ) 
      NOPARALLEL 
  )
  ENABLE 
) 
NOLOGGING 
TABLESPACE TBSPROD 
PCTFREE 10 
INITRANS 1 
STORAGE 
( 
  BUFFER_POOL DEFAULT 
) 
NOCOMPRESS 
NO INMEMORY 
NOPARALLEL;

ALTER TABLE PROMOCIONPAGUENLLEVEMUNIDADES
ADD CONSTRAINT PROMOCIONPAGUENLLEVEMUNID_FK1 FOREIGN KEY
(
  ID 
)
REFERENCES PROMOCION
(
  ID 
)
ENABLE;
