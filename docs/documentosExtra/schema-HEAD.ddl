
    create table Bodega (
        id number not null,
        cantidadEnMasa integer not null,
        cantidadVolumetrica integer not null,
        capacidadEnMasa integer not null,
        capacidadVolumetrica integer not null,
        sucursal_id number not null,
        tipoProducto_id number,
        primary key (id)
    );

    create table CategoriaProducto (
        id number not null,
        nombre varchar(255) not null,
        primary key (id)
    );

    create table Cliente (
        DTYPE varchar(31) not null,
        id number not null,
        idDB number not null,
        puntos integer not null,
        NIT varchar(255) not null,
        direccion varchar(255) not null,
        cedula varchar(255) not null,
        email varchar(255) not null,
        nombre varchar(255) not null,
        primary key (id)
    );

    create table Estante (
        id number not null,
        cantidadEnMasa integer not null,
        cantidadVolumetrica integer not null,
        capacidadEnMasa integer not null,
        capacidadVolumetrica integer not null,
        sucursal_id number not null,
        tipoProducto_id number not null,
        primary key (id)
    );

    create table OrdenPedido (
        proveedorId number not null,
        sucursalId number not null,
        id number not null,
        calificacionDelServicio double not null,
        estado varchar(255) not null,
        fechaEntregaEstipulada date,
        fechaEntregaReal date,
        proveedores_id number not null,
        sucursales_id number,
        primary key (proveedorId, sucursalId)
    );

    create table Producto (
        DTYPE varchar(31) not null,
        id number not null,
        cantidadEnLaPresentacion integer not null,
        codigoBarras varchar(255) not null,
        marca varchar(255) not null,
        nombre varchar(255) not null,
        peso varchar(255) not null,
        presentacion varchar(255) not null,
        unidadDeMedida varchar(255) not null,
        volumen varchar(255) not null,
        cantidadEnBodega integer not null,
        cantidadEnEstante integer not null,
        precioPorUnidadMedida varchar(255) not null,
        precioUnitario double,
        tipo_id number not null,
        bodega_id number,
        estante_id number,
        primary key (id)
    );

    create table Promocion (
        DTYPE varchar(31) not null,
        id number not null,
        fechaExpedicion date,
        fechaFinExistencias date,
        fechaFinalizacion date,
        nombre varchar(255) not null,
        tipo integer not null,
        unidades integer not null,
        montoTotal double,
        porcentajeDescuento double,
        porctDescSegProducto double,
        unidadesOfrecidas integer not null,
        unidadesPagadas integer not null,
        cantidadOfrecida integer not null,
        cantidadPagada integer not null,
        primary key (id)
    );

    create table Promocion_Producto (
        Promocion_id number not null,
        productos_id number not null,
        primary key (Promocion_id, productos_id),
        unique (productos_id)
    );

    create table Proveedor (
        id number not null,
        NIT varchar(255) not null,
        calificacion double not null,
        nombre varchar(255) not null,
        primary key (id)
    );

    create table Proveedor_Producto (
        proveedor_id number not null,
        productosProveedor_id number not null,
        primary key (proveedor_id, productosProveedor_id)
    );

    create table Sucursal (
        id number not null,
        ciudad varchar(255) not null,
        direccion varchar(255) not null,
        nivelAbastecimiento integer not null,
        nivelReOrden integer not null,
        nombre varchar(255) not null,
        nombreSupermercado varchar(255) not null,
        primary key (id)
    );

    create table TipoProducto (
        id number not null,
        nombre varchar(255) not null,
        categoriaProducto_id number not null,
        primary key (id)
    );

    create table TransaccionCliente (
        id number not null,
        fecha date,
        monto double not null,
        cliente_id number not null,
        primary key (id)
    );

    create table Usuario (
        id number not null,
        password varchar(255) not null,
        user varchar(255) not null,
        sucursal_id number not null,
        primary key (id)
    );

    alter table Bodega 
        add constraint FK76EB07083BCB45DA 
        foreign key (sucursal_id) 
        references Sucursal;

    alter table Bodega 
        add constraint FK76EB07083BCC717A 
        foreign key (tipoProducto_id) 
        references TipoProducto;

    alter table Estante 
        add constraint FKCDBB1C43BCB45DA 
        foreign key (sucursal_id) 
        references Sucursal;

    alter table Estante 
        add constraint FKCDBB1C43BCC717A 
        foreign key (tipoProducto_id) 
        references TipoProducto;

    alter table OrdenPedido 
        add constraint FKEB09130FE7CC726C 
        foreign key (sucursales_id) 
        references Sucursal;

    alter table OrdenPedido 
        add constraint FKEB09130F817938EC 
        foreign key (proveedores_id) 
        references Proveedor;

    alter table Producto 
        add constraint FKC80635809FE194DA 
        foreign key (bodega_id) 
        references Bodega;

    alter table Producto 
        add constraint FKC8063580E81F0F1A 
        foreign key (estante_id) 
        references Estante;

    alter table Producto 
        add constraint FKC80635803BC94ADA 
        foreign key (tipo_id) 
        references TipoProducto;

    alter table Promocion_Producto 
        add constraint FK11BD3F2B397A811A 
        foreign key (Promocion_id) 
        references Promocion;

    alter table Promocion_Producto 
        add constraint FK11BD3F2BCB5A73B 
        foreign key (productos_id) 
        references Producto;

    alter table Proveedor_Producto 
        add constraint FKEF7DB1E1C5F2DE2F 
        foreign key (productosProveedor_id) 
        references Producto;

    alter table Proveedor_Producto 
        add constraint FKEF7DB1E1D12C3CDA 
        foreign key (proveedor_id) 
        references Proveedor;

    alter table TipoProducto 
        add constraint FK89EB68D4435C88DA 
        foreign key (categoriaProducto_id) 
        references CategoriaProducto;

    alter table TransaccionCliente 
        add constraint FK998BD28BD8418F5A 
        foreign key (cliente_id) 
        references Cliente;

    alter table Usuario 
        add constraint FK5B4D8B0E3BCB45DA 
        foreign key (sucursal_id) 
        references Sucursal;
