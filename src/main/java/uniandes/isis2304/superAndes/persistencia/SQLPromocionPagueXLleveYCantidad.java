package uniandes.isis2304.superAndes.persistencia;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.isis2304.superAndes.negocio.PromocionPagueXLleveYCantidad;

/**
 * Clase que encapsula los métodos que hacen acceso a la base de datos para el concepto PROMOCIÓN PAGUE x LLEVE y cantidad de SuperAndes.
 * Nótese que es una clase que es sólo conocida en el paquete de persistencia.
 * 
 * @author fj.gonzalez
 */
class SQLPromocionPagueXLleveYCantidad 
{
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes psa;
	/* ****************************************************************
	 * 			Constructor
	 *****************************************************************/
	public  SQLPromocionPagueXLleveYCantidad(PersistenciaSuperAndes psa) 
	{
		this.psa = psa;
	}
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Crea y ejecuta la sentencia SQL para adicionar un Promocion dos productos a la base de datos de SuperAndes
	 * @return EL número de tuplas insertadas
	 */
	public long adicionarPromocionPagueXLleveYCantidad (PersistenceManager pm, long id , int cantidadpagada, int cantidadofrecida) 
	{
		Query q = pm.newQuery(SQL, "INSERT INTO " + psa.darTablaPromocionPagueXLleveYCantidad()+ "(id , cantidadpagada, cantidadofrecida) values (?, ?, ?)");
		q.setParameters(id , cantidadpagada, cantidadofrecida);
		return (long) q.executeUnique();
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de la promocion de la 
	 * base de datos de SuperAndes con el id dado
	 * @param pm - El manejador de persistencia
	 * @param id - id de la promocion buscada
	 * @return Una promocion
	 */
	public PromocionPagueXLleveYCantidad darPromocionPagueXLleveYCantidad(PersistenceManager pm, long id)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaPromocionPagueXLleveYCantidad() + " WHERE id = ?");
		q.setResultClass(PromocionPagueXLleveYCantidad.class);
		q.setParameters(id);
		return (PromocionPagueXLleveYCantidad) q.executeList().get(0);
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de las promociones de la 
	 * base de datos de SuperAndes
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos promocion
	 */
	public List<PromocionPagueXLleveYCantidad > darPromocionesPromocionPagueXLleveYCantidad (PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaPromocionPagueXLleveYCantidad() );
		q.setResultClass(PromocionPagueXLleveYCantidad.class);
		return (List<PromocionPagueXLleveYCantidad >) q.executeList();
	}
	
}
