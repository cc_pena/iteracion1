package uniandes.isis2304.superAndes.persistencia;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.isis2304.superAndes.negocio.Carrito;
import uniandes.isis2304.superAndes.negocio.Cliente;
import uniandes.isis2304.superAndes.negocio.ClienteEmpresa;

public class SQLCarrito 
{
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes psa;
	/* ****************************************************************
	 * 			Constructor
	 *****************************************************************/
	public SQLCarrito(PersistenciaSuperAndes psa) 
	{
		this.psa = psa;
	}
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Crea y ejecuta la sentencia SQL para adicionar un producto al cliente o viceversa.
	 * @return EL número de tuplas insertadas
	 */
	public long adicionarProductoAlCliente (PersistenceManager pm, long idCliente, String idProducto, int cantidad) 
	{
		Query q = pm.newQuery(SQL, "INSERT INTO " + psa.darTablaCarrito()  + "(idcliente, idproducto,cantidad) values (?, ?, ?)");
		q.setParameters(idCliente, idProducto, cantidad);
		return (long) q.executeUnique();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para eliminar una tupla carrito de la base de datos de SuperAndes.
	 * @param pm - El manejador de persistencia
	 * @return EL numero de tuplas eliminadas
	 */
	public long eliminarProductoDelCliente (PersistenceManager pm, long idCliente, String idProducto)
	{
		Query q = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaCarrito() + " WHERE idcliente = ? AND idproducto = ?");
		q.setParameters(idCliente,idProducto);
		
		return (long) q.executeUnique();            
	}
	/**
	 * Crea y ejecuta la sentencia SQL para eliminar una tupla carrito de la base de datos de SuperAndes.
	 * @param pm - El manejador de persistencia
	 * @return EL numero de tuplas eliminadas
	 */
	public long vaciarCarrito (PersistenceManager pm, long idCliente)
	{
		Query q = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaCarrito() + " WHERE idcliente = ?");
		q.setParameters(idCliente);
		return (long) q.executeUnique();            
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la informaci�n de los producto de un cliente determinado en 
	 * base de datos de SUPERANDES
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos con los id productos del cliente
	 */
	public List<Carrito> darProductosDelCliente(PersistenceManager pm, long idCliente)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaCarrito() + " WHERE idcliente = ?" );
		q.setParameters(idCliente);
 		q.setResultClass(Carrito.class);
 		return (List<Carrito>) q.executeList();
		
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de los CARRITOS de la 
	 * base de datos de SUPERANDES
	 * @param pm - El manejador de persistencia
	 * @param id - El identificador unico del cliente
	 * @return Una  objetos ProductoCliente
	 */
	public Carrito darProductoCliente(PersistenceManager pm, long idCliente, String idProducto)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaCarrito() + " WHERE idcliente = ? AND idproducto = ?");
		q.setParameters(idCliente, idProducto);
		q.setResultClass(Carrito.class);
		return (Carrito) q.executeUnique();
	}
	
		
		

}
