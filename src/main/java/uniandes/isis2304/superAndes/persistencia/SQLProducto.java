package uniandes.isis2304.superAndes.persistencia;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.isis2304.superAndes.negocio.Producto;
/**
 * Clase que encapsula los métodos que hacen acceso a la base de datos para el concepto  PRODUCTO de SuperAndes.
 * Nótese que es una clase que es sólo conocida en el paquete de persistencia
 * 
 * @author fj.gonzalez
 */
class SQLProducto 
{

	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes psa;
	/* ****************************************************************
	 * 			Constructor
	 *****************************************************************/
	public SQLProducto(PersistenciaSuperAndes psa) 
	{
		this.psa = psa;
	}
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/

	/**
	 * Crea y ejecuta la sentencia SQL para adicionar un Producto a la base de datos de SuperAndes
	 * @return EL número de tuplas insertadas
	 */
	public long adicionarProducto (PersistenceManager pm, String marca, String nombre, String  presentacion, String unidaddemedida,String volumen,String peso,String codigobarras,String tipo,Integer cantidadpresentacion) 
	{
		Query q = pm.newQuery(SQL, "INSERT INTO " + psa.darTablaProducto () + "(marca, nombre, presentacion, unidaddemedida,volumen,peso,codigodebarras,tipo,cantidadpresentacion) values (?, ?, ?, ?, ?, ?, ?, ?, ?)");
		q.setParameters(marca,  nombre, presentacion, unidaddemedida,volumen,peso,codigobarras,tipo,cantidadpresentacion);
		return (long) q.executeUnique();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para eliminar UN Producto de la base de datos de SuperAndes, por su identificador
	 * @param pm - El manejador de persistencia
	 * @return EL número de tuplas eliminadas
	 */
	public long eliminarProductoPorCodigoBarras (PersistenceManager pm, String cod)
	{
		Query q = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaProducto () + " WHERE codigodebarras = ?");
		q.setParameters(cod);
		return (long) q.executeUnique();            
	}	

	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de UN Producto de la 
	 * base de datos de SuperAndes, por su identificador
	 * @param pm - El manejador de persistencia
	 * @return El objeto Producto que tiene el identificador dado
	 */
	public Producto darProductoPorCodigoBarras (PersistenceManager pm, String cod) 
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaProducto () + " WHERE codigodebarras = ?");
		q.setParameters(cod);
		q.setResultClass(Producto.class);
		return (Producto) q.executeList().get(0);
	}

	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de LOS Productos de la 
	 * base de datos de SuperAndes, por su nombre
	 * @return Una lista de objetos Producto que tienen el nombre dado
	 */
	public List<Producto> darProductoPorNombre (PersistenceManager pm, String nombreProducto) 
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaProducto () + " WHERE nombre = ?");
		q.setResultClass(Producto.class);
		q.setParameters(nombreProducto);
		return (List<Producto>) q.executeList();
	}

	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de LOS ProductoES de la 
	 * base de datos de SuperAndes
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos Producto
	 */
	public List<Producto> darProductos (PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaProducto ());
		q.setResultClass(Producto.class);
		return (List<Producto>) q.executeList();
	}

	public List<Object[]> darProductosPorTipo (PersistenceManager pm, String pTipo)
	{
		Query q = pm.newQuery(SQL, "SELECT p.* FROM " + psa.darTablaProducto () +  " p, "+ psa.darTablaProductoSuministrado()+ " ps WHERE tipo = ? AND p.codigodebarras = ps.codigodebarras");
		q.setParameters(pTipo);
		q.setResultClass(Producto.class);
		return (List<Object[]>) q.executeList();
	}
	
	public List<Producto> darProductosPorTipo2(PersistenceManager pm, String pTipo)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaProducto () +  " p  WHERE p.tipo = ?");
		q.setParameters(pTipo);
		q.setResultClass(Producto.class);
		return (List<Producto>) q.executeList();
	}

	/**
	 * Crea y ejecuta la sentencia SQL para:
	 * Eliminar un Producto y las transacciones que se han hecho de dicho producto.
	 * En caso que el Producto esté referenciado por otra relación, NO SE BORRA NI EL Producto, NI SUS VISITAS
	 * Adiciona entradas al log de la aplicación
	 * @param pm - El manejador de persistencia
	 * @param cod - El Producto que se quiere eliminar
	 * @return Una pareja de números [número de Productos eliminados, número de visitas eliminadas]
	 */
	public long [] eliminarProductoEInfosProducto (PersistenceManager pm, String cod) 
	{
		Query q1 = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaInfoProducto() + " WHERE codigoProducto = ?");
		q1.setParameters(cod);
		Query q2 = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaProducto () + " WHERE id = ?");
		q2.setParameters(cod);

		long infosEliminadas = (long) q1.executeUnique ();
		long ProductosEliminados = (long) q2.executeUnique ();
		return new long[] {infosEliminadas, ProductosEliminados};
	}
}
