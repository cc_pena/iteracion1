package uniandes.isis2304.superAndes.persistencia;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.isis2304.superAndes.negocio.PromocionPague1LleveElSegundo;

/**
 * Clase que encapsula los métodos que hacen acceso a la base de datos para el concepto PROMOCIÓN PAGUE 1 LLEVE EL SEGUNDO de SuperAndes.
 * Nótese que es una clase que es sólo conocida en el paquete de persistencia.
 * 
 * @author fj.gonzalez
 */
class SQLPromocionPague1LleveElSegundo
{

	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes psa;
	/* ****************************************************************
	 * 			Constructor
	 *****************************************************************/
	public SQLPromocionPague1LleveElSegundo(PersistenciaSuperAndes psa) 
	{
		this.psa = psa;
	}
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Crea y ejecuta la sentencia SQL para adicionar un Promocion dos productos a la base de datos de SuperAndes
	 * @return EL número de tuplas insertadas
	 */
	public long adicionarPromocionPague1LleveElSegundo (PersistenceManager pm, long id , double porctdescsegproducto) 
	{
		Query q = pm.newQuery(SQL, "INSERT INTO " + psa.darTablaPromocionPague1LleveElSegundo() + "(id ,  porctdescsegproducto) values (?, ?)");
		q.setParameters(id , porctdescsegproducto);
		return (long) q.executeUnique();
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de la promocion de la 
	 * base de datos de SuperAndes con el id dado
	 * @param pm - El manejador de persistencia
	 * @param id - id de la promocion buscada
	 * @return Una promocion
	 */
	public PromocionPague1LleveElSegundo darPromocionPague1LleveElSegundo(PersistenceManager pm, long id)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaPromocionPague1LleveElSegundo()+ " WHERE id = ?");
		q.setResultClass(PromocionPague1LleveElSegundo.class);
		q.setParameters(id);
		return (PromocionPague1LleveElSegundo)q.executeList().get(0);
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de las promociones de la 
	 * base de datos de SuperAndes
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos promocion
	 */
	public List<PromocionPague1LleveElSegundo > darPromocionesPromocionPague1LleveElSegundo (PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaPromocionPague1LleveElSegundo() );
		q.setResultClass(PromocionPague1LleveElSegundo .class);
		return (List<PromocionPague1LleveElSegundo >) q.executeList();
	}
	
}
