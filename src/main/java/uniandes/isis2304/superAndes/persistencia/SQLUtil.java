package uniandes.isis2304.superAndes.persistencia;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

/**
 * @author fj.gonzalez
 */
public class SQLUtil 
{
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes psa;
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/

	/**
	 * Constructor
	 * @param pp - El Manejador de persistencia de la aplicación
	 */
	public SQLUtil (PersistenciaSuperAndes pp)
	{
		this.psa = pp;
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para obtener un nuevo número de secuencia
	 * @param pm - El manejador de persistencia
	 * @return El número de secuencia generado
	 */
	public long nextval (PersistenceManager pm)
	{
        Query q = pm.newQuery(SQL, "SELECT "+ psa.darSeqSuperAndes () + ".nextval FROM DUAL");
        q.setResultClass(Long.class);
        long resp = (long) q.executeUnique();
        return resp;
	}

	/**
	 * Crea y ejecuta las sentencias SQL para cada tabla de la base de datos - EL ORDEN ES IMPORTANTE 
	 * @param pm - El manejador de persistencia
	 * @return Un arreglo con 7 números que indican el número de tuplas borradas en las tablas ---
	 */
	public long[] limpiarSuperAndes (PersistenceManager pm)
	{
		Query qCompra = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaCompra());
		Query qTransaccion = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaTransaccion());
		Query qClienteN = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaClienteNatural());
		Query qClienteE = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaClienteEmpresa());
		Query qCliente = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaCliente());
		Query qPromo1 = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaPromocionDescuento());
		Query qPromo2 = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaPromocionDosProductos());
		Query qPromo3 = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaPromocionPague1LleveElSegundo());
		Query qPromo4 = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaPromocionPagueNLleveMUnidades());
		Query qPromo5 = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaPromocionPagueXLleveYCantidad());
		Query qPromo = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaPromocion());
		Query qCarrito = pm.newQuery(SQL, "DELETE FROM "+psa.darTablaCarrito());
		Query qSuministrado = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaProductoSuministrado());
		Query qBodega = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaBodega());
		Query qEstante = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaEstante());
		Query qInfoP = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaInfoProducto());
		Query qPdP = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaProductosDelProveedor());
		Query qProd = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaProducto());
		Query qTipo = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaTipoProducto());
		Query qCategoria = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaCategoriaProducto());
		Query qOrden = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaOrdenPedido());
		Query qProveedor = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaProveedor());
		Query qUsuario = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaUsuario());
		Query qSucursal = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaSucursal());
		
		long a1 = (long) qCompra.executeUnique();
		qCarrito.executeUnique();
		long a2 = (long) qTransaccion.executeUnique();
		long a3 = (long) qClienteN.executeUnique();
		long a4 = (long) qClienteE.executeUnique();
		long a5 = (long) qCliente.executeUnique();
		long a6 = (long) qPromo1.executeUnique();
		long a7 = (long) qPromo2.executeUnique();
		long a8 = (long) qPromo3.executeUnique();
		long a9 = (long) qPromo4.executeUnique();
		long a10 = (long) qPromo5.executeUnique();
		long a11 = (long) qPromo.executeUnique();
		long a12 = (long) qSuministrado.executeUnique();
		long a13 = (long) qBodega.executeUnique();
		long a14 = (long) qEstante.executeUnique();
		long a15 = (long) qInfoP.executeUnique();
		long a16 = (long) qPdP.executeUnique();
		long a17 = (long) qProd.executeUnique();
		long a18 = (long) qTipo.executeUnique();
		long a19 = (long) qCategoria.executeUnique();
		long a20 = (long) qOrden.executeUnique();
		long a21 = (long) qProveedor.executeUnique();
		long a22 = (long) qUsuario.executeUnique();
		long a23 = (long) qSucursal.executeUnique();
		
		return new long[] {a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16,a17,a18,a19,a20,a21,a22,a23};
	}

}
