package uniandes.isis2304.superAndes.persistencia;

import java.sql.Timestamp;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.isis2304.superAndes.negocio.*;

/**
 * Clase que encapsula los métodos que hacen acceso a la base de datos para el concepto TRANSACCIONCLIENTE de SuperAndes.
 * Nótese que es una clase que es sólo conocida en el paquete de persistencia
 * 
 * @author cc.pena
 */
class SQLTransaccionCliente 
{
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes psa;
	/* ****************************************************************
	 * 			Constructor
	 *****************************************************************/
	public SQLTransaccionCliente(PersistenciaSuperAndes pp) 
	{
		this.psa = pp;
	}
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Crea y ejecuta la sentencia SQL para adicionar una TRANSACCIONCLIENTE a la base de datos de SuperAndes.
	 * @return El número de tuplas insertadas
	 */
	public long adicionarTransaccion(PersistenceManager pm, long id , Double monto, Timestamp fecha, long idCliente) 
	{
        Query q = pm.newQuery(SQL, "INSERT INTO " + psa.darTablaTransaccion() + "(id, monto, fecha, idcliente) values (?, ?, ?, ?)");
        q.setParameters(id, monto, fecha, idCliente);
        return (long) q.executeUnique();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para eliminar TRANSACCIONCLIENTE de la base de datos de superAndes, por su id.
	 * @return EL número de tuplas eliminadas
	 */
	public long eliminarTransaccion (PersistenceManager pm, long id)
	{
        Query q = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaTransaccion() + " WHERE id = ?");
        q.setParameters(id);
        return (long) q.executeUnique();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para obtener la TRANSACCIONCLIENTE de la base de datos de superAndes, por su id.
	 * @return La tupla con el id dado
	 */
	public TransaccionCliente darTransaccion (PersistenceManager pm, long id)
	{
        Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaTransaccion() + " WHERE id = ?");
        q.setParameters(id);
        q.setResultClass(TransaccionCliente.class);
        return (TransaccionCliente) q.executeUnique();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de las TRANSACCIONES de un cliente, dado su id, en la 
	 * base de datos de SUPERANDES
	 * @return La lista de transacciones del cliente
	 */
	public List<TransaccionCliente> darTransaccionesPorCliente (PersistenceManager pm, long idCliente) 
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaTransaccion() + " WHERE idcliente = ?");
		q.setParameters(idCliente);
		q.setResultClass(TransaccionCliente.class);
		return (List<TransaccionCliente>) q.executeList();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de LAS TRANSACCIONES de la 
	 * base de datos de SUPERANDES
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos Transaccion
	 */
	public List<TransaccionCliente> darTransacciones(PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaTransaccion());
		q.setResultClass(TransaccionCliente.class);
		return (List<TransaccionCliente>) q.executeList();
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para modificar el monto de una transacci�n.
	 */
	public void modificarMonto(PersistenceManager pm, long idTransac, double nuevomonto)
	{
        Query q = pm.newQuery(SQL, "UPDATE " + psa.darTablaTransaccion() + " SET monto = ? WHERE id = ?");
        q.setParameters(nuevomonto, idTransac);
        q.executeUnique();
	}
}
