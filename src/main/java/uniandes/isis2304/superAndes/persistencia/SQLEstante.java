package uniandes.isis2304.superAndes.persistencia;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.isis2304.superAndes.negocio.Bodega;
import uniandes.isis2304.superAndes.negocio.Estante;

/**
 * Clase que encapsula los métodos que hacen acceso a la base de datos para el concepto ESTANTE de SuperAndes.
 * Nótese que es una clase que es sólo conocida en el paquete de persistencia
 * 
 * @author fj.gonzalez
 */
// id , idSucursal, capacidadVolumetrica, capacidadEnMasa , tipoProducto
class SQLEstante
{
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes psa;
	/* ****************************************************************
	 * 			Constructor
	 *****************************************************************/
	public SQLEstante(PersistenciaSuperAndes pp) 
	{
		this.psa = pp;
	}
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	
	/**
	 * Crea y ejecuta la sentencia SQL para adicionar un Estante a la base de datos de SuperAndes.
	 * @return El número de tuplas insertadas
	 */
	public long adicionarEstante (PersistenceManager pm, long id, long  idSucursal, Integer capacidadvolumetrica, Integer capacidadenmasa , String tipoProducto,Integer cantidadvolumetrica, Integer cantidadenmasa ) 
	{
        Query q = pm.newQuery(SQL, "INSERT INTO " + psa.darTablaEstante () + "(id, capacidadvolumetrica, idsucursal, capacidadenmasa, tipoproducto,cantidadenmasa,cantidadvolumetrica) values (?, ?, ?, ?, ?,?,?)");
        q.setParameters(id, capacidadvolumetrica, idSucursal, capacidadenmasa, tipoProducto,cantidadenmasa, cantidadvolumetrica);
        return (long) q.executeUnique();
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para eliminar UNA Estante de la base de datos de SuperAndes, por su identificador
	 * @return EL número de tuplas eliminadas
	 */
	public long eliminarEstantePorId (PersistenceManager pm, long id)
	{
        Query q = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaEstante() + " WHERE id = ?");
        q.setParameters(id);
        return (long) q.executeUnique();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de los Estante de la 
	 * base de datos de SuperAndes
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos Estante
	 */
	public List<Estante> darEstantes (PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaEstante ());
		q.setResultClass(Estante.class);
		List<Estante> resp = (List<Estante>) q.executeList();
		return resp;
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de los Estante de una sucursal en especifico.
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos Estante
	 */
	public List<Estante> darEstantesSucursal (PersistenceManager pm , long idSucursal)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaEstante ()+ " WHERE idSucursal = ?");
		q.setParameters(idSucursal);
		q.setResultClass(Estante.class);
		List<Estante> resp = (List<Estante>) q.executeList();
		return resp;
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de UN Estante de la 
	 * base de datos de SuperAndes, por su identificador
	 * @param pm - El manejador de persistencia
	 * @return El objeto Estante que tiene el identificador dado
	 */
	public Estante darEstante (PersistenceManager pm, long id) 
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaEstante () + " WHERE id = ?");
		q.setResultClass(Estante.class);
		q.setParameters(id);
		return (Estante) q.executeList().get(0);
	}
	/**
	 * Crea y ejecuta la sentencia SQL para modificar la cantidad volumetrica de una estante.
	 */
	public void modificarCantidadVolumetrica(PersistenceManager pm, long id, int aditivoVolum)
	{
        Query q = pm.newQuery(SQL, "UPDATE " + psa.darTablaEstante () + " SET cantidadvolumetrica = "+ aditivoVolum +" WHERE id = ?");
        q.setParameters(id);
        q.executeUnique();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para modificar la cantidad en masa de una estante.
	 */
	public void modificarCantidadEnMasa(PersistenceManager pm, long id, int aditivoMasa)
	{
        Query q = pm.newQuery(SQL, "UPDATE " + psa.darTablaEstante () + " SET cantidadenmasa = "+ aditivoMasa +" WHERE id = ?");
        q.setParameters(id);
        q.executeUnique();
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de UN Bodega de la 
	 * base de datos de SuperAndes, por su identificador
	 * @param pm - El manejador de persistencia
	 * @return El objeto Bodega que tiene el identificador dado
	 */
	public List<Estante> darEstantePreExistente (PersistenceManager pm, String tipopro, long idSucursal) 
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaEstante () + " WHERE tipoproducto = ? AND  idsucursal = ?");
		q.setResultClass(Estante.class);
		q.setParameters(tipopro,idSucursal);
		return (List<Estante>) q.executeList();
	}
	
}
