package uniandes.isis2304.superAndes.persistencia;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.isis2304.superAndes.negocio.*;

/**
 * Clase que encapsula los métodos que hacen acceso a la base de datos para el concepto USUARIO de SuperAndes.
 * Nótese que es una clase que es sólo conocida en el paquete de persistencia
 * 
 * @author cc.pena
 */
class SQLUsuario 
{
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes psa;
	/* ****************************************************************
	 * 			Constructor
	 *****************************************************************/
	public SQLUsuario(PersistenciaSuperAndes pp) 
	{
		this.psa = pp;
	}
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Crea y ejecuta la sentencia SQL para adicionar un Usuario a la base de datos de SuperAndes.
	 * @return El número de tuplas insertadas
	 */
	public long adicionarUsuario (PersistenceManager pm, String user, String pw, long idSucursal) 
	{
		Query q = pm.newQuery(SQL, "INSERT INTO " + psa.darTablaUsuario() + "(username, password, idsucursal) values (?, ?, ?)");
		q.setParameters(user, pw, idSucursal);
		return (long) q.executeUnique();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para eliminar un Usuario a la base de datos de SuperAndes.
	 * @return El número de tuplas eliminadas
	 */
	public long eliminarUsuario (PersistenceManager pm, String user) 
	{
		Query q = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaUsuario() + " WHERE username = ?");
		q.setParameters(user);
		return (long) q.executeUnique();
	}

	/**
	 * Crea y ejecuta la sentencia SQL para obtener todos los Usuarios de la base de datos de SuperAndes.
	 * @return Lista con los usuarios
	 */
	public List<UsuarioPersonal> darUsuarios(PersistenceManager pm) 
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaUsuario());
		q.setResultClass(UsuarioPersonal.class);
		return (List<UsuarioPersonal>) q.executeList();
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para obtener el usuario con un user de la base de datos de SuperAndes.
	 * @return Usurio con el user por parametro
	 */
	public UsuarioPersonal darUsuario(PersistenceManager pm, String user) 
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaUsuario() + " WHERE username = ?");
		q.setParameters(user);
		q.setResultClass(UsuarioPersonal.class);
		return (UsuarioPersonal) q.executeUnique();
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para cambiar la contraseña de un usuario en la 
	 * base de datos de SuperAndes
	 * @return El número de tuplas modificadas
	 */
	public long cambiarContrasena(PersistenceManager pm, String user, String newpw)
	{
		Query q = pm.newQuery(SQL, "UPDATE " + psa.darTablaUsuario()+ " SET password = ? WHERE username = ?");
		q.setParameters(user, newpw);
		return (long) q.executeUnique();    
	}
}
