package uniandes.isis2304.superAndes.persistencia;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import uniandes.isis2304.superAndes.negocio.ProductosDelProveedor;

/**
 * Clase que encapsula los métodos que hacen acceso a la base de datos para el concepto  productos del proveedor de SuperAndes.
 * Nótese que es una clase que es sólo conocida en el paquete de persistencia
 * 
 * @author fj.gonzalez
 */
class SQLProductosDelProveedor 
{
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes psa;
	/* ****************************************************************
	 * 			Constructor
	 *****************************************************************/
	public SQLProductosDelProveedor(PersistenciaSuperAndes psa) 
	{
		this.psa = psa;
	}
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Crea y ejecuta la sentencia SQL para adicionar un ProductosDelProveedor a la base de datos de SuperAndes
	 * @return EL número de tuplas insertadas
	 */
	public long adicionarProductosDelProveedor (PersistenceManager pm, String codigoproducto, String nitproveedor) 
	{
		Query q = pm.newQuery(SQL, "INSERT INTO " + psa.darTablaProductosDelProveedor()  + "(codigoproducto, nitproveedor) values (?, ?)");
		q.setParameters(codigoproducto, nitproveedor);
		return (long) q.executeUnique();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para eliminar una tupla productosdelproveedor de la base de datos de SuperAndes, por su identificador
	 * @param pm - El manejador de persistencia
	 * @return EL número de tuplas eliminadas
	 */
	public long eliminarTupla (PersistenceManager pm, String cod , String nit)
	{
		Query q = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaProductosDelProveedor() + " WHERE codigoproducto = ? AND nitproveedor = ?");
		q.setParameters(cod,nit);
		return (long) q.executeUnique();            
	}
	/**
	 * Crea y ejecuta la sentencia SQL para eliminar los proveedores de un producto de la base de datos de SuperAndes, por su identificador
	 * @param pm - El manejador de persistencia
	 * @return EL número de tuplas eliminadas
	 */
	public long eliminarProveedoresDeUnProducto(PersistenceManager pm, String cod)
	{
		Query q = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaProductosDelProveedor() + " WHERE codigoproducto = ? ");
		q.setParameters(cod);
		return (long) q.executeUnique();  
	}
	/**
	 * Crea y ejecuta la sentencia SQL para eliminar los productos de un proveedor de la base de datos de SuperAndes, por su identificador
	 * @param pm - El manejador de persistencia
	 * @return EL número de tuplas eliminadas
	 */
	public long eliminarProductosDeUnProveedor(PersistenceManager pm, String nit)
	{
		Query q = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaProductosDelProveedor() + " WHERE nitproveedor = ? ");
		q.setParameters(nit);
		return (long) q.executeUnique();  
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de LOS ProductoES de la 
	 * base de datos de SuperAndes
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos Producto
	 */
	public List<ProductosDelProveedor> darProductosProveedor (PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaProductosDelProveedor());
		q.setResultClass(ProductosDelProveedor.class);
		return (List<ProductosDelProveedor>) q.executeList();
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de LOS ProductoES de la 
	 * base de datos de SuperAndes
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos Producto
	 */
	public ProductosDelProveedor darProveedorProducto (PersistenceManager pm, String cod)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaProductosDelProveedor()+ " WHERE codigoproducto = ?");
		q.setParameters(cod);
		q.setResultClass(ProductosDelProveedor.class);
		List<ProductosDelProveedor> lista = q.executeList();
		return (ProductosDelProveedor) lista.get((int)((Math.random()*100)%lista.size()));
	}
	
	
	
}
