
package uniandes.isis2304.superAndes.persistencia;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.jdo.JDODataStoreException;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;
import javax.jdo.Transaction;
import org.apache.log4j.Logger;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import uniandes.isis2304.superAndes.negocio.*;


/**
 * Clase para el manejador de persistencia del proyecto SuperAndes
 * Traduce la información entre objetos Java y tuplas de la base de datos, en ambos sentidos
 * Sigue un patrón SINGLETON (Sólo puede haber UN objeto de esta clase) para comunicarse de manera correcta
 * con la base de datos
 * 
 * @author fj.gonzalez-cc.pena
 */
public class PersistenciaSuperAndes 
{
	/* **************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Logger para escribir la traza de la ejecución
	 */
	private static Logger log = Logger.getLogger(PersistenciaSuperAndes.class.getName());
	/**
	 * Cadena para indicar el tipo de sentencias que se va a utilizar en una consulta
	 */
	public final static String SQL = "javax.jdo.query.SQL";
	/* **************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * Atributo privado que es el único objeto de la clase - Patrón SINGLETON
	 */
	private static PersistenciaSuperAndes instance;
	/**
	 * Fabrica de Manejadores de persistencia, para el manejo correcto de las transacciones
	 */
	private PersistenceManagerFactory pmf;

	/**
	 * Arreglo de cadenas con los nombres de las tablas de la base de datos, en su orden:
	 */
	private List <String> tablas;
	/**
	 * Atributo para el acceso a las sentencias SQL propias a PersistenciaSuperAndes
	 */
	private SQLUtil sqlUtil;

	/**
	 * Atributo para el acceso a la tabla PROVEEDOR de la base de datos
	 */
	private SQLProveedor sqlProveedor;

	/**
	 * Atributo para el acceso a la tabla SUCURSAL de la base de datos
	 */
	private SQLSucursal sqlSucursal;
	/**
	 * Atributo para el acceso a la tabla ESTANTE de la base de datos
	 */
	private SQLEstante sqlEstante;
	/**
	 * Atributo para el acceso a la tabla BODEGA de la base de datos
	 */
	private SQLBodega sqlBodega;
	/**
	 * Atributo para el acceso a la tabla ORDEN_PEDIDO de la base de datos
	 */
	private SQLOrdenPedido sqlOrdenPedido;
	/**
	 * Atributo para el acceso a la tabla INFOPRODUCTO de la base de datos
	 */
	private SQLInfoProducto sqlInfoProducto;
	/**
	 * Atributo para el acceso a la tabla PRODUCTO de la base de datos
	 */
	private SQLProducto sqlProducto;
	/**
	 * Atributo para el acceso a la tabla TIPO PRODUCTO de la base de datos
	 */
	private SQLTipoProducto sqlTipoProd;
	/**
	 * Atributo para el acceso a la tabla CATEGORIA de la base de datos
	 */
	private SQLCategoriaProducto sqlCategoria;
	/**
	 * Atributo para el acceso a la tabla PRODUCTO  suministrado de la base de datos
	 */
	private SQLProductoSuministrado sqlProductoSum;
	/**
	 * Atributo para el acceso a la tabla PRODUCTO del Proveedor  suministrado de la base de datos
	 */
	private SQLProductosDelProveedor sqlProductoProvee;
	/**
	 * Atributo para el acceso a la tabla PROMOCION  suministrado de la base de datos
	 */
	private SQLPromocion sqlPromocion;
	/**
	 * Atributo para el acceso a la tabla PROMOCION DESCUENTO suministrado de la base de datos
	 */
	private SQLPromocionDescuento sqlPromocionDescuento;
	/**
	 * Atributo para el acceso a la tabla PROMOCION DOS PRODUCTOS suministrado de la base de datos
	 */
	private SQLPromocionDosProductos sqlPromocionDosProductos;
	/**
	 * Atributo para el acceso a la tabla PROMOCION PAGUE 1 LLEVE EL SEGUNDO suministrado de la base de datos
	 */
	private SQLPromocionPague1LleveElSegundo sqlPromocionPague1LleveElSegundo;
	/**
	 * Atributo para el acceso a la tabla PROMOCION PAGUE x lleve y cantidad suministrado de la base de datos
	 */
	private SQLPromocionPagueXLleveYCantidad sqlPromocionPagueXLleveYCantidad;
	/**
	 * Atributo para el acceso a la tabla PROMOCION PAGUE x lleve y cantidad suministrado de la base de datos
	 */
	private SQLPromocionPagueNLleveMUnidades sqlPromocionPagueNLleveMUnidades;
	/**
	 * Atributo para el acceso a la tabla CLIENTE suministrado de la base de datos
	 */
	private SQLCliente sqlCliente;
	/**
	 * Atributo para el acceso a la tabla CLIENTENATURAL suministrado de la base de datos
	 */
	private SQLClienteNatural sqlClienteNatural;
	/**
	 * Atributo para el acceso a la tabla CLIENTEEMPRESA suministrado de la base de datos
	 */
	private SQLClienteEmpresa sqlClienteEmpresa;
	/**
	 * Atributo para el acceso a la tabla TRANSACCIONCLIENTE suministrado de la base de datos
	 */
	private SQLTransaccionCliente sqlTransaccionCliente;
	/**
	 * Atributo para el acceso a la tabla COMPRAN suministrado de la base de datos
	 */
	private SQLCompra sqlCompra;
	/**
	 * Atributo para el acceso a la tabla CARRITO suministrado de la base de datos
	 */
	private SQLCarrito sqlCarrito;
	/**
	 * Atributo para el acceso a la tabla USUARIO suministrado de la base de datos
	 */
	private SQLUsuario sqlUsuario;

	/* **************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Constructor privado con valores por defecto - Patrón SINGLETON
	 */
	private  PersistenciaSuperAndes() 
	{
		pmf = JDOHelper.getPersistenceManagerFactory("SuperAndes");		
		crearClasesSQL ();

		// Define los nombres por defecto de las tablas de la base de datos
		tablas = new LinkedList<String> ();
		tablas.add ("SuperAndes_sequence");
		tablas.add ("PROVEEDOR");
		tablas.add("SUCURSAL");
		tablas.add("BODEGA");
		tablas.add("ESTANTE");
		tablas.add("ORDENPEDIDO");
		tablas.add("INFOPRODUCTO");
		tablas.add("PRODUCTO");
		tablas.add("TIPOPRODUCTO");
		tablas.add("CATEGORIAPRODUCTO");
		tablas.add("PRODUCTOSUMINISTRADO");
		tablas.add("PRODUCTOSDELPROVEEDOR");
		tablas.add("PROMOCION");
		tablas.add("PROMOCIONDESCUENTO");
		tablas.add("PROMOCIONDOSPRODUCTOS");
		tablas.add("PROMOCIONPAGUE1LLEVEELSEGUNDO");
		tablas.add("PROMOCIONPAGUENLLEVEMUNIDADES");
		tablas.add("PROMOCIONPAGUEXLLEVEYCANTIDAD");
		tablas.add("CLIENTE");
		tablas.add("CLIENTENATURAL");
		tablas.add("CLIENTEEMPRESA");
		tablas.add("TRANSACCIONCLIENTE");
		tablas.add("COMPRA");
		tablas.add("USUARIO");
		tablas.add("CARRITO");
		System.out.println(tablas.size());
	}

	/**
	 * Constructor privado, que recibe los nombres de las tablas en un objeto Json - Patrón SINGLETON
	 * @param tableConfig - Objeto Json que contiene los nombres de las tablas y de la unidad de persistencia a manejar
	 */
	private PersistenciaSuperAndes (JsonObject tableConfig)
	{
		crearClasesSQL ();
		tablas = leerNombresTablas (tableConfig);

		String unidadPersistencia = tableConfig.get ("unidadPersistencia").getAsString ();
		log.trace ("Accediendo unidad de persistencia: " + unidadPersistencia);
		pmf = JDOHelper.getPersistenceManagerFactory (unidadPersistencia);
	}

	/**
	 * @return Retorna el único objeto PersistenciaSuperAndes existente - Patrón SINGLETON
	 */
	public static PersistenciaSuperAndes getInstance ()
	{
		if (instance == null)
		{
			instance = new PersistenciaSuperAndes ();
		}
		return instance;
	}

	/**
	 * Constructor que toma los nombres de las tablas de la base de datos del objeto tableConfig
	 * @param tableConfig - El objeto JSON con los nombres de las tablas
	 * @return Retorna el único objeto PersistenciaSuperAndes existente - Patrón SINGLETON
	 */
	public static PersistenciaSuperAndes getInstance (JsonObject tableConfig)
	{
		if (instance == null)
		{
			instance = new PersistenciaSuperAndes (tableConfig);
		}
		return instance;
	}

	/**
	 * Cierra la conexión con la base de datos
	 */
	public void cerrarUnidadPersistencia ()
	{
		pmf.close ();
		instance = null;
	}

	/**
	 * Genera una lista con los nombres de las tablas de la base de datos
	 * @param tableConfig - El objeto Json con los nombres de las tablas
	 * @return La lista con los nombres del secuenciador y de las tablas
	 */
	private List <String> leerNombresTablas (JsonObject tableConfig)
	{
		JsonArray nombres = tableConfig.getAsJsonArray("tablas") ;

		List <String> resp = new LinkedList <String> ();
		for (JsonElement nom : nombres)
		{
			resp.add (nom.getAsString ());
		}

		return resp;
	}

	/**
	 * Crea los atributos de clases de apoyo SQL
	 */
	private void crearClasesSQL ()
	{
		sqlUtil = new SQLUtil(this);
		sqlProveedor = new SQLProveedor(this);
		sqlSucursal = new SQLSucursal(this);
		sqlCarrito = new SQLCarrito(this);
		sqlEstante = new SQLEstante(this);
		sqlBodega = new SQLBodega(this);
		sqlOrdenPedido = new SQLOrdenPedido(this);
		sqlInfoProducto = new SQLInfoProducto(this);
		sqlProducto = new SQLProducto(this);
		sqlTipoProd = new SQLTipoProducto(this);
		sqlCategoria = new SQLCategoriaProducto(this);
		sqlProductoSum = new SQLProductoSuministrado(this);
		sqlProductoProvee = new SQLProductosDelProveedor(this);
		sqlPromocion =  new SQLPromocion(this);
		sqlPromocionDescuento = new SQLPromocionDescuento(this);
		sqlPromocionDosProductos = new SQLPromocionDosProductos(this);
		sqlPromocionPague1LleveElSegundo = new SQLPromocionPague1LleveElSegundo(this);
		sqlPromocionPagueNLleveMUnidades = new SQLPromocionPagueNLleveMUnidades(this);
		sqlPromocionPagueXLleveYCantidad = new SQLPromocionPagueXLleveYCantidad(this);
		sqlCliente = new SQLCliente(this);
		sqlClienteNatural = new SQLClienteNatural(this);
		sqlClienteEmpresa = new SQLClienteEmpresa(this);
		sqlTransaccionCliente = new SQLTransaccionCliente(this);
		sqlCompra = new SQLCompra(this);
		sqlUsuario = new SQLUsuario(this);
	}

	/**
	 * @return La cadena de caracteres con el nombre del secuenciador de superAndes
	 */
	public String darSeqSuperAndes ()
	{
		return tablas.get (0);
	}
	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Proveedor de superAndes
	 */
	public String darTablaProveedor ()
	{
		return tablas.get (1);
	}
	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Sucursal de superAndes
	 */
	public String darTablaSucursal ()
	{
		return tablas.get (2);
	}
	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Bodega de superAndes
	 */
	public String darTablaBodega ()
	{
		return tablas.get (3);
	}
	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Estante de superAndes
	 */
	public String darTablaEstante ()
	{
		return tablas.get (4);
	}
	/**
	 * @return La cadena de caracteres con el nombre de la tabla de Orden pedido de superAndes
	 */
	public String darTablaOrdenPedido ()
	{
		return tablas.get (5);
	}
	/**
	 * @return La cadena de caracteres con el nombre de la tabla de info producto de superAndes
	 */
	public String darTablaInfoProducto ()
	{
		return tablas.get (6);
	}
	/**
	 * @return La cadena de caracteres con el nombre de la tabla de  producto de superAndes
	 */
	public String darTablaProducto ()
	{
		return tablas.get (7);
	}
	/**
	 * @return La cadena de caracteres con el nombre de la tabla de tipo producto de superAndes
	 */
	public String darTablaTipoProducto ()
	{
		return tablas.get (8);
	}
	/**
	 * @return La cadena de caracteres con el nombre de la tabla de categoria producto de superAndes
	 */
	public String darTablaCategoriaProducto ()
	{
		return tablas.get (9);
	}
	/**
	 * @return La cadena de caracteres con el nombre de la tabla de producto suministrado de superAndes
	 */
	public String darTablaProductoSuministrado ()
	{
		return tablas.get (10);
	}
	/**
	 * @return La cadena de caracteres con el nombre de la tabla de productos del proveedor de superAndes
	 */
	public String darTablaProductosDelProveedor ()
	{
		return tablas.get (11);
	}
	/**
	 * @return La cadena de caracteres con el nombre de la tabla de promocion de superAndes
	 */
	public String darTablaPromocion ()
	{
		return tablas.get (12);
	}
	/**
	 * @return La cadena de caracteres con el nombre de la tabla de promocion de descuento de superAndes
	 */
	public String darTablaPromocionDescuento ()
	{
		return tablas.get (13);
	}
	/**
	 * @return La cadena de caracteres con el nombre de la tabla de promocion de dos productos de superAndes
	 */
	public String darTablaPromocionDosProductos ()
	{
		return tablas.get (14);
	}
	/**
	 * @return La cadena de caracteres con el nombre de la tabla de promocion de pagar un producto y llevar el segundo con descuento de superAndes
	 */
	public String darTablaPromocionPague1LleveElSegundo ()
	{
		return tablas.get (15);
	}
	/**
	 * @return La cadena de caracteres con el nombre de la tabla de promocion de pague n lleve m unidades(n<m) de superAndes
	 */
	public String darTablaPromocionPagueNLleveMUnidades ()
	{
		return tablas.get (16);
	}
	/**
	 * @return La cadena de caracteres con el nombre de la tabla de promocion de pague x lleve y unidades(x<y) de superAndes
	 */
	public String darTablaPromocionPagueXLleveYCantidad ()
	{
		return tablas.get (17);
	}
	/**
	 * @return La cadena de caracteres con el nombre de la tabla de clientes
	 */
	public String darTablaCliente()
	{
		return tablas.get(18);
	}
	/**
	 * @return La cadena de caracteres con el nombre de la tabla de clientes naturales
	 */
	public String darTablaClienteNatural()
	{
		return tablas.get(19);
	}
	/**
	 * @return La cadena de caracteres con el nombre de la tabla de clientes de empresa
	 */
	public String darTablaClienteEmpresa()
	{
		return tablas.get(20);
	}
	/**
	 * @return La cadena de caracteres con el nombre de la tabla de transacciones
	 */
	public String darTablaTransaccion()
	{
		return tablas.get(21);
	}
	/**
	 * @return La cadena de caracteres con el nombre de la tabla de compras
	 */
	public String darTablaCompra()
	{
		return tablas.get(22);
	}
	/**
	 * @return La cadena de caracteres con el nombre de la tabla de usuario
	 */
	public String darTablaUsuario()
	{
		return tablas.get(23);
	}

	/**
	 * @return La cadena de caracteres con el nombre de la tabla de carrito
	 */
	public String darTablaCarrito()
	{
		return tablas.get(24);
	}

	/**
	 * Transacción para el generador de secuencia de SuperAndes
	 * Adiciona entradas al log de la aplicación
	 * @return El siguiente número del secuenciador de SuperAndes
	 */
	private long nextval()
	{
		long resp = sqlUtil.nextval (pmf.getPersistenceManager());
		log.trace ("Generando secuencia: " + resp);
		return resp;
	}

	/**
	 * Extrae el mensaje de la exception JDODataStoreException embebido en la Exception e, que da el detalle específico del problema encontrado
	 * @param e - La excepción que ocurrio
	 * @return El mensaje de la excepción JDO
	 */
	private String darDetalleException(Exception e) 
	{
		String resp = "";
		if (e.getClass().getName().equals("javax.jdo.JDODataStoreException"))
		{
			JDODataStoreException je = (javax.jdo.JDODataStoreException) e;
			return je.getNestedExceptions() [0].getMessage();
		}
		return resp;
	}
	/* ****************************************************************
	 * 			Métodos para manejar las BODEGAS
	 *****************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla BODEGA
	 * @return El objeto BODEGA adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public Bodega adicionarBodega( Integer capacidadvolumetrica, long  idSucursal, Integer capacidadenmasa , String tipoProducto) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();

		try
		{
			tx.begin();
			long idBodega = nextval ();
			long tuplasInsertadas = 0;

			tuplasInsertadas = sqlBodega.adicionarBodega(pmf.getPersistenceManager(), idBodega, capacidadvolumetrica, idSucursal, capacidadenmasa, tipoProducto,0,0);

			tx.commit();

			log.trace ("Inserción de bodega: " + idBodega + ": " + tuplasInsertadas + " tuplas insertadas");

			return new Bodega(idBodega, idSucursal, capacidadenmasa, capacidadvolumetrica, tipoProducto,0,0);


		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla Bodega, dado el nombre del Bodega
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public long eliminarBodegaPorId(long id) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long resp = sqlBodega.eliminarBodegaPorId(pm, id);
			tx.commit();
			return resp;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return -1;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Método que consulta todas las tuplas en la tabla Bodega que tienen el nombre dado
	 * @return La lista de objetos Bodega, construidos con base en las tuplas de la tabla Bodega
	 */
	public List<Bodega> darBodegasSucursal (long idSucursal) 
	{
		return sqlBodega.darBodegasSucursal(pmf.getPersistenceManager(), idSucursal);
	}
	/**
	 * Método que consulta todas las tuplas en la tabla Bodega
	 * @return La lista de objetos Bodega, construidos con base en las tuplas de la tabla Bodega
	 */
	public List<Bodega> darBodegas ()
	{
		return sqlBodega.darBodegas (pmf.getPersistenceManager());
	}
	/**
	 * Método que consulta el bodega bodega
	 * @return El objeto bodega con dicho id.
	 */
	public Bodega darBodega(long id)
	{
		return sqlBodega.darBodega(pmf.getPersistenceManager(), id);
	}
	/* ****************************************************************
	 * 			Métodos para manejar las CATEGORIAPRODUCTO
	 *****************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla CategoriaProducto
	 * @return El objeto CategoriaProducto adicionado. null si ocurre alguna Excepción.
	 */
	public CategoriaProducto adicionarCategoriaProducto( String nombre ) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{

			tx.begin();
			long tuplasInsertadas = sqlCategoria.adicionarCategoriaProducto(pmf.getPersistenceManager(), nombre);
			tx.commit();

			log.trace ("Inserción de una categoría producto: " + nombre + ": " + tuplasInsertadas + " tuplas insertadas");

			return new CategoriaProducto(nombre);		


		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla CategoriaProducto, dado el nombre de la categoria.
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public long eliminarCategoriaProducto(String nombre) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long resp = sqlCategoria.eliminarCategoriaProducto(pmf.getPersistenceManager(), nombre);
			tx.commit();
			return resp;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return -1;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Método que consulta todas las tuplas en la tabla Bodega
	 * @return La lista de objetos Bodega, construidos con base en las tuplas de la tabla Bodega
	 */
	public List<CategoriaProducto> darCategorias ()
	{
		return sqlCategoria.darCategoriasProducto(pmf.getPersistenceManager());
	}
	/**
	 * Método que consulta una CategoriaProducto.
	 * @return La lista de objetos Bodega, construidos con base en las tuplas de la tabla Bodega
	 */
	public CategoriaProducto darCategoria (String nom)
	{
		return sqlCategoria.darCategoriaProducto(pmf.getPersistenceManager(), nom);
	}
	/* ****************************************************************
	 * 			Métodos para manejar las CLIENTE
	 *****************************************************************/
	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla Cliente, dado el id del Cliente
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public long[] eliminarCliente(long id) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long[] resp = sqlCliente.eliminarClientePorId(pmf.getPersistenceManager(),id);
			tx.commit();
			return resp;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return new long[] {};
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Método que consulta todas las tuplas en la tabla Cliente
	 * @return La lista de objetos Cliente, construidos con base en las tuplas de la tabla Bodega
	 */
	public List<Cliente> darClientes()
	{
		return sqlCliente.darClientes (pmf.getPersistenceManager());
	}
	/**
	 * Método que consulta todas las tuplas en la tabla Cliente
	 * @return La lista de objetos Cliente, construidos con base en las tuplas de la tabla Bodega
	 */
	public List<Cliente> darClientesCarroAbandonado()
	{
		return sqlCliente.darClientesCarroAbandonado(pmf.getPersistenceManager());
	}



	/**
	 * Método que consulta todas las tuplas en la tabla Cliente
	 * @return La lista de objetos Cliente, construidos con base en las tuplas de la tabla Bodega
	 */
	public Cliente darCliente(long id)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			Cliente resp = sqlCliente.darCliente(pm, id);
			tx.commit();
			return resp;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla Cliente, dado el id del Cliente
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public long[] eliminarClienteConSusTransacciones(long id) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long[] resp = sqlCliente.eliminarClienteConSusTransacciones(pmf.getPersistenceManager(),id);
			tx.commit();
			return resp;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return new long[] {};
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Método que cambia los puntos de un cliente, de manera transaccional.
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas cambiadas. -1 si ocurre alguna Excepción
	 */
	public long sumarPuntosACliente(long id, int nuevosPuntos) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long resp = sqlCliente.sumarPuntos(pm, id, nuevosPuntos);
			tx.commit();
			return resp;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return -1 ;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/* ****************************************************************
	 * 			Métodos para manejar las CLIENTE EMPRESA
	 *****************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla Cliente
	 * @return El objeto Cliente adicionado. null si ocurre alguna Excepción.
	 */
	public ClienteEmpresa adicionarClienteEmpresa( int puntos, String nit, String direccion ) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{

			tx.begin();
			long id = nextval ();
			sqlCliente.adicionarCliente(pmf.getPersistenceManager(), id, puntos);
			long tuplasInsertadas = sqlClienteEmpresa.adicionarClienteEmpresa(pmf.getPersistenceManager(), id, nit, direccion);
			tx.commit();
			log.trace ("Inserción de un cliente: " + id + ": " + tuplasInsertadas + " tuplas insertadas");

			return new ClienteEmpresa(id, puntos, nit, direccion);		


		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla Cliente empresa, dado el id del Cliente
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public long[] eliminarClienteEmpresa(long id)
	{
		long[] registro = eliminarCliente(id);
		return new long[] {registro[1],registro[2]};
	}
	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla Cliente empresa, dado el nit de la empresa
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public long eliminarClienteEmpresaPorNIT(String nit)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			ClienteEmpresa ce = sqlClienteEmpresa.darClientePorNit(pm, nit);
			long resp = sqlClienteEmpresa.eliminarClientePorNit(pm, nit);
			//deseamos borrar a su vez la referencia que tiene en la clase padre.
			eliminarCliente(ce.getId());
			tx.commit();
			return resp;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return 0;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Método que consulta todas las tuplas en la tabla Cliente
	 * @return La lista de objetos Cliente, construidos con base en las tuplas de la tabla Bodega
	 */
	public List<ClienteEmpresa> darClientesEmpresa()
	{
		return sqlClienteEmpresa.darClientesEmpresa (pmf.getPersistenceManager());
	}
	/**
	 * Método que consulta un cliente especìfico en tabla ClienteEmpresa
	 * @return El clienteEmpresa.
	 */
	public ClienteEmpresa darClienteEmpresa(String nit)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			ClienteEmpresa ce = sqlClienteEmpresa.darClientePorNit(pm, nit);
			tx.commit();
			Cliente temp = darCliente(ce.getId());
			ce.setPuntos(temp.getPuntos());
			return ce;
		}
		catch (Exception e)
		{
			// e.printStackTrace();

			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/* ****************************************************************
	 * 			Métodos para manejar las CLIENTE NATURAL
	 *****************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla Cliente
	 * @return El objeto Cliente adicionado. null si ocurre alguna Excepción.
	 */
	public ClienteNatural adicionarClienteNatural( int puntos,  String cedula, String nombre, String email ) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{

			tx.begin();
			long id = nextval ();
			sqlCliente.adicionarCliente(pmf.getPersistenceManager(), id, puntos);
			long tuplasInsertadas = sqlClienteNatural.adicionarClienteNatural(pmf.getPersistenceManager(), id,  cedula, nombre, email);
			tx.commit();
			log.trace ("Inserción de un cliente: " + id + ": " + tuplasInsertadas + " tuplas insertadas");

			return new ClienteNatural(id, puntos, cedula, nombre, email);		


		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla Cliente natural, dado el id del Cliente
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public long[] eliminarClienteNatural(long id)
	{
		long[] registro = eliminarCliente(id);
		return new long[] {registro[0],registro[2]};
	}
	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla Cliente empresa, dado el nit de la empresa
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public long eliminarClienteNaturalPorCedula(String cedula)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			ClienteNatural ce = sqlClienteNatural.darClientePorCedula(pm, cedula);
			long resp = sqlClienteNatural.eliminarClientePorCedula(pm, cedula);
			//deseamos borrar a su vez la referencia que tiene en la clase padre.
			tx.commit();
			eliminarCliente(ce.getId());
			return resp;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return 0;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Método que consulta todas las tuplas en la tabla Cliente
	 * @return La lista de objetos Cliente, construidos con base en las tuplas de la tabla Bodega
	 */
	public List<ClienteNatural> darClientesNatural()
	{
		return sqlClienteNatural.darClientesNaturales(pmf.getPersistenceManager());
	}
	/**
	 * Método que consulta un cliente especìfico en tabla ClienteNatural
	 * @return El ClienteNatural.
	 */
	public ClienteNatural darClienteNatural(String cedula)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			ClienteNatural ce = sqlClienteNatural.darClientePorCedula(pm, cedula);
			Cliente temp = darCliente(ce.getId());
			ce.setPuntos(temp.getPuntos());
			ce.setCarroAbandonado(temp.getCarroAbandonado());
			tx.commit();
			return ce;
		}
		catch (Exception e)
		{
			//e.printStackTrace();

			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/* * ************************************************************
	 * 			Métodos para manejar la PROMOCION
	 *****************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla PROMOCION
	 * @return El objeto PROMOCION adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public Promocion adicionarPromocion(Timestamp fechaexpedicion, Timestamp fechafinalizacion, String codigoproducto1, String codigoproducto2, Integer unidades, Integer tipo, String nombre) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{

			tx.begin();
			long idPromocion = nextval ();
			long tuplasInsertadas = sqlPromocion.adicionarPromocion(pm, idPromocion, fechaexpedicion, fechafinalizacion, fechafinalizacion, codigoproducto1, codigoproducto2, unidades, nombre, tipo, "A");
			tx.commit();

			log.trace ("Inserción de promocion: " + idPromocion + ": " + tuplasInsertadas + " tuplas insertadas");

			return new Promocion(idPromocion, fechaexpedicion, fechafinalizacion, fechafinalizacion, unidades, codigoproducto1, codigoproducto2, tipo, "A");



		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla PROMOCION
	 * @return El objeto PROMOCION eliminado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public long[] eliminarPromocion(long id) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long[] tuplasEliminadas = sqlPromocion.eliminarPromocionPorId(pm, id);
			int suma = 0;
			for (int i = 0; i < tuplasEliminadas.length; i++) {
				suma += tuplasEliminadas[i];
			}
			tx.commit();

			log.trace ("Eliminacion de promocion: " + id + ": " + suma + " tuplas eliminadas");

			return tuplasEliminadas;

		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return new long[0];
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que retorna todas las promociones
	 * @return Lista de todas las promociones. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public List<Promocion> darPromociones()
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			List<Promocion> lista = sqlPromocion.darPromociones(pm);
			tx.commit();

			log.trace ("Lista de promociones: " + lista.size() + " tuplas incluidas");

			return lista;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return new ArrayList<>();
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que retorna todas las promociones de un tipo especifico
	 * @return Lista de todas las promociones de un tipo especifico. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public List<Promocion> darPromocionesDeUnTipo(long tipo)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			List<Promocion> lista = sqlPromocion.darPromocionesDeUnTipo(pm, tipo);
			tx.commit();

			log.trace ("Lista de promociones del tipo "+tipo+" :" + lista.size() + " tuplas incluidas");

			return lista;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return new ArrayList<>();
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que retorna la promocion con un id especifico
	 * @return Promocion con un id especifico. null si ocurre alguna Excepción o si no se pudo obtener.
	 */
	public Promocion darPromocion(long id)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			Promocion promocion = sqlPromocion.darPromocion(pm, id);
			tx.commit();

			log.trace ("Obtencion de la promocion "+id+" :" + promocion != null?1:0 + " tuplas incluidas");

			return promocion;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que registra el fin de una promocion
	 * @return Cantidad de tuplas alteradas. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public long[] acabarPromocion(Timestamp fin, long id)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long[] tuplas = sqlPromocion.acabarPromocion(pm, fin, id);
			tx.commit();

			log.trace ("Fin de promocion: " + id + ": " + tuplas[0] +"-" + tuplas[1] + " tuplas eliminadas");

			return tuplas;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return new long[]{0,0};
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/* * ************************************************************
	 * 			Métodos para manejar la PROMOCIONDESCUENTO
	 *****************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla PROMOCIONDESCUENTO
	 * @return El objeto PROMOCIONDESCUENTO adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public PromocionDescuento adicionarPromocionDescuento(Timestamp fechaexpedicion, Timestamp fechafinalizacion, String codigoproducto1, String codigoproducto2, Integer unidades, Integer tipo, String nombre, double porcentajedescuento)
	{
		Promocion inicial = adicionarPromocion(fechaexpedicion, fechafinalizacion, codigoproducto1, codigoproducto2, unidades, tipo, nombre);
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long tuplasInsertadas = sqlPromocionDescuento.adicionarPromocionDescuento(pm, inicial.getId(), porcentajedescuento);
			tx.commit();

			log.trace ("Inserción de promocionDescuento: " + inicial.getId() + ": " + tuplasInsertadas + " tuplas insertadas");

			return new PromocionDescuento(inicial.getId(), inicial.getFechaExpedicion(), inicial.getFechaFinalizacion(), inicial.getFechaFinExistencias(), inicial.getUnidades(), inicial.getCodigoProducto1(), inicial.getCodigoProducto2(), inicial.getTipo(), porcentajedescuento, inicial.getEstado());

		}
		catch (Exception e)
		{
			//e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Método que retorna todas las promociones descuento
	 * @return Lista de todas las promociones descuento. null si ocurre alguna Excepción.
	 */
	public List<PromocionDescuento> darPromocionesDescuento()
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			List<PromocionDescuento> lista = sqlPromocionDescuento.darPromocionesDescuento(pm);
			tx.commit();

			log.trace ("Lista de promociones descuento: " + lista.size() + " tuplas incluidas");

			return lista;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return new ArrayList<>();
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que retorna la promocion descuento con un id especifico
	 * @return Promocion descuento con un id especifico. null si ocurre alguna Excepción o si no se pudo obtener.
	 */
	public PromocionDescuento darPromocionDescuento(long id)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			PromocionDescuento promocion = sqlPromocionDescuento.darPromocionDescuento(pm, id);
			tx.commit();

			log.trace ("Obtencion de la promocion descuento "+id+" :" + promocion != null?1:0 + " tuplas obtenidas");

			return promocion;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/* * ************************************************************
	 * 			Métodos para manejar la PROMOCIONDOSPRODUCTOS
	 *****************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla PROMOCIONDOSPRODUCTOS
	 * @return El objeto PROMOCIONDOSPRODUCTOS adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public PromocionDosProductos adicionarPromocionDosProductos(Timestamp fechaexpedicion, Timestamp fechafinalizacion, String codigoproducto1, String codigoproducto2, Integer unidades, Integer tipo, String nombre, double montototal) 
	{
		Promocion inicial = adicionarPromocion(fechaexpedicion, fechafinalizacion, codigoproducto1, codigoproducto2, unidades, tipo, nombre);
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long tuplasInsertadas = sqlPromocionDosProductos.adicionarPromocionDosProductos(pm, inicial.getId(), montototal);
			tx.commit();

			log.trace ("Inserción de promocionDosProductos: " + inicial.getId() + ": " + tuplasInsertadas + " tuplas insertadas");

			return new PromocionDosProductos(inicial.getId(), inicial.getFechaExpedicion(), inicial.getFechaFinalizacion(), inicial.getFechaFinExistencias(), inicial.getUnidades(), inicial.getCodigoProducto1(), inicial.getCodigoProducto2(), inicial.getTipo(), montototal,inicial.getEstado());

		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Método que retorna todas las promociones dos productos
	 * @return Lista de todas las promociones dos productos. null si ocurre alguna Excepción.
	 */
	public List<PromocionDosProductos> darPromocionesDosProductos()
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			List<PromocionDosProductos> lista = sqlPromocionDosProductos.darPromocionesDosProductos(pm);
			tx.commit();

			log.trace ("Lista de promociones dos productos: " + lista.size() + " tuplas incluidas");

			return lista;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return new ArrayList<>();
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que retorna la promocion dos productos con un id especifico
	 * @return Promocion dos productos con un id especifico. null si ocurre alguna Excepción o si no se pudo obtener.
	 */
	public PromocionDosProductos darPromocionDosProductos(long id)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			PromocionDosProductos promocion = sqlPromocionDosProductos.darPromocionDosProductos(pm, id);
			tx.commit();

			log.trace ("Obtencion de la promocion dos productos "+id+" :" + promocion != null?1:0 + " tuplas obtenidas");

			return promocion;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/* * ************************************************************
	 * 			Métodos para manejar la PROMOCIONPAGUE1LLEVEELSEGUNDO
	 *****************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla PROMOCIONPAGUE1LLEVEELSEGUNDO
	 * @return El objeto PROMOCIONPAGUE1LLEVEELSEGUNDO adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public PromocionPague1LleveElSegundo adicionarPromocionPague1LleveElSegundo(Timestamp fechaexpedicion, Timestamp fechafinalizacion, String codigoproducto1, String codigoproducto2, Integer unidades, Integer tipo, String nombre, double porcdescseg) 
	{
		Promocion inicial = adicionarPromocion(fechaexpedicion, fechafinalizacion, codigoproducto1, codigoproducto2, unidades, tipo, nombre);
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long tuplasInsertadas = sqlPromocionPague1LleveElSegundo.adicionarPromocionPague1LleveElSegundo(pm, inicial.getId(), porcdescseg);
			tx.commit();

			log.trace ("Inserción de promocionPague1LleveElSegundo: " + inicial.getId() + ": " + tuplasInsertadas + " tuplas insertadas");

			return new PromocionPague1LleveElSegundo(inicial.getId(), inicial.getFechaExpedicion(), inicial.getFechaFinalizacion(), inicial.getFechaFinExistencias(), inicial.getUnidades(), inicial.getCodigoProducto1(), inicial.getCodigoProducto2(), inicial.getTipo(), porcdescseg,inicial.getEstado());

		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Método que retorna todas las promociones pague 1 y lleve el segundo
	 * @return Lista de todas las promociones pague 1 y lleve el segundo. null si ocurre alguna Excepción.
	 */
	public List<PromocionPague1LleveElSegundo> darPromocionesPague1LleveElSeg()
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			List<PromocionPague1LleveElSegundo> lista = sqlPromocionPague1LleveElSegundo.darPromocionesPromocionPague1LleveElSegundo(pm);
			tx.commit();

			log.trace ("Lista de promociones pague 1 y lleve el segundo: " + lista.size() + " tuplas incluidas");

			return lista;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return new ArrayList<>();
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que retorna la promociones pague 1 y lleve el segundo con un id especifico
	 * @return promociones pague 1 y lleve el segundo con un id especifico. null si ocurre alguna Excepción o si no se pudo obtener.
	 */
	public PromocionPague1LleveElSegundo darPromocionPague1LleveElSegundo(long id)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			PromocionPague1LleveElSegundo promocion = sqlPromocionPague1LleveElSegundo.darPromocionPague1LleveElSegundo(pm, id);
			tx.commit();

			log.trace ("Obtencion de la promocion pague 1 y lleve el segundo "+id+" :" + promocion != null?1:0 + " tuplas obtenidas");

			return promocion;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/* * ************************************************************
	 * 			Métodos para manejar la PROMOCIONPAGUENLLEVEMUNIDADES
	 *****************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla PROMOCIONPAGUENLLEVEMUNIDADES
	 * @return El objeto PROMOCIONPAGUENLLEVEMUNIDADES adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public PromocionPagueNLleveMUnidades adicionarPromocionPagueNLleveMUnidades(Timestamp fechaexpedicion, Timestamp fechafinalizacion, String codigoproducto1, String codigoproducto2, Integer unidades, Integer tipo, String nombre, int unidadespagadas, int unidadesofrecidas) 
	{
		Promocion inicial = adicionarPromocion(fechaexpedicion, fechafinalizacion, codigoproducto1, codigoproducto2, unidades, tipo, nombre);
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long tuplasInsertadas = sqlPromocionPagueNLleveMUnidades.adicionarPromocionPagueNLleveMUnidades(pm, inicial.getId(), unidadespagadas, unidadesofrecidas);
			tx.commit();

			log.trace ("Inserción de promocionPagueNLleveMUnidades: " + inicial.getId() + ": " + tuplasInsertadas + " tuplas insertadas");

			return new PromocionPagueNLleveMUnidades(inicial.getId(), inicial.getFechaExpedicion(), inicial.getFechaFinalizacion(), inicial.getFechaFinExistencias(), inicial.getUnidades(), inicial.getCodigoProducto1(), inicial.getCodigoProducto2(), inicial.getTipo(), unidadespagadas, unidadesofrecidas,inicial.getEstado());

		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error ("Exception PagueNLleveM : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Método que retorna todas las promociones pague n y lleve m unidades
	 * @return Lista de todas las promociones pague n y lleve m unidades. null si ocurre alguna Excepción.
	 */
	public List<PromocionPagueNLleveMUnidades> darPromocionesPagueNLleveMUnidades()
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			List<PromocionPagueNLleveMUnidades> lista = sqlPromocionPagueNLleveMUnidades.darPromocionesPromocionPagueNLleveMUnidades(pm);
			tx.commit();

			log.trace ("Lista de promociones pague n y lleve m unidades: " + lista.size() + " tuplas incluidas");

			return lista;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return new ArrayList<>();
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que retorna la promociones pague n y lleve m unidades con un id especifico
	 * @return promociones pague n y lleve m unidades con un id especifico. null si ocurre alguna Excepción o si no se pudo obtener.
	 */
	public PromocionPagueNLleveMUnidades darPromocionPagueNLleveMUnidades(long id)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			PromocionPagueNLleveMUnidades promocion = sqlPromocionPagueNLleveMUnidades.darPromocionPagueNLleveMUnidades(pm, id);
			tx.commit();

			log.trace ("Obtencion de la promocion pague n y lleve m unidades "+id+" :" + promocion != null?1:0 + " tuplas obtenidas");

			return promocion;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/* * ************************************************************
	 * 			Métodos para manejar la PROMOCIONPAGUEXLLEVEYCANTIDAD
	 *****************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla PROMOCIONPAGUEXLLEVEYCANTIDAD
	 * @return El objeto PROMOCIONPAGUEXLLEVEYCANTIDAD adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public PromocionPagueXLleveYCantidad adicionarPromocionPagueXLleveYCantidad(Timestamp fechaexpedicion, Timestamp fechafinalizacion, String codigoproducto1, String codigoproducto2, Integer unidades, Integer tipo, String nombre, int cantidadpagada, int cantidasofrecida) 
	{
		Promocion inicial = adicionarPromocion(fechaexpedicion, fechafinalizacion, codigoproducto1, codigoproducto2, unidades, tipo, nombre);
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long tuplasInsertadas = sqlPromocionPagueXLleveYCantidad.adicionarPromocionPagueXLleveYCantidad(pm, inicial.getId(), cantidadpagada, cantidasofrecida);
			tx.commit();

			log.trace ("Inserción de promocionPagueXLleveYCantidad: " + inicial.getId() + ": " + tuplasInsertadas + " tuplas insertadas");

			return new PromocionPagueXLleveYCantidad(inicial.getId(), inicial.getFechaExpedicion(), inicial.getFechaFinalizacion(), inicial.getFechaFinExistencias(), inicial.getUnidades(), inicial.getCodigoProducto1(), inicial.getCodigoProducto2(), inicial.getTipo(), cantidadpagada, cantidasofrecida,inicial.getEstado());

		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Método que retorna todas las promociones pague x y lleve y cantidad
	 * @return Lista de todas las promociones pague x y lleve y cantidad. null si ocurre alguna Excepción.
	 */
	public List<PromocionPagueXLleveYCantidad> darPromocionesPagueXLleveYCantidad()
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			List<PromocionPagueXLleveYCantidad> lista = sqlPromocionPagueXLleveYCantidad.darPromocionesPromocionPagueXLleveYCantidad(pm);
			tx.commit();

			log.trace ("Lista de promociones pague x y lleve y cantidad: " + lista.size() + " tuplas incluidas");

			return lista;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return new ArrayList<>();
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que retorna la promociones pague x y lleve y cantidad con un id especifico
	 * @return promociones pague x y lleve y cantidad con un id especifico. null si ocurre alguna Excepción o si no se pudo obtener.
	 */
	public PromocionPagueXLleveYCantidad darPromocionPagueXLleveYCantidad(long id)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			PromocionPagueXLleveYCantidad promocion = sqlPromocionPagueXLleveYCantidad.darPromocionPagueXLleveYCantidad(pm, id);
			tx.commit();

			log.trace ("Obtencion de la promocion pague x y lleve y cantidad "+id+" :" + promocion != null?1:0 + " tuplas obtenidas");

			return promocion;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/* ****************************************************************
	 * 			Métodos para manejar las COMPRA
	 *****************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla COMPRA
	 * @return El objeto COMPRA adicionado. null si ocurre alguna Excepción.
	 */
	public Object[] adicionarCompra(  String codigoP, long idTransaccion, Integer cantidad, long idSucursal) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		boolean verificacionProducto1 = false;
		boolean verificacionProducto2 = false;
		double monto = 1.0;
		ProductoSuministrado ps2 = null;

		try
		{

			ProductoSuministrado ps = darProdutoSuministrado(codigoP, idSucursal);
			Producto pTemp = darProducto(codigoP);

			Estante e = darEstante(ps.getIdEstante());
			Estante e2 = null;

			Sucursal s = darSucursal(e.getIdSucursal());
			int nivelAbas = s.getNivelDeAbastecimiento();
			int nivelReo = s.getNivelReOrden();
			monto = cantidad * ps.getPrecioUnitario();

			List<Promocion> li = sqlPromocion.darPromocionesDeUnProducto(pm, ps.getCodigoDeBarras());

			for (Promocion promocion : li)
			{
				if(promocion.getEstado().equals("A"))
				{
					if(promocion.getTipo() == 5)
					{
						PromocionDosProductos p2p = darPromocionDosProductos(promocion.getId());
						ps2 = darProdutoSuministrado(p2p.getCodigoProducto2(), idSucursal);
						Producto pTemp2 = darProducto(ps2.getCodigoDeBarras());
						e2 = darEstante(ps2.getIdEstante());
						verificacionProducto2 = procesoDeAbastecimiento(pm, nivelAbas, nivelReo, ps2,pTemp2, e2, idSucursal);
					}
				}
			}

			verificacionProducto1 = procesoDeAbastecimiento(pm, nivelAbas, nivelReo, ps,pTemp, e, idSucursal);
			tx.begin();
			long tuplasInsertadas = sqlCompra.adicionarCompra(pm, codigoP, idTransaccion, cantidad);
			if(ps2 != null)
			{
				sqlCompra.adicionarCompra(pm, ps2.getCodigoDeBarras(), idTransaccion, cantidad);
			}

			log.trace ("Inserción de una nueva compra: " + codigoP  + ", "+ idTransaccion+": " + tuplasInsertadas + " tuplas insertadas");
			tx.commit();

			if(!verificacionProducto1 &&  !verificacionProducto2)
			{
				return new Object[] {new Compra(codigoP, idTransaccion , cantidad),verificacionProducto1 , ps2 , verificacionProducto2, monto }; 
			}
			else
			{
				log.error ("No se puede realizar la compra debido a que no hay unidades suficientes.");
				if(verificacionProducto1)
				{
					OrdenPedido o = adicionarOrdenPedido(new Timestamp(System.currentTimeMillis()) , "EN_PROCESO", 1, ((sqlProductoProvee.darProveedorProducto(pm, codigoP)).getCodigoProducto()), e.getIdSucursal(), new Timestamp(0));
					adicionarInfoProducto(100, (s.getNivelReOrden() - ps.getCantidadEnBodega()), "fevgrbh", 5, o.getId(), codigoP);

				}
				if(verificacionProducto2)
				{
					OrdenPedido o = adicionarOrdenPedido(new Timestamp(System.currentTimeMillis()) , "EN_PROCESO", 1, sqlProductoProvee.darProveedorProducto(pm, ps2.getCodigoDeBarras()).getNITProveedor(), e2.getIdSucursal(), new Timestamp(0));
					adicionarInfoProducto(0, (s.getNivelReOrden() - ps2.getCantidadEnBodega()), "", 5, o.getId(), codigoP);

				}
				return new Object[] {new Compra(codigoP, idTransaccion , cantidad),verificacionProducto1 , ps2 , verificacionProducto2, monto }; 
			}

		}
		catch (Exception e)
		{

			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	public boolean procesoDeAbastecimiento(PersistenceManager pm , int nivelAbas , int nivelReo , ProductoSuministrado ps,Producto p, Estante e, long idSucursal)
	{
		boolean seGeneraOrdenPedido = false;
		int volumen = Integer.parseInt(p.getVolumen().replace(" cm3", ""));
		if(nivelAbas >= ps.getCantidadEnEstante())
		{
			int dif = nivelAbas - ps.getCantidadEnEstante();
			if((nivelReo+dif) <= ps.getCantidadEnBodega())
			{

				if( e.getCapacidadVolumetrica() - e.getCantidadVolumetrica() > volumen*nivelReo )
				{
					sqlProductoSum.modificarCantidadBodega(pm, ps.getCodigoDeBarras(),ps.getIdBodega(), ps.getCantidadEnBodega()-nivelReo, idSucursal);
					sqlProductoSum.modificarCantidadEstante(pm, ps.getCodigoDeBarras(), ps.getIdEstante(), ps.getCantidadEnEstante()+nivelReo, idSucursal);
				}
				else
				{
					sqlProductoSum.modificarCantidadBodega(pm, ps.getCodigoDeBarras(),ps.getIdBodega(), ps.getCantidadEnBodega()-dif+1, idSucursal);
					sqlProductoSum.modificarCantidadEstante(pm, ps.getCodigoDeBarras(), ps.getIdEstante(), ps.getCantidadEnEstante()+dif+1, idSucursal);
				}
			}
			else
			{
				if(ps.getCantidadEnBodega() < nivelReo)
				{
					seGeneraOrdenPedido = true;
					sqlProductoSum.modificarCantidadBodega(pm, ps.getCodigoDeBarras(),ps.getIdBodega(), ps.getCantidadEnBodega()-(ps.getCantidadEnBodega())+1, idSucursal);
					sqlProductoSum.modificarCantidadEstante(pm, ps.getCodigoDeBarras(), ps.getIdEstante(), ps.getCantidadEnEstante()+(ps.getCantidadEnBodega()-1), idSucursal);

				}
				else
				{
					sqlProductoSum.modificarCantidadBodega(pm, ps.getCodigoDeBarras(),ps.getIdBodega(), ps.getCantidadEnBodega()-dif+1, idSucursal);
					sqlProductoSum.modificarCantidadEstante(pm, ps.getCodigoDeBarras(), ps.getIdEstante(), ps.getCantidadEnEstante()+dif+1, idSucursal);
				}
			}
		}
		return seGeneraOrdenPedido;
	}
	/**
	 * Crea y ejecuta la sentencia SQL para modificar la cantidad en estante de un producto.
	 */
	public void modificarCantidadEstante(String cod, long idE, int nuevaCantidadEstante, long idSucursal )
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			sqlProductoSum.modificarCantidadEstante(pm, cod, idE, nuevaCantidadEstante, idSucursal);
			tx.commit();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Crea y ejecuta la sentencia SQL para modificar la cantidad en bodega de un producto.
	 */
	public void modificarCantidadBodega(String cod, long idE, int nuevaCantidadBodega, long idSucursal )
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			sqlProductoSum.modificarCantidadBodega(pm, cod, idE, nuevaCantidadBodega, idSucursal);
			tx.commit();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla Compra, dado el nombre de la compra.
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public long eliminarCompra(String codigoP, long idTrans) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long resp = sqlCompra.eliminarCompra(pmf.getPersistenceManager(), codigoP, idTrans);
			tx.commit();
			return resp;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return -1;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Método que consulta todas las tuplas en la tabla Bodega
	 * @return La lista de objetos Bodega, construidos con base en las tuplas de la tabla Bodega
	 */
	public List<Compra> darCompras ()
	{
		return sqlCompra.darCompras (pmf.getPersistenceManager());
	}
	/* ****************************************************************
	 * 			Métodos para manejar los ESTANTES
	 *****************************************************************/
	public Estante adicionarEstante( Integer capacidadvolumetrica, long  idSucursal, Integer capacidadenmasa , String tipoProducto) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try {

			tx.begin();
			long idEstante = nextval ();
			long tuplasInsertadas = sqlEstante.adicionarEstante(pmf.getPersistenceManager(), idEstante,  idSucursal, capacidadvolumetrica,capacidadenmasa, tipoProducto, 0, 0);
			tx.commit();
			log.trace ("Inserción de Estante: " + idEstante + ": " + tuplasInsertadas + " tuplas insertadas");

			return new Estante(idEstante, idSucursal, capacidadenmasa, capacidadvolumetrica, tipoProducto,0,0);

		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla Estante, dado el nombre del Estante
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public long eliminarEstantePorId(long id) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long resp = sqlEstante.eliminarEstantePorId(pm, id);
			tx.commit();
			return resp;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return -1;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Método que consulta todas las tuplas en la tabla Estante que tienen el nombre dado
	 * @return La lista de objetos Estante, construidos con base en las tuplas de la tabla Estante
	 */
	public List<Estante> darEstantesSucursal (long idSucursal) 
	{
		return sqlEstante.darEstantesSucursal(pmf.getPersistenceManager(), idSucursal);
	}
	/**
	 * Método que consulta todas las tuplas en la tabla Estante
	 * @return La lista de objetos Estante, construidos con base en las tuplas de la tabla Estante
	 */
	public List<Estante> darEstantes ()
	{
		return sqlEstante.darEstantes (pmf.getPersistenceManager());
	}
	/**
	 * Método que consulta el estante Estante
	 * @return El objeto estante con dicho id.
	 */
	public Estante darEstante(long id)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			Estante rta = sqlEstante.darEstante(pm, id);
			tx.commit();

			log.trace ("Insercion de estante: " + rta.getId());

			return rta;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/* ************************************************************
	 * 			Métodos para manejar las PROVEEDOR
	 *****************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla PROVEEDOR
	 * @return El objeto PROVEEDOR adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public Proveedor adicionarProveedor(String nit, String nombre) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long rta = sqlProveedor.adicionarProveedor(pm, nit, nombre, 5.0);
			tx.commit();

			log.trace ("Insercion de proveedor: " + nit + ": " + rta +" tuplas insertadas");

			return new Proveedor(nit, nombre, 5.0);
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla PROVEEDOR
	 * @return El numero de tuplas eliminadas. null si ocurre alguna Excepción o si no se pudo eliminar.
	 */
	public long eliminarProveedor(String nit) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long rta = sqlProveedor.eliminarProveedorPorNit(pm, nit);
			tx.commit();

			log.trace ("Eliminacion de proveedor: " + nit + ": " + rta +" tuplas eliminadas");

			return rta; 
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return 0;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla PROVEEDOR por nombre
	 * @return El numero de tuplas eliminadas. null si ocurre alguna Excepción o si no se pudo eliminar.
	 */
	public long eliminarProveedorPorNombre(String nombre) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long rta = sqlProveedor.eliminarProveedorPorNombre(pm, nombre);
			tx.commit();

			log.trace ("Eliminacion de proveedor: " + nombre + ": " + rta +" tuplas eliminadas");

			return rta; 
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return 0;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que retorna, de manera transaccional, una tupla en la tabla PROVEEDOR
	 * @return El proveedor con nit dada. null si ocurre alguna Excepción o si no se pudo encontrar.
	 */
	public Proveedor darProveedor(String nit) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			Proveedor rta = sqlProveedor.darProveedorPorNIT(pm, nit);
			tx.commit();

			log.trace ("Consulta de proveedor: " + nit + ": " + rta!=null?1:0 +" tuplas encontradas");

			return rta; 
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Método que consulta todas las tuplas en la tabla Proveedor
	 * @return La lista de objetos Proveedor, construidos con base en las tuplas de la tabla Proveedor
	 */
	public List<Proveedor> darProveedores()
	{
		return sqlProveedor.darProveedores(pmf.getPersistenceManager());
	}

	/**
	 * Método que modifica la calificacion de un proveedor
	 * @return Cantidad de tuplas alteradas. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public long modificarCalificacion(String nit, double nuevaCalificacion)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			if(sqlProveedor.darProveedorPorNIT(pm, nit) != null)
			{
				sqlProveedor.modificarCalificacion(pm, nit, nuevaCalificacion);
				return 1;
			}
			else
			{
				log.trace("No existe el proveedor con el NIT dado, no se modifica la calificacion");
				return 0;
			}
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return 0;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/* ****************************************************************
	 * 			Métodos para manejar los PRODUCTO
	 *****************************************************************/
	/**
	 * Método que crea un código de barras aleatorio para el producto.
	 * @return El código de barras.
	 */
	private String crearCodigoBarras(PersistenceManager pm) 
	{
		String tmp = "";
		for(int i = 0; i < 4; i++)
		{
			if(i < 3 )
			{
				tmp += (int) (Math.random()*50);
			}
			else
			{
				String abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
				char letter = abc.charAt((int)(Math.random()*100 % abc.length()));
				tmp += letter;
			}
		}
		try 
		{
			sqlProducto.darProductoPorCodigoBarras(pm, tmp);
			return crearCodigoBarras(pm);
		} 
		catch (Exception e) 
		{
			return tmp;
		}
	}
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla PRODUCTO
	 * @return El objeto BODEGA adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public Producto adicionarProducto( String marca, String nombre, String  presentacion, String unidaddemedida,String volumen,String peso,String tipo,Integer cantidadpresentacion) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			String codigobarras = crearCodigoBarras(pm);
			tx.begin();
			long tuplasInsertadas = sqlProducto.adicionarProducto(pm, marca, nombre, presentacion, unidaddemedida, volumen, peso, codigobarras, tipo, cantidadpresentacion);
			tx.commit();

			log.trace ("Inserción de producto: " + codigobarras + ": " + tuplasInsertadas + " tuplas insertadas");

			return new Producto(codigobarras, marca, nombre, presentacion, unidaddemedida, volumen, peso, cantidadpresentacion, tipo);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla PRODUCTO
	 * @return El numero de tuplas eliminadas. null si ocurre alguna Excepción o si no se pudo eliminar.
	 */
	public long eliminarProducto(String cod) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long rta = sqlProducto.eliminarProductoPorCodigoBarras(pm, cod);
			tx.commit();

			log.trace ("Eliminacion de producto: " + cod + ": " + rta +" tuplas eliminadas");

			return rta; 
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return 0;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Método que retorna, de manera transaccional, una tupla en la tabla Producto
	 * @return El producto con nit dada. null si ocurre alguna Excepción o si no se pudo encontrar.
	 */
	public Producto darProducto(String cod) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			Producto rta = sqlProducto.darProductoPorCodigoBarras(pm, cod);
			tx.commit();

			log.trace ("Consulta de producto: " + cod + ": " + rta!=null?1:0 +" tuplas encontradas");

			return rta; 
		}
		catch (Exception e)
		{
			//e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Método que retorna, de manera transaccional, una tupla en la tabla Producto
	 * @return El producto con nit dada. null si ocurre alguna Excepción o si no se pudo encontrar.
	 */
	public List<Producto> darProductoPorNombre(String nombre) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			List<Producto> rta = sqlProducto.darProductoPorNombre(pm, nombre);
			tx.commit();

			log.trace ("Consulta de producto: " + nombre + ": " + rta!=null?rta.size():0 +" tuplas encontradas");

			return rta; 
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Método que retorna, de manera transaccional, una tupla en la tabla Producto
	 * @return El producto con nit dada. null si ocurre alguna Excepción o si no se pudo encontrar.
	 */
	public List<Object[]> darProductoPorTipo(String pTipo) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			List<Object[]> rta = sqlProducto.darProductosPorTipo(pm, pTipo);
			tx.commit();

			log.trace ("Consulta de productos de tipo: " + pTipo + ": " + rta!=null?rta.size():0 +" tuplas encontradas");

			return rta; 
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	public List<Producto> darProductoPorTipo2(String pTipo) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			List<Producto> rta = sqlProducto.darProductosPorTipo2(pm, pTipo);
			tx.commit();

			log.trace ("Consulta de productos de tipo: " + pTipo + ": " + rta!=null?rta.size():0 +" tuplas encontradas");

			return rta; 
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla Producto, dado el id del Producto
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public long[] eliminarProductoConSusInfoProducto(String cod) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long[] resp = sqlProducto.eliminarProductoEInfosProducto(pm, cod);
			tx.commit();
			return resp;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return new long[] {};
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/* ****************************************************************
	 * 			Métodos para manejar los PRODUCTO SUMINISTRADO
	 *****************************************************************/


	public ProductoSuministrado adicionarProductoSuministrado(double precioUnitario , int cantidadenbodega , String precioporunidadmedida , String codigobarras , int cantidadenestante , long idestante , long idbodega , long idSucursal)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();



			long tuplasInsertadas = sqlProductoSum.adicionarProductoSuministrado(pm, precioUnitario, cantidadenbodega, precioporunidadmedida, codigobarras, cantidadenestante, idestante, idbodega, idSucursal);
			tx.commit();
			log.trace ("Inserción de un producto: " +  ": " + tuplasInsertadas + " tuplas insertadas");


			Producto p = darProducto(codigobarras);
			return new ProductoSuministrado(codigobarras, p.getMarca(), p.getNombre(), p.getPresentacion(), p.getUnidadDeMedida(), p.getVolumen(), p.getPeso(), p.getCantidadPresentacion(), p.getTipo(), precioUnitario, cantidadenbodega, cantidadenestante, precioporunidadmedida, idbodega, idestante, idSucursal);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla PRODUCTO
	 * @return El objeto BODEGA adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public ProductoSuministrado adicionarProductoSuministradoPorOperario( double precioUnitario  , String precioporunidadmedida , String codigobarras ,  long idestante , long idbodega , long idSucursal) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			int cantidadenestante = 0;
			int cantidadenbodega = 0;
			Estante e = sqlEstante.darEstante(pm, idestante);
			Bodega b = sqlBodega.darBodega(pm, idbodega);
			Producto pr = darProducto(codigobarras);
			ProductoSuministrado ps = darProdutoSuministrado(codigobarras, idSucursal);

			if(ps== null)
			{

				if(verificacion(codigobarras))
				{
					ArrayList<InfoProducto> l = (ArrayList<InfoProducto>) darListaMadre(codigobarras);
					tx.begin();
					int unidadesTotales = 0;

					for (int i = 0; i < l.size(); i++) 
					{
						unidadesTotales += l.get(i).getCantidad();
					}
					Integer volumenPro =	Integer.parseInt(pr.getVolumen().replace(" cm3", ""));
					double volumenTotal = unidadesTotales * volumenPro ;
					double cocienteEstante = 0;
					double cocienteBodega = 0;
					if(e.getCantidadVolumetrica()> 0 && b.getCantidadVolumetrica() > 0)
					{
						cocienteEstante = volumenTotal / e.getCantidadVolumetrica();
						cocienteBodega = volumenTotal / b.getCantidadVolumetrica();
					}
					if( cocienteEstante <= 1 )
					{
						cantidadenestante = unidadesTotales;
					}
					else if(cocienteEstante >= 1 && cocienteEstante < 1.3 && cocienteBodega > 0.33 )
					{
						cantidadenestante = 2 * (unidadesTotales / 3 );
						cantidadenbodega = unidadesTotales /3;
					}
					else if( cocienteEstante >= 1.3 && cocienteEstante < 1.6 && cocienteBodega > 0.166)
					{
						cantidadenestante = 5 * (unidadesTotales / 6 );
						cantidadenbodega = unidadesTotales /6;
					}
					else if(cocienteEstante >= 1.6 && cocienteEstante <= 1.99 && cocienteBodega > 0.1)
					{
						cantidadenestante = 9 * (unidadesTotales/10);
						cantidadenbodega = unidadesTotales/10;
					}
					else if( cocienteBodega < 1)
					{
						cantidadenbodega = unidadesTotales;
					}
					else
					{
						cantidadenbodega = (b.getCantidadVolumetrica());
					}

					Integer volumenNuevoEstante = cantidadenestante * volumenPro;
					Integer volumenNuevoBodega = cantidadenbodega * volumenPro;
					volumenNuevoEstante += e.getCantidadVolumetrica();
					volumenNuevoBodega += b.getCantidadVolumetrica();
					long tuplasInsertadas = sqlProductoSum.adicionarProductoSuministrado(pm, precioUnitario, cantidadenbodega, precioporunidadmedida, codigobarras, cantidadenestante, idestante, idbodega, idSucursal);
					sqlInfoProducto.modificarARegistrado(pm, codigobarras);
					sqlEstante.modificarCantidadVolumetrica(pm, e.getId(), volumenNuevoEstante);
					sqlBodega.modificarCantidadVolumetrica(pm, b.getId(), volumenNuevoBodega);
					tx.commit();
					log.trace ("Inserción de producto suministrado: " + codigobarras + ": " + tuplasInsertadas + " tuplas insertadas");

					return new ProductoSuministrado(codigobarras, pr.getMarca(), pr.getNombre(), pr.getPresentacion(), pr.getUnidadDeMedida(), pr.getVolumen(), pr.getPeso(), pr.getCantidadPresentacion(), pr.getTipo(),precioUnitario, cantidadenbodega, cantidadenestante, precioporunidadmedida, idbodega, idestante, idSucursal);
				}
				else
				{
					log.error ("Error : " + "\n El producto no existe .... ");
					return null;
				}
			}
			else
			{
				return null;
			}

		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Método que consulta todas las tuplas en la tabla ProductoSuministrado
	 * @return La lista de objetos ProductoSuministrado, construidos con base en las tuplas de la tabla Bodega.
	 */
	public List<ProductoSuministrado> darProductosSuministrado ()
	{
		return sqlProductoSum.darProductosSuministrados(pmf.getPersistenceManager());
	}

	public List<ProductoSuministrado> darProductosSuministradosSucursalTipo(long idSucursal, String pTipo) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			List<ProductoSuministrado> pSum = sqlProductoSum.darProductosSuministradosSucursalTipo(pm, idSucursal);
			List<ProductoSuministrado> t = new ArrayList<ProductoSuministrado>();
			for (int i = 0; i < pSum.size(); i++) {
				Producto p = darProducto(pSum.get(i).getCodigoDeBarras());
				if(p.getTipo().equals(pTipo))
				{
					pSum.get(i).setNombre(p.getNombre());
					pSum.get(i).setTipo(pTipo);
					t.add(pSum.get(i));
				}
			}
			tx.commit();
			return t;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que retorna, de manera transaccional, una tupla en la tabla PRODUCTO SUMINISTRADO
	 * @return El producto suministrado con nit dada. null si ocurre alguna Excepción o si no se pudo encontrar.
	 */
	public ProductoSuministrado darProdutoSuministrado(String cod, long idSucursal) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			ProductoSuministrado rta = sqlProductoSum.darProductoPorCodigoBarras(pm, cod, idSucursal);
			tx.commit();

			log.trace ("Consulta de producto suministrado: " + cod + ": " + rta!=null?1:0 +" tuplas encontradas");

			return rta; 
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/* ****************************************************************
	 * 			Métodos para manejar los INFO PRODUCTO
	 *****************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla InfoProducto
	 * @return El objeto InfoProducto adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public InfoProducto adicionarInfoProducto(double precioUnitario,  int cantidad , String precioxUnidadMedida, double calidadProducto, long idOrden , String codigoproducto) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			InfoProducto ip = sqlInfoProducto.darInfoProduct(pm, codigoproducto, idOrden);
			long rta = -1;
			//falta verificar sucursal existente.
			if(ip == null)
			{
				rta = sqlInfoProducto.adicionarInfoProducto(pm,  precioUnitario,   cantidad ,  precioxUnidadMedida,  calidadProducto,  idOrden ,  codigoproducto);
			}
			else
			{
				sqlInfoProducto.modificarCantidad(pm, codigoproducto,cantidad);
			}
			tx.commit();

			log.trace ("Insercion de InfoProducto: " + idOrden +","+ codigoproducto+ ": " + rta +" tuplas insertadas");

			return new InfoProducto(idOrden, codigoproducto, precioUnitario, cantidad, precioxUnidadMedida, calidadProducto);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	public long eliminarInfoProducto(long idOrden, String codigoProducto)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long resp = sqlInfoProducto.eliminarInfoProducto(pm, idOrden, codigoProducto);
			tx.commit();
			return resp;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return -1;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/* ****************************************************************
	 * 			Métodos para manejar los ORDEN PEDIDO
	 *****************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla OrdenPedido
	 * @return El objeto OrdenPedido adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public OrdenPedido adicionarOrdenPedido(Timestamp fechaEntregaEstipulada, String estado, double calificacion, String nit,  long idsucursal, Timestamp fechaentregareal) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long id = nextval ();
			Proveedor p = darProveedor(nit);
			Sucursal s = darSucursal(idsucursal);
			if(p != null && s != null)
			{

				//falta verificar sucursal existente.
				long rta = sqlOrdenPedido.adicionarOrdenPedido(pm,  fechaEntregaEstipulada,  estado, calificacion,  nit,   idsucursal,  id,  fechaentregareal);
				tx.commit();

				log.trace ("Insercion de OrdenPedido: " + id + ": " + rta +" tuplas insertadas");

				return new OrdenPedido(id, idsucursal, nit, fechaEntregaEstipulada, fechaentregareal, estado, calificacion);
			}
			else
			{

				log.error ("Error : No existe el proveedor o la sucursal, por ende no se puede crear la orden pedido \\n");
				return null;
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla ordenPedido, dado el nombre de la ordenPedido.
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public long eliminarOrdenPedido( long id) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long resp = sqlOrdenPedido.eliminarOrdenPedidoPorId(pmf.getPersistenceManager(), id);
			tx.commit();
			return resp;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return -1;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla ordenPedido, dado el nombre de la ordenPedido.
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public long eliminarOrdenesPedido( long idSucursal, String nitproveedor) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long resp = sqlOrdenPedido.eliminarOrdenesPedido(pmf.getPersistenceManager(), idSucursal, nitproveedor);
			tx.commit();
			return resp;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return -1;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}	
	/**
	 * Método que consulta todas las tuplas en la tabla OrdenPedido
	 * @return La lista de objetos OrdenPedido, construidos con base en las tuplas de la tabla Bodega.
	 */
	public List<OrdenPedido> darOrdenesPedido ()
	{
		return sqlOrdenPedido.darOrdenesPedido(pmf.getPersistenceManager());
	}
	/**
	 * Método que consulta todas las tuplas en la tabla OrdenPedido
	 * @return La lista de objetos OrdenPedido, construidos con base en las tuplas de la tabla Bodega.
	 */
	public List<OrdenPedido> darOrdenesPedido (String nitproveedor, long idSucursal)
	{
		return sqlOrdenPedido.darOrdenesPedidoSucursalYOrdenPedidoEspecificos(pmf.getPersistenceManager(), nitproveedor, idSucursal);
	}

	public List<OrdenPedido> darOrderPedidoProveedor( String nitproveedor) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			List<OrdenPedido> ordenes = sqlOrdenPedido.darOrdenesPedidoProveedorEspecifico(pm, nitproveedor);
			tx.commit();
			return ordenes;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	public List<OrdenPedido> darOrdenesPedidoEstadoDeProveedor(String nitproveedor, String estado, long idSucursal)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			List<OrdenPedido> ordenes = sqlOrdenPedido.darOrdenesPedidoProveedorEspecificoEstadoPuntualySucur(pm, nitproveedor, estado, idSucursal);
			tx.commit();
			return ordenes;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	public List<OrdenPedido> darOrdenesPedidoEstadoDeProveedor(String nitproveedor, String estado)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			List<OrdenPedido> ordenes = sqlOrdenPedido.darOrdenesPedidoProveedorEspecificoEstadoPuntual(pm, nitproveedor, estado);
			tx.commit();
			return ordenes;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}


	/**
	 * Método que consulta todas las tuplas en la tabla OrdenPedido
	 * @return La lista de objetos OrdenPedido, construidos con base en las tuplas de la tabla Bodega.
	 */
	public OrdenPedido darOrdenPedido (long id)
	{
		return sqlOrdenPedido.darOrdenPedidoPorId(pmf.getPersistenceManager(),id);
	}
	/**
	 * Método que consulta todas las tuplas en la tabla OrdenPedido
	 * @return La lista de objetos OrdenPedido, construidos con base en las tuplas de la tabla Bodega.
	 */
	public void modificarEstado(long id , String nuevoEstado)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			sqlOrdenPedido.modificarEstado(pmf.getPersistenceManager(), id, nuevoEstado);
			tx.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Método que consulta todas las tuplas en la tabla OrdenPedido
	 * @return La lista de objetos OrdenPedido, construidos con base en las tuplas de la tabla Bodega.
	 */
	public void calificarOrden(long id , int calificacion)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			sqlOrdenPedido.calificarOrden(pm, calificacion, id);
			tx.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Método que consulta todas las tuplas en la tabla OrdenPedido
	 * @return La lista de objetos OrdenPedido, construidos con base en las tuplas de la tabla Bodega.
	 */
	public void finalizarFechaReal(long id)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			sqlOrdenPedido.modificarFechaEntregaReal(pm, id);
			tx.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/* ****************************************************************
	 * 			Métodos para manejar los PRODUCTOS DEL PROVEEDOR
	 *****************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla PRODUCTO
	 * @return El objeto ProductoProveedor adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public ProductosDelProveedor adicionarProductoProveedor( String codigoproducto, String nitproveedor) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();

			long tuplasInsertadas = sqlProductoProvee.adicionarProductosDelProveedor(pm, codigoproducto, nitproveedor);
			tx.commit();

			log.trace ("Inserción de producto proveedor: " + codigoproducto +","+nitproveedor + ": " + tuplasInsertadas + " tuplas insertadas");

			return new ProductosDelProveedor(nitproveedor, codigoproducto);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla ProductoProveedor, dado el nombre del ProductoProveedor
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public long eliminarProveedoresDeUnProducto(String codigoBarras) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long resp = sqlProductoProvee.eliminarProveedoresDeUnProducto(pm, codigoBarras);
			tx.commit();
			return resp;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return -1;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla ProductoProveedor, dado el nombre del ProductoProveedor
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public long eliminarProductosDeUnProveedor(String nit) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long resp = sqlProductoProvee.eliminarProductosDeUnProveedor(pm, nit);
			tx.commit();
			return resp;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return -1;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	/**
	 * Método que consulta todas las tuplas en la tabla ProductosDelProveedor
	 * @return La lista de objetos ProductosDelProveedor, construidos con base en las tuplas de la tabla ProductosDelProveedor
	 */
	public List<ProductosDelProveedor> darProductosProveedor ()
	{
		return sqlProductoProvee.darProductosProveedor (pmf.getPersistenceManager());
	}


	/* ************************************************************
	 * 			Métodos para manejar las SUCURSAL
	 *****************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla SUCURSAL
	 * @return El objeto SUCURSAL adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public Sucursal adicionarSucursal(String ciudad , String direccion , String nombre , int nivelReOrden , int nivelAbastecimiento , String nombreSupermercado) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long idSucursal = nextval ();
			long tuplasInsertadas = 0;
			Sucursal exis = sqlSucursal.darSucursalPorNombreYSuperMercado(pm, nombre, nombreSupermercado,ciudad);
			if(exis == null)
			{
				tuplasInsertadas = sqlSucursal.adicionarSucursal(pm, idSucursal, ciudad, direccion, nombre, nivelReOrden, nivelAbastecimiento, nombreSupermercado);
				tx.commit();

			}
			else
			{
				return null;
			}

			log.trace ("Inserción de sucursal: " + idSucursal + ": " + tuplasInsertadas + " tuplas insertadas");

			return new Sucursal(idSucursal, nombre, direccion, ciudad, nivelReOrden, nivelAbastecimiento);

		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla SUCURSAL
	 * @return El numero de tuplas eliminadas. null si ocurre alguna Excepción o si no se pudo eliminar.
	 */
	public long eliminarSucursal(long id) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long rta = sqlSucursal.eliminarSucursalPorId(pm, id);
			tx.commit();

			log.trace ("Eliminacion de la sucursal: " + id + ": " + rta +" tuplas eliminadas");

			return rta; 
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return 0;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla SUCURSAL por nombre
	 * @return El numero de tuplas eliminadas. null si ocurre alguna Excepción o si no se pudo eliminar.
	 */
	public long eliminarSucursalPorNombre(String nombre) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long rta = sqlSucursal.eliminarSucursalPorNombre(pm, nombre);
			tx.commit();

			log.trace ("Eliminacion de la sucursal: " + nombre + ": " + rta +" tuplas eliminadas");

			return rta; 
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return 0;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla SUCURSAL Y TODAS LAS QUE SE RELACIONEN CON LA MISMA
	 * @return El numero de tuplas eliminadas. null si ocurre alguna Excepción o si no se pudo eliminar.
	 */
	public long[] eliminarSucursalCompleto(long id) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long[] rta = sqlSucursal.eliminarSucursalCompleto(pm, id);
			int suma = 0;
			for (int i = 0; i < rta.length; i++) 
			{
				suma += rta[i];
			}
			tx.commit();

			log.trace ("Eliminacion de la sucursal: " + id + ": " + suma +" tuplas eliminadas");

			return rta;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return new long[0];
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que retorna, de manera transaccional, una tupla en la tabla SUCURSAL
	 * @return La sucursal con el id dado. null si ocurre alguna Excepción o si no se pudo eliminar.
	 */
	public Sucursal darSucursal(long id) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			Sucursal rta = sqlSucursal.darSucursalPorId(pm, id);
			tx.commit();

			log.trace ("Obtencion de la sucursal: " + id);

			return rta;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que consulta todas las tuplas en la tabla Sucursal
	 * @return La lista de objetos Sucursal, construidos con base en las tuplas de la tabla Sucursal
	 */
	public List<Sucursal> darSucursales()
	{
		return sqlSucursal.darSucursales(pmf.getPersistenceManager());
	}

	/**
	 * Método que modifica el nivel de reorden de una sucursal
	 * @return Cantidad de tuplas alteradas. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public long cambiarNivelReOrden(long id, int nivelReOrden)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long rta = sqlSucursal.cambiarNivelReOrden(pm, id, nivelReOrden);
			tx.commit();

			log.trace ("Cambio en la sucursal: " + id + ": " + rta +" tuplas alteradas");

			return rta;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return 0;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que modifica el nivel de abastecimiento de una sucursal
	 * @return Cantidad de tuplas alteradas. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public long cambiarNivelAbastecimiento(long id, int nivelAbastecimiento)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long rta = sqlSucursal.cambiarNivelAbastecimiento(pm, id, nivelAbastecimiento);
			tx.commit();

			log.trace ("Cambio en la sucursal: " + id + ": " + rta +" tuplas alteradas");

			return rta;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return 0;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/* **********************************************************
	 * 			Métodos para manejar las TIPO PRODUCTO
	 *****************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla TIPOPRODUCTO
	 * @return El objeto TIPOPRODUCTO adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public TipoProducto adicionarTipoProducto(String nombre, String categoria) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			if(sqlCategoria.darCategoriaProducto(pm, categoria) != null)
			{
				tx.begin();
				long tuplasInsertadas = sqlTipoProd.adicionarTipoProducto(pm, nombre, categoria);
				tx.commit();

				log.trace ("Inserción del tipo producto: " + nombre + ": " + tuplasInsertadas + " tuplas insertadas");

				return new TipoProducto(nombre, categoria);
			}
			else
			{
				log.error ("Error: No se puede crear el tipo de producto ya que la categoria no existe.");
				return null;

			}

		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla TIPOPRODUCTO
	 * @return el numero de tuplas eliminadas. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public long eliminarTipoProducto(String nombre) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long tuplasEliminadas = sqlTipoProd.eliminarTipoProducto(pm, nombre);
			tx.commit();

			log.trace ("Eliminacion del tipo producto: " + nombre + ": " + tuplasEliminadas + " tuplas insertadas");

			return tuplasEliminadas;

		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return 0;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que consulta todas las tuplas en la tabla TipoProducto
	 * @return La lista de objetos TipoProducto, construidos con base en las tuplas de la tabla TipoProducto
	 */
	public List<TipoProducto> darTiposProducto()
	{
		return sqlTipoProd.darTiposProducto(pmf.getPersistenceManager());
	}
	/* **********************************************************
	 * 			Métodos externos
	 *****************************************************************/
	//el prod ya fue entregado pero no ha sido registrado.
	public boolean verificacion(String cod)
	{
		boolean pertenece = false;
		ArrayList<InfoProducto> lista = (ArrayList<InfoProducto>) darListaMadre(cod);
		if(lista != null)
		{
			for (int i = 0; i < lista.size() && !pertenece ; i++)
			{
				if(lista.get(i).getCodigoProducto().equals(cod))
				{
					pertenece = true;
				}
			}
		}
		return pertenece;
	}
	public List<InfoProducto> darListaMadre(String cod)
	{
		return sqlInfoProducto.darListaDeProductosEntregadosYSinRegistrar(pmf.getPersistenceManager(),cod );
	}
	/* **********************************************************
	 * 			Métodos para manejar las TRANSACCIONCLIENTE
	 *****************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla TRANSACCIONCLIENTE
	 * @return El objeto TRANSACCIONCLIENTE adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public TransaccionCliente adicionarTransaccionCliente(Double monto, Timestamp fecha, long idCliente) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			if(sqlCliente.darCliente(pm, idCliente) != null)
			{
				tx.begin();
				long id = nextval ();
				long tuplasInsertadas = sqlTransaccionCliente.adicionarTransaccion(pm, id, monto, fecha, idCliente);
				tx.commit();
				log.trace ("Inserción de una transaccion: " + id + ": " + tuplasInsertadas + " tuplas insertadas");

				return new TransaccionCliente(id, monto, fecha, idCliente);
			}
			else
			{
				log.error ("No se puede agregar la transaccion, ya que el cliente no existe");
				return null;
			}
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla TRANSACCION por nombre
	 * @return El numero de tuplas eliminadas. null si ocurre alguna Excepción o si no se pudo eliminar.
	 */
	public long eliminarTransaccion(long id) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long rta = sqlTransaccionCliente.eliminarTransaccion(pm, id);
			tx.commit();

			log.trace ("Eliminacion de la transaccion: " + id + ": " + rta +" tuplas eliminadas");

			return rta; 
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return 0;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 *  * Método que obtiene, de manera transaccional, una tupla en la tabla TRANSACCIONCLIENTE por id
	 * @return La transaccion deseada. null si ocurre alguna Excepción o si no se pudo eliminar.
	 */
	public TransaccionCliente darTransaccion(long id) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			TransaccionCliente rta = sqlTransaccionCliente.darTransaccion(pm, id);
			tx.commit();

			log.trace ("Obtencion de la transaccion: " + id);

			return rta; 
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que obtiene, de manera transaccional, las transacciones hechas por un cliente de idCliente
	 * @return Las transacciones deseadas. null si ocurre alguna Excepción o si no se pudo eliminar.
	 */
	public List<TransaccionCliente> darTransaccionesPorCliente(long idCliente)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			if(sqlCliente.darCliente(pm, idCliente) != null)
			{
				tx.begin();
				List<TransaccionCliente> rta = sqlTransaccionCliente.darTransaccionesPorCliente(pm, idCliente);
				tx.commit();

				log.trace ("Obtencion de las transacciones del cliente: " + idCliente + ": " + rta.size() + " transacciones encontradas");

				return rta; 
			}
			else
			{
				log.error ("El cliente suministrado no existe");
				return new ArrayList<TransaccionCliente>();
			}
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return new ArrayList<TransaccionCliente>();
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que obtiene, de manera transaccional, todas las transacciones hechas
	 * @return Las transacciones deseadas. null si ocurre alguna Excepción o si no se pudo eliminar.
	 */
	public List<TransaccionCliente> darTransacciones()
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			List<TransaccionCliente> rta = sqlTransaccionCliente.darTransacciones(pm);
			tx.commit();

			log.trace (rta.size() + " transacciones encontradas");

			return rta; 
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return new ArrayList<TransaccionCliente>();
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Crea y ejecuta la sentencia SQL para modificar el monto de una transacci�n.
	 */
	public void modificarMonto( long idTransac, double nuevomonto)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			sqlTransaccionCliente.modificarMonto(pm, idTransac, nuevomonto);
			tx.commit();
		}
		catch(Exception e)
		{
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/* **********************************************************
	 * 			Métodos para manejar los USUARIO
	 *****************************************************************/

	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla USUARIO
	 * @return El objeto USUARIO adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public UsuarioPersonal adicionarUsuario(String user, String pw, long idSucursal) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{

			tx.begin();
			long tuplasInsertadas = sqlUsuario.adicionarUsuario(pm, user, pw, idSucursal);
			tx.commit();
			log.trace ("Inserción de un usuario: " + user + ": " + tuplasInsertadas + " tuplas insertadas");

			return new UsuarioPersonal(user, pw, idSucursal);
		}
		catch (Exception e)
		{
			//     	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que elimina, de manera transaccional, una tupla de la tabla USUARIO
	 * @return El objeto USUARIO eliminado. null si ocurre alguna Excepción o si no se pudo eliminar.
	 */
	public long eliminarUsuario(String user) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long tuplasInsertadas = sqlUsuario.eliminarUsuario(pm, user);
			tx.commit();
			log.trace ("Eliminacion de un usuario: " + user + ": " + tuplasInsertadas + " tuplas eliminadas");

			return tuplasInsertadas;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return 0;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que retorna, de manera transaccional, todas las tuplas de la tabla USUARIO
	 * @return Lista de usuarios. null si ocurre alguna Excepción.
	 */
	public List<UsuarioPersonal> darUsuarios()
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			List<UsuarioPersonal> rta = sqlUsuario.darUsuarios(pm);
			tx.commit();
			log.trace ("Obtencion de todos los usuarios: " + rta.size() + " tuplas obtenidas");

			return rta;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return new ArrayList<UsuarioPersonal>();
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que retorna, de manera transaccional, el USUARIO de la tabla que tenga el user dado por parametro
	 * @return Lista de usuarios. null si ocurre alguna Excepción.
	 */
	public UsuarioPersonal darUsuario(String user)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			UsuarioPersonal rta = sqlUsuario.darUsuario(pm,user);
			tx.commit();
			log.trace ("Obtencion de un usuario: " + user + ": " + rta!=null?1:0 + " tuplas encontradas");

			return rta;
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que modifica la contraseña de un USUARIO
	 * @return Cantidad de tuplas alteradas. null si ocurre alguna Excepción o si no se pudo cambiar.
	 */
	public long cambiarContrasena(String user, String newpw)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			if(sqlUsuario.darUsuario(pm, user) != null)
			{
				sqlUsuario.cambiarContrasena(pm, user, newpw);
				return 1;
			}
			else
			{
				log.trace("No existe el usuario con el user dado, no se modifica la contraseña");
				return 0;
			}
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return 0;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Elimina todas las tuplas de todas las tablas de la base de datos de SuperAndes
	 * Crea y ejecuta las sentencias SQL para cada tabla de la base de datos - EL ORDEN ES IMPORTANTE 
	 * @return Un arreglo con 7 números que indican el número de tuplas borradas en las tablas GUSTAN, SIRVEN, VISITAN, BEBIDA,
	 * TIPOBEBIDA, BEBEDOR y BAR, respectivamente
	 */
	public long [] limpiarSuperAndes ()
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long [] resp = sqlUtil.limpiarSuperAndes (pm);
			tx.commit ();
			log.info ("Borrada la base de datos");
			return resp;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return new long[] {};
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}

	}
	/* **********************************************************
	 * 			M�todos para manejar los CARRITOS
	 *****************************************************************/
	public Carrito adicionarProductoAlCliente( long idCliente, String idProducto, int cantidad, long idSucursal ) 
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{

			tx.begin();
			long tuplasInsertadas = sqlCarrito.adicionarProductoAlCliente(pm, idCliente, idProducto, cantidad);
			ProductoSuministrado ps = darProdutoSuministrado(idProducto, idSucursal);
			ProductoSuministrado ps2 = null;
			Producto pTemp = darProducto(idProducto);
			int volumen = Integer.parseInt(pTemp.getVolumen().replace(" cm3", "")); 
			Estante e = darEstante(ps.getIdEstante());
			Estante e2 = null;

			List<Promocion> li = sqlPromocion.darPromocionesDeUnProducto(pm, ps.getCodigoDeBarras());


			for (Promocion promocion : li) 
			{
				if(promocion.getEstado().equals("A"))
				{
					if(promocion.getTipo() >= 0)
					{
						if(promocion.getTipo() == 1)
						{
							PromocionPagueNLleveMUnidades pgnm = darPromocionPagueNLleveMUnidades(promocion.getId());
							cantidad *= pgnm.getUnidadesOfrecidas();
						}
						else if(promocion.getTipo() == 4)
						{
							cantidad *= 2;

						}
						else if(promocion.getTipo() == 5)
						{
							PromocionDosProductos p2p = darPromocionDosProductos(promocion.getId());
							ps2 = darProdutoSuministrado(p2p.getCodigoProducto2(), idSucursal);
							e2 = darEstante(ps2.getIdEstante()); 
						}
					}
				}
			}





			//Ahora el estante tiene menos cantidad volumetrica ya que se sac� el producto.
			sqlEstante.modificarCantidadVolumetrica(pm, e.getId() ,(e.getCantidadVolumetrica() - (cantidad * volumen))) ;
			//Modificas la cantidad en estante
			sqlProductoSum.modificarCantidadEstante(pm, idProducto, ps.getIdEstante(), (ps.getCantidadEnEstante()-cantidad), idSucursal);
			if(ps2 != null)
			{
				sqlProductoSum.modificarCantidadEstante(pm, ps2.getCodigoDeBarras(), e2.getId(), (ps2.getCantidadEnEstante()-cantidad), idSucursal);
			}
			tx.commit();
			log.trace ("Insercion de un producto " + idProducto + " al cliente  " + idCliente + " con una cantidad de" +tuplasInsertadas+" tuplas insertadas");

			return new Carrito(idCliente, idProducto, cantidad); 


		}
		catch (Exception e)
		{
			//e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	public long eliminarProductoAlCliente(long idCliente, String idProducto, long idSucursal)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			Carrito c = sqlCarrito.darProductoCliente(pm, idCliente, idProducto);
			ProductoSuministrado ps = darProdutoSuministrado(c.getIdProducto(), idSucursal);
			Estante e = darEstante(ps.getIdEstante());
			Producto p = darProducto(ps.getCodigoDeBarras());
			ps.setVolumen(p.getVolumen());
			long rta = sqlCarrito.eliminarProductoDelCliente(pm, idCliente, idProducto);
			sqlEstante.modificarCantidadVolumetrica(pm, ps.getIdEstante() ,(e.getCantidadVolumetrica() + (c.getCantidad() * Integer.parseInt(ps.getVolumen().replace(" cm3", ""))))) ;
			modificarCantidadEstante(c.getIdProducto(), ps.getIdEstante(), (ps.getCantidadEnEstante()+c.cantidad), idSucursal);
			tx.commit();

			log.trace ("Eliminacion de la tupla: " + idCliente +"-"+idProducto + ": " + rta +" tuplas eliminadas");

			return rta; 
		}
		catch (Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return 0;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	public long vaciarCarro(long idCliente)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			long rta = sqlCarrito.vaciarCarrito(pm, idCliente);
			tx.commit();

			log.trace ("Eliminacion de la tupla: " + idCliente + ": " + rta +" tuplas eliminadas");

			return rta; 
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return 0;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	public List<Carrito> darProductosDelCliente(long idCliente)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			if(sqlCliente.darCliente(pm, idCliente) != null)
			{
				tx.begin();
				List<Carrito> rta = sqlCarrito.darProductosDelCliente(pm, idCliente);
				tx.commit();

				log.trace ("Obtencion de los productos del cliente: " + idCliente + ": " + rta.size());

				return rta; 
			}
			else
			{
				log.error ("El cliente suministrado no existe");
				return new ArrayList<Carrito>();
			}
		}
		catch (Exception e)
		{
			//        	e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			return new ArrayList<Carrito>();
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	public void abandonarCarro(long idCliente)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();

		try
		{
			tx.begin();
			sqlCliente.abandonarCarro(pm, idCliente);
			tx.commit();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	public void desabandonarCarro(long idCliente)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();

		try
		{
			tx.begin();
			sqlCliente.desabandonarCarro(pm, idCliente);
			tx.commit();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	/**
	 * Método que consulta todas las tuplas en la tabla TipoProducto
	 * @return La lista de objetos TipoProducto, construidos con base en las tuplas de la tabla TipoProducto
	 */
	public List<OrdenPedido> darOrdenesPedidoEstado(String estado)
	{
		return sqlOrdenPedido.darOrdenesPedidoEnEstado(pmf.getPersistenceManager(), estado);
	}

	/**
	 * Crea y ejecuta la sentencia SQL para encontrar los productos asociados a una orden.
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos InfoProducto
	 */
	public List<InfoProducto> darProductosDeUnaOrden ( long idOrden)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx = pm.currentTransaction();
		try
		{
			tx.begin();
			List<InfoProducto> l = sqlInfoProducto.darProductosDeUnaOrden(pm, idOrden);
			tx.commit();
			return l;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}

	}
	/* **********************************************************
	 * 			Métodos para manejar los REQUERMIENTOS DE CONSULTA
	 *****************************************************************/
	public Object[] analisisOperacionSuperAndes(String tipo, String unidad , String determinado)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();
			Object[] demanda = demandaSuperAndes(pm, tipo, unidad, determinado);
			tx.commit();
			tx.begin();
			List<Object[]> ingreso = ingresoSuperAndes(pm, tipo, unidad, determinado);
			tx.commit();


			return new Object[] {demanda, ingreso};			
		}
		catch(Exception e)
		{
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			e.printStackTrace();
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	@SuppressWarnings("deprecation")
	private Object[] demandaSuperAndes(PersistenceManager pm , String tipo, String unidad , String determinado)
	{
		if(unidad.equals("anio"))
		{
			int anio = Integer.parseInt(determinado);
			//Maxima demanda
			String sql = "SELECT tt.sucursal, " + 
					" tb.\"Dia\", " + 
					" tb.\"month\", " + 
					" tt.maxi " + 
					"FROM  (SELECT sucursal, " + 
					" Max (\"demanda maxima\") AS maxi " + 
					" FROM   (SELECT sucursal, " + 
					" \"month\", " + 
					" \"Dia\", " + 
					" Sum(cant) AS \"demanda maxima\" " + 
					" FROM  (SELECT ps.idsucursal                AS Sucursal, " + 
					" Extract (day FROM tc.fecha)  AS \"Dia\", " + 
					"  Extract(month FROM tc.fecha) AS \"month\", " + 
					"  cc.cantidadproducto          AS cant " + 
					"  FROM "+ darTablaProducto() + " p, " + 
					darTablaProductoSuministrado() + "  ps, " + 
					darTablaTransaccion() + "  tc, " + 
					darTablaCompra() + "  cc " + 
					"  WHERE  p.codigodebarras = ps.codigodebarras " + 
					"  AND tc.id = cc.idtransaccion " + 
					"  AND cc.codigoproducto = ps.codigodebarras " + 
					"  AND p.tipo = ? " + 
					"  AND Extract(year FROM tc.fecha) = ?) " + 
					"  GROUP  BY sucursal, " + 
					"  \"month\", " + 
					"  \"Dia\") " + 
					" GROUP  BY sucursal) tt, " + 
					"(SELECT sucursal, " + 
					"\"month\", " + 
					"\"Dia\", " + 
					"  Sum(cant) AS \"demanda maxima\" " + 
					" FROM  (SELECT ps.idsucursal                AS Sucursal, " + 
					" Extract (day FROM tc.fecha)  AS \"Dia\", " + 
					" Extract(month FROM tc.fecha) AS \"month\", " + 
					"  cc.cantidadproducto          AS cant " + 
					"  FROM "+ darTablaProducto() + " p, " + 
					darTablaProductoSuministrado() + "  ps, " + 
					darTablaTransaccion() + "  tc, " + 
					darTablaCompra() + "  cc " + 
					"  WHERE  p.codigodebarras = ps.codigodebarras " + 
					"  AND tc.id = cc.idtransaccion " + 
					"  AND cc.codigoproducto = ps.codigodebarras " + 
					"  AND p.tipo = ? " + 
					"  AND Extract(year FROM tc.fecha) = ?) " + 
					"  GROUP  BY sucursal, " + 
					" \"month\", " + 
					" \"Dia\") tb " + 
					"WHERE  tt.maxi = tb.\"demanda maxima\"";
			Query q = pm.newQuery(SQL,sql);
			q.setParameters(tipo,anio,tipo,anio);


			List<Object[]> listaMaxima = (List<Object[]>) q.executeList();


			String sql1 = "SELECT tt.sucursal, " + 
					" tb.\"Dia\", " + 
					" tb.\"month\", " + 
					" tt.maxi " + 
					"FROM  (SELECT sucursal, " + 
					" Min (\"demanda maxima\") AS maxi " + 
					" FROM   (SELECT sucursal, " + 
					" \"month\", " + 
					" \"Dia\", " + 
					" Sum(cant) AS \"demanda maxima\" " + 
					" FROM  (SELECT ps.idsucursal                AS Sucursal, " + 
					" Extract (day FROM tc.fecha)  AS \"Dia\", " + 
					"  Extract(month FROM tc.fecha) AS \"month\", " + 
					"  cc.cantidadproducto          AS cant " + 
					"  FROM "+ darTablaProducto() + " p, " + 
					darTablaProductoSuministrado() + "  ps, " + 
					darTablaTransaccion() + "  tc, " + 
					darTablaCompra() + "  cc " + 
					"  WHERE  p.codigodebarras = ps.codigodebarras " + 
					"  AND tc.id = cc.idtransaccion " + 
					"  AND cc.codigoproducto = ps.codigodebarras " + 
					"  AND p.tipo = ? " + 
					"  AND Extract(year FROM tc.fecha) = ?) " + 
					"  GROUP  BY sucursal, " + 
					"  \"month\", " + 
					"  \"Dia\") " + 
					" GROUP  BY sucursal) tt, " + 
					"(SELECT sucursal, " + 
					"\"month\", " + 
					"\"Dia\", " + 
					"  Sum(cant) AS \"demanda maxima\" " + 
					" FROM  (SELECT ps.idsucursal                AS Sucursal, " + 
					" Extract (day FROM tc.fecha)  AS \"Dia\", " + 
					" Extract(month FROM tc.fecha) AS \"month\", " + 
					"  cc.cantidadproducto          AS cant " + 
					"  FROM "+ darTablaProducto() + " p, " + 
					darTablaProductoSuministrado() + "  ps, " + 
					darTablaTransaccion() + "  tc, " + 
					darTablaCompra() + "  cc " + 
					"  WHERE  p.codigodebarras = ps.codigodebarras " + 
					"  AND tc.id = cc.idtransaccion " + 
					"  AND cc.codigoproducto = ps.codigodebarras " + 
					"  AND p.tipo = ? " + 
					"  AND Extract(year FROM tc.fecha) = ?) " + 
					"  GROUP  BY sucursal, " + 
					" \"month\", " + 
					" \"Dia\") tb " + 
					"WHERE  tt.maxi = tb.\"demanda maxima\"";

			Query q1 = pm.newQuery(SQL,sql1);
			q1.setParameters(tipo,anio,tipo,anio);
			List<Object[]> listaMinima = (List<Object[]>) q1.executeList();

			return new Object[] {listaMaxima, listaMinima};


		}
		else if(unidad.equals("mes"))
		{
			//06-2018
			String p[] =  determinado.split("-");


			String sql = "SELECT tt.sucursal, " + 
					" tb.\"Dia\", " + 
					" tt.maxi " + 
					"FROM  (SELECT sucursal, " + 
					" MAX (\"demanda maxima\") AS maxi " + 
					" FROM   (SELECT sucursal, " + 
					" \"Dia\", " + 
					" Sum(cant) AS \"demanda maxima\" " + 
					" FROM  (SELECT ps.idsucursal                AS Sucursal, " + 
					" Extract (day FROM tc.fecha)  AS \"Dia\", " + 
					"  cc.cantidadproducto          AS cant " + 
					"  FROM "+ darTablaProducto() + " p, " + 
					darTablaProductoSuministrado() + "  ps, " + 
					darTablaTransaccion() + "  tc, " + 
					darTablaCompra() + "  cc " + 
					"  WHERE  p.codigodebarras = ps.codigodebarras " + 
					"  AND tc.id = cc.idtransaccion " + 
					"  AND cc.codigoproducto = ps.codigodebarras " + 
					"  AND p.tipo = ? " + 
					"  AND Extract(year FROM tc.fecha) = ? " + 
					"  AND Extract(month FROM tc.fecha) = ?) " + 
					"  GROUP  BY sucursal, " + 
					"  \"Dia\") " + 
					" GROUP  BY sucursal) tt, " + 
					"(SELECT sucursal, " + 
					"\"Dia\", " + 
					"  Sum(cant) AS \"demanda maxima\" " + 
					" FROM  (SELECT ps.idsucursal                AS Sucursal, " + 
					" Extract (day FROM tc.fecha)  AS \"Dia\", " + 
					"  cc.cantidadproducto          AS cant " + 
					"  FROM "+ darTablaProducto() + " p, " + 
					darTablaProductoSuministrado() + "  ps, " + 
					darTablaTransaccion() + "  tc, " + 
					darTablaCompra() + "  cc " + 
					"  WHERE  p.codigodebarras = ps.codigodebarras " + 
					"  AND tc.id = cc.idtransaccion " + 
					"  AND cc.codigoproducto = ps.codigodebarras " + 
					"  AND p.tipo = ? " + 
					"  AND Extract(year FROM tc.fecha) = ? " + 
					"  AND Extract(month FROM tc.fecha) = ?) " + 
					"  GROUP  BY sucursal, " + 
					" \"Dia\") tb " + 
					"WHERE  tt.maxi = tb.\"demanda maxima\"";

			Query q = pm.newQuery(SQL, sql);
			q.setParameters(tipo, p[1] , p[0] , tipo, p[1] , p[0] );
			List<Object[]> listaMaximo = q.executeList();


			String sql1 = "SELECT tt.sucursal, " + 
					" tb.\"Dia\", " + 
					" tt.maxi " + 
					"FROM  (SELECT sucursal, " + 
					" Min (\"demanda maxima\") AS maxi " + 
					" FROM   (SELECT sucursal, " + 
					" \"Dia\", " + 
					" Sum(cant) AS \"demanda maxima\" " + 
					" FROM  (SELECT ps.idsucursal                AS Sucursal, " + 
					" Extract (day FROM tc.fecha)  AS \"Dia\", " + 
					"  cc.cantidadproducto          AS cant " + 
					"  FROM "+ darTablaProducto() + " p, " + 
					darTablaProductoSuministrado() + "  ps, " + 
					darTablaTransaccion() + "  tc, " + 
					darTablaCompra() + "  cc " + 
					"  WHERE  p.codigodebarras = ps.codigodebarras " + 
					"  AND tc.id = cc.idtransaccion " + 
					"  AND cc.codigoproducto = ps.codigodebarras " + 
					"  AND p.tipo = ? " + 
					"  AND Extract(year FROM tc.fecha) = ? " + 
					"  AND Extract(month FROM tc.fecha) = ?) " + 
					"  GROUP  BY sucursal, " + 
					"  \"Dia\") " + 
					" GROUP  BY sucursal) tt, " + 
					"(SELECT sucursal, " + 
					"\"Dia\", " + 
					"  Sum(cant) AS \"demanda maxima\" " + 
					" FROM  (SELECT ps.idsucursal                AS Sucursal, " + 
					" Extract (day FROM tc.fecha)  AS \"Dia\", " + 
					"  cc.cantidadproducto          AS cant " + 
					"  FROM "+ darTablaProducto() + " p, " + 
					darTablaProductoSuministrado() + "  ps, " + 
					darTablaTransaccion() + "  tc, " + 
					darTablaCompra() + "  cc " + 
					"  WHERE  p.codigodebarras = ps.codigodebarras " + 
					"  AND tc.id = cc.idtransaccion " + 
					"  AND cc.codigoproducto = ps.codigodebarras " + 
					"  AND p.tipo = ? " + 
					"  AND Extract(year FROM tc.fecha) = ? " + 
					"  AND Extract(month FROM tc.fecha) = ?) " + 
					"  GROUP  BY sucursal, " + 
					" \"Dia\") tb " + 
					"WHERE  tt.maxi = tb.\"demanda maxima\"";

			Query q1 = pm.newQuery(SQL, sql1);
			q1.setParameters(tipo, p[1] , p[0] , tipo, p[1] , p[0] );
			List<Object[]> listaMinimo = q1.executeList();

			return new Object[] {listaMaximo, listaMinimo};


		}
		else if(unidad.equals("semana"))
		{
			//14-06-2018
			String p[] =  determinado.split("-");

			int dia = Integer.parseInt(p[0]);
			int mes = Integer.parseInt(p[1]);
			int anio = Integer.parseInt(p[2]);


			String sql = "SELECT sucursal, " + 
					"\"Dia\", " + 
					"  Sum(cant) AS \"demanda maxima\" " + 
					" FROM  (SELECT ps.idsucursal                AS Sucursal, " + 
					" Extract (day FROM tc.fecha)  AS \"Dia\", " + 
					"  cc.cantidadproducto          AS cant " + 
					"  FROM "+ darTablaProducto() + " p, " + 
					darTablaProductoSuministrado() + "  ps, " + 
					darTablaTransaccion() + "  tc, " + 
					darTablaCompra() + "  cc " + 
					"  WHERE  p.codigodebarras = ps.codigodebarras " + 
					"  AND tc.id = cc.idtransaccion " + 
					"  AND cc.codigoproducto = ps.codigodebarras " + 
					"  AND p.tipo = ? " + 
					"  AND Extract(year FROM tc.fecha) = ? " + 
					"  AND Extract(month FROM tc.fecha) = ?) " + 
					"  GROUP  BY sucursal, " + 
					" \"Dia\" "+
					" ORDER BY \"demanda maxima\" ";
			Query q = pm.newQuery(SQL,sql);
			q.setParameters(tipo, Integer.parseInt(p[2]) , Integer.parseInt(p[1]));

			Calendar c = Calendar.getInstance();
			c.setTime(new Date(anio, mes , dia));
			int semana = c.get(Calendar.WEEK_OF_MONTH);

			List<Object[]> ly = q.executeList();


			//listas de retorno

			ArrayList<Object[]> listaMayorDemanda = new ArrayList<>();
			ArrayList<Object[]> listaMenorDemanda = new ArrayList<>();


			BigDecimal cantidadMenorTemp = new BigDecimal(Integer.MAX_VALUE);
			BigDecimal cantidadMayorTemp = new BigDecimal(-1);

			boolean terminarMenor= false;
			boolean terminarMayor = false;

			for (int i = 0; i < ly.size() && !(terminarMenor && terminarMayor); i++)
			{
				Object[] tupla = ly.get(i);
				Object[] tupla2 = ly.get((ly.size()-1)-i);

				//dias
				BigDecimal diaTupla2 = (BigDecimal) tupla2[1];
				BigDecimal diaTupla = (BigDecimal) tupla[1];

				//cantidad
				BigDecimal cantidadMenor = (BigDecimal)tupla[2]; 
				BigDecimal cantidadMayor = (BigDecimal)tupla2[2];

				c.setTime(new Date(anio, mes , Integer.parseInt(diaTupla.toString())));
				int sem = c.get(Calendar.WEEK_OF_MONTH);

				c.setTime(new Date(anio, mes , Integer.parseInt(diaTupla2.toString())));
				int sem2 = c.get(Calendar.WEEK_OF_MONTH);

				if(semana == sem)
				{
					if(cantidadMenor.compareTo(cantidadMenorTemp) <= 0)
					{
						listaMenorDemanda.add(tupla);
						cantidadMenorTemp = cantidadMenor;
					}
					else
					{
						terminarMenor = true;
					}
				}

				if(semana == sem2)
				{
					if(cantidadMayor.compareTo(cantidadMayorTemp) >= 0)
					{
						listaMayorDemanda.add(tupla2);
						cantidadMayorTemp = cantidadMayor;
					}
					else
					{
						terminarMayor = true;
					}
				}

			}

			return new Object[] {listaMayorDemanda, listaMenorDemanda};

		}
		return null;
	}


	private List<Object[]> ingresoSuperAndes(PersistenceManager pm , String tipo, String unidad  , String determinado)
	{


		if(unidad.equals("anio"))
		{
			int anio = Integer.parseInt(determinado);
			//Maxima demanda
			String sql = "SELECT tt.sucursal, " + 
					" tb.\"Dia\", " + 
					" tb.\"month\", " + 
					" tt.maxi " + 
					"FROM  (SELECT sucursal, " + 
					" Max (\"demanda maxima\") AS maxi " + 
					" FROM   (SELECT sucursal, " + 
					" \"month\", " + 
					" \"Dia\", " + 
					" Sum(monto) AS \"demanda maxima\" " + 
					" FROM  (SELECT ps.idsucursal                AS Sucursal, " + 
					" Extract (day FROM tc.fecha)  AS \"Dia\", " + 
					"  Extract(month FROM tc.fecha) AS \"month\", " + 
					"  tc.monto          AS monto " + 
					"  FROM "+ darTablaProducto() + " p, " + 
					darTablaProductoSuministrado() + "  ps, " + 
					darTablaTransaccion() + "  tc, " + 
					darTablaCompra() + "  cc " + 
					"  WHERE  p.codigodebarras = ps.codigodebarras " + 
					"  AND tc.id = cc.idtransaccion " + 
					"  AND cc.codigoproducto = ps.codigodebarras " + 
					"  AND p.tipo = ? " + 
					"  AND Extract(year FROM tc.fecha) = ?) " + 
					"  GROUP  BY sucursal, " + 
					"  \"month\", " + 
					"  \"Dia\") " + 
					" GROUP  BY sucursal) tt, " + 
					"(SELECT sucursal, " + 
					"\"month\", " + 
					"\"Dia\", " + 
					"  Sum(monto) AS \"demanda maxima\" " + 
					" FROM  (SELECT ps.idsucursal                AS Sucursal, " + 
					" Extract (day FROM tc.fecha)  AS \"Dia\", " + 
					" Extract(month FROM tc.fecha) AS \"month\", " + 
					" tc.monto          AS monto " + 
					"  FROM "+ darTablaProducto() + " p, " + 
					darTablaProductoSuministrado() + "  ps, " + 
					darTablaTransaccion() + "  tc, " + 
					darTablaCompra() + "  cc " + 
					"  WHERE  p.codigodebarras = ps.codigodebarras " + 
					"  AND tc.id = cc.idtransaccion " + 
					"  AND cc.codigoproducto = ps.codigodebarras " + 
					"  AND p.tipo = ? " + 
					"  AND Extract(year FROM tc.fecha) = ?) " + 
					"  GROUP  BY sucursal, " + 
					" \"month\", " + 
					" \"Dia\") tb " + 
					"WHERE  tt.maxi = tb.\"demanda maxima\"";
			Query q = pm.newQuery(SQL,sql);
			q.setParameters(tipo,anio,tipo,anio);


			List<Object[]> listaMaxima = (List<Object[]>) q.executeList();

			return listaMaxima;


		}
		else if(unidad.equals("mes"))
		{
			//06-2018
			String p[] =  determinado.split("-");


			String sql = "SELECT tt.sucursal, " + 
					" tb.\"Dia\", " + 
					" tt.maxi " + 
					"FROM  (SELECT sucursal, " + 
					" MAX (\"demanda maxima\") AS maxi " + 
					" FROM   (SELECT sucursal, " + 
					" \"Dia\", " + 
					" Sum(monto) AS \"demanda maxima\" " + 
					" FROM  (SELECT ps.idsucursal                AS Sucursal, " + 
					" Extract (day FROM tc.fecha)  AS \"Dia\", " + 
					"  tc.monto          AS monto " + 
					"  FROM "+ darTablaProducto() + " p, " + 
					darTablaProductoSuministrado() + "  ps, " + 
					darTablaTransaccion() + "  tc, " + 
					darTablaCompra() + "  cc " + 
					"  WHERE  p.codigodebarras = ps.codigodebarras " + 
					"  AND tc.id = cc.idtransaccion " + 
					"  AND cc.codigoproducto = ps.codigodebarras " + 
					"  AND p.tipo = ? " + 
					"  AND Extract(year FROM tc.fecha) = ? " + 
					"  AND Extract(month FROM tc.fecha) = ?) " + 
					"  GROUP  BY sucursal, " + 
					"  \"Dia\") " + 
					" GROUP  BY sucursal) tt, " + 
					"(SELECT sucursal, " + 
					"\"Dia\", " + 
					"  Sum(monto) AS \"demanda maxima\" " + 
					" FROM  (SELECT ps.idsucursal                AS Sucursal, " + 
					" Extract (day FROM tc.fecha)  AS \"Dia\", " + 
					" tc.monto          AS monto " + 
					"  FROM "+ darTablaProducto() + " p, " + 
					darTablaProductoSuministrado() + "  ps, " + 
					darTablaTransaccion() + "  tc, " + 
					darTablaCompra() + "  cc " + 
					"  WHERE  p.codigodebarras = ps.codigodebarras " + 
					"  AND tc.id = cc.idtransaccion " + 
					"  AND cc.codigoproducto = ps.codigodebarras " + 
					"  AND p.tipo = ? " + 
					"  AND Extract(year FROM tc.fecha) = ? " + 
					"  AND Extract(month FROM tc.fecha) = ?) " + 
					"  GROUP  BY sucursal, " + 
					" \"Dia\") tb " + 
					"WHERE  tt.maxi = tb.\"demanda maxima\"";

			Query q = pm.newQuery(SQL, sql);
			q.setParameters(tipo, p[1] , p[0] , tipo, p[1] , p[0] );
			List<Object[]> listaMaximo = q.executeList();



			return  listaMaximo;


		}
		else if(unidad.equals("semana"))
		{
			//14-06-2018
			String p[] =  determinado.split("-");

			int Dia = Integer.parseInt(p[0]);
			int mes = Integer.parseInt(p[1]);
			int anio = Integer.parseInt(p[2]);


			String sql = "SELECT sucursal, " + 
					"\"Dia\", " + 
					"  Sum(monto) AS \"demanda maxima\" " + 
					" FROM  (SELECT ps.idsucursal                AS Sucursal, " + 
					" Extract (day FROM tc.fecha)  AS \"Dia\", " + 
					" tc.monto          AS monto " + 
					"  FROM "+ darTablaProducto() + " p, " + 
					darTablaProductoSuministrado() + "  ps, " + 
					darTablaTransaccion() + "  tc, " + 
					darTablaCompra() + "  cc " + 
					"  WHERE  p.codigodebarras = ps.codigodebarras " + 
					"  AND tc.id = cc.idtransaccion " + 
					"  AND cc.codigoproducto = ps.codigodebarras " + 
					"  AND p.tipo = ? " + 
					"  AND Extract(year FROM tc.fecha) = ? " + 
					"  AND Extract(month FROM tc.fecha) = ?) " + 
					"  GROUP  BY sucursal, " + 
					" \"Dia\" "+
					" ORDER BY \"demanda maxima\" ";
			Query q = pm.newQuery(SQL,sql);
			q.setParameters(tipo, Integer.parseInt(p[2]) , Integer.parseInt(p[1]));

			Calendar c = Calendar.getInstance();
			c.setTime(new Date(anio, mes , Dia));
			int semana = c.get(Calendar.WEEK_OF_MONTH);

			List<Object[]> ly = q.executeList();


			//listas de retorno

			ArrayList<Object[]> listaMayorDemanda = new ArrayList<>();

			BigDecimal cantidadMayorTemp = new BigDecimal(-1);

			boolean terminarMayor = false;

			for (int i = 0; i < ly.size() && !terminarMayor; i++)
			{
				Object[] tupla2 = ly.get((ly.size()-1)-i);

				//Dias
				BigDecimal diaTupla2 = (BigDecimal) tupla2[1];

				//cantidad
				BigDecimal cantidadMayor = (BigDecimal)tupla2[2];

				c.setTime(new Date(anio, mes , Integer.parseInt(diaTupla2.toString())));
				int sem2 = c.get(Calendar.WEEK_OF_MONTH);
				if(semana == sem2)
				{
					if(cantidadMayor.compareTo(cantidadMayorTemp) >= 0)
					{
						listaMayorDemanda.add(tupla2);
						cantidadMayorTemp = cantidadMayor;
					}
					else
					{
						terminarMayor = true;
					}
				}

			}

			return  listaMayorDemanda;

		}
		return null;
	}

	public List<BigDecimal> clientesFrecuentes(long idSucursal)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();

			String sql = "SELECT idCliente " + 
					" FROM (" + 
					" SELECT idCliente, COUNT(*) as mesesConsecutivosComprando" + 
					" FROM (" + 
					" SELECT idCliente, mes, anio, COUNT(*) compras" + 
					" FROM (" + 
					" SELECT tc.idCliente, EXTRACT (month from tc.fecha) AS mes, EXTRACT (year from tc.fecha) AS anio" + 
					" FROM "+ darTablaTransaccion()  + " tc," +darTablaCompra()+ " cc , " + darTablaProductoSuministrado()+ " ps" + 
					" WHERE tc.id = cc.idTransaccion AND cc.codigoproducto = ps.codigodebarras AND ps.idsucursal = ?" + 
					" )" + 
					" GROUP BY mes, anio, idCliente)" + 
					" WHERE compras >= 2" + 
					" GROUP BY idCliente )" + 
					" WHERE mesesConsecutivosComprando >= (((SELECT EXTRACT(year from SYSDATE)" + 
					" FROM dual) - ( SELECT EXTRACT(year from fechaMenor)" + 
					" FROM ( SELECT MIN(fecha) AS fechaMenor" + 
					" FROM " + darTablaTransaccion() + 
					"  )))*12)+((SELECT EXTRACT(month from SYSDATE)" + 
					" FROM dual)-(SELECT EXTRACT(month from fechaMenor)" + 
					" FROM ( SELECT MIN(fecha) AS fechaMenor" + 
					" FROM " + darTablaTransaccion() + 
					" )))";
			Query q = pm.newQuery(SQL,sql);
			q.setParameters(idSucursal);
			List<BigDecimal> l = (List<BigDecimal>)q.executeList();
			tx.commit();


			return l;			
		}
		catch(Exception e)
		{
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			e.printStackTrace();
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	public List<Object[]> ventasPorSucursal(String inicio, String fin)
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		System.out.println(inicio+"   "+fin);
		try
		{
			tx.begin();

			String sql = "SELECT idsa, SUM(mt) "
					+ "FROM "
					+ "		(SElECT tc.codigoproducto as prod, tc.cantidadproducto as cant, tc.idtransaccion as trans, ids as idsa "
					+ "		FROM "
					+ "			compra tc "
					+ "		INNER JOIN "
					+ "			(SELECT codigodebarras as cb, idsucursal as ids FROM PRODUCTOSUMINISTRADO) tcm "
					+ "		ON tc.codigoproducto = tcm.cb) A "
					+ "INNER JOIN "
					+ "		(SELECT fecha as fecha, monto as mt, id as id FROM TRANSACCIONCLIENTE WHERE fecha BETWEEN ? AND ?) B "
					+ "ON a.trans = b.id "
					+ "GROUP BY idsa";
			Query q = pm.newQuery(SQL,sql);
			q.setParameters(inicio , fin);
			List<Object[]> l = (List<Object[]>)q.executeList();
			tx.commit();


			return l;			
		}
		catch(Exception e)
		{
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			e.printStackTrace();
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	public List<Producto> darProductos()
	{

		return sqlProducto.darProductos(pmf.getPersistenceManager());
	}
	public ArrayList<String> productosBajaDemanda()
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();

			String sql = "SELECT cod, mes " +
					" FROM " +  
					"(SELECT ip.codigoproducto as cod, Extract(month FROM op.fechaentregareal) AS mes, Count(*) AS contador " +
					" FROM " + 
					darTablaOrdenPedido() + " op " + 
					" INNER JOIN " + 
					darTablaInfoProducto() + " ip" + 
					" ON op.id = ip.idorden " + 
					" GROUP  BY ip.codigoproducto, fechaentregareal " + 
					" ORDER  BY cod, fechaentregareal DESC) " +
					" WHERE contador > 0 " ;
			Query q = pm.newQuery(SQL,sql);
			List<Object[]> l = (List<Object[]>)q.executeList();

			tx.commit();


			boolean  productoChange = false;

			ArrayList<String> codigoDeProductosBajaDemanda = new ArrayList<>();
			for (int i = 0; i < l.size() - 1; i++) 
			{
				Object[] o  = l.get(i);
				Object[] e = l.get(i+1);

				String productoReferencia = (String)o[0];
				String productoRef = (String)e[0];

				int mesO = Integer.parseInt(((BigDecimal)o[1]).toString());
				int mesE = Integer.parseInt(((BigDecimal)e[1]).toString());

				if(productoChange)
				{
					if(productoReferencia.equals(productoRef))
					{
						continue;
					}
					else
					{
						productoChange = false;
					}
				}

				if(productoReferencia.equals(productoRef))
				{
					if( mesO - mesE > 2 )
					{
						codigoDeProductosBajaDemanda.add(productoReferencia);
						productoChange =  true;
					}
				}
			}

			return codigoDeProductosBajaDemanda;


		}
		catch(Exception e)
		{
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			e.printStackTrace();
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}

	public List<Object[]> top20Promociones()
	{
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		try
		{
			tx.begin();

			String sql = "SELECT id, nombre, fechaexpedicion, fechafinexistencias FROM PROMOCION WHERE ROWNUM <= 20 ORDER BY (fechafinexistencias-fechaexpedicion) asc";
			Query q = pm.newQuery(SQL,sql);
			List<Object[]> l = (List<Object[]>)q.executeList();
			tx.commit();


			return l;			
		}
		catch(Exception e)
		{
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			e.printStackTrace();
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	
	
	public List<Object[]> req1Itera3(String codigo, String fechaInicio, String fechaFinal, String tipoCliente, ArrayList<String> seleccion, String ordenamiento)
	{
		try 
		{
			boolean ordenamientoCliente = false;
			
			String tablaClienteAEvaluar = "cliente";
			if(tipoCliente.contains("Empresa"))
			{
				tablaClienteAEvaluar = "clienteempresa";
				if(ordenamiento.contains("nit"))
				{
					ordenamiento = ordenamiento.replace("nit", "cl.nit");
					ordenamientoCliente = true;
				}
			}
			else if(tipoCliente.contains("Natural"))
			{
				tablaClienteAEvaluar = "clientenatural";
				if(ordenamiento.contains("cedula"))
				{
					ordenamiento = ordenamiento.replace("cedula", "cl.cedula");
					ordenamientoCliente = true;

				}
				else if(ordenamiento.contains("nombre"))
				{
					ordenamiento = ordenamiento.replace("nombre", "cl.nombre");
					ordenamientoCliente = true;

				}
				else if(ordenamiento.contains("email"))
				{
					ordenamiento = ordenamiento.replace("email", "cl.email");
					ordenamientoCliente = true;

				}
			}
			else
			{
				 if(ordenamiento.contains("puntos"))
				{
					ordenamiento = ordenamiento.replace("puntos", "cl.puntos");
					ordenamientoCliente = true;
				}
			}
			
			if(ordenamiento.contains("id"))
			{
				ordenamiento = ordenamiento.replace("id", "cl.id");
				ordenamientoCliente = true;
			}
			
			if(ordenamiento.endsWith("1"))
			{
				ordenamiento = ordenamiento.replace("//1", " ASC");
			}
			else
			{
				ordenamiento = ordenamiento.replace("//0", " DESC");

			}
			
			if(!ordenamientoCliente)
			{
				ordenamiento = "T." + ordenamiento;
			}
			
			
			String ordenar = "ORDER BY ";
			ordenar += ordenamiento;
			if(ordenar.length()	< 10 )
			{
				ordenar = "";
			}
			
			String sele = " ";
			boolean tieneInfoCliente = false;
			for (int i = 0; i < seleccion.size() ; i++) 
			{
				if(seleccion.get(i).equals("todoInfo"))
				{
					sele += " cl.*";
					tieneInfoCliente = true;
				}
				else if(seleccion.get(i).equals("cantidad"))
				{
					sele += " T.Cantidad";
				}
				else if(seleccion.get(i).equals("monto"))
				{
					sele += " T.Monto";
				}
				else if(seleccion.get(i).equals("numero"))
				{
					sele += " T.Numero";
				}
				if(i != seleccion.size() - 1 )
				{
					sele+= " ,";
				}
			}		
			
			String sql = "SELECT " + sele +
					" FROM (SELECT a.idcliente , SUM (b.cantidadproducto) AS Cantidad , SUM (a.monto) AS Monto, COUNT(*) AS Numero" +
					" FROM "+ darTablaTransaccion() +" a, "+ darTablaCompra() +" b " + 
					" WHERE a.id = b.idtransaccion AND b.codigoproducto = ? AND " + 
					" a.fecha BETWEEN ? AND ? " + 
					" GROUP BY a.idcliente) T , "+ tablaClienteAEvaluar + " cl" + 
					" WHERE  T.idcliente = cl.id "
					+ ordenar;
			
			if(!tieneInfoCliente)
			{
				sql = "SELECT cl.id " + (sele.equals(" ")? "":",") + sele +
						"FROM (SELECT a.idcliente , SUM(b.cantidadproducto)AS Cantidad , SUM(a.monto) AS Monto, COUNT(*) AS Numero" +
						" FROM "+ darTablaTransaccion() +" a, "+ darTablaCompra() +" b " + 
						" WHERE a.id = b.idtransaccion AND b.codigoproducto = ? AND " + 
						" a.fecha BETWEEN ? AND ? " + 
						" GROUP BY a.idcliente) T , "+ tablaClienteAEvaluar + " cl" + 
						" WHERE  T.idcliente = cl.id "
						+ ordenar;	
			}
			
			PersistenceManager pm = pmf.getPersistenceManager();
			Transaction tx=pm.currentTransaction();
		
		
		try
		{
			Query q = pm.newQuery(SQL,sql);
			q.setParameters(codigo, fechaInicio, fechaFinal);
			tx.begin();
			long tiempoInicial = System.currentTimeMillis();
			List<Object[]> l = (List<Object[]>)q.executeList();
			tx.commit();
			long tiempoFinal = (System.currentTimeMillis() - tiempoInicial)/1000;
			System.out.println(" Tiempo empleado : "+ tiempoFinal + " s");

			return l;			
		}
		catch(Exception e)
		{
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			//e.printStackTrace();
			return null;
		}
		finally
		{

				if (tx.isActive())
				{
					tx.rollback();
				}
				pm.close();
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
		
	}
	
	public List<Object[]> req2Itera3(String codigo, String fechaInicio, String fechaFinal, String tipoCliente, boolean seleccion, String ordenamiento)
	{
		String tablaClienteAEvaluar = "cliente";
		
		if(tipoCliente.contains("Empresa"))
		{
			tablaClienteAEvaluar = "clienteempresa";
			if(ordenamiento.equals("nit"))
			{
				ordenamiento = ordenamiento.replace("nit", "cl.nit");
			}
		}
		else if(tipoCliente.contains("Natural"))
		{
			tablaClienteAEvaluar = "clientenatural";
			if(ordenamiento.equals("cedula"))
			{
				ordenamiento = ordenamiento.replace("cedula", "cl.cedula");

			}
			else if(ordenamiento.equals("nombre"))
			{
				ordenamiento = ordenamiento.replace("nombre", "cl.nit");

			}
			else if(ordenamiento.equals("email"))
			{
				ordenamiento = ordenamiento.replace("email", "cl.email");

			}
		}
		else
		{
			if(ordenamiento.equals("id"))
			{
				ordenamiento = ordenamiento.replace("id", "cl.id");
			}
			else if(ordenamiento.equals("puntos"))
			{
				ordenamiento = ordenamiento.replace("puntos", "cl.puntos");
			}
		}
		
		if(ordenamiento.endsWith("1"))
		{
			ordenamiento = ordenamiento.replace("//1", " ASC");
		}
		else
		{
			ordenamiento = ordenamiento.replace("//0", " DESC");

		}
		
		
		String ordenar = "ORDER BY ";
		ordenar += ordenamiento;
		if(ordenar.length()	< 10 )
		{
			ordenar = "";
		}
		
		String sql = " SELECT * " + 
				" FROM "+ tablaClienteAEvaluar +" c" + 
				" WHERE c.id NOT IN (" + 
				"        SELECT  a.idcliente" + 
				"        FROM "+darTablaTransaccion()+ " a, "+ darTablaCompra()+" b " + 
				"        WHERE a.id = b.idtransaccion AND b.codigoproducto = ? AND" + 
				"        a.fecha BETWEEN ? AND ? ) " + 
				" "+ ordenar;
		if(!seleccion)
		{
			 sql = " SELECT c.id " + 
					" FROM "+ tablaClienteAEvaluar +" c" + 
					" WHERE c.id NOT IN (" + 
					"        SELECT  a.idcliente" + 
					"        FROM "+darTablaTransaccion()+ " a, "+ darTablaCompra()+" b " + 
					"        WHERE a.id = b.idtransaccion AND b.codigoproducto = ? AND" + 
					"        a.fecha BETWEEN ? AND ? ) " + 
					" "+ordenar;
		}
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();
		
		try
		{
			Query q = pm.newQuery(SQL,sql);
			q.setParameters(codigo, fechaInicio, fechaFinal);
			tx.begin();
			List<Object[]> l = (List<Object[]>)q.executeList();
			long tiempoInicial = System.currentTimeMillis();
			tx.commit();
			double tiempoFinal = (System.currentTimeMillis() - tiempoInicial)/1000.0;
			System.out.println(" Tiempo empleado : "+ tiempoFinal + " s");

			return l;			
		}
		catch(Exception e)
		{
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			//e.printStackTrace();
			return null;
		}
		finally
		{
			if (tx.isActive())
			{
				tx.rollback();
			}
			pm.close();
		}
	}
	
	
	public List<List<Object[]>> req3Itera3()
	{
		//proveedores m�s solicitados
		String sql1 = "SELECT A.semana, B.nitproveedor,A.maximo " + 
				" FROM   (SELECT semana, Max(\"contador\") AS Maximo " + 
				"        FROM   (SELECT semana, nitproveedor, Count(*) AS \"contador\" " + 
				"                FROM  " + darTablaOrdenPedido() +  
				"                GROUP  BY semana, nitproveedor) " + 
				"        GROUP  BY semana) A, " + 
				"       (SELECT semana,   nitproveedor, Count(*) AS contador " + 
				"        FROM  " + darTablaOrdenPedido() +  
				"        GROUP  BY semana, nitproveedor) B " + 
				" WHERE  A.semana = B.semana " + 
				"       AND A.maximo = B.contador" + 
				" ORDER  BY A.semana";
		
		
		//proveedores menos solicitados
		String sql2 = "SELECT A.semana, B.nitproveedor, A.minimo " + 
				"FROM   (SELECT semana, Min(\"contador\") AS Minimo " + 
				"        FROM   (SELECT semana,   nitproveedor,  Count(*) AS \"contador\" " + 
				"                FROM  " + darTablaOrdenPedido() + 
				"                GROUP  BY semana,   nitproveedor) " + 
				"        GROUP  BY semana) A, " + 
				"       (SELECT semana, nitproveedor,  Count(*) AS contador " + 
				"        FROM  " + darTablaOrdenPedido() + 
				"        GROUP  BY semana, nitproveedor) B " + 
				" WHERE  A.semana = B.semana " + 
				"       AND A.minimo = B.contador " + 
				" ORDER  BY A.semana" ;
		
		//producto menos vendido
		String sql3 = "SELECT A.semana, B.codigoproducto, A.minimo " + 
				" FROM   (SELECT C.semana, Min(C.\"contador\") AS Minimo " + 
				" FROM   (SELECT tr.semana, cm.codigoproducto, sum(cm.cantidadproducto) AS \"contador\" " + 
				"                FROM  "+darTablaTransaccion()+" tr, "+darTablaCompra()+" cm " + 
				"                WHERE  tr.id = cm.idtransaccion " + 
				"                GROUP  BY tr.semana, cm.codigoproducto) C " + 
				"        GROUP  BY C.semana) A, " + 
				"       (SELECT tr.semana, cm.codigoproducto, sum(cm.cantidadproducto) AS contador " + 
				"        FROM  "+ darTablaTransaccion() +" tr, " + 
				"           "+darTablaCompra() +"    cm " + 
				"        WHERE  tr.id = cm.idtransaccion " + 
				"        GROUP  BY tr.semana, cm.codigoproducto) B " + 
				"WHERE A.semana = B.semana " + 
				"       AND A.minimo = B.contador " +
				" ORDER  BY A.semana" ;
		
		String sql4 = "SELECT A.semana, B.codigoproducto, A.maximo " + 
				" FROM   (SELECT C.semana, Max(C.\"contador\") AS Maximo " + 
				" FROM   (SELECT tr.semana, cm.codigoproducto, sum(cm.cantidadproducto) AS \"contador\" " + 
				"                FROM  "+darTablaTransaccion()+" tr, "+darTablaCompra()+" cm " + 
				"                WHERE  tr.id = cm.idtransaccion " + 
				"                GROUP  BY tr.semana, cm.codigoproducto) C " + 
				"        GROUP  BY C.semana) A, " + 
				"       (SELECT tr.semana, cm.codigoproducto, sum(cm.cantidadproducto)AS contador " + 
				"        FROM  "+ darTablaTransaccion() +" tr, " + 
				"           "+ darTablaCompra() +"   cm " + 
				"        WHERE  tr.id = cm.idtransaccion " + 
				"        GROUP  BY tr.semana,  cm.codigoproducto) B " + 
				"WHERE A.semana = B.semana " + 
				"       AND A.maximo = B.contador " +
				" ORDER  BY A.semana" ;
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();

		
			Query q = pm.newQuery(SQL,sql1);
			Query q1 = pm.newQuery(SQL,sql2);
			Query q3 = pm.newQuery(SQL,sql3);
			Query q4 = pm.newQuery(SQL,sql4);
			try
			{
			tx.begin();
			long tiempoInicial1 = System.currentTimeMillis();
			List<Object[]> l = (List<Object[]>)q.executeList();
			double tiempoFinal1 = (System.currentTimeMillis() - tiempoInicial1)/1000.0;
			System.out.println(" Tiempo empleado en dar los proveedores mas solicitados: "+ tiempoFinal1 + " s");
			
			long tiempoInicial2 = System.currentTimeMillis();
			List<Object[]> l2 = (List<Object[]>)q1.executeList();
			double tiempoFinal2 = (System.currentTimeMillis() - tiempoInicial2)/1000.0;
			System.out.println(" Tiempo empleado en dar los proveedores menos solicitados: "+ tiempoFinal2 + " s");
			
			long tiempoInicial3 = System.currentTimeMillis();
			List<Object[]> l3 = (List<Object[]>)q3.executeList();
			double tiempoFinal3 = (System.currentTimeMillis() - tiempoInicial3)/1000.0;
			System.out.println(" Tiempo empleado en dar los productos menos vendidos: "+ tiempoFinal3 + " s");
			
			long tiempoInicial4 = System.currentTimeMillis();
			List<Object[]> l4 = (List<Object[]>)q4.executeList();
			double tiempoFinal4 = (System.currentTimeMillis() - tiempoInicial4)/1000.0;
			System.out.println(" Tiempo empleado en dar los proveedores mas vendidos: "+ tiempoFinal4 + " s");
			tx.commit();
			
			ArrayList<List<Object[]>> array = new ArrayList<>();
			
			
			
			array.add(l);
			array.add(l2);
			array.add(l3);
			array.add(l4);
			return array;			
			
			}
			catch(Exception e)
			{
				log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
				//e.printStackTrace();
				return null;
			}
			finally
			{
					if (tx.isActive())
					{
						tx.rollback();
					}
					pm.close();
			}
		
		
	}
	
	public List<List<Object[]>> req4Iter3()
	{
		//al menos una compra al mes
		String sqlA = " SELECT cl.id, cl.puntos FROM (SELECT t.idcliente, Count(*) FROM (" + 
				" SELECT tr.idcliente , extract(month from tr.fecha) as mes" + 
				" FROM "+darTablaTransaccion()+ " tr  " + 
				" GROUP BY tr.idcliente, extract(month from tr.fecha) )T " + 
				" group by t.idcliente HAVING Count(*) > 11" + 
				" ORDER BY  t.idcliente) V , " + darTablaCliente() +  " cl" + 
				" WHERE v.idcliente = cl.id";
		
		String sqlB = " SELECT cl.id, cl.puntos  FROM (SELECT Y.idcliente" + 
				" from (SELECT tr.idcliente, COUNT(*) AS conta" + 
				" 					FROM "+darTablaTransaccion()+ " tr  " + 
				"                                WHERE tr.monto > 100000" + 
				"                                    GROUP By tr.idcliente)Y , (SELECT tr.idcliente, COUNT(*) AS cuenta" + 
				" 					FROM "+darTablaTransaccion()+ " tr  " + 
				"                                    GROUP By tr.idcliente)T" + 
				" WHERE Y.idcliente = T.idcliente AND Y.conta = T.cuenta" + 
				" GROUP By Y.idcliente" + 
				" ORDER BY Y.idcliente) U, "+ darTablaCliente() +" cl " + 
				" WHERE u.idcliente = cl.id";
		
		String sqlC = " SELECT cl.id, cl.puntos  FROM (" + 
				" SELECT P.idcliente FROM (" + 
				" SELECT tc.idcliente , COUNT(tc.id) AS conta" + 
				" FROM "+darTablaTransaccion()+" tc , "+ darTablaCompra() +" cm , "+ darTablaProducto() + " p" + 
				" WHERE cm.codigoproducto = p.codigodebarras AND tc.id = cm.idtransaccion AND (p.tipo = 'tp-5-1' or p.tipo = 'tp-8-1')" + 
				" GROUP BY tc.idcliente , tc.id , p.tipo" + 
				" ORDER BY tc.idcliente) P, (SELECT tr.idcliente, COUNT(*) AS cuenta" + 
				" 					FROM "+darTablaTransaccion()+ " tr  " + 
				"                                    GROUP By tr.idcliente) M" + 
				"                    WHERE P.idcliente = M.idcliente AND P.conta = M.cuenta) S,"+darTablaCliente()+" cl" + 
				"                    WHERE S.idcliente = cl.id";
		
		PersistenceManager pm = pmf.getPersistenceManager();
		Transaction tx=pm.currentTransaction();

		try
		{
			Query q = pm.newQuery(SQL,sqlA);
			Query q1 = pm.newQuery(SQL,sqlB);
			Query q2 = pm.newQuery(SQL,sqlC);
			
			tx.begin();
			long tiempoInicial1 = System.currentTimeMillis();
			List<Object[]> l = (List<Object[]>)q.executeList();
			double tiempoFinal1 = (System.currentTimeMillis() - tiempoInicial1)/1000.0;
			System.out.println(" Tiempo empleado en dar los buenos clientes por el tipo de compras todos los meses: "+ tiempoFinal1 + " s");
			
			long tiempoInicial2 = System.currentTimeMillis();
			List<Object[]> l2 = (List<Object[]>)q1.executeList();
			double tiempoFinal2 = (System.currentTimeMillis() - tiempoInicial2)/1000.0;
			System.out.println(" Tiempo empleado en dar los buenos clientes por el tipo de todas sus compras mayores a 100.000 :"+ tiempoFinal2 + " s");
			
			long tiempoInicial3 = System.currentTimeMillis();
			List<Object[]> l3 = (List<Object[]>)q2.executeList();
			double tiempoFinal3 = (System.currentTimeMillis() - tiempoInicial3)/1000.0;
			System.out.println(" Tiempo empleado en los buenos clientes por tener en todas sus compras productos de Herramienta y tecnologia: "+ tiempoFinal3 + " s");
			
			tx.commit();
			ArrayList<List<Object[]>> array = new ArrayList<>();
			
			
			
			array.add(l);
			array.add(l2);
			array.add(l3);
			
			
			return array;
		}
		catch(Exception e)
		{
			log.error ("Exception : " + e.getMessage() + "\n" + darDetalleException(e));
			//e.printStackTrace();
			return null;
		}
		finally
		{
				if (tx.isActive())
				{
					tx.rollback();
				}
				pm.close();
		}
	}
	
	
	
	


	/**
	 * Carga de datos
	 */
	public void cargaMasiva()
	{
		long mili = System.currentTimeMillis();
		System.out.println(mili);
		PersistenceManager pm = pmf.getPersistenceManager();
		System.out.println("--");
		//iteracion de productos y proveedores
		//
		//		for (int i = 0; i < 10; i++) //categoria
		//		{
		//			try
		//			{
		//			sqlCategoria.adicionarCategoriaProducto(pm, "c"+i);
		//			}
		//			catch(Exception e)
		//			{
		//				//
		//			a}
		//			for (int j = 0; j < 10; j++) //tipo
		//			{
		//				String tipo = "tp"+"-"+(j+1)+"-"+(i+1);
		//				try
		//				{
		//				sqlTipoProd.adicionarTipoProducto(pm, tipo, "c"+i);
		//				}
		//				catch(Exception e)
		//				{
		//					//
		//				}
		//				for (int k = 0; k < 100; k++) //produ
		//				{
		//					try
		//					{
		//					String codigo = "cod"+"-"+(k+1)+"-"+(i+1)+"-"+(j+1);
		//					sqlProducto.adicionarProducto(pm, "Nestle", "p"+k , "BlaBla", k%2==0?"gr":"cm3", "100 cm3", "100 gr", codigo , tipo, 100);
		//					for (int l = 0; l < 20; l++) 
		//					{
		//						St<ring nit = "prov"+"-"+(l+1)+"-"+(k+1)+"-"+(j+1)+"-"+(i+1);
		//						adicionarProveedor( nit , nit+"prov");
		//						sqlProductoProvee.adicionarProductosDelProveedor(pm, codigo,nit);
		//						System.out.println("Me faltaba");
		//					}
		//					}
		//					catch(Exception e)
		//					{
		//						System.out.println(e.getMessage());
		//						//
		//					}
		//				}
		//			}
		//		}
		
//		List<Sucursal> sucursales = darSucursales();
//		
//		List<Producto> productos = darProductos();
//	//	int i = 11805;
//		List<Estante> estantes = darEstantes();
//		
//		List<Bodega> bodegas = darBodegas();
		
		System.out.println("Acabo for productos");

		
		int p=0;
		
		List<ProductoSuministrado> lis = darProductosSuministrado();


		for (int j = 0; j <1000; j++) 
		{
			
			
//			if(i == 12395)
//			{
//				break;
//			}
			//int nivel = (int) (((i+1)*Math.random()*1000)%200);
			//Sucursal s = adicionarSucursal("city"+(i%10), "Cll "+i, "Sucur"+i, nivel , nivel, "Exito");
			//Long idSucursal = s.getId();
			//	System.out.println(sucursal.getId());n
			//sucursales.
			//			for (int j = 0; j < 200; j++) 
			//			{
			//				adicionarUsuario("user"+j+"-"+idSucursal, "123", idSucursal);
			//				//usuario	
			//			}



				
//				for (int k = 0; k < 10; k++)
//				{
//					int indice = (int)((Math.random()*100000)%13176);
//					
//					ProductoSuministrado ps= adicionarProductoSuministrado((30000*(j+1))%1000, 100, "t", productos.get(p).getCodigoDeBarras() , 50, estantes.get(indice).getId(), bodegas.get(indice).getId(), estantes.get(indice).getIdSucursal());
//					
//					if(ps== null)
//					{
//						System.out.println("Jeje");
//					}
//					p++;
//					
//				}
//				int l = 1;
//				int b= 4;	


				//					for (int k = 0; k < 10; k++) 
				//					{
				////						if(k == 99)
				////						{
				////							l= 1;
				////							b = 10;
				////						}
				////
				////						else if((k+1)%10 == 0)
				////						{
				////							l++;
				////							b = 1;
				////						}
				////						else
				////						{
				////							b++;
				////						}
				//
				//
				////						String tipo = "tp-"+l+"-"+b;
				////					//	System.out.println(tipo);
				////
				////								Bodega bg = adicionarBodega(1000, idSucursal, 1000, tipo);
				////								Estante et = adicionarEstante(1000, idSucursal, 1000, tipo);
				//					}

			



			//							for(int m=0 ; m < 10; m++)
			//							{
			//								String cod = productos.get(i-11289).getCodigoDeBarras();
		//							ProductoSuministrado ps= adicionarProductoSuministrado((30000*idSucursal)%1000, 100, "t", cod , 50, et.getId(), bg.getId(), idSucursal);
			//								if(ps==null)
			//								{
			//									System.out.println("Que putas");
			//								}
			//
			//								//productos sumini
			//							}

			//bodegas y estnates
		
				
				System.out.println("Bodegini y estantini+----------------------------------------------------------------");
				//clientes
				for (int n = 0; n < 20; n++) 
				{
					ProductoSuministrado ps = lis.get((int)((Math.random()*10000000)%(lis.size()-1)));
					String codNuevo = ps.getCodigoDeBarras();
					Long idSucur = ps.getIdSucursal();
					System.out.println(codNuevo);
					if(n%5 == 0)
					{
						ClienteEmpresa ce = adicionarClienteEmpresa((int)((n+1)*Math.random()*100), "nit"+(n+1)+(j+1), "Kra"+(20-n)*(j+1)+"#"+n);
						for (int j2 = 0; j2 < 10 ; j2++)
						{
							TransaccionCliente tc = adicionarTransaccionCliente((j2+1)*(n+1)*Math.random()*100, new Timestamp(2018, ((j2+1)*(n+1))%12, ((j2+1)*(n+1))%28, 0, 0, 0, 0), ce.getId());
							adicionarCompra( codNuevo, tc.getId(), ((j2*100)%50)+1, idSucur);
						}
					}
					else
					{
						ClienteNatural cn = adicionarClienteNatural((int)((n+1)*Math.random()*100), "ced"+(n+1)+"-"+(j+1), "Anita"+(n+2)+".P", "pb"+(n*j)+n+"-"+j+"@gmail.com");
						for (int j2 = 0; j2 < 10 ; j2++)
						{
							TransaccionCliente tc = adicionarTransaccionCliente((j2+1)*(n+1)*Math.random()*100, new Timestamp(2018, ((j2+1)*(n+1))%12, ((j2+1)*(n+1))%28, 0, 0, 0, 0), cn.getId());
							adicionarCompra( codNuevo, tc.getId(), ((j2*100)%50)+1, idSucur);

						}
					}

				}
			}
//			i++;
//		}
		//		System.out.println("Acabo for sucursales");
		//
		//		ArrayList<String> tiposEstado = new ArrayList<>();
		//		tiposEstado.add("EN_PROCESO");
		//		tiposEstado.add("CONSOLIDADA");
		//		tiposEstado.add("EN_CAMINO");
		//		tiposEstado.add("ENTREGADA");
		//		tiposEstado.add("ENTREGADA_TARDE");
		//		
		//		//orden pedido e info producto
		//		for (int r = 0; r < 10000; r++) 
		//		{
		//			int mes = (r+1)%12;
		//			int dia = (r+1)%28;
		//			Timestamp f1 = new Timestamp(2018, mes , dia , 0, 0, 0, 0);
		//			String estado = tiposEstado.get((int)((Math.random()*100)%5));
		//			Timestamp f2 = f1;
		//			if(estado.equals("ENTREGADA_TARDE"))
		//			{
		//				mes = mes==12?1:mes++;
		//				dia = dia==29?1:dia++;
		//				f2 =  new Timestamp(2018, mes, dia, 0, 0, 0, 0);
		//			}
		//			OrdenPedido op = adicionarOrdenPedido(f1, estado , 4.0, "prov"+(((Math.random())*100)%20)+(((Math.random())*100)%100)+(((Math.random())*100)%10)+(((Math.random())*100)%10), (long)(Math.random()*80000)%1000, f2);
		//			adicionarInfoProducto(700000, 17, "", (Math.random()*100)%6, op.getId(),   "cod"+"-"+(int)(((Math.random())*100)%100)+"-"+(int)(((Math.random())*100)%10)+"-"+(int)(((Math.random())*100)%10););
		//
		//		}
		//		System.out.println("Acabo for orden pedido");
		//		//promociones
		//		for (int j = 0; j < 100; j++) 
		//		{
		//			int mes = (j+1)%12;
		//			int dia = (j+1)%28;
		//			Timestamp f1 = new Timestamp(2018, mes , dia , 0, 0, 0, 0);
		//			adicionarPromocionDescuento(f1, f1,  "cod"+"-"+(int)(((Math.random())*100)%100)+"-"+(int)(((Math.random())*100)%10)+"-"+(int)(((Math.random())*100)%10);, null, 0, 2, "Prom"+j, (Math.random()*150)%99);
		//		}
		//		for (int j = 0; j < 100; j++) 
		//		{
		//			int mes = (j+1)%12;
		//			int dia = (j+1)%28;
		//			Timestamp f1 = new Timestamp(2018, mes , dia , 0, 0, 0, 0);
		//			adicionarPromocionDosProductos(f1, f1,  "cod"+"-"+(int)(((Math.random())*100)%100)+"-"+(int)(((Math.random())*100)%10)+"-"+(int)(((Math.random())*100)%10);,  "cod"+"-"+(int)(((Math.random())*100)%100)+"-"+(int)(((Math.random())*100)%10)+"-"+(int)(((Math.random())*100)%10); , 0, 5, "Promo"+j, 1000);
		//		}
		//		for (int j = 0; j < 100; j++) 
		//		{
		//			int mes = (j+1)%12;
		//			int dia = (j+1)%28;
		//			Timestamp f1 = new Timestamp(2018, mes , dia , 0, 0, 0, 0);
		//			adicionarPromocionPague1LleveElSegundo(f1, f1,  "cod"+"-"+(int)(((Math.random())*100)%100)+"-"+(int)(((Math.random())*100)%10)+"-"+(int)(((Math.random())*100)%10);, null, 0, 4, "Promy"+j, (Math.random()*150)%99);
		//		}
		//		for (int j = 0; j < 100; j++) 
		//		{
		//			int mes = (j+1)%12;
		//			int dia = (j+1)%28;
		//			Timestamp f1 = new Timestamp(2018, mes , dia , 0, 0, 0, 0);
		//			adicionarPromocionPagueNLleveMUnidades(f1, f1,  "cod"+"-"+(int)(((Math.random())*100)%100)+"-"+(int)(((Math.random())*100)%10)+"-"+(int)(((Math.random())*100)%10);, null, 0, 1, "Pro"+j, 1, 2);
		//		}
		//		System.out.println("Acabo for promo");
		long mili2 = System.currentTimeMillis();


		System.out.println("---------"+(mili2-mili)+"-----------");
	}
	public List<String> tama�oTablas()
	{
		List<String> lista = new ArrayList<String>();
		int total = 0;
		for (int i = 1; i < tablas.size(); i++) 
		{
			String sqlC = "SELECT COUNT(*) FROM " + tablas.get(i);
			PersistenceManager pm = pmf.getPersistenceManager();
			Query q2 = pm.newQuery(SQL,sqlC);
			List<BigDecimal> l = (List<BigDecimal>)q2.execute();
			total+= l.get(0).intValue();
			String temp = "El tama�o de la tabla " + tablas.get(i) + " es de " + l + " tuplas";
			lista.add(temp);
		}
		String fin = "La cantidad total de tuplas es de " + total+".";
		lista.add(fin);
		return lista;
	}

	public void cargitaMasiva()
	{

	}
}