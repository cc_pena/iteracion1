package uniandes.isis2304.superAndes.persistencia;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.isis2304.superAndes.negocio.InfoProducto;

/**
 * Clase que encapsula los métodos que hacen acceso a la base de datos para el concepto INFO PRODUCTO de SuperAndes.
 * Nótese que es una clase que es sólo conocida en el paquete de persistencia
 * 
 * @author fj.gonzalez
 */
class SQLInfoProducto 
{
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes psa;
	/* ****************************************************************
	 * 			Constructor
	 *****************************************************************/
	public SQLInfoProducto(PersistenciaSuperAndes pp) 
	{
		this.psa = pp;
	}
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Crea y ejecuta la sentencia SQL para adicionar una INFOPRODUCTO a la base de datos de SuperAndes
	 * @return EL número de tuplas insertadas
	 */
	public long adicionarInfoProducto (PersistenceManager pm, double precioUnitario,  int cantidad , String precioxUnidadMedida, double calidadProducto, long idOrden , String codigoproducto) 
	{
        Query q = pm.newQuery(SQL, "INSERT INTO " + psa.darTablaInfoProducto( ) + " (preciounitario, cantidad, preciounidadmedida, calidad , idorden, codigoproducto) values (?, ?, ?, ? , ?, ?)");
        q.setParameters( precioUnitario,  cantidad,  precioxUnidadMedida,  calidadProducto,  idOrden, codigoproducto);
        return (long)q.executeUnique();            
	}
	/**
	 * Crea y ejecuta la sentencia SQL para eliminar UN InfoProducto de la base de datos de SuperAndes, por sus identificador.
	 * @return EL número de tuplas eliminadas
	 */
	public long eliminarInfoProducto (PersistenceManager pm, long idorden, String codigoProducto) 
	{
        Query q = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaInfoProducto () + " WHERE idorden = ? AND codigoproducto = ?");
        q.setParameters(idorden, codigoProducto);
        return (long) q.executeUnique();            
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de los InfoProducto de la 
	 * base de datos de SuperAndes
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos InfoProducto
	 */
	public List<InfoProducto> darInfoProducto (PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaInfoProducto ());
		q.setResultClass(InfoProducto.class);
		return (List<InfoProducto>) q.executeList();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar las ordenes asociadas a un producto puntual junto con la info de dicho producto.
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos InfoProducto
	 */
	public List<InfoProducto> darInfoProducto (PersistenceManager pm, String codBarras)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaInfoProducto () +" WHERE codigoProducto = ? ");
		q.setResultClass(InfoProducto.class);
		q.setParameters(codBarras);
		return (List<InfoProducto>) q.executeList();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar las ordenes asociadas a un producto puntual junto con la info de dicho producto.
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos InfoProducto
	 */
	public InfoProducto darInfoProduct (PersistenceManager pm, String codBarras,long idorden)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaInfoProducto () +" WHERE codigoProducto = ? AND idorden = ? ");
		q.setResultClass(InfoProducto.class);
		q.setParameters(codBarras,idorden);
		List<InfoProducto> inf =  q.executeList();
		if(inf.size() > 0)
		{
			return (InfoProducto) inf.get(0);
		}
		else
		{
			return null;
		}
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar los productos asociados a una orden.
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos InfoProducto
	 */
	public List<InfoProducto> darProductosDeUnaOrden (PersistenceManager pm, long idOrden)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaInfoProducto () +" WHERE idorden = ? ");
		q.setParameters(idOrden);
		q.setResultClass(InfoProducto.class);
		List<InfoProducto> l =  (List<InfoProducto>) q.executeList();
		return l.size() > 0 ? l : null;
	}
	public List<InfoProducto> darListaDeProductosEntregadosYSinRegistrar(PersistenceManager pm, String cod)
	{
		String sql = "SELECT * FROM " + psa.darTablaInfoProducto () +" WHERE codigoProducto = ? AND (entregado = 'ENTREGADO A TIEMPO' OR entregado = 'ENTREGADO TARDE') ";
		Query q = pm.newQuery(SQL, sql);
		q.setParameters(cod);
		return (List<InfoProducto>)q.executeList();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para modificar la calificación de un proveedor.
	 */
	public void modificarARegistrado(PersistenceManager pm, String cod)
	{
        Query q = pm.newQuery(SQL, "UPDATE " + psa.darTablaInfoProducto()+ " SET entregado = 'REGISTRADO' WHERE codigoproducto = ?");
        q.setParameters(cod);
        q.executeUnique();
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para modificar la calificación de un proveedor.
	 */
	public void modificarCantidad(PersistenceManager pm, String cod, int cantidad)
	{
        Query q = pm.newQuery(SQL, "UPDATE " + psa.darTablaInfoProducto()+ " SET cantidad = cantidad + ?  WHERE codigoproducto = ?");
        q.setParameters(cod);
        q.executeUnique();
	}
	
	
	
	
}
