package uniandes.isis2304.superAndes.persistencia;
import java.util.List;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import uniandes.isis2304.superAndes.negocio.Proveedor;

/**
 * Clase que encapsula los métodos que hacen acceso a la base de datos para el concepto PROVEEDOR de SuperAndes.
 * Nótese que es una clase que es sólo conocida en el paquete de persistencia
 * 
 * @author fj.gonzalez
 */
class SQLProveedor 
{
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes psa;
	/* ****************************************************************
	 * 			Constructor
	 *****************************************************************/
	/**
	 * Constructor
	 * @param pp - El Manejador de persistencia de la aplicación
	 */
	public SQLProveedor (PersistenciaSuperAndes pp)
	{
		this.psa = pp;
	}
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	
	/**
	 * Crea y ejecuta la sentencia SQL para adicionar un PROVEEDOR a la base de datos de SuperAndes.
	 * @return El número de tuplas insertadas
	 */
	public long adicionarProveedor (PersistenceManager pm, String nit, String nombre, double  calificacion) 
	{
        Query q = pm.newQuery(SQL, "INSERT INTO " + psa.darTablaProveedor () + "(nit, nombre, calificacion) values (?, ?, ?)");
        q.setParameters(nit, nombre, calificacion);
        return (long) q.executeUnique();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para eliminar PROVEEDORES de la base de datos de superAndes, por su nombre.
	 * @return EL número de tuplas eliminadas
	 */
	public long eliminarProveedorPorNombre (PersistenceManager pm, String nombreProveedor)
	{
        Query q = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaProveedor () + " WHERE nombre = ?");
        q.setParameters(nombreProveedor);
        return (long) q.executeUnique();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para eliminar PROVEEDORES de la base de datos de superAndes, por su nit.
	 * @return EL número de tuplas eliminadas
	 */
	public long eliminarProveedorPorNit (PersistenceManager pm, String nit)
	{
        Query q = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaProveedor () + " WHERE nit = ?");
        q.setParameters(nit);
        return (long) q.executeUnique();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de UN PROVEEDOR de la 
	 * base de datos de SUPERANDES, por su nit
	 * @return El objeto PROVEEDOR que tiene el nit dado
	 */
	public Proveedor darProveedorPorNIT (PersistenceManager pm, String nit) 
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaProveedor () + " WHERE nit = ?");
		q.setResultClass(Proveedor.class);
		q.setParameters(nit);
		return (Proveedor) q.executeUnique();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de LOS PROVEEDORES de la 
	 * base de datos de SUPERANDES
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos Proveedor
	 */
	public List<Proveedor> darProveedores (PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaProveedor());
		q.setResultClass(Proveedor.class);
		return (List<Proveedor>) q.executeList();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para modificar la calificación de un proveedor.
	 */
	public void modificarCalificacion(PersistenceManager pm, String nit, double nuevaCalificacion)
	{
        Query q = pm.newQuery(SQL, "UPDATE " + psa.darTablaProveedor () + " SET calificacion = "+ nuevaCalificacion +"WHERE nit = ?");
        q.setParameters(nit);
        q.executeUnique();
	}
		
}
