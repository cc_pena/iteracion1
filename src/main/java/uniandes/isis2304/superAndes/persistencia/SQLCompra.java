package uniandes.isis2304.superAndes.persistencia;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.isis2304.superAndes.negocio.*;

/**
 * Clase que encapsula los métodos que hacen acceso a la base de datos para el concepto COMPRA de SuperAndes.
 * Nótese que es una clase que es sólo conocida en el paquete de persistencia
 * 
 * @author cc.pena
 */
class SQLCompra 
{
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes psa;
	/* ****************************************************************
	 * 			Constructor
	 *****************************************************************/
	public SQLCompra(PersistenciaSuperAndes pp) 
	{
		this.psa = pp;
	}
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Crea y ejecuta la sentencia SQL para adicionar una COMPRA a la base de datos de SuperAndes.
	 * @return El número de tuplas insertadas
	 */
	public long adicionarCompra (PersistenceManager pm, String codigoP, long idTransaccion, Integer cantidad) 
	{
        Query q = pm.newQuery(SQL, "INSERT INTO " + psa.darTablaCompra () + "(codigoproducto,idtransaccion,cantidadproducto) values (?, ?, ?)");
        q.setParameters(codigoP, idTransaccion, cantidad);
        return (long) q.executeUnique();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para eliminar una COMPRA de la base de datos de superAndes, por el codigo de producto y el id de la transaccion.
	 * @return EL número de tuplas eliminadas
	 */
	public long eliminarCompra (PersistenceManager pm, String codigoP, long idTransaccion)
	{
        Query q = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaCompra () + " WHERE codigoproducto = ? AND idtransaccion = ?");
        q.setParameters(codigoP, idTransaccion);
        return (long) q.executeUnique();
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para obtener una COMPRA de la base de datos de superAndes, por el codigo de producto y el id de la transaccion.
	 * @return la compra
	 */
	public Compra darCompra (PersistenceManager pm, String codigoP, long idTransaccion)
	{
        Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaCompra () + " WHERE codigoproducto = ? AND idtransaccion = ?");
        q.setParameters(codigoP, idTransaccion);
        q.setResultClass(Compra.class);
        return (Compra) q.executeUnique();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de las compras de la 
	 * base de datos de SuperAndes
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos compra
	 */
	public List<Compra> darCompras (PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaCompra());
		q.setResultClass(Compra.class);
		return (List<Compra>) q.executeList();
	}
}
