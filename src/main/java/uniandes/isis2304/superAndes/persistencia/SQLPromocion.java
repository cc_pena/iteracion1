package uniandes.isis2304.superAndes.persistencia;

import java.sql.Timestamp;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import uniandes.isis2304.superAndes.negocio.Promocion;

/**
 * Clase que encapsula los métodos que hacen acceso a la base de datos para el concepto  PROMOCIÓN de SuperAndes.
 * Nótese que es una clase que es sólo conocida en el paquete de persistencia
 * 
 * @author fj.gonzalez
 */
class SQLPromocion
{
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes psa;
	/* ****************************************************************
	 * 			Constructor
	 *****************************************************************/
	public SQLPromocion(PersistenciaSuperAndes psa) 
	{
		this.psa = psa;
	}
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Crea y ejecuta la sentencia SQL para adicionar un Promocion a la base de datos de SuperAndes
	 * @return EL número de tuplas insertadas
	 */
	public long adicionarPromocion (PersistenceManager pm, long id , Timestamp fechaexpedicion, Timestamp fechafinalizacion, Timestamp fechafinexistencias, String codigoproducto1, String codigoproducto2, Integer unidades, String nombre, long tipo,String estado) 
	{
		Query q = pm.newQuery(SQL, "INSERT INTO " + psa.darTablaPromocion() + "(id ,  fechaexpedicion,  fechafinalizacion,  fechafinexistencias,  codigoproducto1, codigoproducto2, unidades,nombre,tipo,estado) values (?, ?, ?, ?, ?, ?, ?,?, ?,?)");
		q.setParameters(id ,  fechaexpedicion,  fechafinalizacion,  fechafinexistencias,  codigoproducto1,  codigoproducto2, unidades,nombre,tipo,estado);
		return (long) q.executeUnique();
	}
	/**
	 * **
	 * Crea y ejecuta la sentencia SQL para eliminar UNA promoción de la base de datos de SuperAndes, por su identificador
	 * @param pm - El manejador de persistencia
	 * @return EL número de tuplas eliminadas
	 */
	public long[] eliminarPromocionPorId (PersistenceManager pm, long id)
	{
		Query q1 = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaPromocionDescuento() + " WHERE id = ?");
		q1.setParameters(id);
		Query q2 = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaPromocionDosProductos() + " WHERE id = ?");
		q2.setParameters(id);
		Query q3 = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaPromocionPague1LleveElSegundo() + " WHERE id = ?");
		q3.setParameters(id);
		Query q4 = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaPromocionPagueNLleveMUnidades() + " WHERE id = ?");
		q4.setParameters(id);
		Query q5 = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaPromocionPagueXLleveYCantidad() + " WHERE id = ?");
		q5.setParameters(id);

		Query q = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaPromocion () + " WHERE id = ?");
		q.setParameters(id);
		return new long[] {(long)q1.executeUnique(), (long)q2.executeUnique(), (long)q3.executeUnique(), (long)q4.executeUnique(), (long)q5.executeUnique(), (long)q.executeUnique()};            
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de las promociones de la 
	 * base de datos de SuperAndes
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos promocion
	 */
	public List<Promocion> darPromociones (PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaPromocion ());
		q.setResultClass(Promocion.class);
		return (List<Promocion>) q.executeList();
	}

	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de la promocion de la 
	 * base de datos de SuperAndes con el id dado
	 * @param pm - El manejador de persistencia
	 * @param id - id de la promocion buscada
	 * @return Una promocion
	 */
	public Promocion darPromocion(PersistenceManager pm, long id)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaPromocion ()+ " WHERE id = ? ");
		q.setParameters(id);
		q.setResultClass(Promocion.class);
		List list = q.executeList();
		
		if(list == null || list.size() == 0)
		{
			return null;
		}
		return (Promocion) list.get(0);
	}

	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de las promociones de la 
	 * base de datos de SuperAndes
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos promocion
	 */
	public List<Promocion> darPromocionesDeUnTipo (PersistenceManager pm, long tipo)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaPromocion ()+ " WHERE tipo = ?");
		q.setResultClass(Promocion.class);
		q.setParameters(tipo);
		return (List<Promocion>) q.executeList();
	}

	/**
	 * Crea y ejecuta la sentencia SQL para acabar UNA promoción de la base de datos de SuperAndes, por su identificador
	 * @param pm - El manejador de persistencia
	 * @return EL número de tuplas eliminadas
	 */
	public long[] acabarPromocion(PersistenceManager pm, Timestamp fechafin, long id)
	{
		Query q = pm.newQuery(SQL, "UPDATE " + psa.darTablaPromocion () + " SET fechafinexistencias = ? WHERE id = ?");
		q.setParameters(fechafin,id);
		Query q1 = pm.newQuery(SQL, "UPDATE " + psa.darTablaPromocion () + " SET estado = 'I' WHERE id = ?");
		q1.setParameters(id);
		
		return new long[]{ (long) q.executeUnique() , (long) q1.executeUnique() };        
	}
	
	public List<Promocion> darPromocionesDeUnProducto(PersistenceManager pm,String cod)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaPromocion ()+ " WHERE codigoproducto1 = ?");
		q.setResultClass(Promocion.class);
		q.setParameters(cod);
		return (List<Promocion>) q.executeList();
	}



}
