package uniandes.isis2304.superAndes.persistencia;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.isis2304.superAndes.negocio.Producto;
import uniandes.isis2304.superAndes.negocio.ProductoSuministrado;
/**
 * Clase que encapsula los métodos que hacen acceso a la base de datos para el concepto  PRODUCTO SUMINISTRADO  de SuperAndes.
 * Nótese que es una clase que es sólo conocida en el paquete de persistencia
 * 
 * @author fj.gonzalez
 */
 class SQLProductoSuministrado 
{
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes psa;
	/* ****************************************************************
	 * 			Constructor
	 *****************************************************************/
	public SQLProductoSuministrado(PersistenciaSuperAndes psa) 
	{
		this.psa = psa;
	}
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Crea y ejecuta la sentencia SQL para adicionar un Producto a la base de datos de SuperAndes
	 * @return EL número de tuplas insertadas
	 */
	public long adicionarProductoSuministrado (PersistenceManager pm, double precioUnitario , int cantidadenbodega , String precioporunidadmedida , String codigobarras , int cantidadenestante , long idestante , long idbodega , long idSucursal) 
	{
		Query q = pm.newQuery(SQL, "INSERT INTO " + psa.darTablaProductoSuministrado () + " (precioUnitario , cantidadenbodega ,  precioporunidadmedida ,  codigodebarras , cantidadenestante , idestante ,  idbodega , idsucursal) values (?, ?, ?, ?, ?, ?, ?, ?)");
		q.setParameters( precioUnitario ,  cantidadenbodega , precioporunidadmedida ,  codigobarras , cantidadenestante , idestante , idbodega , idSucursal);
		return (long) q.executeUnique();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para eliminar UN Producto de la base de datos de SuperAndes, por su identificador
	 * @param pm - El manejador de persistencia
	 * @param idProducto - El identificador del bebeodor
	 * @return EL número de tuplas eliminadas
	 */
	public long eliminarProductoSuministradoPorCodigoBarras (PersistenceManager pm, String cod, long idSucursal)
	{
		Query q = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaProductoSuministrado() + " WHERE codigodebarras = ? AND idsucursal = ?");
		q.setParameters(cod, idSucursal);
		return (long) q.executeUnique();            
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de LOS Productos suministrado de la 
	 * base de datos de SuperAndes
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos ProductoSuministrado
	 */
	public List<ProductoSuministrado> darProductosSuministrados (PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaProductoSuministrado());
		q.setResultClass(ProductoSuministrado.class);
		return (List<ProductoSuministrado>) q.executeList();
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de LOS Productos suministrado de la 
	 * base de datos de SuperAndes con una sucursal y tipo especificos
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos ProductoSuministrado
	 */
	public List<ProductoSuministrado> darProductosSuministradosSucursalTipo (PersistenceManager pm, long idSucursal)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaProductoSuministrado() + " WHERE idSucursal = ?");
		q.setParameters(idSucursal);
		q.setResultClass(ProductoSuministrado.class);
		return (List<ProductoSuministrado>) q.executeList();
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de UN Producto de la 
	 * base de datos de SuperAndes, por su identificador
	 * @param pm - El manejador de persistencia
	 * @return El objeto Producto que tiene el identificador dado
	 */
	public ProductoSuministrado darProductoPorCodigoBarras (PersistenceManager pm, String cod, long idSucursal) 
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaProductoSuministrado () + " WHERE codigodebarras = ? AND idsucursal = ?");
		q.setResultClass(ProductoSuministrado.class);
		q.setParameters(cod, idSucursal);
		return (ProductoSuministrado) q.executeList().get(0);
	}
	/**
	 * Crea y ejecuta la sentencia SQL para modificar la cantidad en bodega de un producto.
	 */
	public void modificarCantidadBodega(PersistenceManager pm, String cod, long  idB , int nuevaCantidadBodega, long idSucursal)
	{
        Query q = pm.newQuery(SQL, "UPDATE " + psa.darTablaProductoSuministrado() + " SET cantidadenbodega = "+ nuevaCantidadBodega +" WHERE idbodega = ? AND codigodebarras = ? AND idsucursal = ?");
        q.setParameters(idB,cod, idSucursal);
        q.executeUnique();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para modificar la cantidad en estante de un producto.
	 */
	public void modificarCantidadEstante(PersistenceManager pm, String cod, long idE, int nuevaCantidadEstante, long idSucursal )
	{
        Query q = pm.newQuery(SQL, "UPDATE " + psa.darTablaProductoSuministrado() + " SET cantidadenestante = "+ nuevaCantidadEstante +" WHERE idestante = ? AND codigodebarras = ? AND idsucursal = ?");
        q.setParameters(idE,cod, idSucursal);
       q.executeUnique();
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para modificar el precio del producto.
	 */
	public void modificarPrecio(PersistenceManager pm, String cod, double precio, long idSucursal  )
	{
        Query q = pm.newQuery(SQL, "UPDATE " + psa.darTablaProductoSuministrado() + " SET preciounitario = "+ precio +" WHERE codigodebarras = ? AND idsucursal = ?");
        q.setParameters(cod, idSucursal, idSucursal);
       q.executeUnique();
	}

}
