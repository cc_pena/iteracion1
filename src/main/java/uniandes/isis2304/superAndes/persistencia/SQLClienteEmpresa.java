package uniandes.isis2304.superAndes.persistencia;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.isis2304.superAndes.negocio.*;

/**
 * Clase que encapsula los métodos que hacen acceso a la base de datos para el concepto CLIENTEEMPRESA de SuperAndes.
 * Nótese que es una clase que es sólo conocida en el paquete de persistencia
 * 
 * @author cc.pena
 */
class SQLClienteEmpresa 
{
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes psa;
	
	/* ****************************************************************
	 * 			Constructor
	 *****************************************************************/
	public SQLClienteEmpresa(PersistenciaSuperAndes pp) 
	{
		this.psa = pp;
	}
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Crea y ejecuta la sentencia SQL para adicionar un CLIENTE EMPRESA a la base de datos de SuperAndes.
	 * @return El número de tuplas insertadas
	 */
	public long adicionarClienteEmpresa(PersistenceManager pm, long id , String nit, String direccion) 
	{
         Query q = pm.newQuery(SQL, "INSERT INTO " + psa.darTablaClienteEmpresa () + "(id,nit,direccion) values (?, ?, ?)");
         q.setParameters(id, nit, direccion);
         return (long) q.executeUnique();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para eliminar CLIENTES EMPRESA de la base de datos de superAndes, por su id.
	 * @return EL número de tuplas eliminadas
	 */
	public long eliminarClientePorId (PersistenceManager pm, long id)
	{
         Query q = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaClienteEmpresa() + " WHERE id = ?");
         q.setParameters(id);
         return (long) q.executeUnique();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para eliminar CLIENTES EMPRESA de la base de datos de superAndes, por su nit.
	 * @return EL número de tuplas eliminadas
	 */
	public long eliminarClientePorNit (PersistenceManager pm, String nit)
	{
         Query q = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaClienteEmpresa() + " WHERE nit = ?");
         q.setParameters(nit);
         return (long) q.executeUnique();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de los CLIENTES EMPRESA de la 
	 * base de datos de SUPERANDES
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos cliente empresa
	 */
	public List<ClienteEmpresa> darClientesEmpresa(PersistenceManager pm)
	{
 		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaClienteEmpresa());
 		q.setResultClass(ClienteEmpresa.class);
 		return (List<ClienteEmpresa>) q.executeList();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para obtener CLIENTES EMPRESA de la base de datos de superAndes, por su nit.
	 * @return EL número de tuplas obtenidas
	 */
	public ClienteEmpresa darClientePorNit (PersistenceManager pm, String nit)
	{
         Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaClienteEmpresa() + " WHERE nit = ?");
 		  q.setResultClass(ClienteEmpresa.class);
          q.setParameters(nit);
         return (ClienteEmpresa) q.executeUnique();
	}
}
