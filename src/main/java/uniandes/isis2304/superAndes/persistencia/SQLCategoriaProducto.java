package uniandes.isis2304.superAndes.persistencia;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.isis2304.superAndes.negocio.CategoriaProducto;
/**
 * Clase que encapsula los métodos que hacen acceso a la base de datos para el concepto CATEGORIA PRODUCTO de SuperAndes.
 * Nótese que es una clase que es sólo conocida en el paquete de persistencia
 * 
 * @author fj.gonzalez
 */
class SQLCategoriaProducto 
{
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes psa;
	/* ****************************************************************
	 * 			Constructor
	 *****************************************************************/
	public SQLCategoriaProducto(PersistenciaSuperAndes pp) 
	{
		this.psa = pp;
	}
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Crea y ejecuta la sentencia SQL para adicionar una CategoriaProducto a la base de datos de SuperAndes
	 * @return EL número de tuplas insertadas
	 */
	public long adicionarCategoriaProducto (PersistenceManager pm, String nombre ) 
	{
		Query q = pm.newQuery(SQL, "INSERT INTO " + psa.darTablaCategoriaProducto( )+ "( nombre ) values (?)");
		q.setParameters(nombre);
		return (long)q.executeUnique();            
	}
	/**
	 * Crea y ejecuta la sentencia SQL para eliminar UN CategoriaProducto de la base de datos de SuperAndes, por sus identificador.
	 * @return EL número de tuplas eliminadas
	 */
	public long eliminarCategoriaProducto (PersistenceManager pm, String nombre) 
	{
		
        Query q = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaCategoriaProducto () + " WHERE nombre = ? ");
        q.setParameters(nombre);
        return (long) q.executeUnique();            
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de los CategoriaProducto de la 
	 * base de datos de SuperAndes
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos CategoriaProducto
	 */
	public List<CategoriaProducto> darCategoriasProducto (PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaCategoriaProducto ());
		q.setResultClass(CategoriaProducto.class);
		return (List<CategoriaProducto>) q.executeList();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de UN CategoriaProducto de la 
	 * base de datos de SUPERANDES, por su nit
	 * @return El objeto CategoriaProducto que tiene el nit dado
	 */
	public CategoriaProducto darCategoriaProducto (PersistenceManager pm, String nombre) 
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaCategoriaProducto () + " WHERE nombre = ?");
		q.setResultClass(CategoriaProducto.class);
		q.setParameters(nombre);
		return (CategoriaProducto) q.executeUnique();
	}
}
