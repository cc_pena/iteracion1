package uniandes.isis2304.superAndes.persistencia;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.isis2304.superAndes.negocio.TipoProducto;
/**
 * Clase que encapsula los métodos que hacen acceso a la base de datos para el concepto TIPO PRODUCTO de SuperAndes.
 * Nótese que es una clase que es sólo conocida en el paquete de persistencia
 * 
 * @author fj.gonzalez
 */
class SQLTipoProducto 
{
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes psa;
	/* ****************************************************************
	 * 			Constructor
	 *****************************************************************/
	public SQLTipoProducto(PersistenciaSuperAndes pp) 
	{
		this.psa = pp;
	}
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/	
	/**
	 * Crea y ejecuta la sentencia SQL para adicionar una TIPOPRODUCTO a la base de datos de SuperAndes
	 * @return EL número de tuplas insertadas
	 */
	public long adicionarTipoProducto (PersistenceManager pm, String nombre , String categoria) 
	{
		Query q = pm.newQuery(SQL, "INSERT INTO " + psa.darTablaTipoProducto( )+ "( nombre , categoria) values (?, ?)");
		q.setParameters(nombre, categoria);
		return (long)q.executeUnique();            
	}
	/**
	 * Crea y ejecuta la sentencia SQL para eliminar UN TipoProducto de la base de datos de SuperAndes, por sus identificador.
	 * @return EL número de tuplas eliminadas
	 */
	public long eliminarTipoProducto (PersistenceManager pm, String nombre) 
	{
        Query q = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaTipoProducto () + " WHERE nombre = ? ");
        q.setParameters(nombre);
        return (long) q.executeUnique();            
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de los TipoProducto de la 
	 * base de datos de SuperAndes
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos TipoProducto
	 */
	public List<TipoProducto> darTiposProducto (PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaTipoProducto ());
		q.setResultClass(TipoProducto.class);
		return (List<TipoProducto>) q.execute();
	}

}
