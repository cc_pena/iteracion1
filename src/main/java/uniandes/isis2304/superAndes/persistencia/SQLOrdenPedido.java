package uniandes.isis2304.superAndes.persistencia;

import java.sql.Timestamp;
import java.util.List;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import uniandes.isis2304.superAndes.negocio.OrdenPedido;
/**
 * Clase que encapsula los métodos que hacen acceso a la base de datos para el concepto ORDEN PEDIDO de SuperAndes.
 * Nótese que es una clase que es sólo conocida en el paquete de persistencia.
 * 
 * @author fj.gonzalez
 */
class SQLOrdenPedido 
{
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes psa;
	/* ****************************************************************
	 * 			Constructor
	 *****************************************************************/
	public SQLOrdenPedido(PersistenciaSuperAndes pp) 
	{
		this.psa = pp;
	}
	/* ****************************************************************
	 * 			Método
	 *****************************************************************/
	/**
	 * Crea y ejecuta la sentencia SQL para adicionar una ORDEN PEDIDO a la base de datos de SuperAndes
	 * @return EL número de tuplas insertadas
	 */
	public long adicionarOrdenPedido (PersistenceManager pm, Timestamp fechaEntregaEstipulada, String estado, double calificacion, String nit,  long idsucursal, long id, Timestamp fechaentregareal) 
	{
        Query q = pm.newQuery(SQL, "INSERT INTO " + psa.darTablaOrdenPedido() + "(fechaentregaestipulada, estado, calificacionservicio, nitproveedor , idsucursal, id, fechaentregareal) values (?, ?, ?, ? , ?, ?, ?)");
        q.setParameters( fechaEntregaEstipulada,  estado,  calificacion,  nit,  idsucursal, id, fechaentregareal);
        return (long)q.executeUnique();            
	}
	/**
	 * Crea y ejecuta la sentencia SQL para eliminar una ORDEN PEDIDO a la base de datos de SuperAndes
	 * @return EL número de tuplas insertadas
	 */
	public long eliminarOrdenPedidoPorId (PersistenceManager pm, long id) 
	{
        Query q = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaOrdenPedido() + " WHERE id = ? ");
        q.setParameters( id);
        return (long)q.executeUnique();            
	}
	/**
	 * Crea y ejecuta la sentencia SQL para eliminar una ORDEN PEDIDO a la base de datos de SuperAndes
	 * @return EL número de tuplas insertadas
	 */
	public long eliminarOrdenesPedido (PersistenceManager pm, long idSucursal, String nitproveedor) 
	{
        Query q = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaOrdenPedido() + " WHERE idsucursal = ? AND nitproveedor = ? ");
        q.setParameters( idSucursal , nitproveedor);
        return (long)q.executeUnique();            
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de los ORDEN PEDIDO de la 
	 * base de datos de Parranderos
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos ORDEN PEDIDO
	 */
	public List<OrdenPedido> darOrdenesPedido (PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaOrdenPedido() );
		q.setResultClass(OrdenPedido.class);
		return (List<OrdenPedido>) q.executeList();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de los ORDEN PEDIDO de la 
	 * base de datos de Parranderos
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos ORDEN PEDIDO
	 */
	public List<OrdenPedido> darOrdenesPedidoEnEstado (PersistenceManager pm, String estado)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaOrdenPedido() + " WHERE estado = ?" );
		q.setResultClass(OrdenPedido.class);
		q.setParameters(estado);
		return (List<OrdenPedido>) q.executeList();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de los ORDEN PEDIDO de un OrdenPedido y sucursal en especifico
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos ORDEN PEDIDO
	 */
	public List<OrdenPedido> darOrdenesPedidoSucursalYOrdenPedidoEspecificos(PersistenceManager pm, String nitproveedor, long idSucursal)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaOrdenPedido() + " WHERE idsucursal = ? AND nitproveedor = ?");
		q.setResultClass(OrdenPedido.class);
		q.setParameters(nitproveedor, idSucursal);
		return (List<OrdenPedido>) q.execute();
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de los ORDEN PEDIDO de un OrdenPedido y sucursal en especifico
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos ORDEN PEDIDO
	 */
	public List<OrdenPedido> darOrdenesPedidoProveedorEspecifico(PersistenceManager pm, String nitproveedor)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaOrdenPedido() + " WHERE nitproveedor = ?");
		q.setResultClass(OrdenPedido.class);
		q.setParameters(nitproveedor);
		return (List<OrdenPedido>) q.executeList();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de los ORDEN PEDIDO de un OrdenPedido y sucursal en especifico
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos ORDEN PEDIDO
	 */
	public List<OrdenPedido> darOrdenesPedidoProveedorEspecificoEstadoPuntualySucur(PersistenceManager pm, String nitproveedor, String estado, long idSucursal)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaOrdenPedido() + " WHERE nitproveedor = ? AND estado = ? AND idsucursal = ?");
		q.setResultClass(OrdenPedido.class);
		q.setParameters(nitproveedor,estado,idSucursal);
		return (List<OrdenPedido>) q.executeList();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de los ORDEN PEDIDO de un OrdenPedido y sucursal en especifico
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos ORDEN PEDIDO
	 */
	public List<OrdenPedido> darOrdenesPedidoProveedorEspecificoEstadoPuntual(PersistenceManager pm, String nitproveedor, String estado)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaOrdenPedido() + " WHERE nitproveedor = ? AND estado = ? ");
		q.setResultClass(OrdenPedido.class);
		q.setParameters(nitproveedor,estado);
		return (List<OrdenPedido>) q.executeList();
	}

	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de UN OrdenPedido de la 
	 * base de datos de SuperAndes, por su identificador
	 * @param pm - El manejador de persistencia
	 * @return El objeto OrdenPedido que tiene el identificador dado
	 */
	public OrdenPedido darOrdenPedidoPorId (PersistenceManager pm, long cod) 
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaOrdenPedido() + " WHERE id = ?");
		q.setResultClass(OrdenPedido.class);
		q.setParameters(cod);
		return (OrdenPedido) q.executeUnique();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para modificar la calificación de un proveedor.
	 */
	public void modificarEstado(PersistenceManager pm, long id, String nuevoEstado)
	{
        Query q = pm.newQuery(SQL, "UPDATE " + psa.darTablaOrdenPedido () + " SET estado = ? WHERE id = ?");
        q.setParameters(nuevoEstado, id);
        q.executeUnique();
	}
	
	public List<OrdenPedido> darOrdenesSucursal(PersistenceManager pm, long idSucursal)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaOrdenPedido() + " WHERE idsucursal=?");
		q.setParameters(idSucursal);
		q.setResultClass(OrdenPedido.class);
		return (List<OrdenPedido>) q.executeList();
	}
	
	public void calificarOrden(PersistenceManager pm, int calificacion, long idSucursal)
	{
		Query q = pm.newQuery(SQL, "UPDATE " + psa.darTablaOrdenPedido () + " SET calificacionservicio = ? WHERE id = ?");
		q.setParameters(calificacion, idSucursal);
		q.executeUnique();
	}
	
	public void modificarFechaEntregaReal(PersistenceManager pm, long id)
	{
		Query q = pm.newQuery(SQL, "UPDATE " + psa.darTablaOrdenPedido () + " SET fechaentregareal = ? WHERE id = ?");
		q.setParameters(new Timestamp(System.currentTimeMillis()), id);
		q.executeUnique();
	}
}
