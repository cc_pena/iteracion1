package uniandes.isis2304.superAndes.persistencia;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.isis2304.superAndes.negocio.PromocionPagueNLleveMUnidades;

/**
 * Clase que encapsula los métodos que hacen acceso a la base de datos para el concepto PROMOCIÓN PAGUE N LLEVE M UNIDADES de SuperAndes.
 * Nótese que es una clase que es sólo conocida en el paquete de persistencia.
 * 
 * @author fj.gonzalez
 */
class SQLPromocionPagueNLleveMUnidades
{
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes psa;
	/* ****************************************************************
	 * 			Constructor
	 *****************************************************************/
	public SQLPromocionPagueNLleveMUnidades(PersistenciaSuperAndes psa) 
	{
		this.psa = psa;
	}
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Crea y ejecuta la sentencia SQL para adicionar un Promocion dos productos a la base de datos de SuperAndes
	 * @return EL número de tuplas insertadas
	 */
	public long adicionarPromocionPagueNLleveMUnidades (PersistenceManager pm, long id , int unidadespagadas, int unidadesofrecidas) 
	{
		Query q = pm.newQuery(SQL, "INSERT INTO " + psa.darTablaPromocionPagueNLleveMUnidades()+ "(id , unidadespagadas, unidadesofrecidas) values (?, ?, ?)");
		q.setParameters(id , unidadespagadas, unidadesofrecidas);
		return (long) q.executeUnique();
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de la promocion de la 
	 * base de datos de SuperAndes con el id dado
	 * @param pm - El manejador de persistencia
	 * @param id - id de la promocion buscada
	 * @return Una promocion
	 */
	public PromocionPagueNLleveMUnidades darPromocionPagueNLleveMUnidades(PersistenceManager pm, long id)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaPromocionPagueNLleveMUnidades()+ " WHERE id = ?");
		q.setResultClass(PromocionPagueNLleveMUnidades.class);
		q.setParameters(id);
		return (PromocionPagueNLleveMUnidades) q.executeList().get(0);
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de las promociones de la 
	 * base de datos de SuperAndes
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos promocion
	 */
	public List<PromocionPagueNLleveMUnidades > darPromocionesPromocionPagueNLleveMUnidades (PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaPromocionPagueNLleveMUnidades() );
		q.setResultClass(PromocionPagueNLleveMUnidades .class);
		return (List<PromocionPagueNLleveMUnidades >) q.executeList();
	}
}
