package uniandes.isis2304.superAndes.persistencia;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.isis2304.superAndes.negocio.*;

/**
 * Clase que encapsula los métodos que hacen acceso a la base de datos para el concepto CLIENTENATURAL de SuperAndes.
 * Nótese que es una clase que es sólo conocida en el paquete de persistencia
 * 
 * @author cc.pena
 */
class SQLClienteNatural 
{
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes psa;

	/* ****************************************************************
	 * 			Constructor
	 *****************************************************************/
	public SQLClienteNatural(PersistenciaSuperAndes pp) 
	{
		this.psa = pp;
	}
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Crea y ejecuta la sentencia SQL para adicionar un CLIENTE NATURAL a la base de datos de SuperAndes.
	 * @return El número de tuplas insertadas
	 */
	public long adicionarClienteNatural(PersistenceManager pm, long id , String cedula, String nombre, String email) 
	{
		Query q = pm.newQuery(SQL, "INSERT INTO " + psa.darTablaClienteNatural () + "(id,cedula,nombre,email) values (?, ?, ?, ?)");
		q.setParameters(id, cedula, nombre, email);
		return (long) q.executeUnique();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para eliminar CLIENTES NATURALES de la base de datos de superAndes, por su id.
	 * @return EL número de tuplas eliminadas
	 */
	public long eliminarClientePorId (PersistenceManager pm, long id)
	{
		Query q = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaClienteNatural() + " WHERE id = ?");
		q.setParameters(id);
		return (long) q.executeUnique();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para eliminar CLIENTES NATURALES de la base de datos de superAndes, por su cedula.
	 * @return EL número de tuplas eliminadas
	 */
	public long eliminarClientePorCedula (PersistenceManager pm, String cedula)
	{
		Query q = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaClienteNatural() + " WHERE cedula = ?");
		q.setParameters(cedula);
		return (long) q.executeUnique();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de los CLIENTES NATURALES de la 
	 * base de datos de SUPERANDES
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos cliente
	 */
	public List<ClienteNatural> darClientesNaturales(PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaClienteNatural());
		q.setResultClass(ClienteNatural.class);
		return (List<ClienteNatural>) q.executeList();
	}
	/**
	 * 
	 * Crea y ejecuta la sentencia SQL para pedir el cliente con una cedula dada
	 * base de datos de SuperAndes
	 * @return El cliente con la cedula dada
	 */
	public ClienteNatural darClientePorCedula(PersistenceManager pm, String cedula) 
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaClienteNatural()+ " WHERE cedula = ?");
		q.setResultClass(ClienteNatural.class);
		q.setParameters(cedula);
		return (ClienteNatural) q.executeList().get(0);        
	}
}
