package uniandes.isis2304.superAndes.persistencia;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.isis2304.superAndes.negocio.*;

/**
 * Clase que encapsula los métodos que hacen acceso a la base de datos para el concepto CLIENTE de SuperAndes.
 * Nótese que es una clase que es sólo conocida en el paquete de persistencia
 * 
 * @author cc.pena
 */
class SQLCliente 
{
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes psa;

	/* ****************************************************************
	 * 			Constructor
	 *****************************************************************/
	public SQLCliente(PersistenciaSuperAndes pp) 
	{
		this.psa = pp;
	}
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Crea y ejecuta la sentencia SQL para adicionar un CLIENTE a la base de datos de SuperAndes.
	 * @return El número de tuplas insertadas
	 */
	public long adicionarCliente(PersistenceManager pm, long id , Integer puntos) 
	{
		Query q = pm.newQuery(SQL, "INSERT INTO " + psa.darTablaCliente () + "(id,puntos) values (?, ?)");
		q.setParameters(id, puntos);
		return (long) q.executeUnique();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para eliminar CLIENTES de la base de datos de superAndes, por su id.
	 * @return EL número de tuplas eliminadas
	 */
	public long[] eliminarClientePorId (PersistenceManager pm, long id)
	
	{
		Query q1 = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaClienteNatural() + " WHERE id = ?");
		q1.setParameters(id);
		Query q2 = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaClienteEmpresa() + " WHERE id = ?");
		q2.setParameters(id);

		Query q = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaCliente() + " WHERE id = ?");
		q.setParameters(id);
		
		return new long[] {(long) q1.executeUnique(), (long) q2.executeUnique(), (long) q.executeUnique()};
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de los CLIENTES de la 
	 * base de datos de SUPERANDES
	 * @param pm - El manejador de persistencia
	 * @param id - El identificador unico del cliente
	 * @return Una lista de objetos cliente
	 */
	public Cliente darCliente(PersistenceManager pm, long id)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaCliente() + " WHERE id = ?");
		q.setParameters(id);
		q.setResultClass(Cliente.class);
		return (Cliente) q.executeList().get(0);
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de los CLIENTES de la 
	 * base de datos de SUPERANDES
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos cliente
	 */
	public List<Cliente> darClientes(PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaCliente());
		q.setResultClass(Cliente.class);
		return (List<Cliente>) q.executeList();
	}
	
	/**
	 * 
	 * Crea y ejecuta la sentencia SQL para cambiar la cantidad de puntos de un cliente por el id en la
	 * base de datos de SuperAndes
	 * @return El número de tuplas modificadas
	 */
	public long sumarPuntos (PersistenceManager pm, long id, int nuevosPuntos) 
	{
		Query q = pm.newQuery(SQL, "UPDATE " + psa.darTablaCliente()+ " SET puntos = puntos + ? WHERE id = ?");
		q.setParameters(nuevosPuntos, id);
		return (long) q.executeUnique();            
	}
	/**
	 * Eliminar un cliente y sus transacciones.
	 * @return EL número de tuplas eliminadas
	 */
	public long[] eliminarClienteConSusTransacciones(PersistenceManager pm, long idCliente)
	{
		Query q1 = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaTransaccion() + " WHERE idcliente = ?");
		q1.setParameters(idCliente);



		long transaccionesEliminadas = (long) q1.executeUnique ();
		long clientesEliminados = eliminarClientePorId(pm, idCliente)[2];

		return new long[] {transaccionesEliminadas, clientesEliminados};
	}
	/**
	 * 
	 * Crea y ejecuta la sentencia SQL para cambiar el estado de su carrito a abanadonado base de datos de SuperAndes
	 * @return El número de tuplas modificadas
	 */
	public void abandonarCarro (PersistenceManager pm, long id) 
	{
		Query q = pm.newQuery(SQL, "UPDATE " + psa.darTablaCliente()+ " SET carroabandonado = 'V' WHERE id = ?");
		q.setParameters(id);
		q.executeUnique();    
	}
	
	/**
	 * 
	 * Crea y ejecuta la sentencia SQL para cambiar el estado de su carrito a desabanadonado base de datos de SuperAndes
	 * @return El número de tuplas modificadas
	 */
	public void desabandonarCarro (PersistenceManager pm, long id) 
	{
		Query q = pm.newQuery(SQL, "UPDATE " + psa.darTablaCliente()+ " SET carroabandonado = 'F' WHERE id = ?");
		q.setParameters(id);
		q.executeUnique();    
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de los CLIENTES de la 
	 * base de datos de SUPERANDES
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos cliente
	 */
	public List<Cliente> darClientesCarroAbandonado(PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaCliente()+ " WHERE carroabandonado = 'V' ");
		q.setResultClass(Cliente.class);
		return (List<Cliente>) q.executeList();
	}
	

}
