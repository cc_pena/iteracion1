package uniandes.isis2304.superAndes.persistencia;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.isis2304.superAndes.negocio.Bodega;

/**
 * Clase que encapsula los métodos que hacen acceso a la base de datos para el concepto BODEGA de SuperAndes.
 * Nótese que es una clase que es sólo conocida en el paquete de persistencia
 * 
 * @author fj.gonzalez
 */
//id,  capacidadVolumetrica,idSucursal, capacidadEnMasa , tipoProducto
class SQLBodega 
{
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes psa;
	/* ****************************************************************
	 * 			Constructor
	 *****************************************************************/
	public SQLBodega(PersistenciaSuperAndes pp) 
	{
		this.psa = pp;
	}
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	
	/**
	 * Crea y ejecuta la sentencia SQL para adicionar un BODEGA a la base de datos de SuperAndes.
	 * @return El número de tuplas insertadas
	 */
	public long adicionarBodega (PersistenceManager pm, long id, Integer capacidadvolumetrica, long  idSucursal, Integer capacidadenmasa , String tipoProducto,Integer cantidadvolumetrica, Integer cantidadenmasa) 
	{
        Query q = pm.newQuery(SQL, "INSERT INTO " + psa.darTablaBodega () + "(id, capacidadvolumetrica, idsucursal, capacidadenmasa, tipoproducto, cantidadenmasa, cantidadvolumetrica) values (?, ?, ?, ?, ?,?,?)");
        q.setParameters(id, capacidadvolumetrica, idSucursal, capacidadenmasa, tipoProducto,cantidadenmasa,cantidadvolumetrica);
        return (long) q.executeUnique();
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para eliminar UNA BODEGA de la base de datos de SuperAndes, por su identificador
	 * @return EL número de tuplas eliminadas
	 */
	public long eliminarBodegaPorId (PersistenceManager pm, long id)
	{
        Query q = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaBodega() + " WHERE id = ?");
        q.setParameters(id);
        return (long) q.executeUnique();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de los Bodega de la 
	 * base de datos de SuperAndes
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos Bodega
	 */
	public List<Bodega> darBodegas (PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaBodega ());
		q.setResultClass(Bodega.class);
		List<Bodega> resp = (List<Bodega>) q.executeList();
		return resp;
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de los Bodega de una sucursal en especifico.
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos Bodega
	 */
	public List<Bodega> darBodegasSucursal (PersistenceManager pm , long idSucursal)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaBodega ()+ " WHERE idSucursal = ?");
		q.setParameters(idSucursal);
		q.setResultClass(Bodega.class);
		List<Bodega> resp = (List<Bodega>) q.executeList();
		return resp;
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de UN Bodega de la 
	 * base de datos de SuperAndes, por su identificador
	 * @param pm - El manejador de persistencia
	 * @return El objeto Bodega que tiene el identificador dado
	 */
	public Bodega darBodega (PersistenceManager pm, long id) 
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaBodega () + " WHERE id = ?");
		q.setResultClass(Bodega.class);
		q.setParameters(id);
		return (Bodega) q.executeList().get(0);
	}
	/**
	 * Crea y ejecuta la sentencia SQL para modificar la cantidad volumetrica de una bodega.
	 */
	public void modificarCantidadVolumetrica(PersistenceManager pm, long id, int aditivoVolum)
	{
        Query q = pm.newQuery(SQL, "UPDATE " + psa.darTablaProveedor () + " SET cantidadvolumetrica = cantidadvolumetrica +"+ aditivoVolum +"WHERE id = ?");
        q.setParameters(id);
        q.executeUnique();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para modificar la cantidad en masa de una bodega.
	 */
	public void modificarCantidadEnMasa(PersistenceManager pm, long id, int aditivoMasa)
	{
        Query q = pm.newQuery(SQL, "UPDATE " + psa.darTablaProveedor () + " SET cantidadenmasa = cantidadenmasa +"+ aditivoMasa +"WHERE id = ?");
        q.setParameters(id);
        q.executeUnique();
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de UN Bodega de la 
	 * base de datos de SuperAndes, por su identificador
	 * @param pm - El manejador de persistencia
	 * @return El objeto Bodega que tiene el identificador dado
	 */
	public List<Bodega> darBodegaPreExistente (PersistenceManager pm, String tipopro, long idSucursal) 
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaBodega () + " WHERE tipoproducto = ? AND idsucursal = ?");
		q.setResultClass(Bodega.class);
		q.setParameters(tipopro,idSucursal);
		return (List<Bodega>) q.executeList();
	}
}
