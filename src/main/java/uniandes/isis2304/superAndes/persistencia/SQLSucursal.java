package uniandes.isis2304.superAndes.persistencia;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.isis2304.superAndes.negocio.*;

/**
 * Clase que encapsula los métodos que hacen acceso a la base de datos para el concepto SUCURSAL de SuperAndes.
 * Nótese que es una clase que es sólo conocida en el paquete de persistencia
 * 
 * @author fj.gonzalez
 */
class SQLSucursal 
{
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaSuperAndes.SQL;
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaSuperAndes psa;
	/* ****************************************************************
	 * 			Constructor
	 *****************************************************************/
	public SQLSucursal(PersistenciaSuperAndes pp) 
	{
		this.psa = pp;
	}
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * Crea y ejecuta la sentencia SQL para adicionar un SUCURSAL a la base de datos de SuperAndes.
	 * @return El número de tuplas insertadas
	 */
	public long adicionarSucursal (PersistenceManager pm, long id , String ciudad , String direccion , String nombre , int nivelReOrden , int nivelAbastecimiento , String nombreSupermercado) 
	{
		Query q = pm.newQuery(SQL, "INSERT INTO " + psa.darTablaSucursal () + "(ciudad, direccion, nombre, nivelreorden, nombresupermercado, id , nivelabastecimiento) values (?, ?, ?, ?, ?, ?, ?)");
		q.setParameters(ciudad, direccion, nombre, nivelReOrden, nombreSupermercado , id , nivelAbastecimiento);
		return (long) q.executeUnique();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para eliminar SUCURSAL de la base de datos de superAndes, por su nombre.
	 * @return EL número de tuplas eliminadas
	 */
	public long eliminarSucursalPorNombre (PersistenceManager pm, String nombreSucursal)
	{
		Query q = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaSucursal () + " WHERE nombre = ?");
		q.setParameters(nombreSucursal);
		return (long) q.executeUnique();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para eliminar SucursalES de la base de datos de superAndes, por su nit.
	 * @return EL número de tuplas eliminadas
	 */
	public long eliminarSucursalPorId (PersistenceManager pm, long id)
	{
		Query q = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaSucursal() + " WHERE id = ?");
		q.setParameters(id);
		return (long) q.executeUnique();
	}

	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de UN Sucursal de la 
	 * base de datos de SUPERANDES, por su nit
	 * @return El objeto Sucursal que tiene el nit dado
	 */
	public Sucursal darSucursalPorId (PersistenceManager pm, long id) 
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaSucursal() + " WHERE id = ?");
		q.setResultClass(Sucursal.class);
		q.setParameters(id);
		return (Sucursal) q.executeUnique();
	}
	
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de UN Sucursal de la 
	 * base de datos de SUPERANDES, por su nit
	 * @return El objeto Sucursal que tiene el nit dado
	 */
	public Sucursal darSucursalPorNombreYSuperMercado (PersistenceManager pm, String nombre, String supermercado, String ciudad) 
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaSucursal() + " WHERE nombre = ? AND nombresupermercado = ? AND ciudad = ?");
		q.setResultClass(Sucursal.class);
		q.setParameters(nombre, supermercado,ciudad);
		return (Sucursal) q.executeUnique();
	}
	/**
	 * Crea y ejecuta la sentencia SQL para encontrar la información de LAS SUCURSALES de la 
	 * base de datos de SUPERANDES
	 * @param pm - El manejador de persistencia
	 * @return Una lista de objetos sucursal
	 */
	public List<Sucursal> darSucursales (PersistenceManager pm)
	{
		Query q = pm.newQuery(SQL, "SELECT * FROM " + psa.darTablaSucursal());
		q.setResultClass(Sucursal.class);
		return (List<Sucursal>) q.executeList();
	}
	/**
	 * 
	 * Crea y ejecuta la sentencia SQL para cambiar la nivel de reorden y nivel de abastecimiento de una sucursal en la 
	 * base de datos de SuperAndes
	 * @return El número de tuplas modificadas
	 */
	public long cambiarNivelReOrden (PersistenceManager pm, long idSucursal, int nivelReOrden) 
	{
		Query q = pm.newQuery(SQL, "UPDATE " + psa.darTablaSucursal()+ " SET nivelreorden = ? WHERE id = ?");
		q.setParameters(nivelReOrden, idSucursal);
		return (long) q.executeUnique();            
	}
	/**
	 * 
	 * Crea y ejecuta la sentencia SQL para cambiar la nivel de reorden y nivel de abastecimiento de una sucursal en la 
	 * base de datos de SuperAndes
	 * @return El número de tuplas modificadas
	 */
	public long cambiarNivelAbastecimiento (PersistenceManager pm, long idSucursal, int nivelAbast) 
	{
		Query q = pm.newQuery(SQL, "UPDATE " + psa.darTablaSucursal()+ " SET nivelabastecimiento = ? WHERE id = ?");
		q.setParameters(nivelAbast, idSucursal);
		return (long) q.executeUnique();            
	}
	/**
	 * Crea y ejecuta la sentencia SQL para:
	 * @return Una pareja de números [número de sucursales eliminadas, número de estantes eliminados, número de bodegas eliminadas]
	 */
	public long [] eliminarSucursalCompleto(PersistenceManager pm, long idSucursal)
	{
		Query q1 = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaBodega() + " WHERE idsucursal = ?");
		q1.setParameters(idSucursal);
		Query q2 = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaEstante() + " WHERE idsucursal = ?");
		q2.setParameters(idSucursal);
		Query q3 = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaUsuario() + " WHERE idsucursal = ?");
		q3.setParameters(idSucursal);
		Query q4 = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaOrdenPedido() + " WHERE idsucursal = ?");
		q4.setParameters(idSucursal);
	    Query q = pm.newQuery(SQL, "DELETE FROM " + psa.darTablaSucursal () + " WHERE id = ?");
	    q.setParameters(idSucursal);
	    
	    long bodegasEliminadas = (long) q1.executeUnique ();
	    long estantesEliminados = (long) q2.executeUnique ();
	    long empleadosEliminados = (long) q3.executeUnique ();
	    long ordenesPedidoEliminadas = (long) q4.executeUnique ();
	    long sucursalEliminada = (long) q.executeUnique ();
	    
	    
	    return new long[] {bodegasEliminadas, estantesEliminados , empleadosEliminados,ordenesPedidoEliminadas, sucursalEliminada};

	}




}
