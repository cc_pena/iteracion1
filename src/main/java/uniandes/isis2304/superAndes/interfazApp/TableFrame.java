package uniandes.isis2304.superAndes.interfazApp;

import javax.swing.*;
import java.awt.*;

public class TableFrame extends JFrame
{
	private JTable table;
	
	public TableFrame(Object[][] datos, Object[] nombres, int x, int y, String titulo)
	{
		setTitle(titulo);
		setSize(300, 300);
		setLocation(x, y);
		
		table = new JTable(datos, nombres);
		
		setLayout(new BorderLayout());
		add(table.getTableHeader(), BorderLayout.PAGE_START);
		JScrollPane sc = new JScrollPane(table);
		add(sc, BorderLayout.CENTER);
	}
}
