package uniandes.isis2304.superAndes.interfazApp;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.*;

import uniandes.isis2304.superAndes.negocio.Carrito;

public class ResumenFrame extends JFrame implements ActionListener
{
	
	private JPanel panel = new JPanel();
	
	private InterfazSuperAndesApp principal;
	
	public ResumenFrame(InterfazSuperAndesApp pN)
	{
		principal = pN;
		setLocationRelativeTo( principal );
		getContentPane().setBackground(Color.CYAN);
		int ancho = setCompras();
		panel.setBackground(Color.CYAN);
		
		setSize(400, 110*ancho);
		add(panel);
		repaint();
		validate();
		revalidate();
	}
	
	public int setCompras()
	{
		List<Carrito> c = principal.darCarrito();
		if(c.isEmpty())
		{
			JLabel label = new JLabel("Su carrito esta vacio");
			label.setHorizontalAlignment(SwingConstants.CENTER);
			label.setVerticalAlignment(SwingConstants.CENTER);
			panel.add(label);
			return 1;
		}
		panel.setLayout(new GridLayout(c.size(),1));
		for (int i = 0; i < c.size(); i++) 
		{
			JPanel p = new JPanel();
			p.setOpaque(false);
			p.setLayout(new GridLayout(1,3));
			
			JLabel a = new JLabel();
			a.setHorizontalAlignment(SwingConstants.CENTER);
			a.setBorder(BorderFactory.createLineBorder(Color.RED, 2));
			a.setOpaque(false);
			a.setText(c.get(i).getIdProducto());
			p.add(a);
			
			JLabel b = new JLabel();
			b.setHorizontalAlignment(SwingConstants.CENTER);
			b.setBorder(BorderFactory.createLineBorder(Color.RED, 2));
			b.setOpaque(false);
			b.setText(""+ c.get(i).getCantidad());
			p.add(b);
			
			JButton but = new JButton();
			but.setBackground(Color.CYAN);
			but.setBorder(BorderFactory.createLineBorder(Color.RED, 2));
			but.setActionCommand(""+i);
			but.addActionListener(this);
			but.setIcon(new ImageIcon(new ImageIcon("./src/main/resources/config/delete.png").getImage().getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH)));
			p.add(but);
			
			panel.add(p);
		}
		return c.size();
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		// TODO Auto-generated method stub
		int indice = Integer.parseInt(e.getActionCommand());
		JPanel sub = (JPanel)panel.getComponent(indice);
		String codigo = ((JLabel)sub.getComponent(0)).getText();
		int cantidad = Integer.parseInt(((JLabel)sub.getComponent(1)).getText());
		
		System.out.println(codigo + "---"+cantidad);
		
		principal.eliminarDelCarrito(codigo);
		
		panel.remove(sub);
		if(panel.getComponents().length == 0)
		{
			JLabel label = new JLabel("Su carrito esta vacio");
			label.setHorizontalAlignment(SwingConstants.CENTER);
			panel.add(label);
		}
		panel.revalidate();
		panel.repaint();
	}
}
