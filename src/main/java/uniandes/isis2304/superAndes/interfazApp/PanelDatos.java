package uniandes.isis2304.superAndes.interfazApp;

import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;

/**
 * Clase de interfaz para mostrar los resultados de la ejecucion de las 
 * operaciones realizadas por el usuario
 * @author German Bravo
 */
@SuppressWarnings("serial")
public class PanelDatos extends JPanel
{
    // -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------


    // -----------------------------------------------------------------
    // Atributos de interfaz
    // -----------------------------------------------------------------
	/**
	 * area de texto con barras de deslizamiento
	 */
	private JTextArea textArea;

    // -----------------------------------------------------------------
    // Constructores
    // -----------------------------------------------------------------

    /**
     * Construye el panel
     * 
     */
    public PanelDatos ()
    {
        setBorder (new TitledBorder ("Panel de informacion"));
        setLayout( new BorderLayout( ) );
        
        textArea = new JTextArea("Aqui sale el resultado de las operaciones solicitadas");
        textArea.setEditable(false);
        add (new JScrollPane(textArea), BorderLayout.CENTER);
    }

    // -----------------------------------------------------------------
    // Metodos
    // -----------------------------------------------------------------

    /**
     * Actualiza el panel con la informacion recibida por parametro.
     * @param texto El texto con el que actualiza el area
     */
    public void actualizarInterfaz (String texto)
    {
    	textArea.setText(texto);
    }
    
    public String darTexto()
    {
    	return textArea.getText();
    }

}
