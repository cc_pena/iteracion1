
package uniandes.isis2304.superAndes.interfazApp;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.*;

import uniandes.isis2304.superAndes.negocio.Carrito;
import uniandes.isis2304.superAndes.negocio.OrdenPedido;

public class OrdenesFrame extends JFrame implements ActionListener
{
	private JPanel panel = new JPanel();
	
	private InterfazSuperAndesApp principal;
	
	public OrdenesFrame(InterfazSuperAndesApp pN){
		principal = pN;
		setLocationRelativeTo( principal );
		getContentPane().setBackground(Color.RED);
		int ancho = setOrdenes();
		panel.setBackground(Color.RED);
		
		setSize(400, 110*ancho);
		add(panel);
		repaint();
		validate();
		revalidate();
	}
	
	public int setOrdenes()
	{
		List<OrdenPedido> c = principal.darOrdenes();
		int resta = 0;
		if(c.isEmpty())
		{
			JLabel label = new JLabel("Usted no tiene ordenes");
			label.setHorizontalAlignment(SwingConstants.CENTER);
			label.setVerticalAlignment(SwingConstants.CENTER);
			panel.add(label);
			return 1;
		}
		
		for (int i = 0; i < c.size(); i++) 
		{
			if(c.get(i).getEstado().contains("EN_PROCESO"))
			{
				resta++;
				continue;
			}
			
			JPanel p = new JPanel();
			p.setLayout(new GridLayout(1,3));
			
			JLabel a = new JLabel();
			a.setHorizontalAlignment(SwingConstants.CENTER);
			if(c.get(i).getEstado().contains("ENTREGADO"))
			{
				p.setBackground(Color.GREEN);
			}
			if(c.get(i).getEstado().contains("EN_CAMINO"))
			{
				p.setBackground(Color.YELLOW);
			}
			if(c.get(i).getEstado().contains("CONSOLIDADO"))
			{
				p.setBackground(Color.BLUE);
			}
			else
			{
				a.setOpaque(false);
			}
			a.setText(c.get(i).getId()+"");
			p.add(a);
			
			JLabel b = new JLabel();
			b.setHorizontalAlignment(SwingConstants.CENTER);
			b.setOpaque(false);
			b.setText(""+ c.get(i).getEstado());
			p.add(b);
			
			if(!c.get(i).getEstado().equals("CONSOLIDADA"))
			{
				JLabel l = new JLabel("Ya se consolido!");
				l.setOpaque(false);
				l.setHorizontalAlignment(SwingConstants.CENTER);
				p.add(l);
				panel.add(p);
				continue;
			}
			JButton but = new JButton();
			but.setBackground(Color.CYAN);
			but.setActionCommand(""+i);
			but.addActionListener(this);
			but.setIcon(new ImageIcon(new ImageIcon("./src/main/resources/config/check.png").getImage().getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH)));
			p.add(but);
			panel.add(p);
			
		}
		panel.setLayout(new GridLayout(c.size()-resta,1));
		return c.size()-resta;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		int indice = Integer.parseInt(arg0.getActionCommand());
		JPanel sub = (JPanel)panel.getComponent(indice);
		Long id = Long.parseLong(((JLabel)sub.getComponent(0)).getText());
		String estado = ((JLabel)sub.getComponent(1)).getText();
		
		System.out.println(id + "---"+estado);
		
		principal.iniciarOrdenPedido(id);
		((JLabel)sub.getComponent(1)).setText("EN_CAMINO");
		((JButton)sub.getComponent(2)).setEnabled(false);
		sub.setBackground(Color.YELLOW);
		sub.setOpaque(true);
		
		panel.revalidate();
		panel.repaint();
	}
}
