package uniandes.isis2304.superAndes.interfazApp;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.*;

import uniandes.isis2304.superAndes.negocio.ProductoSuministrado;

/**
 * Clase que representa la ventana del carrito de compras de un usuario
 * @author cc.pena
 * Imagenes tomadas de:
 * 	https://odio-el-c-sharp.blogspot.com/2015/05/suma-resta-multiplicacion-division-en-c.html
 * 	https://www.vectorstock.com/royalty-free-vector/icons-check-mark-tick-and-cross-cancel-vector-16313302
 * 	https://www.freepik.es/iconos-gratis/carrito-compras_886348.htm
 */
public class CarritoFrame extends JFrame implements ActionListener, ItemListener
{
	private static final long serialVersionUID = 1L;

	private InterfazSuperAndesApp principal;

	private JComboBox<String> tipos;

	private JComboBox<String> productos;

	private JTextField cantidad;

	private JButton menos;
	public final static String MENOS = "menos";

	private JTextField tama�o;

	private JButton mas;
	public final static String MAS = "mas";

	private JButton check;
	public final static String CHECK = "check";

	private JButton delete;
	public final static String DEL = "del";

	private JButton resumen;
	public final static String SUM = "sum";

	private JButton abandonar;
	public final static String ABA = "aba";

	private JButton comprar;
	public final static String BUY = "buy";

	private JPanel panelCentro;

	private int contador = 0;

	public CarritoFrame(InterfazSuperAndesApp pN)
	{
		principal = pN;

		setTitle( "Carrito" ); 

		setSize( 900, 350 );
		setResizable( true );
		setLocationRelativeTo( principal );
		getContentPane().setBackground(new Color(251, 135, 106));
		panelCentro = new JPanel();
		panelCentro.setOpaque(false);
		setLayout(new BorderLayout());
		JLabel l = new JLabel(new ImageIcon( new ImageIcon("./src/main/resources/config/carrito.png").getImage().getScaledInstance(160, 160, java.awt.Image.SCALE_SMOOTH) ));
		l.setBorder(BorderFactory.createStrokeBorder(new BasicStroke(5.0f)));
		add(l, BorderLayout.NORTH);
		panelCentro.setLayout(new GridLayout(1,1));
		panelCentro.add(a�adirProducto());
		add(panelCentro,BorderLayout.CENTER);

		JPanel panelSur = new JPanel();
		panelSur.setOpaque(false);
		panelSur.setLayout(new GridLayout(2,5));

		panelSur.add(new JLabel());		

		resumen = new JButton("Resumen");
		resumen.setActionCommand(SUM);
		resumen.addActionListener(this);
		panelSur.add(resumen);

		abandonar = new JButton("Abandonar");
		abandonar.setActionCommand(ABA);
		abandonar.addActionListener(this);
		panelSur.add(abandonar);

		comprar = new JButton("Comprar");
		comprar.setActionCommand(BUY);
		comprar.addActionListener(this);
		panelSur.add(comprar);

		panelSur.add(new JLabel());

		panelSur.add(new JLabel());
		panelSur.add(new JLabel());
		panelSur.add(new JLabel());
		panelSur.add(new JLabel());
		panelSur.add(new JLabel());
		//	panelSur.add(new JLabel());


		add(panelSur, BorderLayout.SOUTH);
	}

	public JPanel a�adirProducto()
	{
		JPanel ret = new JPanel();
		ret.setOpaque(false);

		tipos = new JComboBox<String>();
		tipos.addItem("Seleccione un tipo");
		tipos.setEnabled(true);
		ArrayList<String> arr = principal.darTipos();
		for (int i = 0; i < arr.size(); i++) 
		{
			tipos.addItem(arr.get(i));
		}
		tipos.addItemListener(this);
		ret.add(tipos);

		productos = new JComboBox<String>();
		productos.setEnabled(false);
		productos.addItem("Seleccione un producto");
		productos.addItemListener(this);
		ret.add(productos);

		cantidad = new JTextField("Aun no hay producto");
		cantidad.setEnabled(false);
		cantidad.setOpaque(false);
		ret.add(cantidad);

		menos = new JButton();
		menos.setBorder(null);
		menos.setActionCommand(MENOS);
		menos.addActionListener(this);
		menos.setEnabled(false);
		menos.setIcon(new ImageIcon(new ImageIcon("./src/main/resources/config/minus.png").getImage().getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH)));
		ret.add(menos);

		tama�o = new JTextField(tamanio());
		tama�o.setEnabled(false);
		ret.add(tama�o);

		mas = new JButton();
		mas.setBorder(null);
		mas.setActionCommand(MAS);
		mas.addActionListener(this);
		mas.setEnabled(false);
		mas.setIcon(new ImageIcon(new ImageIcon("./src/main/resources/config/plus.png").getImage().getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH)));
		ret.add(mas);

		check = new JButton();
		check.setBorder(null);
		check.setActionCommand(CHECK);
		check.addActionListener(this);
		check.setEnabled(false);
		check.setIcon(new ImageIcon(new ImageIcon("./src/main/resources/config/check.png").getImage().getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH)));
		ret.add(check);

		delete = new JButton();
		delete.setBorder(null);
		delete.setActionCommand(DEL);
		delete.addActionListener(this);
		delete.setEnabled(false);
		delete.setIcon(new ImageIcon(new ImageIcon("./src/main/resources/config/delete.png").getImage().getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH)));
		ret.add(delete);

		return ret;
	}
	
	public void confirmar()
	{
		principal.agregarAlCarrito( ((String)productos.getSelectedItem()).split(" // ")[1] , contador);
	}
	
	public String tamanio()
	{
		return "  " + contador + " ";
	}

	/**
	 * Manejo de los eventos del comboBox
	 * @param pEvento Acci�n que gener� el evento. pEvento != null.
	 */
	@Override
	public void itemStateChanged( ItemEvent pEvento )
	{
		if( pEvento.getSource( ) == tipos)
		{

			if( pEvento.getStateChange( ) == ItemEvent.SELECTED )
			{

				if( ((String)tipos.getSelectedItem()).equals("Seleccione un tipo") )
				{
					productos.removeAllItems();
					productos.addItem("Seleccione un producto");
					productos.setEnabled(false);
				}
				else
				{
					String tipo = (String)tipos.getSelectedItem( );
					ArrayList<String> arr = principal.darProductosTipo(tipo);
					for (int j = 0; j < arr.size(); j++) 
					{
						productos.addItem(arr.get(j));
					}
					productos.setEnabled(true);
				}
			}
			return;
		}
		if( pEvento.getSource( ) == productos )
		{
			if( pEvento.getStateChange( ) == ItemEvent.SELECTED )
			{
				String rta = (String) productos.getSelectedItem();
				if(rta.equals("Seleccione un producto"))
				{
					limpiar();
				}
				else
				{
					ProductoSuministrado producto = principal.darProductoPorCodigo(rta.split(" // ")[1]);
					cantidad.setText("Hay "+producto.getCantidadEnEstante()+" en estante");
					if(producto.getCantidadEnEstante()<20)
					{
						cantidad.setDisabledTextColor(Color.RED);
					}
					else
					{
						cantidad.setDisabledTextColor(Color.GREEN);
					}
					menos.setEnabled(true);
					mas.setEnabled(true);
					check.setEnabled(true);
					delete.setEnabled(true);
				}
			}
			return;
		}
	}
	
	public void limpiar()
	{
		menos.setEnabled(false);
		mas.setEnabled(false);
		check.setEnabled(false);
		delete.setEnabled(false);
		contador = 0;
		tama�o.setText(tamanio());
		cantidad.setText("Aun no hay producto");
		cantidad.setDisabledTextColor(Color.DARK_GRAY);
		productos.setEnabled(false);
		tipos.setSelectedIndex(0);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		String evento = e.getActionCommand();
		if(evento.equals(MAS))
		{
			contador++;
			tama�o.setText(tamanio());
		}
		if(evento.equals(MENOS))
		{
			if(contador > 0)
			{
				contador--;
			}
			tama�o.setText(tamanio());
		}
		if(evento.equals(DEL))
		{
			limpiar();
		}
		if(evento.equals(SUM))
		{
			ResumenFrame res = new ResumenFrame(principal);
			res.setVisible(true);
		}
		if(evento.equals(ABA))
		{
			principal.abandonarCarrito();
			JOptionPane.showMessageDialog(this, "Se ha abandonado el carrito");
			limpiar();
			this.dispose();
		}
		if(evento.equals(BUY))
		{
			principal.comprar();
			JOptionPane.showMessageDialog(this, "Gracias por su compra");
			this.dispose();
		}
		if(evento.equals(CHECK))
		{
			if(contador == 0)
			{
				JOptionPane.showMessageDialog(this, "La cantidad no es valida", "Super Andes", JOptionPane.INFORMATION_MESSAGE);
			}
			else
			{
				confirmar();
			}
			limpiar();
		}
	}
}
