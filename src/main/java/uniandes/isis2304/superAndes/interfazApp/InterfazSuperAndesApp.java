package uniandes.isis2304.superAndes.interfazApp;

import java.awt.*;
import java.awt.event.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.jdo.JDODataStoreException;
import javax.swing.*;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;

import uniandes.isis2304.superAndes.negocio.*;

/**
 * Clase principal de la interfaz
 * @author Cristhian Penia y Francisco Gonzalez
 */
@SuppressWarnings("serial")

public class InterfazSuperAndesApp extends JFrame implements ActionListener
{
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Logger para escribir la traza de la ejecucion
	 */
	private static Logger log = Logger.getLogger(InterfazSuperAndesApp.class.getName());

	/**
	 * Ruta al archivo de configuracion de la interfaz PARA LOS CLIENTES
	 */
	private static final String CONFIG_INTERFAZ_CLIENTES = "./src/main/resources/config/interfaceConfigAppCliente.json"; 

	/**
	 * Ruta al archivo de configuracion de la interfaz PARA LOS USUARIOS DE PERSONAL
	 */
	private static final String CONFIG_INTERFAZ_PERSONAL = "./src/main/resources/config/interfaceConfigAppUsuario.json"; 

	/**
	 * Ruta al archivo de configuracion de la interfaz PARA LOS USUARIOS DE SUPERANDES
	 */
	private static final String CONFIG_INTERFAZ_SUPERANDES = "./src/main/resources/config/interfaceConfigAppAndes.json"; 
	
	/**
	 * Ruta al archivo de configuracion de la interfaz PARA LOS USUARIOS PROVEEDORES
	 */
	private static final String CONFIG_INTERFAZ_PROVEEDOR = "./src/main/resources/config/interfaceConfigAppProveedor.json"; 


	/**
	 * Ruta al archivo de configuracion de los nombres de tablas de la base de datos
	 */
	private static final String CONFIG_TABLAS = "./src/main/resources/config/TablasBD.json"; 


	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * Objeto JSON con los nombres de las tablas de la base de datos que se quieren utilizar
	 */
	private JsonObject tableConfig;

	/**
	 * Asociacion a la clase principal del negocio.
	 */
	private SuperAndes superAndes;

	/* ****************************************************************
	 * 			Atributos de interfaz
	 *****************************************************************/
	/**
	 * Objeto JSON con la configuracion de interfaz de la app.
	 */
	private JsonObject guiConfig;

	/**
	 * Panel de despliegue de interaccion para los requerimientos
	 */
	private PanelDatos panelDatos;

	/**
	 * Menu de la aplicacion
	 */
	private JMenuBar menuBar;

	/**
	 * En caso de ser un cliente, se representa con su identificador
	 */
	private String identificacionUser;

	/**
	 * Representa el tipo de persona que esta usando la app en su ejecucion {1 = natural, 2 = empresa, 3 = UsuarioPersonal, 4 = SuperAndes}
	 */
	private int estadoAplicacion;

	/* ****************************************************************
	 * 			Metodos
	 *****************************************************************/
	/**
	 * Construye la ventana principal de la aplicacion. <br>
	 * <b>post:</b> Todos los componentes de la interfaz fueron inicializados.
	 * @throws InterruptedException 
	 */
	public InterfazSuperAndesApp( ) throws InterruptedException
	{
		tableConfig = openConfig ("Tablas BD", CONFIG_TABLAS);
		superAndes = new SuperAndes (tableConfig);

		Object[] options = {"Cliente Natural","Cliente Empleado","Usuario","SuperAndes", "Proveedor"};
		int n = JOptionPane.showOptionDialog(this,"Como desea ingresar?","SuperAndes",JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,null,options,options[2]);
		if(n == 0)
		{
			String cedula = JOptionPane.showInputDialog(this, "Digite su cedula", "SuperAndes", JOptionPane.PLAIN_MESSAGE);
			if(cedula == null || cedula.isEmpty())
			{
				JOptionPane.showMessageDialog(this, "Por favor ingrese un NIT valido", "SuperAndes", JOptionPane.PLAIN_MESSAGE);
				System.exit(0);
			}
			if(superAndes.darClienteNatural(cedula) == null)
			{
				JOptionPane.showMessageDialog(this, "Lo sentimos, su cedula no se encuentra en nuestros registros", "SuperAndes", JOptionPane.PLAIN_MESSAGE);
				System.exit(0);
			}
			else
			{
				String temp = "Ingrese Sucursal";
				List<Sucursal> sucursales = superAndes.darSucursales();
				String[] sucurs = new String[sucursales.size()+1];
				sucurs[0] = "Ingrese Sucursal";
				for (int i = 1; i < sucursales.size()+1; i++) 
				{
					sucurs[i] = "" +sucursales.get(i-1).getId();
				}
				
				JComboBox<String> c = new JComboBox<String>(sucurs);
		        c.setEditable(false);
				
				Object[] options2 = new Object[] {};
		        JOptionPane jop = new JOptionPane("Seleccione una sucursal",JOptionPane.QUESTION_MESSAGE,JOptionPane.DEFAULT_OPTION,null,options2, null);
		        
		        jop.add(c);
		        JDialog dial = new JDialog();
		        dial.getContentPane().add(jop);
		        dial.setLocationRelativeTo(null);
		        dial.pack();
		        dial.setVisible(true);
		        dial.setResizable(false);
				while(temp.equals("Ingrese Sucursal"))
				{
					Thread.sleep(200);
			        temp = (String)c.getSelectedItem();
				}
				dial.setVisible(false);
				estadoAplicacion = 1;
				identificacionUser = cedula + "///" + temp;
				guiConfig = openConfig ("Interfaz", CONFIG_INTERFAZ_CLIENTES);
			}
		}
		if(n == 1)
		{
			String nit = JOptionPane.showInputDialog(this, "Digite su NIT", "SuperAndes", JOptionPane.PLAIN_MESSAGE);
			if(nit == null || nit.isEmpty())
			{
				JOptionPane.showMessageDialog(this, "Por favor ingrese un NIT valido", "SuperAndes", JOptionPane.PLAIN_MESSAGE);
				System.exit(0);
			}
			if(superAndes.darClienteEmpresa(nit) == null)
			{
				JOptionPane.showMessageDialog(this, "Lo sentimos, su NIT no se encuentra en nuestros registros", "SuperAndes", JOptionPane.PLAIN_MESSAGE);
				System.exit(0);
			}
			else
			{
				String temp = "Ingrese Sucursal";
				List<Sucursal> sucursales = superAndes.darSucursales();
				String[] sucurs = new String[sucursales.size()+1];
				sucurs[0] = "Ingrese Sucursal";
				for (int i = 1; i < sucursales.size()+1; i++) 
				{
					sucurs[i] = "" +sucursales.get(i-1).getId();
				}
				
				JComboBox<String> c = new JComboBox<String>(sucurs);
		        c.setEditable(false);
				
				Object[] options2 = new Object[] {};
		        JOptionPane jop = new JOptionPane("Seleccione una sucursal",JOptionPane.QUESTION_MESSAGE,JOptionPane.DEFAULT_OPTION,null,options2, null);
		        
		        jop.add(c);
		        JDialog dial = new JDialog();
		        dial.getContentPane().add(jop);
		        dial.setLocationRelativeTo(null);
		        dial.pack();
		        dial.setVisible(true);
		        dial.setResizable(false);
				while(temp.equals("Ingrese Sucursal"))
				{
					Thread.sleep(200);
			        temp = (String)c.getSelectedItem();
				}
				dial.setVisible(false);
				estadoAplicacion = 2;
				identificacionUser = nit + "///" + temp;
				guiConfig = openConfig ("Interfaz", CONFIG_INTERFAZ_CLIENTES);
			}
		}
		if(n == 2)
		{
			String user;
			while(true)
			{
				user = JOptionPane.showInputDialog(this, "Digite su usuario", "SuperAndes", JOptionPane.PLAIN_MESSAGE);
				if(user == null || user.isEmpty()) JOptionPane.showMessageDialog(this, "Por favor ingrese un usuario valido");
				else break;
			}
			JPasswordField pwd = new JPasswordField(30);
			while(true) 
			{
				int accion = JOptionPane.showConfirmDialog(this, pwd, "Digite su contrasenia", JOptionPane.OK_CANCEL_OPTION);
				if(accion < 0 || pwd.getPassword().length == 0) {
					JOptionPane.showMessageDialog(this, "Contrasenia Invalida, por favor digite algo valido", "SuperAndes", JOptionPane.PLAIN_MESSAGE);
				}
				else break;
			}
			String password = new String(pwd.getPassword());
			if(validarUsuario(user, password) == 1) 
			{
				JOptionPane.showMessageDialog(this, "Lo sentimos, su usuario no se encuentra en nuestros registros", "SuperAndes", JOptionPane.PLAIN_MESSAGE);
				System.exit(0);
			}
			if(validarUsuario(user, password) == 2) 
			{
				JOptionPane.showMessageDialog(this, "Lo sentimos, su usuario no concuerda con la contrasenia", "SuperAndes", JOptionPane.PLAIN_MESSAGE);
				System.exit(0);
			}
			if(validarUsuario(user, password) == 3) 
			{
				estadoAplicacion = 3;
				identificacionUser = user + "///" + superAndes.darUsuario(user).getIdSucursal();
				guiConfig = openConfig ("Interfaz", CONFIG_INTERFAZ_PERSONAL);
			}
		}
		if(n == 3)
		{
			JOptionPane.showMessageDialog(this, "Esta opcion es solo para acceso administrativo", "SuperAndes", JOptionPane.WARNING_MESSAGE);
			estadoAplicacion = 4;
			identificacionUser = "";
			guiConfig = openConfig ("Interfaz", CONFIG_INTERFAZ_SUPERANDES);
		}
		if(n == 4)
		{
			String nit = JOptionPane.showInputDialog(this, "Digite su NIT", "SuperAndes", JOptionPane.PLAIN_MESSAGE);
			if(nit == null || nit.isEmpty())
			{
				JOptionPane.showMessageDialog(this, "Por favor ingrese un NIT valido", "SuperAndes", JOptionPane.PLAIN_MESSAGE);
				System.exit(0);
			}
			if(superAndes.darProveedor(nit) == null)
			{
				JOptionPane.showMessageDialog(this, "Lo sentimos, su NIT no se encuentra en nuestros registros", "SuperAndes", JOptionPane.PLAIN_MESSAGE);
				System.exit(0);
			}
			else
			{
				estadoAplicacion = 5;
				identificacionUser = nit;
				guiConfig = openConfig("Interfaz", CONFIG_INTERFAZ_PROVEEDOR);
			}
		}

		// Configura la apariencia del frame que contiene la interfaz grafica
		configurarFrame ( );
		if (guiConfig != null) 	   
		{
			crearMenu( guiConfig.getAsJsonArray("menuBar") );
		}

		String path = guiConfig.get("bannerPath").getAsString();
		panelDatos = new PanelDatos ( );
		
		setLayout (new BorderLayout());
		add (new JLabel (new ImageIcon (path)), BorderLayout.NORTH );          
		add( panelDatos, BorderLayout.CENTER );        
	}

	/* ****************************************************************
	 * 			Metodos de configuracion de la interfaz
	 *****************************************************************/
	/**
	 * Lee datos de configuracion para la aplicacio, a partir de un archivo JSON o con valores por defecto si hay errores.
	 * @param tipo - El tipo de configuracion deseada
	 * @param archConfig - Archivo Json que contiene la configuracion
	 * @return Un objeto JSON con la configuracion del tipo especificado
	 * 			NULL si hay un error en el archivo.
	 */
	private JsonObject openConfig (String tipo, String archConfig)
	{
		JsonObject config = null;
		try 
		{
			Gson gson = new Gson( );
			FileReader file = new FileReader (archConfig);
			JsonReader reader = new JsonReader ( file );
			config = gson.fromJson(reader, JsonObject.class);
			log.info ("Se encontro un archivo de configuracion valido: " + tipo);
		} 
		catch (Exception e)
		{
			//			e.printStackTrace ();
			log.info ("NO se encontro un archivo de configuracion valido");			
			JOptionPane.showMessageDialog(null, "No se encontro un archivo de configuracion de interfaz valido: " + tipo, "SuperAndes App", JOptionPane.ERROR_MESSAGE);
		}	
		return config;
	}

	/**
	 * Metodo para configurar el frame principal de la aplicacion
	 */
	private void configurarFrame(  )
	{
		int alto = 0;
		int ancho = 0;
		String titulo = "";	

		if ( guiConfig == null )
		{
			log.info ( "Se aplica configuracion por defecto" );			
			titulo = "SuperAndes APP Default";
			alto = 300;
			ancho = 500;
		}
		else
		{
			log.info ( "Se aplica configuracion indicada en el archivo de configuracion" );
			titulo = guiConfig.get("title").getAsString();
			alto= guiConfig.get("frameH").getAsInt();
			ancho = guiConfig.get("frameW").getAsInt();
		}

		setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		setLocation (50,50);
		setResizable( true );
		setBackground( Color.WHITE );

		setTitle( titulo );
		setSize ( ancho, alto);        
	}

	/**
	 * Metodo para crear el menu de la aplicacion con base em el objeto JSON leido
	 * Genera una barra de menu y los menus con sus respectivas opciones
	 * @param jsonMenu - Arreglo Json con los menùs deseados
	 */
	private void crearMenu(  JsonArray jsonMenu )
	{    	
		// Creacion de la barra de menus
		menuBar = new JMenuBar();       
		for (JsonElement men : jsonMenu)
		{
			// Creacion de cada uno de los menus
			JsonObject jom = men.getAsJsonObject(); 

			String menuTitle = jom.get("menuTitle").getAsString();        	
			JsonArray opciones = jom.getAsJsonArray("options");

			JMenu menu = new JMenu( menuTitle);

			for (JsonElement op : opciones)
			{       	
				// Creacion de cada una de las opciones del menu
				JsonObject jo = op.getAsJsonObject(); 
				String lb =   jo.get("label").getAsString();
				String event = jo.get("event").getAsString();

				JMenuItem mItem = new JMenuItem( lb );
				mItem.addActionListener( this );
				mItem.setActionCommand(event);

				menu.add(mItem);
			}       
			menuBar.add( menu );
		}        
		setJMenuBar ( menuBar );	
	}

	/* ****************************************************************
	 * 			CRUD de Clientes
	 *****************************************************************/

	public void darTransacciones()
	{
		System.out.println("aa");
		try 
		{
			if(estadoAplicacion == 1)
			{
				ClienteNatural cn = superAndes.darClienteNatural(identificacionUser.split("///")[0]);
				System.out.println(cn);
				List<TransaccionCliente> transacciones = superAndes.darTransaccionesPorCliente(cn.getId());
				String rta = "";
				for (TransaccionCliente t : transacciones) {
					rta += t.toString() + "\n";
				}
				if(rta.isEmpty()){
					rta += "No tienes ninguna transaccion con nosotros, acercate a nuestras sucursales y realiza tu primera compra!";
				}
				else{
					rta += "Gracias por preferirnos!";
				}
				panelDatos.actualizarInterfaz(rta);
			}
			else if(estadoAplicacion == 2)
			{
				ClienteEmpresa ce = superAndes.darClienteEmpresa(identificacionUser.split("///")[0]);
				List<TransaccionCliente> transacciones = superAndes.darTransaccionesPorCliente(ce.getId());
				String rta = "";
				for (TransaccionCliente t : transacciones) {
					rta += t.toString() + "\n";
				}
				if(rta.isEmpty()){
					rta += "No tienes ninguna transaccion con nosotros, acercate a nuestras sucursales y realiza tu primera compra!";
				}
				else{
					rta += "Gracias por preferirnos!";
				}
				panelDatos.actualizarInterfaz(rta);
			}
		} 
		catch (Exception e) 
		{
//			e.printStackTrace();
			// TODO: handle exception
		}
	}

	public void darInformacion()
	{
		try 
		{
			String rta = "";
			if(estadoAplicacion == 1)
			{
				ClienteNatural cn = superAndes.darClienteNatural(identificacionUser.split("///")[0]);
				 rta = "Su informacion se presenta a continuacion: \n" + cn.toString();
			}
			else if(estadoAplicacion == 2)
			{
				ClienteEmpresa ce = superAndes.darClienteEmpresa(identificacionUser.split("///")[0]);
				 rta = "Su informacion se presenta a continuacion: \n" + ce.toString();

			}
			panelDatos.actualizarInterfaz(rta);
		} 
		catch (Exception e) 
		{
			// TODO: handle exception
		}
	}
	
	public void abandonarCarrito()
	{
		if(estadoAplicacion == 1)
		{
			String cedula = identificacionUser.split("///")[0];
			ClienteNatural c = superAndes.darClienteNatural(cedula);
			superAndes.abandonarCarro(c.getId());
		}
		if(estadoAplicacion == 2)
		{
			String nit = identificacionUser.split("///")[0];
			ClienteEmpresa c = superAndes.darClienteEmpresa(nit);
			superAndes.abandonarCarro(c.getId());
		}
	}
	
	public List<Carrito> darCarrito()
	{
		if(estadoAplicacion == 1)
		{
			String cedula = identificacionUser.split("///")[0];
			ClienteNatural c = superAndes.darClienteNatural(cedula);
			return superAndes.darProductosDelCliente(c.getId());
		}
		else if(estadoAplicacion == 2)
		{
			String nit = identificacionUser.split("///")[0];
			ClienteEmpresa c = superAndes.darClienteEmpresa(nit);
			return superAndes.darProductosDelCliente(c.getId());
		}
		return null;
	}
	
	public void agregarAlCarrito(String idP, int cantidad)
	{
		if(estadoAplicacion == 1)
		{
			String cedula = identificacionUser.split("///")[0];
			ClienteNatural c = superAndes.darClienteNatural(cedula);
			superAndes.adicionarProductoAlCliente(c.getId(), idP, cantidad, Long.parseLong(identificacionUser.split("///")[1]));
		}
		else if(estadoAplicacion == 2)
		{
			String nit = identificacionUser.split("///")[0];
			ClienteEmpresa c = superAndes.darClienteEmpresa(nit);
			superAndes.adicionarProductoAlCliente(c.getId(), idP, cantidad,Long.parseLong(identificacionUser.split("///")[1]));
		}
	}
	
	public void comprar()
	{
		if(estadoAplicacion == 1)
		{
			String cedula = identificacionUser.split("///")[0];
			ClienteNatural c = superAndes.darClienteNatural(cedula);
			superAndes.pagar(c.getId(), new Timestamp(System.currentTimeMillis()), Long.parseLong(identificacionUser.split("///")[1]));
		}
		else if(estadoAplicacion == 2)
		{
			String nit = identificacionUser.split("///")[0];
			ClienteEmpresa c = superAndes.darClienteEmpresa(nit);
			superAndes.pagar(c.getId(), new Timestamp(System.currentTimeMillis()), Long.parseLong(identificacionUser.split("///")[1]));
		}
	}

	public void eliminarDelCarrito(String idP)
	{
		if(estadoAplicacion == 1)
		{
			String cedula = identificacionUser.split("///")[0];
			ClienteNatural c = superAndes.darClienteNatural(cedula);
			superAndes.eliminarProductoAlCliente(c.getId(), idP, Long.parseLong(identificacionUser.split("///")[1]));
		}
		else if(estadoAplicacion == 2)
		{
			String nit = identificacionUser.split("///")[0];
			ClienteEmpresa c = superAndes.darClienteEmpresa(nit);
			superAndes.eliminarProductoAlCliente(c.getId(), idP, Long.parseLong(identificacionUser.split("///")[1]));
		}
	}
	
	public void registrarCompra()
	{
		double monto = 0.0;
		long idCliente = 0;
		if(estadoAplicacion == 1)
		{
			ClienteNatural cn = superAndes.darClienteNatural(identificacionUser.split("///")[0]);
			idCliente = cn.getId();
		}
		else if(estadoAplicacion == 2)
		{
			ClienteEmpresa ce = superAndes.darClienteEmpresa(identificacionUser.split("///")[0]);
			idCliente = ce.getId();
		}
		
		Timestamp fecha = new Timestamp(System.currentTimeMillis());
		TransaccionCliente t = null;
		try 
		{
			t = superAndes.adicionarTransaccionCliente(0.0, fecha, idCliente);
		} 
		catch (Exception e) 
		{
			JOptionPane.showMessageDialog(this, "Algo sali� mal, lo sentimos.","SuperAndes", JOptionPane.PLAIN_MESSAGE);
			// TODO: handle exception
		}
		
		int cantidad = Integer.parseInt(JOptionPane.showInputDialog(this, "�Cuantos productos va a registrar?"));
		for(int i = 0; i < cantidad; i++)
		{
			String codigo = JOptionPane.showInputDialog(this, "Ingrese el codigoBarras producto");
			int cantidadA = Integer.parseInt(JOptionPane.showInputDialog(this, "�cuantas unidades del producto va a comprar?"));
			ProductoSuministrado temp = superAndes.darProdutoSuministrado(codigo, Long.parseLong(identificacionUser.split("///")[1]));
			
			monto += temp.getPrecioUnitario()*cantidadA;
			
			Object[] obj = superAndes.adicionarCompra(codigo, t.getId(), cantidadA, Long.parseLong(identificacionUser.split("///")[1]));
			if((boolean)obj[1] == true || (boolean)obj[3]==true)
			{
				JOptionPane.showMessageDialog(this,"Su transacci�n no fue exitosa debido a que la cantidad solicitada no est� en bodega o estante.","SuperAndes", JOptionPane.ERROR_MESSAGE);
			}
		}
		superAndes.eliminarTransaccion(t.getId());
		t.setMonto(monto);
		TransaccionCliente finalmente =superAndes.adicionarTransaccionCliente(t.getMonto(), t.getFecha(), t.getIdCliente());
		String rta = "Se finalizo la transaccion: \n";
		rta += finalmente.toString();
		panelDatos.actualizarInterfaz(rta);
	}

	public void tamanioTablas(){
		String completo = "------------------SUPERANDES----------------------\n";
		List<String> lista = superAndes.tama�oTablas();
		for (int i = 0; i < lista.size(); i++) {
			completo += lista.get(i) + "\n";
		}
		panelDatos.actualizarInterfaz(completo);
	}
	
	/* ****************************************************************
	 * 			CRUD de Usuario
	 *****************************************************************/ 
	/**
	 * Metodo para verificar la existencia de un usuario 
	 * @param user usuario a ingresar
	 * @param password contrasenia del usuario
	 * @return 1 si el usuario no existe, 2 si el password no corresponde, o 3 si concuerda la info
	 */
	public int validarUsuario(String user, String password)
	{
		UsuarioPersonal usuario = superAndes.darUsuario(user);
		if(usuario == null) return 1;
		if(usuario.getPassword().equals(password)) return 3;
		return 2;
	}

	public void recogerCarritos()
	{
		superAndes.devolverProductosDeCarrosAbandonados(Long.parseLong(identificacionUser.split("///")[1]));
	}
	
	public void registrarPromocion()
	{
		String[] options = { "Pague N lleve M unidades", "Descuento regular", "Pague X cantidad, lleve Y", "Pague 1 lleve el 2 con descuento", "Dos productos"};
		int input = JOptionPane.showOptionDialog(this,"Cual promocion desea implementar?","SuperAndes",JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,null,options,options[2]);
		try{
			String fechaInicio = JOptionPane.showInputDialog(this, "Ingrese la fecha de inicio", "dd/MM/yyyy");
			Timestamp inicio = new Timestamp(new SimpleDateFormat("dd/MM/yyyy").parse(fechaInicio).getTime());
			String fechaFin = JOptionPane.showInputDialog(this, "Ingrese la fecha de fin", "dd/MM/yyyy");
			Timestamp fin = new Timestamp(new SimpleDateFormat("dd/MM/yyyy").parse(fechaFin).getTime());
			int unidades = Integer.parseInt(JOptionPane.showInputDialog(this, "Cuantas unidades quiere que cubra la promocion?"));
			String nombre = JOptionPane.showInputDialog(this, "Como desea llamar a la promocion?");
			String codigo1 = JOptionPane.showInputDialog(this, "Cual es el producto al que quiere aplicarle la promocion?");
			if(input == 0)
			{
				input++;
				int N = Integer.parseInt(JOptionPane.showInputDialog(this, "Cuantos productos debe pagar el cliente?"));
				int M = Integer.parseInt(JOptionPane.showInputDialog(this, "Cuantos productos obtendra el cliente? Tenga en cuenta que este valor debe ser mayor."));
				PromocionPagueNLleveMUnidades prod = superAndes.adicionarPromocionPagueNLleveMUnidades(inicio, fin, codigo1, null, unidades, input, nombre, N, M);
				String rta = "Promocion agregada:\n";
				rta += prod.toString();
				panelDatos.actualizarInterfaz(rta);
			}
			else if(input == 1)
			{
				input++;
				double N = Integer.parseInt(JOptionPane.showInputDialog(this, "De cuanto es el descuento?","1%-100%"));
				PromocionDescuento prod = superAndes.adicionarPromocionDescuento(inicio, fin, codigo1, null, unidades, input, nombre, N, Long.parseLong(identificacionUser.split("///")[1]));
				String rta = "Promocion agregada:\n";
				rta += prod.toString();
				panelDatos.actualizarInterfaz(rta);
			}
			else if(input == 2)
			{
				input++;
				int X = Integer.parseInt(JOptionPane.showInputDialog(this, "Cuanta cantidad debe pagar el cliente?"));
				int Y = Integer.parseInt(JOptionPane.showInputDialog(this, "Cuanta cantidad obtendra el cliente? Tenga en cuenta que este valor debe ser mayor."));
				PromocionPagueXLleveYCantidad prod = superAndes.adicionarPromocionPagueXLleveYCantidad(inicio, fin, codigo1, null, unidades, input, nombre, X, Y);
				String rta = "Promocion agregada:\n";
				rta += prod.toString();
				panelDatos.actualizarInterfaz(rta);
			}
			else if(input == 3)
			{
				input++;
				int descuento = Integer.parseInt(JOptionPane.showInputDialog(this, "De cuanto es el descuento de la segunda unidad?", "1%-100%"));
				PromocionPague1LleveElSegundo prod = superAndes.adicionarPromocionPague1LleveElSegundo(inicio, fin, codigo1, null, unidades, input, nombre, descuento);
				String rta = "Promocion agregada:\n";
				rta += prod.toString();
				panelDatos.actualizarInterfaz(rta);
			}
			else if(input == 4)
			{
				input++;
				String codigo2 = JOptionPane.showInputDialog(this, "Cual es el segundo producto?");
				int monto = Integer.parseInt(JOptionPane.showInputDialog(this, "Cual va a ser el costo?"));
				PromocionDosProductos prod = superAndes.adicionarPromocionDosProductos(inicio, fin, codigo1, codigo2, unidades, input, nombre, monto);
				String rta = "Promocion agregada:\n";
				rta += prod.toString();
				panelDatos.actualizarInterfaz(rta);
			}
		}
		catch(Exception e)
		{
			//e.printStackTrace();
			JOptionPane.showMessageDialog(this, "Ocurrio un error en el proceso: " + e.getMessage(), "SuperAndes", JOptionPane.PLAIN_MESSAGE);
		}
	}
	
	public void mostrarCarrito()
	{
		if(estadoAplicacion == 1 && superAndes.darClienteNatural(identificacionUser.split("///")[0]).getCarroAbandonado().equals("V") )
		{
			JOptionPane.showMessageDialog(this, "Usted ha abandonado su carrito, espere a que nuestro personal lo recoga para que pueda continuar comprando!", "Super Andes", JOptionPane.INFORMATION_MESSAGE);
		}
		else
		{
			CarritoFrame frame = new CarritoFrame(this);
			frame.setVisible(true);
		}
	}

	public void finalizarPromocion()
	{
		String id = JOptionPane.showInputDialog(this, "Ingrese el identificador de la promocion");
		String fecha = JOptionPane.showInputDialog(this, "Ingrese la fecha de finalizacion", "dd/MM/yyyy");
		Timestamp fin;
		try 
		{
			fin = new Timestamp(new SimpleDateFormat("dd/MM/yyyy").parse(fecha).getTime());
		} 
		catch (ParseException e) 
		{
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(this, "Ha ingresado la fecha erroneamente","SuperAndes", JOptionPane.PLAIN_MESSAGE);
			return;
		}
		String rta = "Se eliminaron " +superAndes.acabarPromocion(fin, Long.parseLong(id)) + " registros";
		panelDatos.actualizarInterfaz(rta);
	}

	public void registrarPedido()
	{
		try 
		{
			String fechaEntrega = JOptionPane.showInputDialog(this, "Ingrese la fecha de entrega", "dd/MM/yyyy");
			Timestamp entrega = new Timestamp(new SimpleDateFormat("dd/MM/yyyy").parse(fechaEntrega).getTime());
			String[] options = {"EN CAMINO", "ENTREGADO A TIEMPO", "ENTREGADO TARDE", "REGISTRADO"};
			int input = JOptionPane.showOptionDialog(this,"En que estado desea registrarlo?","SuperAndes",JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,null,options,options[2]);
			String nit = JOptionPane.showInputDialog(this, "Quien fue el proveedor? ingrese su nit");
			int calificacion = 1;
			Timestamp llegada = entrega;
			if(input != 0){
				String fechaLlegada = JOptionPane.showInputDialog(this, "Ingrese la fecha de llegada", "dd/MM/yyyy");
				llegada = new Timestamp(new SimpleDateFormat("dd/MM/yyyy").parse(fechaLlegada).getTime());
				String[] califs = {"1","2","3","4","5"};
				calificacion = JOptionPane.showOptionDialog(this,"Como calificaria el pedido?","SuperAndes",JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,null,califs,califs[2]);
				calificacion++;
			}
			System.out.println(entrega);
			System.out.println(options[input]);
			System.out.println(calificacion);
			System.out.println(nit);
			System.out.println(Long.parseLong(identificacionUser.split("///")[1]));
			System.out.println(llegada);
			OrdenPedido orden = superAndes.adicionarOrdenPedido(entrega, options[input], calificacion, nit, Long.parseLong(identificacionUser.split("///")[1]), llegada);
			String rta = "Se agrego la orden: \n";
			rta += orden.toString();
			panelDatos.actualizarInterfaz(rta);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			// TODO: handle exception
			JOptionPane.showMessageDialog(this, "Agrego los elementos de forma erronea","SuperAndes", JOptionPane.PLAIN_MESSAGE);
		}
	}
	
	public ArrayList<String> darTipos()
	{
		ArrayList<String> arr = new ArrayList<String>();
		List<TipoProducto> ret = superAndes.darTiposProducto();
		for (int i = 0; i < ret.size(); i++) 
		{
			arr.add(ret.get(i).getNombre());
		}
		return arr;
	}
	
	public ArrayList<String> darProductosTipo(String pTipo)
	{
		ArrayList<String> arr = new ArrayList<String>();
		try 
		{
			List<ProductoSuministrado> ret = superAndes.darProductosSuministradosSucursalTipo(Long.parseLong(identificacionUser.split("///")[1]), pTipo);
			for (int i = 0; i < ret.size(); i++) 
			{
				arr.add(ret.get(i).getNombre() + " // " + ret.get(i).getCodigoDeBarras());
			}
			return arr;
		} 
		catch (Exception e) {
			List<Producto> ret = superAndes.darProductosPorTipo2(pTipo);
			// TODO: handle exception
			for (int i = 0; i < ret.size(); i++) 
			{
				arr.add(ret.get(i).getNombre() + " // " + ret.get(i).getCodigoDeBarras());
			}
			return arr;
		}
		
	}
	
	public ProductoSuministrado darProductoPorCodigo(String codigo)
	{
		return superAndes.darProdutoSuministrado(codigo, Long.parseLong(identificacionUser.split("///")[1]));
	}

	public void finalizarPedido()
	{
		try 
		{
			String idT = JOptionPane.showInputDialog(this, "Ingrese el id del pedido a finalizar");
			long id = Long.parseLong(idT);
			String fechaFinal = JOptionPane.showInputDialog(this, "Ingrese la fecha de llegada", "dd/MM/yyyy");
			Timestamp llegada = new Timestamp(new SimpleDateFormat("dd/MM/yyyy").parse(fechaFinal).getTime());
			String[] options = {"EN CAMINO", "ENTREGADO A TIEMPO", "ENTREGADO TARDE", "REGISTRADO"};
			int input = JOptionPane.showOptionDialog(this,"En que estado desea registrarlo?","SuperAndes",JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,null,options,options[2]);
			int calificacion = 0;
			String[] califs = {"1","2","3","4","5"};
			calificacion = JOptionPane.showOptionDialog(this,"Como calificaria el pedido?","SuperAndes",JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,null,califs,califs[2]);
			calificacion++;
			OrdenPedido orden = superAndes.darOrdenPedido(id);
			superAndes.eliminarOrdenPedido(id);
			OrdenPedido finalizada = superAndes.adicionarOrdenPedido(orden.getFechaEntregaEstipulada(), options[input], calificacion, orden.getNITProveedor(), orden.getIdSucursal(), llegada);
			String rta = "Se finalizo la orden: \n";
			rta += finalizada.toString();
			panelDatos.actualizarInterfaz(rta);
		}
		catch (Exception e) 
		{
			JOptionPane.showMessageDialog(this, "Agrego los elementos de forma erronea","SuperAndes", JOptionPane.PLAIN_MESSAGE);
			// TODO: handle exception
		}
	}

	public void registrarVenta()
	{
		double monto = 0.0;
		long idCliente = Long.parseLong(JOptionPane.showInputDialog(this, "Cual es el cliente que realiza la compra?"));
		String fecha = JOptionPane.showInputDialog(this, "Cual es la fecha de la compra?", "dd/MM/yyyy");
		TransaccionCliente t = null;
		try 
		{
			Timestamp date = new Timestamp(new SimpleDateFormat("dd/MM/yyyy").parse(fecha).getTime());
			t = superAndes.adicionarTransaccionCliente(0.0, date, idCliente);
		} 
		catch (Exception e) 
		{
			JOptionPane.showMessageDialog(this, "Algo sali� mal, lo sentimos.","SuperAndes", JOptionPane.PLAIN_MESSAGE);
			// TODO: handle exception
		}
		
		int cantidad = Integer.parseInt(JOptionPane.showInputDialog(this, "�Cuantos productos va a registrar?"));
		for(int i = 0; i < cantidad; i++)
		{
			String codigo = JOptionPane.showInputDialog(this, "Ingrese un producto");
			int cantidadA = Integer.parseInt(JOptionPane.showInputDialog(this, "cuantas unidades del producto va a comprar?"));
			ProductoSuministrado temp = superAndes.darProdutoSuministrado(codigo, Long.parseLong(identificacionUser.split("///")[1]));
			
			
			
			Object[] obj = superAndes.adicionarCompra(codigo, t.getId(), cantidadA, Long.parseLong(identificacionUser.split("///")[1]));
			monto += (double)obj[4];
			if((boolean)obj[1] == true || (boolean)obj[3]==true)
			{
				JOptionPane.showMessageDialog(this,"Su transacci�n no fue exitosa debido a que la cantidad solicitada no est� en bodega o estante.","SuperAndes", JOptionPane.ERROR_MESSAGE);
			}
		}
		superAndes.eliminarTransaccion(t.getId());
		t.setMonto(monto);
		TransaccionCliente finalmente =superAndes.adicionarTransaccionCliente(t.getMonto(), t.getFecha(), t.getIdCliente());
		String rta = "Se finalizo la transaccion: \n";
		rta += finalmente.toString();
		panelDatos.actualizarInterfaz(rta);
	}
	
	public void consolidarPedido()
	{
		try 
		{
			String fecha = JOptionPane.showInputDialog(this, "Cual es la fecha de entrega estipulada?", "dd/MM/yyyy");
			Timestamp date = new Timestamp(new SimpleDateFormat("dd/MM/yyyy").parse(fecha).getTime());
			superAndes.consolidarOrdenesPedido(Long.parseLong(identificacionUser.split("///")[1]), date);
			JOptionPane.showMessageDialog(this, "Se ha realizado la orden para el " + fecha,"SuperAndes", JOptionPane.PLAIN_MESSAGE);
		} 
		catch (Exception e) 
		{
			if(e.getMessage().contains("ordenes"))
			{
				JOptionPane.showMessageDialog(this, e.getMessage(),"SuperAndes", JOptionPane.PLAIN_MESSAGE);
			}
			else
			{
				JOptionPane.showMessageDialog(this, "Algo sali� mal, lo sentimos.","SuperAndes", JOptionPane.ERROR_MESSAGE);
			}
			// TODO: handle exception
		}
	}
	
	public void registrarLlegada()
	{
		try 
		{
			String nit = JOptionPane.showInputDialog(this, "Ingrese el nit del proveedor");
			int calificacion = 0;
			String[] califs = {"1","2","3","4","5"};
			calificacion = JOptionPane.showOptionDialog(this,"Como calificaria el pedido?","SuperAndes",JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,null,califs,califs[2]);
			calificacion++;
			superAndes.registrarLLegadaOrdenPedidoConsolidadaProveedor(nit, calificacion);
			JOptionPane.showMessageDialog(this, "Se ha finalizado la orden.","SuperAndes", JOptionPane.PLAIN_MESSAGE);
		} 
		catch (Exception e) 
		{
			// TODO: handle exception
		}
	}
	
	/* ****************************************************************
	 * 			ITERACION 3
	 *****************************************************************/
	public void deployIteracion3()
	{
		ListaFrame deploy = new ListaFrame(this);
	}
	
	public List<Object[]> req1Iter3(String codigo, String fechaInicio, String fechaFinal, String tipoCliente, ArrayList<String> seleccion, String ordenamiento)
	{
		return superAndes.req1Ite3(codigo, fechaInicio, fechaFinal, tipoCliente, seleccion, ordenamiento);
	}
	
	public List<Object[]> req2Iter3(String codigo, String fechaInicio, String fechaFinal, String tipoCliente, boolean seleccion, String ordenamiento)
	{
		return superAndes.req2Ite3(codigo, fechaInicio, fechaFinal, tipoCliente, seleccion, ordenamiento);
	}
	
	public List<List<Object[]>> req3Iter3()
	{
		return superAndes.req3Ite3();
	}
	
	public List<List<Object[]>> req4Iter3()
	{
		return superAndes.req4Ite3();
	}
	/* ****************************************************************
	 * 			CRUD de SuperAndes
	 *****************************************************************/

	public void registrarProveedor()
	{
		String nit = JOptionPane.showInputDialog(this, "Ingrese el Numero de Identificacion Tributaria del proveedor");
		String nombre = JOptionPane.showInputDialog(this, "Ingrese el nombre del proveedor");
		Proveedor proveedor = superAndes.adicionarProveedor(nit, nombre);
		if(proveedor == null)
		{
			JOptionPane.showMessageDialog(this, "No fue posible agregar al proveedor","SuperAndes", JOptionPane.PLAIN_MESSAGE);
			return;
		}
		String rta = "Se agrego el proveedor: \n";
		rta += proveedor.toString();
		panelDatos.actualizarInterfaz(rta);
	}

	public void registrarSucursal()
	{
		String ciudad = JOptionPane.showInputDialog(this, "Ingrese la ciudad donde se ubicara");
		String direccion = JOptionPane.showInputDialog(this, "Ingrese la direccion");
		String nombre = JOptionPane.showInputDialog(this, "Ingrese el nombre de la sucursal (Recomendamos asociarlo al barrio o localidad donde se ubica)");
		int nivelReOrden = Integer.parseInt(JOptionPane.showInputDialog(this, "Ingrese el nivel de reorden para esta sucursal"));
		int nivelAbastecimiento = Integer.parseInt(JOptionPane.showInputDialog(this, "Ingrese el nivel de abastecimiento para esta sucursal"));
		String nombreSupermercado = JOptionPane.showInputDialog(this, "Quien es el duenio de la sucursal?");
		Sucursal sucursal = superAndes.adicionarSucursal(ciudad, direccion, nombre, nivelReOrden, nivelAbastecimiento, nombreSupermercado);
		if(sucursal == null)
		{
			JOptionPane.showMessageDialog(this, "No fue posible agregar la sucursal","SuperAndes", JOptionPane.PLAIN_MESSAGE);
		}
		String rta = "Se agrego la sucursal: \n";
		rta += sucursal.toString();
		panelDatos.actualizarInterfaz(rta);
	}

	public void registrarCliente()
	{
		String[] clientes = {"Natural","Empresa"};
		int tipo = JOptionPane.showOptionDialog(this,"Cliente Natural o de Empresa?","SuperAndes",JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,null,clientes,clientes[0]);
		if(tipo == 0)
		{
			String cedula = JOptionPane.showInputDialog(this, "Cual es la cedula del cliente?");
			String nombre = JOptionPane.showInputDialog(this, "Como se llama el cliente?");
			String email = JOptionPane.showInputDialog(this, "Cual es el correo del cliente?");
			ClienteNatural c = superAndes.adicionarClienteNatural(0, cedula, nombre, email);
			if(c == null)
			{
				JOptionPane.showMessageDialog(this, "No fue posible agregar al cliente","SuperAndes", JOptionPane.PLAIN_MESSAGE);
				return;
			}
			String rta = "Se agrego el cliente: \n";
			rta += c.toString();
			panelDatos.actualizarInterfaz(rta);
		}
		else
		{
			String nit = JOptionPane.showInputDialog(this, "Cual es el NIT de la empresa asociada al cliente?");
			String direccion = JOptionPane.showInputDialog(this, "Cual es la direccion de la empresa asociada al cliente?");
			ClienteEmpresa c = superAndes.adicionarClienteEmpresa(0, nit, direccion);
			if(c == null)
			{
				JOptionPane.showMessageDialog(this, "No fue posible agregar al cliente","SuperAndes", JOptionPane.PLAIN_MESSAGE);
				return;
			}
			String rta = "Se agrego el cliente: \n";
			rta += c.toString();
			panelDatos.actualizarInterfaz(rta);
		}
	}

	public String registrarProducto()
	{
		String marca = JOptionPane.showInputDialog(this, "Cual es la marca del producto?");
		String nombre = JOptionPane.showInputDialog(this, "Cual es el nombre del producto?");
		String presentacion = JOptionPane.showInputDialog(this, "Como es la presentacion del producto a los clientes?");
		String unidaddemedida = JOptionPane.showInputDialog(this, "Como es la unidad de medida del producto?","gr/cm3");
		String volumen = JOptionPane.showInputDialog(this, "Cual es el volumen del producto?","### cm3");
		String peso = JOptionPane.showInputDialog(this, "Cual es el peso del producto?","### gr");
		String tipo = JOptionPane.showInputDialog(this, "En que tipo de producto se categorizaria el producto?");
		Integer cantidadpresentacion = Integer.parseInt(JOptionPane.showInputDialog(this, "Cual es el peso mostrado en la presentacion del producto?"));
		Producto p = superAndes.adicionarProducto(marca, nombre, presentacion, unidaddemedida, volumen, peso, tipo, cantidadpresentacion);
		if(p == null)
		{
			JOptionPane.showMessageDialog(this, "No fue posible agregar el producto","SuperAndes", JOptionPane.PLAIN_MESSAGE);
			return "";
		}
		String rta = "Se agrego el producto: \n";
		rta += p.toString();
		panelDatos.actualizarInterfaz(rta);
		return p.getCodigoDeBarras();
	}

	public void registrarBodega()
	{
		Integer capacidadvolumetrica = Integer.parseInt(JOptionPane.showInputDialog(this, "Cual es la capacidad volumetrica de la bodega?"));
		Integer capacidadenmasa = Integer.parseInt(JOptionPane.showInputDialog(this, "Cual es la capacidad en masa de la bodega?"));
		long idSucursal = Long.parseLong(JOptionPane.showInputDialog(this, "A que sucursal pertenece esta bodega?"));
		String tipoProducto = JOptionPane.showInputDialog(this, "Que tipo de producto se almacenaran en la bodega?");
		Bodega bodega = superAndes.adicionarBodega(capacidadvolumetrica, idSucursal, capacidadenmasa, tipoProducto);
		if(bodega == null)
		{
			JOptionPane.showMessageDialog(this, "No fue posible agregar la bodega","SuperAndes", JOptionPane.PLAIN_MESSAGE);
			return;
		}
		String rta = "Se agrego la bodega: \n";
		rta += bodega.toString();
		panelDatos.actualizarInterfaz(rta);
	}

	public void registrarEstante()
	{
		Integer capacidadvolumetrica = Integer.parseInt(JOptionPane.showInputDialog(this, "Cual es la capacidad volumetrica del estante?"));
		Integer capacidadenmasa = Integer.parseInt(JOptionPane.showInputDialog(this, "Cual es la capacidad en masa del estante?"));
		long idSucursal = Long.parseLong(JOptionPane.showInputDialog(this, "A que sucursal pertenece este estante?"));
		String tipoProducto = JOptionPane.showInputDialog(this, "Que tipo de producto se almacenaran en el estante?");
		Estante estante = superAndes.adicionarEstante(capacidadvolumetrica, idSucursal, capacidadenmasa, tipoProducto);
		if(estante == null)
		{
			JOptionPane.showMessageDialog(this, "No fue posible agregar el estante","SuperAndes", JOptionPane.PLAIN_MESSAGE);
			return;
		}
		String rta = "Se agrego el estante: \n";
		rta += estante.toString();
		panelDatos.actualizarInterfaz(rta);
	}
	
	/* ****************************************************************
	 * 			Programa Proveedor
	 *****************************************************************/
	
	public void registrarProductoProveedor()
	{
		String[] options = {"Producto Nuevo","Ya Existente"};
		String resultado = "";
		String cod ="";
		int tipo = JOptionPane.showOptionDialog(this,"Desea a�adir un producto nuevo o registrarse con uno ya existente?","SuperAndes",JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,null,options,options[0]);
		if(tipo == 0)
		{
			cod = registrarProducto();
			resultado += panelDatos.darTexto();
			resultado += "\n";
		}
		if(!cod.equals(""))
		{

			ProductosDelProveedor p = superAndes.adicionarProductoProveedor(cod, identificacionUser);
			resultado = "Tambien se ha agregado un producto al proveedor: \n";
			resultado += p.toString();
		}
		else
		{
			String codigoProducto = JOptionPane.showInputDialog(this, "Por favor ingrese el codigo del producto");
			ProductosDelProveedor p = superAndes.adicionarProductoProveedor(codigoProducto, identificacionUser);
			resultado = "Se ha agregado un producto al proveedor: \n";
			resultado += p.toString();

		}
		
		
		panelDatos.actualizarInterfaz(resultado);
	}
	
	public void prepararOrden()
	{
		OrdenesFrame ordenes = new OrdenesFrame(this);
		ordenes.setVisible(true);
	}
	
	public List<OrdenPedido> darOrdenes()
	{
		return superAndes.darOrdenesPedidoProveedor(identificacionUser);
	}
	
	public void iniciarOrdenPedido(long id)
	{
		superAndes.registrarSalidaOrdenPedidoConsolidada(identificacionUser);
	}

	/* ****************************************************************
	 * 			Metodos de la Interaccion
	 *****************************************************************/
	
	/**
	 * Limpia todas las tuplas de todas las tablas de la base de datos de superandes
	 * Muestra en el panel de datos el numero de tuplas eliminadas de cada tabla
	 */
	public void limpiarBD ()
	{
		
		JOptionPane.showMessageDialog(this, "Esta opcion es irreversible, proceda con precaucion");
		try 
		{
			// Ejecucion de la demo y recoleccion de los resultados
			long eliminados [] = superAndes.limpiarSuperAndes();

			// Generacion de la cadena de caracteres con la traza de la ejecucion de la demo
			String resultado = "\n\n************ Limpiando la base de datos ************ \n";
			if( eliminados.length != 0 )
			{
				resultado += "Se limpiaron " + eliminados.length + " tablas";
			}
			else
			{
				resultado += "No se pudo limpiar la base de datos";
			}
			panelDatos.actualizarInterfaz(resultado);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	/**
	 * Muestra la documentacion Javadoc del proyectp
	 */
	public void mostrarJavadoc ()
	{
		mostrarArchivo ("doc/index.html");
	}
	
	/**
	 * Muestra la documentacion del proyectp
	 */
	public void mostrarDocumento ()
	{
		mostrarArchivo ("docs/It2_C-10-cc.pena-fj.gonzalez.pdf");
	}

	/**
	 * Muestra la informacion acerca del desarrollo de esta apicacion
	 */
	public void acercaDe ()
	{
		String resultado = "Proyecto #2 Sistemas Transaccionales \n"
				+ "Cristhian Penia - 201714313"
				+ "Francisco Gonzalez - 201713245";

		panelDatos.actualizarInterfaz(resultado);
	}
	/**
	 * Abre el archivo dado como parametro con la aplicacion por defecto del sistema
	 * @param nombreArchivo - El nombre del archivo que se quiere mostrar
	 */
	private void mostrarArchivo (String nombreArchivo)
	{
		try
		{
			Desktop.getDesktop().open(new File(nombreArchivo));
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/**
	 * Metodo para la ejecucion de los eventos que enlazan el menu con los metodos de negocio
	 * Invoca al metodo correspondiente segun el evento recibido
	 * @param pEvento - El evento del usuario
	 */
	@Override
	public void actionPerformed(ActionEvent pEvento)
	{
		String evento = pEvento.getActionCommand( );		
		try 
		{
			Method req = InterfazSuperAndesApp.class.getMethod ( evento );			
			req.invoke ( this );
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		} 
	}

	public void reqC1()
	{
		String[] options = {"semana","mes","anio"};
		int tipo = JOptionPane.showOptionDialog(this,"En que unidades desea?","SuperAndes",JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,null,options,options[0]);
		String estado = JOptionPane.showInputDialog(this, "Por favor ingrese el tipo", "Super Andes", JOptionPane.INFORMATION_MESSAGE);
		List<Object[]>[] arr = null;
		if(tipo == 0)
		{
			String dia = JOptionPane.showInputDialog(this, "Ingrese el dia");
			String mes = JOptionPane.showInputDialog(this, "Ingrese el mes");
			String anio = JOptionPane.showInputDialog(this, "Ingrese el anio");
			arr = superAndes.analisisOperacionSuperAndes(estado, options[tipo], dia+"-"+mes+"-"+anio);
		}
		if(tipo == 1)
		{
			String mes = JOptionPane.showInputDialog(this, "Ingrese el mes");
			String anio = JOptionPane.showInputDialog(this, "Ingrese el anio");
			arr = superAndes.analisisOperacionSuperAndes(estado, options[tipo], mes+"-"+anio);
		}
		if(tipo == 2)
		{
			String anio = JOptionPane.showInputDialog(this, "Ingrese el anio");
			arr = superAndes.analisisOperacionSuperAndes(estado, options[tipo], anio);
		}
		
		System.out.println(arr[0].size()+" --- " + arr[1].size()+" ---- " + arr[2].size());
		
		if(tipo == 2)
		{
			String[] t1 = {"SUCURSAL","DIA","MES","DEMANDA"};
			String[][] temp = new String[arr[0].size()][4];
			for (int i = 0; i < arr[0].size(); i++) 
			{
				String a = "" + (BigDecimal)arr[0].get(i)[0];
				String b = "" + (BigDecimal)arr[0].get(i)[1];
				String c = "" + (BigDecimal)arr[0].get(i)[2];
				String d = "" + (BigDecimal)arr[0].get(i)[3];
				String[] pra = {a,b,c,d};
				temp[i] = pra;
			}
			TableFrame tabla = new TableFrame(temp, t1, 30, 30, "MAYOR DEMANDA");
			tabla.setVisible(true);
			
			String[] t2 = {"SUCURSAL","DIA","MES","DEMANDA"};
			String[][] temp2 = new String[arr[1].size()][4];
			for (int i = 0; i < arr[1].size(); i++) 
			{
				String a = "" + (BigDecimal)arr[1].get(i)[0];
				String b = "" + (BigDecimal)arr[1].get(i)[1];
				String c = "" + (BigDecimal)arr[1].get(i)[2];
				String d = "" + (BigDecimal)arr[1].get(i)[3];
				String[] pra = {a,b,c,d};
				temp2[i] = pra;
			}
			TableFrame tabla2 = new TableFrame(temp2, t2, 490, 30, "MENOR DEMANDA");
			tabla2.setVisible(true);
			
			String[] t3 = {"SUCURSAL","DIA","MES","INGRESO"};
			String[][] temp3 = new String[arr[2].size()][4];
			for (int i = 0; i < arr[2].size(); i++) 
			{
				String a = "" + (BigDecimal)arr[2].get(i)[0];
				String b = "" + (BigDecimal)arr[2].get(i)[1];
				String c = "" + (BigDecimal)arr[2].get(i)[2];
				String d = "" + (BigDecimal)arr[2].get(i)[3];
				String[] pra = {a,b,c,d};
				temp3[i] = pra;
			}
			TableFrame tabla3 = new TableFrame(temp3, t3, 920, 30, "MAYOR INGRESO");
			tabla3.setVisible(true);
		}
		
		else
		{
			String[] t1 = {"SUCURSAL","DIA","DEMANDA"};
			String[][] temp = new String[arr[0].size()][3];
			for (int i = 0; i < arr[0].size(); i++) 
			{
				String a = "" + (BigDecimal)arr[0].get(i)[0];
				String b = "" + (BigDecimal)arr[0].get(i)[1];
				String c = "" + (BigDecimal)arr[0].get(i)[2];
				String[] pra = {a,b,c};
				temp[i] = pra;
			}
			TableFrame tabla = new TableFrame(temp, t1, 30, 30, "MAYOR DEMANDA");
			tabla.setVisible(true);
			
			String[] t2 = {"SUCURSAL","DIA","DEMANDA"};
			String[][] temp2 = new String[arr[1].size()][3];
			for (int i = 0; i < arr[1].size(); i++) 
			{
				String a = "" + (BigDecimal)arr[1].get(i)[0];
				String b = "" + (BigDecimal)arr[1].get(i)[1];
				String c = "" + (BigDecimal)arr[1].get(i)[2];
				String[] pra = {a,b,c};
				temp2[i] = pra;
			}
			TableFrame tabla2 = new TableFrame(temp2, t2, 490, 30, "MENOR DEMANDA");
			tabla2.setVisible(true);
			
			String[] t3 = {"SUCURSAL","DIA","INGRESO"};
			String[][] temp3 = new String[arr[2].size()][3];
			for (int i = 0; i < arr[2].size(); i++) 
			{
				String a = "" + (BigDecimal)arr[2].get(i)[0];
				String b = "" + (BigDecimal)arr[2].get(i)[1];
				String c = "" + (BigDecimal)arr[2].get(i)[2];
				String[] pra = {a,b,c};
				temp3[i] = pra;
			}
			TableFrame tabla3 = new TableFrame(temp3, t3, 920, 30, "MAYOR INGRESO");
			tabla3.setVisible(true);
		}
		
	}
	
	public void reqC2()
	{
		List<Cliente> cf = superAndes.darClientesFrecuentes(Long.parseLong(identificacionUser.split("///")[1]));
		System.out.println(cf.size());
		String[] t = {"ID", "PUNTOS"};
		String[][] temp = new String[cf.size()][2];
		for (int i = 0; i < cf.size(); i++)
		{
			String[] arr = {""+cf.get(i).getId(), ""+cf.get(i).getPuntos()};
			temp[i] = arr;
		}
		TableFrame table = new TableFrame(temp, t, 30, 30, "CLIENTES FRECUENTES");
		table.setVisible(true);
	}
	
	public void bonoReqC3()
	{
		List<String> cf = superAndes.productosBajaDemanda();
		System.out.println(cf.size());
		String rta = "Los productos con menor demanda son:\n";
		for (int i = 0; i < cf.size(); i++) 
		{
			rta+=(i+1)+"=> " + cf.get(i) + "\n";
		}
		panelDatos.actualizarInterfaz(rta);
	}
	
	public void req1C1()
	{
		try 
		{
			String fecha1 = JOptionPane.showInputDialog(this, "Con cual fecha iniciaria el rango?", "dd/MM/yyyy");
			String fecha2 = JOptionPane.showInputDialog(this, "Con cual fecha terminaria el rango?", "dd/MM/yyyy");
			List<Object[]> lista = superAndes.ventasFrecuentes(fecha1, fecha2);
			String[] t = {"ID", "MONTO RECOLECTADO"};
			String[][] temp = new String[lista.size()][2];
			
			for (int i = 0; i < lista.size(); i++)
			{
				String[] arr = {""+(BigDecimal)lista.get(i)[0], ""+(BigDecimal)lista.get(i)[1]};
				temp[i] = arr;
			}
			TableFrame table = new TableFrame(temp, t, 30, 30, "VENTAS POR SUCURSAL");
			table.setVisible(true);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			// TODO: handle exception
		}
	}
	
	public void req1C2()
	{
		try 
		{
			List<Object[]> lista = superAndes.top20promos();
			String[] t = {"ID", "NOMBRE", "EXPEDICION", "FINALIZACION"};
			String[][] temp = new String[lista.size()][4];
			
			for (int i = 0; i < lista.size(); i++)
			{
				String[] arr = {""+(BigDecimal)lista.get(i)[0], ""+(String)lista.get(i)[1], ((Timestamp)lista.get(i)[2]).toString().substring(0, 11), ((Timestamp)lista.get(i)[3]).toString().substring(0, 11)};
				temp[i] = arr;
			}
			TableFrame table = new TableFrame(temp, t, 30, 30, "VENTAS POR SUCURSAL");
			table.setVisible(true);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			// TODO: handle exception
		}
	}
	
	
	
	public void cargaMasiva()
	{
		superAndes.cargaMasiva();
	}
	
	/* ****************************************************************
	 * 			Programa principal
	 *****************************************************************/
	/**
	 * Este metodo ejecuta la aplicacion, creando una nueva interfaz
	 * @param args Arreglo de argumentos que se recibe por linea de comandos
	 */
	public static void main( String[] args )
	{
		try
		{
			// Unifica la interfaz para Mac y para Windows.
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			InterfazSuperAndesApp interfaz = new InterfazSuperAndesApp( );
			interfaz.setVisible( true );
		}
		catch( Exception e )
		{
			e.printStackTrace( );
		}
	}
}
