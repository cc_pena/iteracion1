package uniandes.isis2304.superAndes.interfazApp;

import java.util.*;
import java.util.List;

import javax.swing.*;
import javax.swing.event.*;

import uniandes.isis2304.superAndes.negocio.ProductoSuministrado;

import java.awt.*;
import java.awt.event.*;
import java.math.BigDecimal;

public class ListaFrame
{
	private InterfazSuperAndesApp principal;

	public ListaFrame(InterfazSuperAndesApp pN) 
	{
		principal = pN;
		String[] consultas = {"1","2","3","4"};
		int consulta = JOptionPane.showOptionDialog(null,"Que consulta desea realizar?","SuperAndes",JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,null,consultas,null);
		consulta++;
		if(consulta == 1)
		{
			Req1_2 req = new Req1_2(true);
			req.setVisible(true);
		}
		if(consulta == 2)
		{
			Req1_2 req = new Req1_2(false);
			req.setVisible(true);
		}
		if(consulta == 3)
		{
			List<List<Object[]>> total = principal.req3Iter3();
			
			for (int i = 0; i < total.size(); i++) 
			{
				List<Object[]> actual = total.get(i);
				String[] header = new String[actual.get(0).length];
				String[][] matrix = new String[actual.size()][actual.get(0).length];
				if(i == 0){ header[0] = "Semana";header[1] = "NIT"; header[2] = "Maximo";}
				if(i == 1){ header[0] = "Semana";header[1] = "NIT"; header[2] = "Minimo";}
				if(i == 2){ header[0] = "Semana";header[1] = "Producto"; header[2] = "Maximo";}
				if(i == 3){ header[0] = "Semana";header[1] = "Producto"; header[2] = "Minimo";}
				for (int j = 0; j < actual.size(); j++) 
				{
					matrix[j][0] = ""+(BigDecimal)actual.get(j)[0];
					matrix[j][1] = (String)actual.get(j)[1];
					matrix[j][2] = ""+(BigDecimal)actual.get(j)[2];
				}
				System.out.println("------------" + i +"----------");
				TableFrame table = new TableFrame(matrix, header, 30, 30, "REQUERIMIENTO 3");
				table.setVisible(true);
			}
		}
		if(consulta == 4)
		{
			List<List<Object[]>> total = principal.req4Iter3();
			
			for (int i = 0; i < total.size(); i++) 
			{
				List<Object[]> actual = total.get(i);
				if(actual.isEmpty()){
					System.out.println("La tabla " + (i+1) + " esta vacia");
					continue;
				}
				String[] header = new String[actual.get(0).length];
				String[][] matrix = new String[actual.size()][actual.get(0).length];
				header[0] = "ID";
				header[1] = "PUNTOS";
				for (int j = 0; j < actual.size(); j++) 
				{
					matrix[j][0] = ""+(BigDecimal)actual.get(j)[0];
					matrix[j][1] = ""+(BigDecimal)actual.get(j)[1];
				}
				System.out.println("------------" + i +"----------");
				if(i == 0){
					TableFrame table = new TableFrame(matrix, header, 30, 30, "Clientes que compran una vez al mes");
					table.setVisible(true);
				}
				else if(i == 1){
					TableFrame table = new TableFrame(matrix, header, 30, 30, "Clientes que siempre compran algo costoso");
					table.setVisible(true);
				}
				else if(i == 2){
					TableFrame table = new TableFrame(matrix, header, 30, 30, "Clientes que compran tecnologia o herramientas");
					table.setVisible(true);
				}
			}
		}
	}

	private class Req1_2 extends JFrame implements ActionListener, ItemListener, ListSelectionListener
	{
		private String codigo;
		private String cli = "Cliente";

		private String[] respuesta;

		private JComboBox<String> tipos;

		private JComboBox<String> productos;

		private JComboBox<String> cliente;

		private JComboBox<String> ordenes;

		private JButton confirmar;

		private JList<String> list;

		private JCheckBox ordenamiento;
		
		private JCheckBox verTodo;

		private JTextField inicio;

		private JTextField fin;
		
		private boolean primeroOSegundo;

		public Req1_2(boolean primeroOSegundo)
		{
			this.primeroOSegundo = primeroOSegundo;
			setTitle("Requerimiento 1");
			if(!primeroOSegundo)
				setTitle("Requerimiento 2");
			setLayout(new BorderLayout());
			JPanel panel = new JPanel();
			setSize(400, 600);
			setPreferredSize(new Dimension(400,600));
			panel.setLayout(new GridLayout(4,2));
			panel.setSize(300,300);
			panel.setPreferredSize(new Dimension(300,300));

			tipos = new JComboBox<String>();
			tipos.addItem("Seleccione un tipo");
			tipos.setEnabled(true);
			ArrayList<String> arr = principal.darTipos();
			for (int i = 0; i < arr.size(); i++) 
			{
				tipos.addItem(arr.get(i));
			}
			tipos.addItemListener(this);
			panel.add(tipos);

			productos = new JComboBox<String>();
			productos.setEnabled(false);
			productos.addItem("Seleccione un producto");
			productos.addItemListener(this);
			panel.add(productos);

			panel.add(new JLabel("Ingrese la fecha de inicio"));
			inicio = new JTextField();
			inicio.setText("dd/mm/yyyy");
			panel.add(inicio);

			panel.add(new JLabel("Ingrese la fecha de finalizacion"));
			fin = new JTextField();
			fin.setText("dd/mm/yyyy");
			panel.add(fin);

			panel.add(new JLabel("Seleccione un tipo de cliente"));
			cliente = new JComboBox<String>();
			cliente.setEnabled(true);
			cliente.addItem("General");
			cliente.addItem("Natural");
			cliente.addItem("Empresarial");
			cliente.addItemListener(this);
			panel.add(cliente);

			JPanel sur = new JPanel();
			sur.setLayout(new GridLayout(3,1));

			if(primeroOSegundo)
			{
				String[] arr2 = new String[]{"Toda la informacion del usuario","Cantidad de unidades compradas","Monto total","Numero de transacciones"};
				list = new JList<String>(arr2);
				list.setVisibleRowCount(4);
				list.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
				list.addListSelectionListener(this);
				JScrollPane temp = new JScrollPane(list);
				temp.setPreferredSize(new Dimension(150,100));
				temp.setBorder(BorderFactory.createTitledBorder("Que desea ver?"));
				sur.add(temp);
			}
			else
			{
				verTodo= new JCheckBox("Desea ver toda la informacion del cliente?");
				sur.add(verTodo);
			}

			ordenes = new JComboBox<String>();
			ordenes.setEnabled(false);
			ordenes.setBorder(BorderFactory.createTitledBorder("Ordenado por que valor?"));
			ordenamiento = new JCheckBox("Ascendente");
			ordenamiento.setEnabled(false);
			JPanel subsub = new JPanel();
			subsub.setLayout(new GridLayout(1,2));
			subsub.add(ordenes);
			subsub.add(ordenamiento);
			sur.add(subsub);

			confirmar = new JButton("Confirmar");
			confirmar.addActionListener(this);
			confirmar.setActionCommand("c");
			sur.add(confirmar);

			add(panel, BorderLayout.NORTH);
			add(sur, BorderLayout.CENTER);
		}
		@Override
		public void itemStateChanged(ItemEvent pEvento) {
			// TODO Auto-generated method stub
			if( pEvento.getSource( ) == tipos)
			{

				if( pEvento.getStateChange( ) == ItemEvent.SELECTED )
				{

					if( ((String)tipos.getSelectedItem()).equals("Seleccione un tipo") )
					{
						productos.removeAllItems();
						productos.addItem("Seleccione un producto");
						productos.setEnabled(false);
					}
					else
					{
						String tipo = (String)tipos.getSelectedItem( );
						ArrayList<String> arr = principal.darProductosTipo(tipo);
						for (int j = 0; j < arr.size(); j++) 
						{
							productos.addItem(arr.get(j));
						}
						productos.setEnabled(true);
					}
				}
				return;
			}
			if( pEvento.getSource( ) == productos )
			{
				if( pEvento.getStateChange( ) == ItemEvent.SELECTED )
				{
					String rta = (String) productos.getSelectedItem();
					if(rta.equals("Seleccione un producto"))
					{
						codigo = null;
					}
					else
					{
						codigo = rta.split(" // ")[1];
					}
				}
				return;
			}
			if(pEvento.getSource() == cliente)
			{
				if( pEvento.getStateChange( ) == ItemEvent.SELECTED )
				{
					String rta = (String) cliente.getSelectedItem();
					if(rta.equals("General"))
					{
						cli = "Cliente";
					}
					else if(rta.equals("Natural"))
					{
						cli = "Natural";
					}
					else if(rta.equals("Empresarial"))
					{
						cli = "Empresa";
					}

					if(ordenes != null){
						try {
							ordenes.removeAllItems();
						} catch (Exception e) {
							// TODO: handle exception
						}
					}
					if(primeroOSegundo)
					{
						List<String> lista = list.getSelectedValuesList();
						if(lista.size() == 0) ordenes.setEnabled(false);
						if(lista.contains("Toda la informacion del usuario"))
						{
							if(cli.equals("Cliente"))
							{
								ordenes.addItem("Puntos");
							}
							if(cli.equals("Natural"))
							{
								ordenes.addItem("Cedula");
								ordenes.addItem("Nombre");
								ordenes.addItem("Email");
							}
							if(cli.equals("Empresa"))
							{
								ordenes.addItem("NIT");
							}
						}
						if(lista.contains("Cantidad de unidades compradas"))
						{
							ordenes.addItem("Cantidad");
						}
						if(lista.contains("Monto total"))
						{
							ordenes.addItem("Monto");
						}
						if(lista.contains("Numero de transacciones"))
						{
							ordenes.addItem("Numero");
						}
					}
					if(!primeroOSegundo && verTodo.isSelected())
					{
						if(cli.equals("Cliente"))
						{
							ordenes.addItem("Puntos");
						}
						if(cli.equals("Natural"))
						{
							ordenes.addItem("Cedula");
							ordenes.addItem("Nombre");
							ordenes.addItem("Email");
						}
						if(cli.equals("Empresa"))
						{
							ordenes.addItem("NIT");
						}
					}
					if(ordenes.getItemCount() == 0) ordenamiento.setEnabled(false);
					else ordenamiento.setEnabled(false);
				}
				return;
			}
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub 	
			if(e.getActionCommand().equals("c"))
			{
				respuesta = new String[5];
				ArrayList<String> listica = new ArrayList<>();
				respuesta[0] = codigo;
				respuesta[1] = inicio.getText();
				respuesta[2] = fin.getText();
				respuesta[3] = cli;
				boolean si = false;
				List<String> lista = null;
				if(primeroOSegundo)
				{
					lista = list.getSelectedValuesList();
					if(lista.contains("Toda la informacion del usuario"))
						listica.add("todoInfo");
					if(lista.contains("Cantidad de unidades compradas"))
						listica.add("cantidad");
					if(lista.contains("Monto total"))
						listica.add("monto");
					if(lista.contains("Numero de transacciones"))
						listica.add("numero");
				}
				else
				{
					si = verTodo.isSelected();
				}
				try 
				{
					respuesta[4] = ((String)ordenes.getSelectedItem()).toLowerCase()+"//"+(ordenamiento.isSelected()?1:0);
				} 
				catch (Exception e2) 
				{
					// TODO: handle exception
					respuesta[4] = "id//1";
				}
				List<Object[]> resultado = null;
				if(primeroOSegundo) {
					resultado = principal.req1Iter3(respuesta[0], respuesta[1],respuesta[2],respuesta[3],listica,respuesta[4]);
				}
				else{
					resultado = principal.req2Iter3(respuesta[0], respuesta[1], respuesta[2], respuesta[3], si, respuesta[4]);
				}
				
				if(resultado.isEmpty())
				{
					JOptionPane.showMessageDialog(this, "No hay resultados para la consulta dada", "Super Andes", JOptionPane.INFORMATION_MESSAGE);
					return;
				}

				String[] header = new String[(resultado.get(0) instanceof Object[]?resultado.get(0).length:1)];
				String[][] temp = new String[resultado.size()][(resultado.get(0) instanceof Object[]?resultado.get(0).length:1)];
				int actual = 0;
				
				if(primeroOSegundo && listica != null && !listica.contains("todoInfo"))
				{
					header[0] = "ID";
					if(resultado.get(0) instanceof Object[])
					{
						for (int i = 0; i < resultado.size(); i++)
						{
							String[] arr = new String[(resultado.get(0) instanceof Object[]?resultado.get(0).length:1)];
							arr[0] = ""+(BigDecimal)resultado.get(i)[0];
							temp[i] = arr;
						}
					}
					else
					{
						for (int i = 0; i < resultado.size(); i++)
						{
							String[] arr = new String[1];
							arr[0] = ""+resultado.get(i);
							temp[i] = arr;
						}
					}
					actual++;
				}
				if(!primeroOSegundo && !si)
				{
					header[0] = "ID";
					if(resultado.get(0) instanceof Object[])
					{
						for (int i = 0; i < resultado.size(); i++)
						{
							String[] arr = new String[(resultado.get(0) instanceof Object[]?resultado.get(0).length:1)];
							arr[0] = ""+(BigDecimal)resultado.get(i)[0];
							temp[i] = arr;
						}
					}
					else
					{
						for (int i = 0; i < resultado.size(); i++)
						{
							String[] arr = new String[1];
							arr[0] = ""+resultado.get(i);
							temp[i] = arr;
						}
					}
					actual++;
					TableFrame table = new TableFrame(temp, header, 30, 30, "VENTAS POR SUCURSAL");
					table.setVisible(true);
					return;
				}
				else
				{
					if(cli.contains("Cliente"))
					{
						header[0] = "ID"; header[1] = "PUNTOS"; header[2] = "CARROABANDONADO";
						for (int i = 0; i < resultado.size(); i++)
						{
							String[] arr = new String[(resultado.get(0) instanceof Object[]?resultado.get(0).length:1)];
							arr[0] = ""+(BigDecimal)resultado.get(i)[0];
							arr[1] = ""+(BigDecimal)resultado.get(i)[1];
							arr[2] = ""+(String)resultado.get(i)[2];
							temp[i] = arr;
						}
						actual += 3;
					}
					else if(cli.contains("Natural"))
					{
						header[0] = "ID"; header[1] = "CEDULA";header[2] = "NOMBRE"; header[3] = "EMAIL";
						for (int i = 0; i < resultado.size(); i++)
						{
							String[] arr = new String[(resultado.get(0) instanceof Object[]?resultado.get(0).length:1)];
							arr[0] = ""+(BigDecimal)resultado.get(i)[0];
							arr[1] = ""+(String)resultado.get(i)[1];
							arr[2] = ""+(String)resultado.get(i)[2];
							arr[3] = ""+(String)resultado.get(i)[3];
							temp[i] = arr;
						}
						actual +=4;
					}
					else
					{
						header[0] = "ID"; header[1] = "NIT"; header[2] = "DIRECCION";
						for (int i = 0; i < resultado.size(); i++)
						{
							String[] arr = new String[(resultado.get(0) instanceof Object[]?resultado.get(0).length:1)];
							arr[0] = ""+(BigDecimal)resultado.get(i)[0];
							arr[1] = ""+(String)resultado.get(i)[1];
							arr[2] = ""+(String)resultado.get(i)[2];
							temp[i] = arr;
						}
						actual +=3;
					}
					if(!primeroOSegundo){
						TableFrame table = new TableFrame(temp, header, 30, 30, "VENTAS POR SUCURSAL");
						table.setVisible(true);
						return;
					}
				}
				if(listica.contains("cantidad"))
				{
					header[actual] = "CANTIDAD DE UNIDADES";
					for (int i = 0; i < resultado.size(); i++)
					{
						String[] arr = temp[i];
						arr[actual] = ""+(BigDecimal)resultado.get(i)[actual];
						temp[i] = arr;
					}
					actual++;
				}
				if(listica.contains("monto"))
				{
					header[actual] = "MONTO TOTAL";
					for (int i = 0; i < resultado.size(); i++)
					{
						String[] arr = temp[i];
						arr[actual] = ""+(BigDecimal)resultado.get(i)[actual];
						temp[i] = arr;
					}
					actual++;
				}
				if(listica.contains("numero"))
				{
					header[header.length-1] = "# TRANSACCIONES";
					for (int i = 0; i < resultado.size(); i++)
					{
						String[] arr = temp[i];
						arr[actual] = ""+(BigDecimal)resultado.get(i)[actual];
						temp[i] = arr;
					}
					actual++;
				}
				TableFrame table = new TableFrame(temp, header, 30, 30, "VENTAS POR SUCURSAL");
				table.setVisible(true);
			}
		}
		@Override
		public void valueChanged(ListSelectionEvent e) {
			// TODO Auto-generated method stub
			if(e.getValueIsAdjusting() == false) 
			{
				ordenes.removeAllItems();
				if(list.getSelectedIndex() == -1) {
					ordenes.setEnabled(false); ordenamiento.setEnabled(false);
				}
				else 
				{
					ordenes.setEnabled(true);
					List<String> lista = list.getSelectedValuesList();
					if(lista.contains("Toda la informacion del usuario"))
					{
						if(cli.equals("Cliente"))
						{
							ordenes.addItem("Puntos");
						}
						if(cli.equals("Natural"))
						{
							ordenes.addItem("Cedula");
							ordenes.addItem("Nombre");
							ordenes.addItem("Email");
						}
						if(cli.equals("Empresa"))
						{
							ordenes.addItem("NIT");
						}
					}
					if(lista.contains("Cantidad de unidades compradas"))
					{
						ordenes.addItem("Cantidad");
					}
					if(lista.contains("Monto total"))
					{
						ordenes.addItem("Monto");
					}
					if(lista.contains("Numero de transacciones"))
					{
						ordenes.addItem("Numero");
					}
					if(ordenes.getItemCount() == 0) ordenamiento.setEnabled(false);
					else ordenamiento.setEnabled(true);
				}
			}
		}
	}
}

