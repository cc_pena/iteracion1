package uniandes.isis2304.superAndes.negocio;

import java.sql.Timestamp;

/**
 * Clase para modelar el concepto PROMOCION DE DESCUENTO del negocio de SuperAndes
 *
 * @author fj.gonzalez
 */
public class PromocionDescuento extends Promocion implements VOPromocionDescuento
{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El procentaje de descuento.
	 */
	private Double porcentajeDescuento;
	/* ****************************************************************
	 * 			Constructores
	 *****************************************************************/
	/**
	 * Constructor vacio.
	 */
	public PromocionDescuento()
	{
		super();
		porcentajeDescuento = 0.0;
		this.setTipo(2);
	}
	/**
	 * Constructor con valores.
	 */
	public PromocionDescuento(long pId, Timestamp pFechaExpedicion,Timestamp pFechaFinalizacion,Timestamp pFechaFinExistencias, Integer pUnidades , String pCodigoBarrasProducto1 , String pCodigoBarrasProducto2 , Integer pTipo, Double pPorc, String pEstado)
	{
		super(pId, pFechaExpedicion, pFechaFinalizacion, pFechaFinExistencias, pUnidades, pCodigoBarrasProducto1, pCodigoBarrasProducto2, pTipo, pEstado);
		porcentajeDescuento = pPorc;     
		this.setTipo(2);
	}
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	@Override
	public Double getPorcentajeDescuento()
	{
		return porcentajeDescuento;
	}
	/**
	 * @param porcentajeDescuento - El nuevo porcentaje de descuento.
	 */
	public void setPorcentajeDescuento(Double porcentajeDescuento) 
	{
		this.porcentajeDescuento = porcentajeDescuento;
	}
	
	public long getId()
	{

		return id;
	}
	/**
	 * @param id - El nuevo id de la promoción.
	 */
	public void setId(long id) 
	{
		this.id = id;
	}
	
	/**
	 * @return Una cadena de caracteres con la información básica de la promoción de descuento.
	 */
	@Override
	public String toString()
	{
		return "PromocionDescuento : " + super.toString() + "porcentaje de descuento" + porcentajeDescuento;
		
	}

}
