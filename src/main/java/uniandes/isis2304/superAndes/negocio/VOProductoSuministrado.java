package uniandes.isis2304.superAndes.negocio;
/**
 * Interfaz para los métodos get de PRODUCTO_SUMINISTRADO.
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz. 
 * 
 * @author fj.gonzalez
 */
public interface VOProductoSuministrado
{
	/**
	 * @return El precio del producto.
	 */
	public Double getPrecioUnitario();
	/**
	 * @return La cantidad del producto que hay en bodega.
	 */
	public Integer getCantidadEnBodega();
	/**
	 * @return  La cantidad del producto que hay en estante.
	 */
	public Integer getCantidadEnEstante();
	/**
	 * @return El precio x unidad de medida del producto.
	 */
	public String getPrecioPorUnidadMedida();
	/**
	 * @return El id de la bodega a la que pertenece el producto.
	 */
	public long getIdBodega();
	/**
	 * @return El id del estante al que pertenece el producto.
	 */
	public long getIdEstante();
	/**
	 * @return El id de la sucursal que pertenecer el producto.
	 */
	public long getIdSucursal();
		/**
	 * @return Una cadena de caracteres con la información básica del producto sumistrado.
	 */
	@Override
	public String toString();
	
}
