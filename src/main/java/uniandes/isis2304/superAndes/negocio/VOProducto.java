package uniandes.isis2304.superAndes.negocio;
/**
 * Interfaz para los métodos get de PRODUCTO.
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz. 
 * 
 * @author fj.gonzalez
 */
public interface VOProducto
{
	/**
	 * @return El código de barras del producto.
	 */
	public String getCodigoDeBarras();
	/**
	 * @return La marca del producto.
	 */
	public String getMarca();
	/**
	 * @return El nombre del producto.
	 */
	public String getNombre();
	/**
	 * @return La presentación estipulada para un producto.
	 */
	public String getPresentacion();
	/**
	 * @return La unidad de medida del producto. Puede ser en gr o ml.
	 */
	public String getUnidadDeMedida();
	/**
	 * @return El volumen del producto.
	 */
	public String getVolumen();
	/**
	 * @return El peso del producto.
	 */
	public String getPeso();
	/**
	 * @return Cantidad en la presentacion.
	 */
	public Integer getCantidadPresentacion();
	/**
	 * @return El id que referencia el tipo de producto.
	 */
	public String getTipo();
	/**
	 * @return Una cadena de caracteres con la información básica del producto.
	 */
	@Override
	public String toString();

}
