package uniandes.isis2304.superAndes.negocio;
/**
 * Interfaz para los métodos get de SUCURSAL.
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz. 
 * 
 * @author fj.gonzalez
 */
public interface VOSucursal 
{

	/**
	 * @return El id de la sucursal.
	 */
	public long getId();
	/**
	 * @return El nombre de la sucursal.
	 */
	public String getNombre();
	/**
	 * @return La direccion de la sucursal.
	 */
	public String getDireccion();
	/**
	 * @return La ciudad de la sucursal.
	 */
	public String getCiudad();
	/**
	 * @return El nivel de reorden establecido para esta sucursal.
	 */
	public Integer getNivelReOrden();
	/**
	 * @return El nivel de abastecimiento del estante.
	 */
	public Integer getNivelDeAbastecimiento();
	/**
	 * @return El nombre supermercado.
	 */
	public String getNombreSupermercado();
	/**
	 * @return Una cadena de caracteres con la información básica de la sucursal.
	 */
	@Override
	public String toString();
}
