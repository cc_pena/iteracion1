package uniandes.isis2304.superAndes.negocio;

public class Carrito implements VOCarrito
{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El id del cliente asociado a un producto puntual.
	 */
	public long idCliente;
	/**
	 * El c�digo de barras del producto asociado al cliente.
	 */
	public String idProducto;
	/**
	 * La cantidad de unidades que se lleva de dicho producto.
	 */
	public int cantidad;
	
	/* ****************************************************************
	 * 			Constructor
	 *****************************************************************/
	public Carrito() 
	{
		this.idCliente = 0;
		this.idProducto = "";
		this.cantidad = 0;
	}
	public Carrito(long idCliente, String idProducto, int cantidad)
	{
		this.idCliente = idCliente;
		this.idProducto = idProducto;
		this.cantidad = cantidad;
	}
	/* ****************************************************************
	 * 			M�todos
	 *****************************************************************/
	@Override
	public long getIdCliente() 
	{
		return idCliente;
	}

	@Override
	public String getIdProducto() 
	{
		return idProducto;
	}
	
	@Override
	public int getCantidad() 
	{
		return cantidad;
	}

	/**
	 * @param idCliente the idCliente to set
	 */
	public void setIdCliente(long idCliente) 
	{
		this.idCliente = idCliente;
	}

	/**
	 * @param idProducto the idProducto to set
	 */
	public void setIdProducto(String idProducto) 
	{
		this.idProducto = idProducto;
	}
	/**
	 * @param cantidad the cantidad to set
	 */
	public void setCantidad(int cantidad)
	{
		this.cantidad =  cantidad;
	}
	/**
	 * @return cadena con la informacion basica del carrito
	 */
	@Override
	public String toString() {
		return "Carrito " + "idCliente=" + idCliente + ", idProducto=" + idProducto + ", cantidad=" + cantidad +"]";
	}
	

}
