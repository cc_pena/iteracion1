package uniandes.isis2304.superAndes.negocio;
/**
 * Interfaz para los métodos get de INFO_PRODUCTO.
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz. 
 * 
 * @author fj.gonzalez
 */
public interface VOInfoProducto 
{
	/**
	 * @return El id de la orden pedido.
	 */
	public long getIdOrden();
	/**
	 * @return El codigo del producto.
	 */
	public String getCodigoProducto();
	/**
	 * @return El precio asociado a dicho producto
	 */
	public Double getPrecioUnitario();
	/**
	 * @return La cantidad de dicho producto.
	 */
	public Integer getCantidad();
	/**
	 * @return El precioXUnidadDeMedida de dicho producto.
	 */
	public String getPrecioUnidadMedida();
	/**
	 * @return La calidad del producto.
	 */
	public Double getCalidad();
	/**
	 * @return Una cadena de caracteres con la información básica del infoProducto.
	 */
	@Override
	public String toString();
	

}
