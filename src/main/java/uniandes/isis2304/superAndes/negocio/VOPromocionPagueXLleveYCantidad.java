package uniandes.isis2304.superAndes.negocio;
/**
 * Interfaz para los métodos get de PromocionPagueXLleveYCantidad .
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz. 
 * 
 * @author fj.gonzalez
 */
public interface VOPromocionPagueXLleveYCantidad 
{
	/**
	 * @return La cantidad que se pagarán del producto.
	 */
	public Integer getCantidadPagadas();
	/**
	 * @return La cantidad ofrecidas del producto.
	 */
	public Integer getCantidadOfrecidas();
	/**
	 * @return Una cadena de caracteres con la información básica de la promoción pague x lleve y cantidad.
	 */
	@Override
	public String toString();


}
