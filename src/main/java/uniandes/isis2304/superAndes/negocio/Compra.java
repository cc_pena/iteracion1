package uniandes.isis2304.superAndes.negocio;

/**
 * Clase para modelar la relación COMPRAN del negocio SuperAndes:
 * Cada objeto de esta clase representa el hecho que un producto esta asociado a una transaccion y viceversa.
 * Se modela mediante los identificadores del producto y la transaccion respectivamente.
 * Debe existir un producto con el codigo dado
 * Debe existir una transaccion con el identificador dado 
 * 
 * @author cc.pena
 */
public class Compra implements VOCompran
{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	
	/**
	 * codigo del producto asociado
	 */
	private String codigoProductoSuministrado;
	
	/**
	 * id de la transaccion asociada
	 */
	private long idTransaccionCliente;
	
	/**
	 * Cantidad de productos del producto asociado
	 */
	private Integer cantidad;
	
	/* ****************************************************************
	 * 			Constructores
	 *****************************************************************/
	
	/**
	 * Constructor por defecto
	 */
	public Compra()
	{
		this.codigoProductoSuministrado = "";
		this.idTransaccionCliente = 0;
		this.cantidad = 0;
	}
	
	/**
	 * Constructor con valores
	 * @param codigoProductoSuministrado - codigo del producto asociado (ya debe existir)
	 * @param idTransaccionCliente - id de la transaccion asociada (ya debe existir)
	 * @param cantidad - cantidad de productos del producto asociado
	 */
	public Compra(String codigoProductoSuministrado, long idTransaccionCliente, Integer cantidad)
	{
		this.codigoProductoSuministrado = codigoProductoSuministrado;
		this.idTransaccionCliente = idTransaccionCliente;
	}
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/

	public String getCodigoProductoSuministrado() {
		return codigoProductoSuministrado;
	}

	/**
	 * @param codigoProductoSuministrado - nuevo codigo del producto suministrado (ya debe existir)
	 */
	public void setCodigoProductoSuministrado(String codigoProductoSuministrado) {
		this.codigoProductoSuministrado = codigoProductoSuministrado;
	}

	public long getIdTransaccionCliente() {
		return idTransaccionCliente;
	}

	/**
	 * @param idTransaccionCliente - nuevo id de la transaccion (ya debe existir)
	 */
	public void setIdTransaccionCliente(long idTransaccionCliente) {
		this.idTransaccionCliente = idTransaccionCliente;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad - nueva cantidad de productos del producto asociado
	 */
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	
	/** 
	 * @return Una cadena con la información básica
	 */
	@Override
	public String toString() {
		return "Compran [codigoProducto="+codigoProductoSuministrado + ", idTransaccion=" + idTransaccionCliente + ", cantidad=" + cantidad + "]";
	}
}
