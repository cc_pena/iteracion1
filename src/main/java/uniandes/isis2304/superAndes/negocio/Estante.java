package uniandes.isis2304.superAndes.negocio;

/**
 * Clase para modelar el concepto ESTANTE del negocio SuperAndes
 * 
 * @author cc.pena
 */
public class Estante implements VOEstante
{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	
	/**
	 * Identificador unico del estante
	 */
	private long id;
	
	/**
	 * sucursal asociada al estante
	 */
	private long idSucursal;
	
	/**
	 * capacidad en masa del estante
	 */
	private Integer capacidadEnMasa;
	
	/**
	 * capacidad volumetrica del estante
	 */
	private Integer capacidadVolumetrica;
	
	/**
	 * tipo de producto que maneja el estante
	 */
	private String tipoProducto;
	
	/**
	 * cantidad en masa del estante
	 */
	private Integer cantidadEnMasa;
	
	/**
	 * cantidad volumetrica del estante
	 */
	private Integer cantidadVolumetrica;

	/* ****************************************************************
	 * 			Constructores
	 *****************************************************************/
	
	/**
	 * Constructor por defecto
	 */
	public Estante()
	{
		this.id = 0;
		this.idSucursal = 0;
		this.capacidadEnMasa = 0;
		this.capacidadVolumetrica = 0;
		this.tipoProducto = "";
		this.cantidadEnMasa = 0;
		this.cantidadVolumetrica = 0;
	}
	
	/**
	 * @param cantidadEnMasa the cantidadEnMasa to set
	 */
	public void setCantidadEnMasa(Integer cantidadEnMasa) {
		this.cantidadEnMasa = cantidadEnMasa;
	}

	/**
	 * @param cantidadVolumetrica the cantidadVolumetrica to set
	 */
	public void setCantidadVolumetrica(Integer cantidadVolumetrica) {
		this.cantidadVolumetrica = cantidadVolumetrica;
	}

	/**
	 * Constructor con valores
	 * @param id - id unico del estante
	 * @param idSucursal - id de la sucursal asociada
	 * @param capacidadEnMasa - capacidad en masa del estante
	 * @param capacidadVolumetrica - capacidad en volumen del estante
	 * @param tipoProducto - tipo de producto que admite el estante
	 */
	public Estante(long id, long idSucursal, Integer capacidadEnMasa, Integer capacidadVolumetrica, String tipoProducto,Integer cantidadEnMasa, Integer cantidadVolumetrica) {
		this.id = id;
		this.idSucursal = idSucursal;
		this.capacidadEnMasa = capacidadEnMasa;
		this.capacidadVolumetrica = capacidadVolumetrica;
		this.tipoProducto = tipoProducto;
		this.cantidadEnMasa = cantidadEnMasa;
		this.cantidadVolumetrica = cantidadVolumetrica;
	}
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/

	public long getId() {
		return id;
	}

	/**
	 * @param id - el nuevo id del estante
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	public long getIdSucursal() {
		return idSucursal;
	}

	/**
	 * @param idSucursal - la nueva sucursal asociada
	 */
	public void setIdSucursal(long idSucursal) {
		this.idSucursal = idSucursal;
	}
	
	public Integer getCapacidadEnMasa() {
		return capacidadEnMasa;
	}

	public Integer getCantidadEnMasa() 
	{
		return cantidadEnMasa;
	}

	public Integer getCantidadVolumetrica()
	{
		return cantidadVolumetrica;
	}
	/**
	 * @param capacidadEnMasa - la nueva capacidad en masa del estante
	 */
	public void setCapacidadEnMasa(Integer capacidadEnMasa) {
		this.capacidadEnMasa = capacidadEnMasa;
	}

	public Integer getCapacidadVolumetrica() {
		return capacidadVolumetrica;
	}
	
	/**
	 * @param capacidadVolumetrica - la nueva capacidad volumetrica del estante
	 */
	public void setCapacidadVolumetrica(Integer capacidadVolumetrica) {
		this.capacidadVolumetrica = capacidadVolumetrica;
	}

	public String getTipoProducto() {
		return tipoProducto;
	}

	/**
	 * @param tipoProducto - el nuevo tipo de producto que maneja el estante
	 */
	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}
	
	/**
	 * @return Una cadena de caracteres con la información básica del estante.
	 */
	@Override
	public String toString() {
		return "Estante [id="+id+", idSucursal="+idSucursal+", capacidadMasa="+capacidadEnMasa+", capacidadVolumetrica="+capacidadVolumetrica+", tipoProducto="+tipoProducto+", cantidadActualEnMasa =" + cantidadEnMasa+", cantidadVolumetrica=" + cantidadVolumetrica+"]";
	}

}
