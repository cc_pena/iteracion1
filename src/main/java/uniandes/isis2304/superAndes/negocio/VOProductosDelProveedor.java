package uniandes.isis2304.superAndes.negocio;
/**
 * Interfaz para los métodos get de PRODUCTOSDELPROVEEDOR.
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz. 
 * 
 * @author fj.gonzalez
 */
public interface VOProductosDelProveedor
{
	/**
	 * @return El nit del proveedor
	 */
	public String getNITProveedor();
	/**
	 * @return El id del producto.
	 */
	public String getCodigoProducto();
	/**
	 * @return Una cadena de caracteres con la información básica de los productos de los proveedores.
	 */
	@Override
	public String toString();
	

}
