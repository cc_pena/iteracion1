package uniandes.isis2304.superAndes.negocio;
/**
 * Interfaz para los métodos get de CLIENTE_NATURAL.
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz. 
 * 
 * @author fj.gonzalez
 */
public interface VOClienteNatural 
{
	/**
	 * @return La cédula del cliente.
	 */
	public String getCedula();
	/**
	 * @return El nombre del cliente.
	 */
	public String getNombre();
	/**
	 * @return El email del cliente.
	 */
	public String getEmail();
	/**
	 * @return Una cadena de caracteres con la información básica del clienteNatural
	 */
	@Override
	public String toString();
	

}
