package uniandes.isis2304.superAndes.negocio;
/**
 * Interfaz para los métodos get de CLIENTE.
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz. 
 * 
 * @author fj.gonzalez
 */
public interface VOCliente
{
	/**
	 * @return El id del cliente.
	 */
	public long getId();
	/**
	 * @return Los puntos del cliente.
	 */
	public int getPuntos();
	/**
	 * @return El carro est� o no abandonado.
	 */
	public String getCarroAbandonado();
	/**
	 * @return Una cadena de caracteres con la información básica del cliente.
	 */
	@Override
	public String toString();
	

}
