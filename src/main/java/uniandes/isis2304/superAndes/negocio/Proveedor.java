package uniandes.isis2304.superAndes.negocio;
/**
 * Clase para modelar el concepto PROVEEDOR del negocio de SuperAndes
 *
 * @author fj.gonzalez
 */
public class Proveedor implements VOProveedor
{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El NIT del proveedor.
	 */
	private String nit;
	/**
	 * El nombre del proveedor.
	 */
	private String nombre;
	/**
	 * La calificación del proveedor.
	 */
	private Double calificacion;
	/* ****************************************************************
	 * 			Constructores.
	 *****************************************************************/
	/**
	 * Constructor vacio.
	 */
	public Proveedor() 
	{
		nit = "";
		nombre = "";
		calificacion = 0.0;
	}
	/**
	 * Constructor con valores.
	 */
	public Proveedor(String pNit, String pNombre , Double pCal)
	{
		nit = pNit;
		nombre = pNombre;
		calificacion = pCal;
	}
	/* ****************************************************************
	 * 			Métodos.
	 *****************************************************************/
	public String getNIT() 
	{
		return nit;
	}

	
	public String getNombre() 
	{
		return nombre;
	}

	
	public Double getCalificacion() 
	{
		return calificacion;
	}
	/**
	 * @param nit - El nuevo nit del proveedor.
	 */
	public void setNit(String nit)
	{
		this.nit = nit;
	}
	/**
	 * @param nombre - El nuevo nombre del proveedor.
	 */
	public void setNombre(String nombre)
	{
		this.nombre = nombre;
	}
	/**
	 * @param calificacion - La nueva calificación del proveedor.
	 */
	public void setCalificacion(Double calificacion)
	{
		this.calificacion = calificacion;
	}
	/**
	 * @return Una cadena de caracteres con la información básica del proveedor.
	 */
	@Override
	public String toString()
	{
		return "Proveedor [NIT=" + nit + ", nombre =" + nombre + ", calificación =" + calificacion + "]";
	}
	
}
