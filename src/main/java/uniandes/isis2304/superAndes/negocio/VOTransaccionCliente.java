package uniandes.isis2304.superAndes.negocio;

import java.sql.Timestamp;
/**
 * Interfaz para los métodos get de TRANSACCION_CLIENTE.
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz. 
 * 
 * @author fj.gonzalez
 */
public interface VOTransaccionCliente 
{
	/**
	 * @return El id de la transacción
	 * 
	 */
	public long getId();
	/**
	 * @return El monto de la transacción.
	 */
	public Double getMonto();
	/**
	 * @return La fecha de la transacción
	 */
	public Timestamp getFecha();
	/**
	 * @return El id del cliente.
	 */
	public long getIdCliente();
	/**
	 * @return Una cadena de caracteres con la información básica de la transaccion de un cliente.
	 */
	@Override
	public String toString();

}
