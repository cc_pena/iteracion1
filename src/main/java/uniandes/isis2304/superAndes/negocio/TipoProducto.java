package uniandes.isis2304.superAndes.negocio;
/**
 * Clase para modelar el concepto TIPO PRODUCTO del negocio de SuperAndes
 *
 * @author fj.gonzalez
 */
public class TipoProducto implements VOTipoProducto
{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El nombre del tipo de producto
	 */
	private String nombre;
	/**
	 * La cateogría del tipo de producto.
	 */
	private String categoria;
	/* ****************************************************************
	 * 			Constructores
	 *****************************************************************/
	/**
	 * Constructor vacio.
	 */
	public TipoProducto() 
	{
		nombre = "";
		categoria = "";
	}
	/**
	 * Constructor con valores.
	 */
	public TipoProducto(String pNom , String pCat)
	{
		nombre = pNom;
		categoria = pCat;
	}
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	public String getNombre() 
	{
		return nombre;
	}

	
	public String getCategoria() 
	{
		return categoria;
	}
	/**
	 * @param nombre - El nuevo nombre del tipo de la producto.
	 */
	public void setNombre(String nombre)
	{
		this.nombre = nombre;
	}
	/**
	 * @param categoria - La nueva categoría del tipo de producto.
	 */
	public void setCategoria(String categoria)
	{
		this.categoria = categoria;
	}
	/**
	 * @return Una cadena de caracteres con la información básica del tipo de producto.
	 */
	@Override
	public String toString()
	{
		return "Tipo de producto [nombre=" + nombre + ", + categoria=" + categoria +  "]";

	}
	
	
}
