package uniandes.isis2304.superAndes.negocio;

import java.sql.Timestamp;
/**
 * Clase para modelar el concepto PROMOCION del negocio de SuperAndes
 *
 * @author fj.gonzalez
 */
public class Promocion implements VOPromocion
{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El identificador ÚNICO de la promoción.
	 */
	protected long id;

	/**
	 * La fecha de expedición de la promoción.
	 */
	private Timestamp fechaExpedicion;
	/**
	 * La fecha de finalización de la promoción.
	 */
	private Timestamp fechaFinalizacion;
	/**
	 * La fecha de fin de existencias de la promoción.
	 */
	private Timestamp fechaFinExistencias;
	/**
	 * La cantidad de unidades que se venderán del producto en la promoción.
	 */
	private Integer unidades;
	/**
	 * El código de barras del producto de la promoción
	 */
	private String codigoProducto1;
	/**
	 * El código de barras del producto de la promoción(2).
	 */
	private String codigoProducto2;
	/**
	 * El tipo de la promoción.
	 */
	private Integer tipo;
	/**
	 * El nombre de la promocion.
	 */
	private String nombre;
	
	private String estado;
	/* ****************************************************************
	 * 			Constructores
	 *****************************************************************/
	/**
	 * Constructor vacio.
	 */
	public Promocion()
	{
		id = 0;
		fechaExpedicion = new Timestamp(0);
		fechaFinalizacion = new Timestamp(0);
		fechaFinExistencias = new Timestamp(0);
		unidades = 0;
		codigoProducto1 = "";
		codigoProducto2 = "";
		estado = "A";
		tipo = 0;
		nombre = "";
	}
	/**
	 * Constructor con valores
	 */
	public Promocion(long pId, Timestamp pFechaExpedicion,Timestamp pFechaFinalizacion,Timestamp pFechaFinExistencias, Integer pUnidades , String pCodigoBarrasProducto1 , String pCodigoBarrasProducto2 , Integer pTipo, String pEstado )
	{
		id = pId;
		fechaExpedicion = pFechaExpedicion;
		fechaFinalizacion = pFechaFinalizacion;
		fechaFinExistencias = pFechaFinExistencias;
		unidades = pUnidades;
		codigoProducto1 = pCodigoBarrasProducto1;
		codigoProducto2 = pCodigoBarrasProducto2;
		tipo = pTipo;
		estado = pEstado;
		
	}
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	public long getId()
	{

		return id;
	}


	public Timestamp getFechaExpedicion() 
	{

		return fechaExpedicion;
	}


	public Timestamp getFechaFinalizacion() 
	{

		return fechaFinalizacion;
	}


	public Timestamp getFechaFinExistencias()
	{

		return fechaFinExistencias;
	}


	public Integer getUnidades() 
	{

		return unidades;
	}


	public String getCodigoProducto1()
	{

		return codigoProducto1;
	}

	public String getCodigoProducto2() 
	{

		return codigoProducto2;
	}


	public Integer getTipo() 
	{
		return tipo;
	}
	
	public String getNombre() 
	{

		return nombre;
	}
	
	/**
	 * @param id - El nuevo id de la promoción.
	 */
	public void setId(long id) 
	{
		this.id = id;
	}

	/**
	 * @param fechaExpedicion - La nueva fecha de expedicion
	 */
	public void setFechaExpedicion(Timestamp fechaExpedicion)
	{
		this.fechaExpedicion = fechaExpedicion;
	}

	/**
	 * @param fechaFinalizacion - La nueva de fechaFinalizacion
	 */
	public void setFechaFinalizacion(Timestamp fechaFinalizacion)
	{
		this.fechaFinalizacion = fechaFinalizacion;
	}

	/**
	 * @param fechaFinExistencias - La nueva fecha de fin de existencias
	 */
	public void setFechaFinExistencias(Timestamp fechaFinExistencias)
	{
		this.fechaFinExistencias = fechaFinExistencias;
	}

	/**
	 * @param unidades -Las nuevas unidades .
	 */
	public void setUnidades(Integer unidades) 
	{
		this.unidades = unidades;
	}

	/**
	 * @param codigoBarrasProducto1 - El nuevo codigoBarras del producto
	 */
	public void setCodigoProducto1(String codigoBarrasProducto1) 
	{
		this.codigoProducto1 = codigoBarrasProducto1;
	}

	/**
	 * @param codigoBarrasProducto2 - El nuevo codigoBarras del producto
	 */
	public void setCodigoProducto2(String codigoBarrasProducto2)
	{
		this.codigoProducto2 = codigoBarrasProducto2;
	}

	/**
	 * @param tipoPromocion - El nuevo tipo de promoción.
	 */
	public void setTipo(Integer tipoPromocion) 
	{
		this.tipo = tipoPromocion;
	}
	/**
	 * @param codigoBarrasProducto2 - El nuevo codigoBarras del producto
	 */
	public void setNombre(String nombre)
	{
		this.nombre = nombre;
	}
	/**
	 * @return Una cadena de caracteres con la información de un promoción.
	 */
	public String toString()
	{
		return "Promocion [id=" + id + ", fecha de expedición=" + fechaExpedicion + ", fecha de finalización=" + fechaFinalizacion + ", fecha de fin de existencias=" + fechaFinExistencias + ", unidades=" + unidades + ", tipo de promoción=" + tipo+  ", codigo de barras producto 1=" + codigoProducto1+", codigo de barras producto 2=" + codigoProducto2+ ", nombre "+ nombre +"]";

	}
	@Override
	public String getEstado()
	{
		// TODO Auto-generated method stub
		return estado;
	}
	
	public void setEstado(String estado)
	{
		this.estado = estado;
	}
	



}
