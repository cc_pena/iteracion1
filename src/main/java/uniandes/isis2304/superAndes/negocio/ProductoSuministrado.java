package uniandes.isis2304.superAndes.negocio;

/**
 * Clase para modelar el concepto PRODUCTOSUMINISTRADO del negocio SuperAndes
 * 
 * @author cc.pena
 */
public class ProductoSuministrado extends Producto implements VOProductoSuministrado
{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	
	/**
	 * El precio del producto suministrado
	 */
	private Double precioUnitario;
	
	/**
	 * cantidad de producto en bodega
	 */
	private Integer cantidadEnBodega;
	
	/**
	 * cantidad de producto en estante
	 */
	private Integer cantidadEnEstante;
	
	/**
	 * precio por unidad de medida del producto
	 */
	private String precioPorUnidadMedida;
	
	/**
	 * id de la bodega asociada
	 */
	private long idBodega;
	
	/**
	 * id del estante asociado
	 */
	private long idEstante;
	
	/**
	 * id de la sucursal asociada
	 */
	private long idSucursal;
	
	/* ****************************************************************
	 * 			Constructores
	 *****************************************************************/
	
	/**
	 * Constructor por defecto
	 */
	public ProductoSuministrado()
	{
		super();
		this.precioUnitario = 0.0;
		this.cantidadEnBodega = 0;
		this.cantidadEnEstante = 0;
		this.precioPorUnidadMedida = "";
		this.idBodega = 0;
		this.idEstante = 0;
		this.idSucursal = 0;
	}

	/**
	 * Constructor con valores
	 * @param codigoBarras - codigo de barras UNICO del producto
	 * @param marca - Marca del producto
	 * @param nombre - Nombre del producto
	 * @param presentacion - Presentacion del producto
	 * @param unidadMedida - Unidad de medida del producto
	 * @param volumen - Volumen del producto
	 * @param peso - Peso del producto
	 * @param cantidadEnLaPresentacion - Cantidad en la presentacion
	 * @param tipoProducto - Tipo del producto
	 * @param precio
	 * @param cantidadEnBodega
	 * @param cantidadEstante
	 * @param precioXUnidadMedida
	 * @param idBodega
	 * @param idEstante
	 * @param idPromocion
	 */
	public ProductoSuministrado(String codigoBarras, String marca, String nombre, String presentacion, String unidadMedida,
			String volumen, String peso, Integer cantidadEnLaPresentacion, String tipoProducto, Double precio, Integer cantidadEnBodega,
			Integer cantidadEstante, String precioXUnidadMedida, long idBodega, long idEstante, long idSucursal) 
	{
		super(codigoBarras, marca, nombre, presentacion, precioXUnidadMedida, volumen, peso, cantidadEnLaPresentacion, tipoProducto);
		this.precioUnitario = precio;
		this.cantidadEnBodega = cantidadEnBodega;
		this.cantidadEnEstante = cantidadEstante;
		this.precioPorUnidadMedida = precioXUnidadMedida;
		this.idBodega = idBodega;
		this.idEstante = idEstante;
		this.idSucursal = idSucursal;
	}
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	
	public String getCodigoDeBarras() {
		return codigoDeBarras;
	}

	/**
	 * @param codigoBarras - nuevo codigo de barras UNICO del producto
	 */
	public void setCodigoDeBarras(String codigoBarras) {
		this.codigoDeBarras = codigoBarras;
	}

	public Double getPrecioUnitario() {
		return precioUnitario;
	}

	/**
	 * @param precio - el precio del producto
	 */
	public void setPrecioUnitario(Double precio) {
		this.precioUnitario = precio;
	}

	public Integer getCantidadEnBodega() {
		return cantidadEnBodega;
	}

	/**
	 * @param cantidadEnBodega - cantidad de productos en bodega
	 */
	public void setCantidadEnBodega(Integer cantidadEnBodega) {
		this.cantidadEnBodega = cantidadEnBodega;
	}

	public Integer getCantidadEnEstante() {
		return cantidadEnEstante;
	}

	/**
	 * @param cantidadEstante - cantidad de productos en estante
	 */
	public void setCantidadEnEstante(Integer cantidadEstante) {
		this.cantidadEnEstante = cantidadEstante;
	}

	public String getPrecioPorUnidadMedida() {
		return precioPorUnidadMedida;
	}

	/**
	 * @param precioXUnidadMedida - precio por unidad de medida
	 */
	public void setPrecioPorUnidadMedida(String precioXUnidadMedida) {
		this.precioPorUnidadMedida = precioXUnidadMedida;
	}

	public long getIdBodega() {
		return idBodega;
	}

	/**
	 * @param idBodega - id de la bodega asociada
	 */
	public void setIdBodega(long idBodega) {
		this.idBodega = idBodega;
	}

	public long getIdEstante() {
		return idEstante;
	}

	/**
	 * @param idEstante - id del estante asociado
	 */
	public void setIdEstante(long idEstante) {
		this.idEstante = idEstante;
	}

	
	
	public long getIdSucursal() {
		return idSucursal;
	}
	
	public void setIdSucursal(long idSucursal) {
		this.idSucursal = idSucursal;
	}
	
	@Override
	public String toString() {
		return "ProductoSuministrado "+ super.toStringTemporal()+", precio="+precioUnitario+", cantidadEnBodega="+cantidadEnBodega+", cantidadEstante="+cantidadEnEstante+", precioXUnidadMedida="+precioPorUnidadMedida+", idBodega="+idBodega+", idEstante="+idEstante+"]";
	}
}
