package uniandes.isis2304.superAndes.negocio;

public class UsuarioPersonal implements VOUsuarioPersonal
{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	
	/**
	 * Usuario del personal
	 */
	private String username;
	/**
	 * Contraseña del personal
	 */
	private String password;
	/**
	 * Identificador de la sucursal
	 */
	private long idSucursal;
	
	/* ****************************************************************
	 * 			Constructores
	 *****************************************************************/
	
	/**
	 * Constructor por defecto
	 */
	public UsuarioPersonal()
	{
		this.username="";
		this.password="";
		this.idSucursal=0;
	}
	/**
	 * Constructor con valores
	 * @param username - usuario del personal
	 * @param password - contraseña del personal
	 * @param idSucursal - sucursal asociada
	 */
	public UsuarioPersonal(String username, String password, long idSucursal) 
	{
		this.username = username;
		this.password = password;
		this.idSucursal = idSucursal;
	}
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/

	public String getUsername() {
		return username;
	}
	
	/**
	 * @param username - nuevo usuario del personal
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	
	/**
	 * @param password - nueva contraseña del personal
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	public long getIdSucursal() {
		return idSucursal;
	}
	/**
	 * @param idSucursal - nueva sucursal asociada
	 */
	public void setIdSucursal(long idSucursal) {
		this.idSucursal = idSucursal;
	}
	/**
	 * @return Una cadena de caracteres con la información básica del usuario de tipo personal.
	 */
	@Override
	public String toString() {
		return "UsuarioPersonal [username="+username+", password="+password+"]";
	}
}
