package uniandes.isis2304.superAndes.negocio;

import java.sql.Timestamp;
/**
 * Interfaz para los métodos get de PROMOCION.
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz. 
 * 
 * @author fj.gonzalez
 */
public interface VOPromocion 
{
	/**
	 * @return El id de la promoción.
	 */
	public long getId();
	/**
	 * @return El nombre de la promoción.
	 */
	public String getNombre();
	/**
	 * @return La fecha de expedición de la promoción.
	 */
	public Timestamp getFechaExpedicion();
	/**
	 * @return La fecha de finalización de la promoción.
	 */
	public Timestamp getFechaFinalizacion();
	/**
	 * @return La fecha de fin de existencias de la promoción.
	 */
	public Timestamp getFechaFinExistencias();
	/**
	 * @return Las unidades que se venderán de la promoción.
	 */
	public Integer getUnidades();
	/**
	 * @return El código de barras del producto1.
	 */
	public String getCodigoProducto1();
	/**
	 * @return El código de barras del producto2.
	 */
	public String getCodigoProducto2();
	/**
	 * @return El tipo de la promoción.
	 */
	public Integer getTipo();
	/**
	 * @return El estado de la promoci�n
	 */
	public String getEstado();
	/**
	 * @return Una cadena de caracteres con la información básica de la promoción.
	 */
	@Override
	public String toString();

}
