package uniandes.isis2304.superAndes.negocio;

import java.sql.Timestamp;

/**
 * Clase para modelar el concepto PROMOCION PAGUE 1 Y LLEVE EL SEGUNDO CON DESCUENTO del negocio de SuperAndes
 *
 * @author fj.gonzalez
 */
public class PromocionPague1LleveElSegundo extends Promocion implements VOPromocionPague1LleveElSegundo
{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El procentaje de descuento sobre la segunda unidad del producto.
	 */
	private Double porcentajeDescuento;
	/* ****************************************************************
	 * 			Constructores
	 *****************************************************************/
	/**
	 * Constructor vacio.
	 */
	public PromocionPague1LleveElSegundo()
	{
		super();
		porcentajeDescuento = 0.0;
		this.setTipo(4);
	}
	/**
	 * Constructor con valores.
	 */
	public PromocionPague1LleveElSegundo(long pId, Timestamp pFechaExpedicion,Timestamp pFechaFinalizacion,Timestamp pFechaFinExistencias, Integer pUnidades , String pCodigoBarrasProducto1 , String pCodigoBarrasProducto2 , Integer pTipo, Double pPorc,String pEstado)
	{
		super(pId, pFechaExpedicion, pFechaFinalizacion, pFechaFinExistencias, pUnidades, pCodigoBarrasProducto1, pCodigoBarrasProducto2, pTipo, pEstado);
		porcentajeDescuento = pPorc;  
		this.setTipo(4);
	}
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	
	public long getId()
	{

		return id;
	}
	/**
	 * @param id - El nuevo id de la promoción.
	 */
	public void setId(long id) 
	{
		this.id = id;
	}
	
	@Override
	public Double getporctDescSegProducto()
	{
		// TODO Auto-generated method stub
		return porcentajeDescuento;
	}
	/**
	 * @param porcentajeDescuento - El nuevo porcentaje de descuento.
	 */
	public void setPorcentajeDescuento(Double porcentajeDescuento) 
	{
		this.porcentajeDescuento = porcentajeDescuento;
	}
	/**
	 * @return Una cadena de caracteres con la información básica de la promoción pague 1 lleve el segundo con descuento.
	 */
	@Override
	public String toString()
	{
		return "PromocionPague1LleveElSegundo : " + super.toString() + "porcentaje de descuento" + porcentajeDescuento;
		
	}



}
