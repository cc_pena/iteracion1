package uniandes.isis2304.superAndes.negocio;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


import org.apache.log4j.Logger;

import com.google.gson.JsonObject;
import uniandes.isis2304.superAndes.persistencia.PersistenciaSuperAndes;

/**
 * Clase principal del negocio
 * Satisface todos los requerimientos funcionales del negocio
 *
 * @author cc.pena-fj.gonzalez
 */
public class SuperAndes
{
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Logger para escribir la traza de la ejecución
	 */
	private static Logger log = Logger.getLogger(SuperAndes.class.getName());

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia
	 */
	private PersistenciaSuperAndes pp;

	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	/**
	 * El constructor por defecto
	 */
	public SuperAndes ()
	{
		pp = PersistenciaSuperAndes.getInstance ();
	}

	/**
	 * El constructor qye recibe los nombres de las tablas en tableConfig
	 * @param tableConfig - Objeto Json con los nombres de las tablas y de la unidad de persistencia
	 */
	public SuperAndes (JsonObject tableConfig)
	{
		pp = PersistenciaSuperAndes.getInstance (tableConfig);
	}

	/**
	 * Cierra la conexión con la base de datos (Unidad de persistencia)
	 */
	public void cerrarUnidadPersistencia ()
	{
		pp.cerrarUnidadPersistencia ();
	}
	/* ****************************************************************
	 * 			Métodos para manejar las BODEGAS
	 *****************************************************************/

	public Bodega adicionarBodega(Integer capacidadvolumetrica, long  idSucursal, Integer capacidadenmasa , String tipoProducto)
	{
		log.info ("Adicionando bodega :  " + idSucursal + "," + tipoProducto);
		Bodega bod = pp.adicionarBodega(capacidadvolumetrica, idSucursal, capacidadenmasa, tipoProducto);
		log.info ("Adicionando bodega :  " + bod);
		return bod;
	}
	/**
	 * Método que elimina una Bodega, dado el nombre del Bodega
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public long eliminarBodegaPorId(long id) 
	{
		log.info ("Eliminando bodega :  " + id);
		long ll = pp.eliminarBodegaPorId(id);
		log.info ("Eliminando bodega :  " + ll + "tuplas eliminadas");
		return ll;
	}
	/**
	 * Método que consulta todas las tuplas en la tabla Bodega que tienen el nombre dado
	 * @return La lista de objetos Bodega, construidos con base en las tuplas de la tabla Bodega
	 */
	public List<Bodega> darBodegasSucursal (long idSucursal) 
	{
		log.info ("Dando bodegas de sucursal :  " + idSucursal);
		List<Bodega> liBod = pp.darBodegasSucursal(idSucursal);
		log.info ("Dando bodegas de sucursal :  " + liBod.size() +  " número de bodegas");
		return liBod;
	}
	/**
	 * Método que consulta todas las tuplas en la tabla Bodega
	 * @return La lista de objetos Bodega, construidos con base en las tuplas de la tabla Bodega
	 */
	public List<Bodega> darBodegas ()
	{
		log.info ("Dando bodegas");
		List<Bodega> liBod = pp.darBodegas();
		log.info ("Dando bodegas" + liBod.size() +  " número de bodegas");
		return liBod;
	}
	/**
	 * Método que consulta el bodega bodega
	 * @return El objeto bodega con dicho id.
	 */
	public Bodega darBodega(long id)
	{
		log.info ("Dando bodega :  " + id);
		Bodega bod = pp.darBodega(id);
		log.info ("Dando bodega :  " + bod);
		return bod;
	}
	/**
	 * Encuentra todos los Bodegas en SuperAndes y los devuelce como VO
	 * Adiciona entradas al log de la aplicación
	 * @return Una lista de objetos Bodega con todos las bodegas que conoce la aplicación, llenos con su información básica
	 */
	public List<VOBodega> darVOBodegas ()
	{
		log.info ("Generando los VO de Bodega");
		List<VOBodega> vobodegas = new LinkedList<VOBodega> ();
		for (Bodega Bodega: pp.darBodegas())
		{
			vobodegas.add (Bodega);
		}
		log.info ("Generando los VO de bodegas: " + vobodegas.size () + " bodegas existentes");
		return vobodegas;
	}

	public void modificarCantidadEstante (String cod, long idE, int nuevaCantidadEstante, long idSucursal )
	{
		log.info ("Modificar cantidad estante");
		pp.modificarCantidadEstante(cod, idE, nuevaCantidadEstante, idSucursal);
		log.info ("Modificado cantidad estante ");

	}

	public void modificarCantidadBodega(String cod, long idE, int nuevaCantidadBodega, long idSucursal)
	{
		log.info ("Modificar cantidad bodega");
		pp.modificarCantidadBodega(cod, idE, nuevaCantidadBodega, idSucursal);
		log.info ("Modificado cantidad bodega ");
	}
	/* ****************************************************************
	 * 			Métodos para manejar las CATEGORIA PRODUCTO
	 *****************************************************************/
	public CategoriaProducto adicionarCategoriaProducto(String nombre)
	{
		log.info ("Adicionando CategoriaProducto :  " + nombre);
		CategoriaProducto catego = pp.adicionarCategoriaProducto(nombre);
		log.info ("Adicionando CategoriaProducto :  " + catego);
		return catego;
	}
	/**
	 * Método que elimina una CategoriaProducto, dado el nombre del CategoriaProducto
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public long eliminarCategoriaProductoPorNombre(String nombre) 
	{
		log.info ("Eliminando CategoriaProducto :  " + nombre);
		long ll = pp.eliminarCategoriaProducto(nombre);
		log.info ("Eliminando CategoriaProducto :  " + ll + "tuplas eliminadas");
		return ll;
	}
	/**
	 * Método que consulta todas las tuplas en la tabla CategoriaProducto
	 * @return La lista de objetos CategoriaProducto, construidos con base en las tuplas de la tabla CategoriaProducto
	 */
	public List<CategoriaProducto> darCategoriaProductos ()
	{
		log.info ("Dando CategoriaProductos");
		List<CategoriaProducto> licatego = pp.darCategorias();
		log.info ("Dando CategoriaProductos" + licatego.size() +  " número de CategoriaProductos");
		return licatego;
	}
	/**
	 * Método que consulta el CategoriaProducto CategoriaProducto
	 * @return El objeto CategoriaProducto con dicho id.
	 */
	public CategoriaProducto darCategoriaProducto(String nombre)
	{
		log.info ("Dando CategoriaProducto :  " + nombre);
		CategoriaProducto catego = pp.darCategoria(nombre);
		log.info ("Dando CategoriaProducto :  " + catego);
		return catego;
	}
	/**
	 * Encuentra todos los categorias en SuperAndes y los devuelce como VO
	 * Adiciona entradas al log de la aplicación
	 * @return Una lista de objetos categoria con todos las categorias que conoce la aplicación, llenos con su información básica
	 */
	public List<VOCategoria> darVOCategorias ()
	{
		log.info ("Generando los VO de Categoria");
		List<VOCategoria> vocategorias = new LinkedList<VOCategoria> ();
		for (CategoriaProducto Categ: pp.darCategorias())
		{
			vocategorias.add (Categ);
		}
		log.info ("Generando los VO de categorias: " + vocategorias.size () + " categorias existentes");
		return vocategorias;
	}
	/* ****************************************************************
	 * 			Métodos para manejar las CLIENTE
	 *****************************************************************/
	/**
	 * Método que elimina una Cliente, dado el id del Cliente
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public long[] eliminarClientePorId(long id) 
	{
		log.info ("Eliminando Cliente :  " + id);
		long[] ll = pp.eliminarCliente(id);
		log.info ("Eliminando Cliente :  " );
		return ll;
	}
	/**
	 * Método que consulta todas las tuplas en la tabla Cliente
	 * @return La lista de objetos Cliente, construidos con base en las tuplas de la tabla Cliente
	 */
	public List<Cliente> darClientes ()
	{
		log.info ("Dando Clientes");
		List<Cliente> licatego = pp.darClientes();
		log.info ("Dando Clientes" + licatego.size() +  " número de Clientes");
		return licatego;
	}
	/**
	 * Método que elimina un Cliente completo, dado el id del Cliente
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public long[] eliminarClienteConSusTransacciones(long id) 
	{
		log.info ("Eliminando Cliente :  " + id);
		long[] ll = pp.eliminarClienteConSusTransacciones(id);
		log.info ("Eliminando Cliente : " );
		return ll;
	}
	/**
	 * Método que cambia los puntos de un cliente, .
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas cambiadas. -1 si ocurre alguna Excepción
	 */
	public long sumarPuntosACliente(long id, int nuevosPuntos) 
	{
		log.info ("Sumando puntos al cliente :  " + id);
		long ll = pp.sumarPuntosACliente(id, nuevosPuntos);
		log.info ("Sumando puntos al cliente :  " + ll + "tuplas cambiadas");
		return ll;
	}
	/**
	 * Encuentra todos los clientes en SuperAndes y los devuelce como VO
	 * Adiciona entradas al log de la aplicación
	 * @return Una lista de objetos cliente con todos las clientes que conoce la aplicación, llenos con su información básica
	 */
	public List<VOCliente> darVOClientes ()
	{
		log.info ("Generando los VO de Cliente");
		List<VOCliente> voclientes = new LinkedList<VOCliente> ();
		for (Cliente Categ: pp.darClientes())
		{
			voclientes.add (Categ);
		}
		log.info ("Generando los VO de clientes: " + voclientes.size () + " clientes existentes");
		return voclientes;
	}

	/* ****************************************************************
	 * 			Métodos para manejar las CLIENTE EMPRESA
	 *****************************************************************/
	public ClienteEmpresa adicionarClienteEmpresa(int puntos, String nit, String direccion)
	{
		log.info ("Adicionando ClienteEmpresa :  " +nit);
		ClienteEmpresa clienEmp = pp.adicionarClienteEmpresa(puntos, nit, direccion);
		log.info ("Adicionando ClienteEmpresa :  " + clienEmp);
		return clienEmp;
	}
	/**
	 * Método que elimina una ClienteEmpresa, dado el nombre del ClienteEmpresa
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public long[] eliminarClienteEmpresaPorId(long id) 
	{
		log.info ("Eliminando ClienteEmpresa :  " + id);
		long[] ll = pp.eliminarClienteEmpresa(id);
		log.info ("Eliminando ClienteEmpresa :  " + ll + "tuplas eliminadas");
		return ll;
	}
	/**
	 * Método que elimina, , una tupla en la tabla Cliente empresa, dado el nit de la empresa
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public long eliminarClientePorNit (String nit) 
	{
		log.info ("Eliminando ClienteEmpresa :  " + nit);
		long ll = pp.eliminarClienteEmpresaPorNIT(nit);
		log.info ("Eliminando ClienteEmpresa :  " + ll + "tuplas eliminadas");
		return ll;
	}
	/**
	 * Método que consulta todas las tuplas en la tabla ClienteEmpresa
	 * @return La lista de objetos ClienteEmpresa, construidos con base en las tuplas de la tabla ClienteEmpresa
	 */
	public List<ClienteEmpresa> darClienteEmpresas ()
	{
		log.info ("Dando ClienteEmpresas");
		List<ClienteEmpresa> liclienEmp = pp.darClientesEmpresa();
		log.info ("Dando ClienteEmpresas" + liclienEmp.size() +  " número de ClienteEmpresas");
		return liclienEmp;
	}
	/**
	 * Método que consulta el ClienteEmpresa ClienteEmpresa
	 * @return El objeto ClienteEmpresa con dicho id.
	 */
	public ClienteEmpresa darClienteEmpresa(String nit)
	{
		log.info ("Dando ClienteEmpresa :  " + nit);
		ClienteEmpresa clienEmp = pp.darClienteEmpresa(nit);
		log.info ("Dando ClienteEmpresa :  " + clienEmp);
		return clienEmp;
	}
	/* ****************************************************************
	 * 			Métodos para manejar las CLIENTE NATURAL
	 *****************************************************************/
	public ClienteNatural adicionarClienteNatural(int puntos,  String cedula, String nombre, String email)
	{
		log.info ("Adicionando ClienteNatural :  " +cedula);
		ClienteNatural clienEmp = pp.adicionarClienteNatural(puntos, cedula, nombre, email);
		log.info ("Adicionando ClienteNatural :  " + clienEmp);
		return clienEmp;
	}
	/**
	 * Método que elimina una ClienteNatural, dado el nombre del ClienteNatural
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public long[] eliminarClienteNaturalPorId(long id) 
	{
		log.info ("Eliminando ClienteNatural :  " + id);
		long[] ll = pp.eliminarClienteNatural(id);
		log.info ("Eliminando ClienteNatural :  " + ll + "tuplas eliminadas");
		return ll;
	}
	/**
	 * Método que elimina, , una tupla en la tabla Cliente Natural, dado el nit de la Natural
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public long eliminarClientePorCedula (String cedula) 
	{
		log.info ("Eliminando ClienteNatural :  " + cedula);
		long ll = pp.eliminarClienteNaturalPorCedula(cedula);
		log.info ("Eliminando ClienteNatural :  " + ll + "tuplas eliminadas");
		return ll;
	}
	/**
	 * Método que consulta todas las tuplas en la tabla ClienteNatural
	 * @return La lista de objetos ClienteNatural, construidos con base en las tuplas de la tabla ClienteNatural
	 */
	public List<ClienteNatural> darClienteNatural ()
	{
		log.info ("Dando ClienteNatural");
		List<ClienteNatural> liclienEmp = pp.darClientesNatural();
		log.info ("Dando ClienteNatural" + liclienEmp.size() +  " número de ClienteNaturals");
		return liclienEmp;
	}
	/**
	 * Método que consulta el ClienteNatural ClienteNatural
	 * @return El objeto ClienteNatural con dicho id.
	 */
	public ClienteNatural darClienteNatural(String cedula)
	{
		log.info ("Dando ClienteNatural :  " + cedula);
		ClienteNatural clienEmp = pp.darClienteNatural(cedula);
		log.info ("Dando ClienteNatural :  " + clienEmp);
		return clienEmp;
	}
	/* ****************************************************************
	 * 			Métodos para manejar las COMPRAS
	 *****************************************************************/
	public Object[] adicionarCompra( String codigoP, long idTrans, Integer cantidad, long idSucursal)
	{
		log.info ("Adicionando Compra :  " + idTrans +" , "+ codigoP);
		Object[] o =  pp.adicionarCompra(codigoP, idTrans, cantidad, idSucursal);
		log.info ("Adicionando Compra :  " + o[0]);
		return o;
	}
	/**
	 * Método que elimina una Compra, dado el nombre del Compra
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public long eliminarCompraPorId(String codigoP, long idTrans) 
	{
		log.info ("Eliminando Compra :  " + idTrans +" , "+ codigoP);
		long ll = pp.eliminarCompra(codigoP, idTrans);
		log.info ("Eliminando Compra :  " + ll + "tuplas eliminadas");
		return ll;
	}
	/**
	 * Método que consulta todas las tuplas en la tabla Compra
	 * @return La lista de objetos Compra, construidos con base en las tuplas de la tabla Compra
	 */
	public List<Compra> darCompras ()
	{
		log.info ("Dando Compras");
		List<Compra> liBod = pp.darCompras();
		log.info ("Dando Compras" + liBod.size() +  " número de Compras");
		return liBod;
	}
	/**
	 * Encuentra todos los Compras en SuperAndes y los devuelce como VO
	 * Adiciona entradas al log de la aplicación
	 * @return Una lista de objetos Compra con todos las Compras que conoce la aplicación, llenos con su información básica
	 */
	public List<VOCompran> darVOCompras ()
	{
		log.info ("Generando los VO de Compra");
		List<VOCompran> voCompras = new LinkedList<VOCompran> ();
		for (Compra Categ: pp.darCompras())
		{
			voCompras.add (Categ);
		}
		log.info ("Generando los VO de Compras: " + voCompras.size () + " Compras existentes");
		return voCompras;
	}
	/* ****************************************************************
	 * 			Métodos para manejar las Estantes
	 *****************************************************************/

	public Estante adicionarEstante(Integer capacidadvolumetrica, long  idSucursal, Integer capacidadenmasa , String tipoProducto)
	{
		log.info ("Adicionando Estante :  " + idSucursal + "," + tipoProducto);
		Estante bod = pp.adicionarEstante(capacidadvolumetrica, idSucursal, capacidadenmasa, tipoProducto);
		log.info ("Adicionando Estante :  " + bod);
		return bod;
	}
	/**
	 * Método que elimina una Estante, dado el nombre del Estante
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public long eliminarEstantePorId(long id) 
	{
		log.info ("Eliminando Estante :  " + id);
		long ll = pp.eliminarEstantePorId(id);
		log.info ("Eliminando Estante :  " + ll + "tuplas eliminadas");
		return ll;
	}
	/**
	 * Método que consulta todas las tuplas en la tabla Estante que tienen el nombre dado
	 * @return La lista de objetos Estante, construidos con base en las tuplas de la tabla Estante
	 */
	public List<Estante> darEstantesSucursal (long idSucursal) 
	{
		log.info ("Dando Estantes de sucursal :  " + idSucursal);
		List<Estante> liBod = pp.darEstantesSucursal(idSucursal);
		log.info ("Dando Estantes de sucursal :  " + liBod.size() +  " número de Estantes");
		return liBod;
	}
	/**
	 * Método que consulta todas las tuplas en la tabla Estante
	 * @return La lista de objetos Estante, construidos con base en las tuplas de la tabla Estante
	 */
	public List<Estante> darEstantes ()
	{
		log.info ("Dando Estantes");
		List<Estante> liBod = pp.darEstantes();
		log.info ("Dando Estantes" + liBod.size() +  " número de Estantes");
		return liBod;
	}
	/**
	 * Método que consulta el Estante Estante
	 * @return El objeto Estante con dicho id.
	 */
	public Estante darEstante(long id)
	{
		log.info ("Dando Estante :  " + id);
		Estante bod = pp.darEstante(id);
		log.info ("Dando Estante :  " + bod);
		return bod;
	}
	/* ****************************************************************
	 * 			Métodos para manejar los INFO PRODUCTO
	 *****************************************************************/
	/**
	 * Método que inserta, , una tupla en la tabla InfoProducto
	 * @return El objeto InfoProducto adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public InfoProducto adicionarInfoProducto(double precioUnitario,  int cantidad , String precioxUnidadMedida, double calidadProducto, long idOrden , String codigoproducto, String entregado) 
	{
		log.info ("Adicionando InfoProducto :  " + idOrden +"," + codigoproducto);
		InfoProducto infoProducto = pp.adicionarInfoProducto(precioUnitario, cantidad, precioxUnidadMedida, calidadProducto, idOrden, codigoproducto);
		log.info ("Adicionando InfoProducto :  " + infoProducto);
		return infoProducto;
	}
	/* ****************************************************************
	 * 			Métodos para manejar los ORDENES PEDIDO
	 *****************************************************************/
	/**
	 * Método que inserta, , una tupla en la tabla OrdenPedido
	 * @return El objeto OrdenPedido adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public OrdenPedido adicionarOrdenPedido(Timestamp fechaEntregaEstipulada, String estado, double calificacion, String nit,  long idsucursal, Timestamp fechaentregareal) 
	{
		log.info ("Adicionando OrdenPedido :  " + nit + "," + idsucursal);
		OrdenPedido ordenPedi = pp.adicionarOrdenPedido(fechaEntregaEstipulada, estado, calificacion, nit, idsucursal, fechaentregareal);
		log.info ("Adicionando OrdenPedido :  " + ordenPedi);
		return ordenPedi;
	}

	/**
	 * Método que elimina, , una tupla en la tabla ordenPedido, dado el nombre de la ordenPedido.
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public long eliminarOrdenPedido( long id) 
	{
		log.info ("Eliminando OrdenPedido :  " + id);
		long ll = pp.eliminarOrdenPedido(id);
		log.info ("Eliminando OrdenPedido :  " + ll + "tuplas eliminadas");
		return ll;
	}
	/**
	 * Método que elimina, , una tupla en la tabla ordenPedido, dado el nombre de la ordenPedido.
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public long eliminarOrdenesPedido( long idSucursal, String nitproveedor) 
	{
		log.info ("Eliminando OrdenPedido :  " + idSucursal +" , "+ nitproveedor);
		long ll = pp.eliminarOrdenesPedido(idSucursal, nitproveedor);
		log.info ("Eliminando OrdenPedido :  " + ll + "tuplas eliminadas");
		return ll;
	}	
	/**
	 * Método que consulta todas las tuplas en la tabla OrdenPedido
	 * @return La lista de objetos OrdenPedido, construidos con base en las tuplas de la tabla Bodega.
	 */
	public List<OrdenPedido> darOrdenesPedido ()
	{
		log.info ("Dando OrdenesPedido");
		List<OrdenPedido> liBod = pp.darOrdenesPedido();
		log.info ("Dando OrdenesPedido" + liBod.size() +  " número de OrdenesPedido");
		return liBod;
	}
	/**
	 * Método que consulta todas las tuplas en la tabla OrdenPedido
	 * @return La lista de objetos OrdenPedido, construidos con base en las tuplas de la tabla Bodega.
	 */
	public List<OrdenPedido> darOrdenesPedido (String nitproveedor, long idSucursal)
	{
		log.info ("Dando OrdenesPedido"+ idSucursal +" , "+ nitproveedor);
		List<OrdenPedido> liBod = pp.darOrdenesPedido(nitproveedor, idSucursal);
		log.info ("Dando OrdenesPedido" + liBod.size() +  " número de OrdenesPedido");
		return liBod;
	}
	/**
	 * Metodo que consulta todas las tuplas en la tabla OrdenPedido
	 * @return La lista de objetos OrdenPedido, construidos con base en las tuplas de la tabla Bodega.
	 */
	public List<OrdenPedido> darOrdenesPedidoProveedor(String nitproveedor)
	{
		log.info ("Dando OrdenesPedido "+ nitproveedor);
		List<OrdenPedido> liBod = pp.darOrderPedidoProveedor(nitproveedor);
		log.info ("Dando OrdenesPedido" + liBod.size() +  " número de OrdenesPedido");
		return liBod;
	}
	/**
	 * Método que consulta todas las tuplas en la tabla OrdenPedido
	 * @return La lista de objetos OrdenPedido, construidos con base en las tuplas de la tabla Bodega.
	 */
	public OrdenPedido darOrdenPedido (long id)
	{
		log.info ("Dando OrdenPedido :  " + id);
		OrdenPedido bod = pp.darOrdenPedido(id);
		log.info ("Dando OrdenPedido :  " + bod);
		return bod;
	}
	/**
	 * Método que consulta todas las tuplas en la tabla OrdenPedido
	 * @return La lista de objetos OrdenPedido, construidos con base en las tuplas de la tabla Bodega.
	 */
	public void modificarOrdenPedidoEstado(long id , String nuevoEstado)
	{
		log.info ("Modificar estado orden pedido:  " + id);
		pp.modificarEstado(id, nuevoEstado);
	}
	/**
	 * Encuentra todos los OrdenPedidos en SuperAndes y los devuelce como VO
	 * Adiciona entradas al log de la aplicación
	 * @return Una lista de objetos OrdenPedido con todos las OrdenPedidos que conoce la aplicación, llenos con su información básica
	 */
	public List<VOOrdenPedido> darVOOrdenesPedido ()
	{
		log.info ("Generando los VO de OrdenPedido");
		List<VOOrdenPedido> voOrdenPedidos = new LinkedList<VOOrdenPedido> ();
		for (OrdenPedido orden: pp.darOrdenesPedido())
		{
			voOrdenPedidos.add (orden);
		}
		log.info ("Generando los VO de OrdenPedidos: " + voOrdenPedidos.size () + " OrdenPedidos existentes");
		return voOrdenPedidos;
	}
	/* ****************************************************************
	 * 			Métodos para manejar los PRODUCTO
	 *****************************************************************/
	/**
	 * Método que inserta, , una tupla en la tabla PRODUCTO
	 * @return El objeto PRODUCTO adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public Producto adicionarProducto( String marca, String nombre, String  presentacion, String unidaddemedida,String volumen,String peso,String tipo,Integer cantidadpresentacion) 
	{
		log.info ("Adicionando Producto :  " + nombre);
		Producto prod = pp.adicionarProducto(marca, nombre, presentacion, unidaddemedida, volumen, peso, tipo, cantidadpresentacion);
		log.info ("Adicionando Producto :  " + prod);
		return prod;
	}
	/**
	 * Método que elimina, , una tupla en la tabla PRODUCTO
	 * @return El numero de tuplas eliminadas. null si ocurre alguna Excepción o si no se pudo eliminar.
	 */
	public long eliminarProducto(String cod) 
	{
		log.info ("Eliminando Producto :  " + cod);
		long ll = pp.eliminarProducto(cod);
		log.info ("Eliminando Producto :  " + ll + "tuplas eliminadas");
		return ll;
	}
	/**
	 * Método que retorna, , una tupla en la tabla Producto
	 * @return El producto con nit dada. null si ocurre alguna Excepción o si no se pudo encontrar.
	 */
	public Producto darProducto(String cod) 
	{
		log.info ("Dando Producto :  " + cod);
		Producto prod = pp.darProducto(cod);
		log.info ("Dando Producto :  " + prod);
		return prod;
	}
	/**
	 * Método que retorna, , una tupla en la tabla Producto
	 * @return El producto con nit dada. null si ocurre alguna Excepción o si no se pudo encontrar.
	 */
	public List<Producto> darProductoPorNombre(String nombre) 
	{
		log.info ("Dando ProductoPorNombre");
		List<Producto> listaProductos = pp.darProductoPorNombre(nombre);
		log.info ("Dando ProductoPorNombre" + listaProductos.size() +  " número de ProductoPorNombre");
		return listaProductos;
	}
	/**
	 * Método que retorna, , una tupla en la tabla Producto
	 * @return El producto con nit dada. null si ocurre alguna Excepción o si no se pudo encontrar.
	 */
	public List<Object[]> darProductosPorTipo(String pTipo) 
	{
		log.info ("Dando ProductoPorTipo");
		List<Object[]> listaProductos = pp.darProductoPorTipo(pTipo);
		log.info ("Dando ProductoPorTipo" + listaProductos.size() +  " número de ProductoPorTipo");
		return listaProductos;
	}
	/**
	 * Método que retorna, , una tupla en la tabla Producto
	 * @return El producto con nit dada. null si ocurre alguna Excepción o si no se pudo encontrar.
	 */
	public List<Producto> darProductosPorTipo2(String pTipo) 
	{
		log.info ("Dando ProductoPorTipo2");
		List<Producto> listaProductos = pp.darProductoPorTipo2(pTipo);
		log.info ("Dando ProductoPorTipo2" + listaProductos.size() +  " número de ProductoPorTipo2");
		return listaProductos;
	}
	/**
	 * Método que elimina, , una tupla en la tabla Producto, dado el id del Producto
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public long[] eliminarProductoConSusInfoProducto(String cod) 
	{
		log.info ("Eliminando ProductoConSusInfoProducto :  " + cod);
		long[] ll = pp.eliminarProductoConSusInfoProducto(cod);
		log.info ("Eliminando ProductoConSusInfoProducto :  " + ll + "tuplas eliminadas");
		return ll;
	}
	/* ****************************************************************
	 * 			Métodos para manejar los PRODUCTOS DEL PROVEEDOR
	 *****************************************************************/
	/**
	 * Método que inserta, , una tupla en la tabla PRODUCTO
	 * @return El objeto ProductoProveedor adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public ProductosDelProveedor adicionarProductoProveedor( String codigoproducto, String nitproveedor) 
	{
		log.info ("Adicionando ProductosDelProveedor :  " + codigoproducto + ", "+ nitproveedor);
		ProductosDelProveedor prod = pp.adicionarProductoProveedor(codigoproducto, nitproveedor);
		log.info ("Adicionando ProductosDelProveedor :  " + prod);
		return prod;
	}

	/**
	 * Método que elimina, , una tupla en la tabla ProductoProveedor, dado el nombre del ProductoProveedor
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public long eliminarProveedoresDeUnProducto(String codigoBarras) 
	{
		log.info ("Eliminando proveedores de un producto :  " + codigoBarras);
		long ll = pp.eliminarProveedoresDeUnProducto(codigoBarras);
		log.info ("Eliminando proveedores de un producto :  " + ll + "tuplas eliminadas");
		return ll;
	}

	/**
	 * Método que elimina, , una tupla en la tabla ProductoProveedor, dado el nombre del ProductoProveedor
	 * Adiciona entradas al log de la aplicación
	 * @return El número de tuplas eliminadas. -1 si ocurre alguna Excepción
	 */
	public long eliminarProductosDeUnProveedor(String nit) 
	{
		log.info ("Eliminando productos de un proveedor :  " + nit);
		long ll = pp.eliminarProveedoresDeUnProducto(nit);
		log.info ("Eliminando productos de un proveedor :  " + ll + "tuplas eliminadas");
		return ll;
	}
	/**
	 * Método que consulta todas las tuplas en la tabla ProductosDelProveedor
	 * @return La lista de objetos ProductosDelProveedor, construidos con base en las tuplas de la tabla ProductosDelProveedor
	 */
	public List<ProductosDelProveedor> darProductosProveedor ()
	{
		log.info ("Dando ProductosProveedor");
		List<ProductosDelProveedor> liProDpORV = pp.darProductosProveedor();
		log.info ("Dando ProductosProveedor" + liProDpORV.size() +  " número de ProductosProveedor");
		return liProDpORV;
	}
	/**
	 * Encuentra todos los Productos en SuperAndes y los devuelce como VO
	 * Adiciona entradas al log de la aplicación
	 * @return Una lista de objetos Producto con todos las Productos que conoce la aplicación, llenos con su información básica
	 */
	public List<VOProductosDelProveedor> darVOProductosProveedor ()
	{
		log.info ("Generando los VO de Producto");
		List<VOProductosDelProveedor> voProducProvee = new LinkedList<VOProductosDelProveedor> ();
		for (ProductosDelProveedor produProve: pp.darProductosProveedor())
		{
			voProducProvee.add (produProve);
		}
		log.info ("Generando los VO de Productos: " + voProducProvee.size () + " Productos existentes");
		return voProducProvee;
	}
	/* ****************************************************************
	 * 			Métodos para manejar los PRODUCTO SUMINISTRADO
	 *****************************************************************/

	public ProductoSuministrado adicionarProductoSum(double precioUnitario , int cantidadenbodega , String precioporunidadmedida , String codigobarras , int cantidadenestante , long idestante , long idbodega , long idSucursal)
	{
		ProductoSuministrado ps = pp.adicionarProductoSuministrado(precioUnitario, cantidadenbodega, precioporunidadmedida, codigobarras, cantidadenestante, idestante, idbodega, idSucursal);
		return ps;
	}
	/**
	 * Método que inserta, , una tupla en la tabla PRODUCTO
	 * @return El objeto BODEGA adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public ProductoSuministrado adicionarProductoSuministradoPorOperario( double precioUnitario  , String precioporunidadmedida , String codigobarras ,  long idestante , long idbodega , long idSucursal) 
	{
		log.info ("Adicionando ProductoSuministrado :  " + codigobarras);
		ProductoSuministrado prod = pp.adicionarProductoSuministradoPorOperario(precioUnitario, precioporunidadmedida, codigobarras, idestante, idbodega,  idSucursal);
		log.info ("Adicionando ProductoSuministrado :  " + prod);
		return prod;
	}
	/**
	 * Método que consulta todas las tuplas en la tabla ProductoSuministrado
	 * @return La lista de objetos ProductoSuministrado, construidos con base en las tuplas de la tabla Bodega.
	 */
	public List<ProductoSuministrado> darProductosSuministrado ()
	{
		log.info ("Dando ProductosSuministrado");
		List<ProductoSuministrado> liProdSum = pp.darProductosSuministrado();
		log.info ("Dando ProductosSuministrado" + liProdSum.size() +  " número de ProductosSuministrado");
		return liProdSum;
	}

	/**
	 * Método que consulta todas las tuplas en la tabla ProductoSuministrado con idSucursal y tipo especificos
	 * @return La lista de objetos ProductoSuministrado, construidos con base en las tuplas de la tabla Bodega.
	 */
	public List<ProductoSuministrado> darProductosSuministradosSucursalTipo (long idSucursal, String pTipo)
	{
		log.info ("Dando ProductosSuministrado");
		List<ProductoSuministrado> liProdSum = pp.darProductosSuministradosSucursalTipo(idSucursal, pTipo);
		log.info ("Dando ProductosSuministrado" + liProdSum.size() +  " número de ProductosSuministrado");
		return liProdSum;
	}

	/**
	 * Método que retorna, , una tupla en la tabla PRODUCTO SUMINISTRADO
	 * @return El producto suministrado con nit dada. null si ocurre alguna Excepción o si no se pudo encontrar.
	 */
	public ProductoSuministrado darProdutoSuministrado(String cod, long idSucursal) 
	{
		log.info ("Dando ProductoSuministrado :  " + cod);
		ProductoSuministrado prodSum = pp.darProdutoSuministrado(cod, idSucursal);
		log.info ("Dando ProductoSuministrado :  " + prodSum);
		return prodSum;
	}

	/**
	 * Encuentra todos los Productos en SuperAndes y los devuelce como VO
	 * Adiciona entradas al log de la aplicación
	 * @return Una lista de objetos Producto con todos las Productos que conoce la aplicación, llenos con su información básica
	 */
	public List<VOProductoSuministrado> darVOProductoSuministrado ()
	{
		log.info ("Generando los VO de Producto");
		List<VOProductoSuministrado> voProductoSumi = new LinkedList<VOProductoSuministrado> ();
		for (ProductoSuministrado produProve: pp.darProductosSuministrado())
		{
			voProductoSumi.add (produProve);
		}
		log.info ("Generando los VO de Productos: " + voProductoSumi.size () + " Productos existentes");
		return voProductoSumi;
	}
	/* ****************************************************************
	 * 			Métodos para manejar los PROMOCION
	 *****************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla PROMOCION
	 * @return El objeto PROMOCION adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public Promocion adicionarPromocion(Timestamp fechaexpedicion, Timestamp fechafinalizacion, String codigoproducto1, String codigoproducto2, Integer unidades, Integer tipo, String nombre) 
	{

		log.info ("Adicionando Promocion :  " + codigoproducto1);
		Promocion prom = pp.adicionarPromocion(fechaexpedicion, fechafinalizacion, codigoproducto1, codigoproducto2, unidades, tipo, nombre);
		log.info ("Adicionando Promocion :  " + prom);
		return prom;
	}

	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla PROMOCION
	 * @return El objeto PROMOCION eliminado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public long[] eliminarPromocion(long id) 
	{
		log.info ("Eliminando Promocion :  " + id);
		long[] ll = pp.eliminarPromocion(id);
		log.info ("Eliminando Promocion :  " + ll + "tuplas eliminadas");
		return ll;
	}

	/**
	 * Método que retorna todas las promociones
	 * @return Lista de todas las promociones. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public List<Promocion> darPromociones()
	{
		log.info ("Dando Promociones");
		List<Promocion> liProdSum = pp.darPromociones();
		log.info ("Dando Promociones" + liProdSum.size() +  " número de Promociones");
		return liProdSum;
	}

	/**
	 * Método que retorna todas las promociones de un tipo especifico
	 * @return Lista de todas las promociones de un tipo especifico. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public List<Promocion> darPromocionesDeUnTipo(long tipo)
	{
		log.info ("Dando Promociones");
		List<Promocion> liProdSum = pp.darPromocionesDeUnTipo(tipo);
		log.info ("Dando Promociones" + liProdSum.size() +  " número de Promociones");
		return liProdSum;
	}

	/**
	 * Método que retorna la promocion con un id especifico
	 * @return Promocion con un id especifico. null si ocurre alguna Excepción o si no se pudo obtener.
	 */
	public Promocion darPromocion(long id)
	{
		log.info ("Dando Promocion :  " + id);
		Promocion prom = pp.darPromocion(id);
		log.info ("Dando Promocion :  " + prom);
		return prom;
	}

	/**
	 * Método que registra el fin de una promocion
	 * @return Cantidad de tuplas alteradas. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public long[] acabarPromocion(Timestamp fin, long id)
	{
		log.info ("Acabando Promocion :  " + id);
		long[] ll = pp.acabarPromocion(fin, id);
		log.info ("Acabando Promocion :  " + ll + "tuplas eliminadas");
		return ll;
	}
	/**
	 * Encuentra todos los promocions en SuperAndes y los devuelce como VO
	 * Adiciona entradas al log de la aplicación
	 * @return Una lista de objetos promocion con todos las promocions que conoce la aplicación, llenos con su información básica
	 */
	public List<VOPromocion> darVOPromociones ()
	{
		log.info ("Generando los VO de promocion");
		List<VOPromocion> vopromocionSumi = new LinkedList<VOPromocion> ();
		for (Promocion produProve: pp.darPromociones())
		{
			vopromocionSumi.add (produProve);
		}
		log.info ("Generando los VO de promocions: " + vopromocionSumi.size () + " promocions existentes");
		return vopromocionSumi;
	}
	/* ****************************************************************
	 * 			Métodos para manejar los PROMOCION DESCUENTO
	 *****************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla PROMOCIONDESCUENTO
	 * @return El objeto PROMOCIONDESCUENTO adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public PromocionDescuento adicionarPromocionDescuento(Timestamp fechaexpedicion, Timestamp fechafinalizacion, String codigoproducto1, String codigoproducto2, Integer unidades, Integer tipo, String nombre, double porcentajedescuento, long idSucursal) 
	{

		log.info ("Adicionando PromocionDescuentoDescuento :  " + codigoproducto1);
		PromocionDescuento prom = pp.adicionarPromocionDescuento(fechaexpedicion, fechafinalizacion, codigoproducto1, codigoproducto2, unidades, tipo, nombre, porcentajedescuento);
		log.info ("Adicionando PromocionDescuentoDescuento :  " + prom);
		return prom;
	}
	/**
	 * Método que retorna todas las promociones descuento
	 * @return Lista de todas las promociones descuento. null si ocurre alguna Excepción.
	 */
	public List<PromocionDescuento> darPromocionesDescuento()
	{
		log.info ("Dando PromocionesDescuento");
		List<PromocionDescuento> listaPromDesc = pp.darPromocionesDescuento();
		log.info ("Dando PromocionesDescuento" + listaPromDesc.size() +  " número de PromocionesDescuento");
		return listaPromDesc;
	}

	/**
	 * Método que retorna la promocion descuento con un id especifico
	 * @return Promocion descuento con un id especifico. null si ocurre alguna Excepción o si no se pudo obtener.
	 */
	public PromocionDescuento darPromocionDescuento(long id)
	{
		log.info ("Dando PromocionDescuento");
		PromocionDescuento prodDesc = pp.darPromocionDescuento(id);
		log.info ("Dando PromocionDescuento" + prodDesc +  " número de PromocionDescuento");
		return prodDesc;
	}

	/* ****************************************************************
	 * 			Métodos para manejar los PROMOCION DOS PRODUCTOS
	 *****************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla PROMOCIONDOSPRODUCTOS
	 * @return El objeto PROMOCIONDOSPRODUCTOS adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public PromocionDosProductos adicionarPromocionDosProductos(Timestamp fechaexpedicion, Timestamp fechafinalizacion, String codigoproducto1, String codigoproducto2, Integer unidades, Integer tipo, String nombre, double montototal) 
	{
		log.info ("Adicionando PromocionDosProductos :  " + codigoproducto1);
		PromocionDosProductos prom2prod = pp.adicionarPromocionDosProductos(fechaexpedicion, fechafinalizacion, codigoproducto1, codigoproducto2, unidades, tipo, nombre, montototal);
		log.info ("Adicionando PromocionDosProductos :  " + prom2prod);
		return prom2prod;
	}
	/**
	 * Método que retorna todas las promociones dos productos
	 * @return Lista de todas las promociones dos productos. null si ocurre alguna Excepción.
	 */
	public List<PromocionDosProductos> darPromocionesDosProductos()
	{
		log.info ("Dando PromocionDosProductos");
		List<PromocionDosProductos> listaPromDesc = pp.darPromocionesDosProductos();
		log.info ("Dando PromocionDosProductos" + listaPromDesc.size() +  " número de PromocionesDescuento");
		return listaPromDesc;
	}

	/**
	 * Método que retorna la promocion dos productos con un id especifico
	 * @return Promocion dos productos con un id especifico. null si ocurre alguna Excepción o si no se pudo obtener.
	 */
	public PromocionDosProductos darPromocionDosProductos(long id)
	{
		log.info ("Dando PromocionDescuento");
		PromocionDosProductos prom2prod = pp.darPromocionDosProductos(id);
		log.info ("Dando PromocionDescuento" + prom2prod );
		return prom2prod;
	}

	/* *************************************************************************
	 * 			Métodos para manejar las PROMOCION PAGUE 1 LLEVE EL SEGUNDO
	 ***************************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla PROMOCIONPAGUE1LLEVEELSEGUNDO
	 * @return El objeto PROMOCIONPAGUE1LLEVEELSEGUNDO adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public PromocionPague1LleveElSegundo adicionarPromocionPague1LleveElSegundo(Timestamp fechaexpedicion, Timestamp fechafinalizacion, String codigoproducto1, String codigoproducto2, Integer unidades, Integer tipo, String nombre, double porcdescseg) 
	{
		log.info ("Adicionando PromocionPague1LleveElSegundo :  " + codigoproducto1);
		PromocionPague1LleveElSegundo prom2prod = pp.adicionarPromocionPague1LleveElSegundo(fechaexpedicion, fechafinalizacion, codigoproducto1, codigoproducto2, unidades, tipo, nombre, porcdescseg);
		log.info ("Adicionando PromocionPague1LleveElSegundo :  " + prom2prod);
		return prom2prod;
	}
	/**
	 * Método que retorna todas las promociones pague 1 y lleve el segundo
	 * @return Lista de todas las promociones pague 1 y lleve el segundo. null si ocurre alguna Excepción.
	 */
	public List<PromocionPague1LleveElSegundo> darPromocionesPague1LleveElSeg()
	{
		log.info ("Dando PromocionPague1LleveElSegundo");
		List<PromocionPague1LleveElSegundo> listaPromDesc = pp.darPromocionesPague1LleveElSeg();
		log.info ("Dando PromocionPague1LleveElSegundo" + listaPromDesc.size() +  " número de PromocionesDescuento");
		return listaPromDesc;
	}

	/**
	 * Método que retorna la promociones pague 1 y lleve el segundo con un id especifico
	 * @return promociones pague 1 y lleve el segundo con un id especifico. null si ocurre alguna Excepción o si no se pudo obtener.
	 */
	public PromocionPague1LleveElSegundo darPromocionPague1LleveElSegundo(long id)
	{
		log.info ("Dando PromocionPague1LleveElSegundo");
		PromocionPague1LleveElSegundo promPague = pp.darPromocionPague1LleveElSegundo(id);
		log.info ("Dando PromocionPague1LleveElSegundo" + promPague );
		return promPague;
	}

	/* *************************************************************************
	 * 			Métodos para manejar las PROMOCION PAGUENLLEVEMUNIDADES
	 ***************************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla PROMOCIONPAGUENLLEVEMUNIDADES
	 * @return El objeto PROMOCIONPAGUENLLEVEMUNIDADES adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public PromocionPagueNLleveMUnidades adicionarPromocionPagueNLleveMUnidades(Timestamp fechaexpedicion, Timestamp fechafinalizacion, String codigoproducto1, String codigoproducto2, Integer unidades, Integer tipo, String nombre, int unidadespagadas, int unidadesofrecidas) 
	{
		log.info ("Adicionando PromocionPagueNLleveMUnidades :  " + codigoproducto1);
		PromocionPagueNLleveMUnidades prom2prod = pp.adicionarPromocionPagueNLleveMUnidades(fechaexpedicion, fechafinalizacion, codigoproducto1, codigoproducto2, unidades, tipo, nombre, unidadespagadas, unidadesofrecidas);
		log.info ("Adicionando PromocionPagueNLleveMUnidades :  " + prom2prod);
		return prom2prod;
	}
	/**
	 * Método que retorna todas las promociones pague n y lleve m unidades
	 * @return Lista de todas las promociones pague n y lleve m unidades. null si ocurre alguna Excepción.
	 */
	public List<PromocionPagueNLleveMUnidades> darPromocionesPagueNLleveMUnidades()
	{
		log.info ("Dando PromocionPagueNLleveMUnidades");
		List<PromocionPagueNLleveMUnidades> listaPromDesc = pp.darPromocionesPagueNLleveMUnidades();
		log.info ("Dando PromocionPagueNLleveMUnidades" + listaPromDesc.size() +  " número de PromocionPagueNLleveMUnidades");
		return listaPromDesc;
	}

	/**
	 * Método que retorna la promociones pague n y lleve m unidades con un id especifico
	 * @return promociones pague n y lleve m unidades con un id especifico. null si ocurre alguna Excepción o si no se pudo obtener.
	 */
	public PromocionPagueNLleveMUnidades darPromocionPagueNLleveMUnidades(long id)
	{
		log.info ("Dando PromocionPagueNLleveMUnidades");
		PromocionPagueNLleveMUnidades promPagueNLleveM = pp.darPromocionPagueNLleveMUnidades(id);
		log.info ("Dando PromocionPagueNLleveMUnidades" + promPagueNLleveM );
		return promPagueNLleveM;
	}

	/* *************************************************************************
	 * 			Métodos para manejar las PROMOCION PAGUEXLLEVEYCANTIDAD
	 ***************************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla PROMOCIONPAGUEXLLEVEYCANTIDAD
	 * @return El objeto PROMOCIONPAGUEXLLEVEYCANTIDAD adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public PromocionPagueXLleveYCantidad adicionarPromocionPagueXLleveYCantidad(Timestamp fechaexpedicion, Timestamp fechafinalizacion, String codigoproducto1, String codigoproducto2, Integer unidades, Integer tipo, String nombre, int cantidadpagada, int cantidasofrecida) 
	{
		log.info ("Adicionando PromocionPagueXLleveYCantidad :  " + codigoproducto1);
		PromocionPagueXLleveYCantidad prom2prod = pp.adicionarPromocionPagueXLleveYCantidad(fechaexpedicion, fechafinalizacion, codigoproducto1, codigoproducto2, unidades, tipo, nombre, cantidadpagada, cantidasofrecida);
		log.info ("Adicionando PromocionPagueXLleveYCantidad :  " + prom2prod);
		return prom2prod;
	}
	/**
	 * Método que retorna todas las promociones pague x y lleve y cantidad
	 * @return Lista de todas las promociones pague x y lleve y cantidad. null si ocurre alguna Excepción.
	 */
	public List<PromocionPagueXLleveYCantidad> darPromocionesPagueXLleveYCantidad()
	{
		log.info ("Dando PromocionPagueXLleveYCantidad");
		List<PromocionPagueXLleveYCantidad> listaPromDesc = pp.darPromocionesPagueXLleveYCantidad();
		log.info ("Dando PromocionPagueXLleveYCantidad" + listaPromDesc.size() +  " número de PromocionPagueNLleveMUnidades");
		return listaPromDesc;
	}

	/**
	 * Método que retorna la promociones pague x y lleve y cantidad con un id especifico
	 * @return promociones pague x y lleve y cantidad con un id especifico. null si ocurre alguna Excepción o si no se pudo obtener.
	 */
	public PromocionPagueXLleveYCantidad darPromocionPagueXLleveYCantidad(long id)
	{
		log.info ("Dando PromocionPagueXLleveYCantidad");
		PromocionPagueXLleveYCantidad promPagueNLleveM = pp.darPromocionPagueXLleveYCantidad(id);
		log.info ("Dando PromocionPagueXLleveYCantidad" + promPagueNLleveM );
		return promPagueNLleveM;
	}


	/* ****************************************************************
	 * 			Métodos para manejar los PROVEEDOR
	 *****************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla PROVEEDOR
	 * @return El objeto PROVEEDOR adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public Proveedor adicionarProveedor(String nit, String nombre) 
	{
		log.info ("Adicionando Proveedor :  " + nit);
		Proveedor prom = pp.adicionarProveedor(nit, nombre);
		log.info ("Adicionando Proveedor :  " + prom);
		return prom;
	}

	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla PROVEEDOR
	 * @return El numero de tuplas eliminadas. null si ocurre alguna Excepción o si no se pudo eliminar.
	 */
	public long eliminarProveedor(String nit) 
	{
		log.info ("Eliminando proveedor :  " + nit);
		long ll = pp.eliminarProveedor(nit);
		log.info ("Eliminando proveedor :  " + ll + "tuplas eliminadas");
		return ll;
	}

	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla PROVEEDOR por nombre
	 * @return El numero de tuplas eliminadas. null si ocurre alguna Excepción o si no se pudo eliminar.
	 */
	public long eliminarProveedorPorNombre(String nombre) 
	{
		log.info ("Eliminando proveedor :  " + nombre);
		long ll = pp.eliminarProveedorPorNombre(nombre);
		log.info ("Eliminando proveedor :  " + ll + "tuplas eliminadas");
		return ll;
	}

	/**
	 * Método que retorna, de manera transaccional, una tupla en la tabla PROVEEDOR
	 * @return El proveedor con nit dada. null si ocurre alguna Excepción o si no se pudo encontrar.
	 */
	public Proveedor darProveedor(String nit) 
	{
		log.info ("Dando Proveedor :  " + nit);
		Proveedor prom = pp.darProveedor(nit);
		log.info ("Dando Proveedor :  " + prom);
		return prom;

	}
	/**
	 * Método que consulta todas las tuplas en la tabla Proveedor
	 * @return La lista de objetos Proveedor, construidos con base en las tuplas de la tabla Proveedor
	 */
	public List<Proveedor> darProveedores()
	{
		log.info ("Dando Proveedores");
		List<Proveedor> liProdSum = pp.darProveedores();
		log.info ("Dando Proveedores" + liProdSum.size() +  " número de Proveedores");
		return liProdSum;
	}

	/**
	 * Método que modifica la calificacion de un proveedor
	 * @return Cantidad de tuplas alteradas. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public long modificarCalificacionDelProveedor(String nit, double nuevaCalificacion)
	{
		log.info ("Modificar calificación del proveedor:  " + nit);
		return pp.modificarCalificacion(nit, nuevaCalificacion);
	}
	/**
	 * Encuentra todos los Proveedors en SuperAndes y los devuelce como VO
	 * Adiciona entradas al log de la aplicación
	 * @return Una lista de objetos Proveedor con todos las Proveedors que conoce la aplicación, llenos con su información básica
	 */
	public List<VOProveedor> darVOProveedores ()
	{
		log.info ("Generando los VO de Proveedor");
		List<VOProveedor> voProveedorSumi = new LinkedList<VOProveedor> ();
		for (Proveedor produProve: pp.darProveedores())
		{
			voProveedorSumi.add (produProve);
		}
		log.info ("Generando los VO de Proveedors: " + voProveedorSumi.size () + " Proveedors existentes");
		return voProveedorSumi;
	}
	/* ****************************************************************
	 * 			Métodos para manejar las SUCURSALES
	 *****************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla SUCURSAL
	 * @return El objeto SUCURSAL adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public Sucursal adicionarSucursal(String ciudad , String direccion , String nombre , int nivelReOrden , int nivelAbastecimiento , String nombreSupermercado) 
	{
		log.info ("Adicionando Sucursal :  " + ciudad + ", "+ direccion + ", "+ nombreSupermercado);
		Sucursal sucurs = pp.adicionarSucursal(ciudad, direccion, nombre, nivelReOrden, nivelAbastecimiento, nombreSupermercado);
		log.info ("Adicionando Sucursal :  " + sucurs);
		return sucurs;
	}

	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla SUCURSAL
	 * @return El numero de tuplas eliminadas. null si ocurre alguna Excepción o si no se pudo eliminar.
	 */
	public long eliminarSucursal(long id) 
	{
		log.info ("Eliminando sucursal :  " + id);
		long ll = pp.eliminarSucursal(id);
		log.info ("Eliminando sucursal :  " + ll + "tuplas eliminadas");
		return ll;
	}

	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla SUCURSAL por nombre
	 * @return El numero de tuplas eliminadas. null si ocurre alguna Excepción o si no se pudo eliminar.
	 */
	public long eliminarSucursalPorNombre(String nombre) 
	{
		log.info ("Eliminando sucursal :  " + nombre);
		long ll = pp.eliminarSucursalPorNombre(nombre);
		log.info ("Eliminando sucursal :  " + ll + "tuplas eliminadas");
		return ll;
	}

	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla SUCURSAL Y TODAS LAS QUE SE RELACIONEN CON LA MISMA
	 * @return El numero de tuplas eliminadas. null si ocurre alguna Excepción o si no se pudo eliminar.
	 */
	public long[] eliminarSucursalCompleto(long id) 
	{
		log.info ("Eliminando SucursalCompleto :  " + id);
		long[] ll = pp.eliminarSucursalCompleto(id);
		log.info ("Eliminando SucursalCompleto :  " + ll + "tuplas eliminadas");
		return ll;
	}

	/**
	 * Método que retorna, de manera transaccional, una tupla en la tabla SUCURSAL
	 * @return La sucursal con el id dado. null si ocurre alguna Excepción o si no se pudo eliminar.
	 */
	public Sucursal darSucursal(long id) 
	{
		log.info ("Dando Sucursal :  " + id);
		Sucursal prod = pp.darSucursal(id);
		log.info ("Dando Sucursal :  " + prod);
		return prod;
	}

	/**
	 * Método que consulta todas las tuplas en la tabla Sucursal
	 * @return La lista de objetos Sucursal, construidos con base en las tuplas de la tabla Sucursal
	 */
	public List<Sucursal> darSucursales()
	{
		log.info ("Dando Sucursales");
		List<Sucursal> liSucu = pp.darSucursales();
		log.info ("Dando Sucursales" + liSucu.size() +  " número de Sucursales");
		return liSucu;
	}

	/**
	 * Método que modifica el nivel de reorden de una sucursal
	 * @return Cantidad de tuplas alteradas. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public long cambiarNivelReOrden(long id, int nivelReOrden)
	{
		log.info ("Modificar nivel de reorden:  " + id);
		return pp.cambiarNivelReOrden(id, nivelReOrden);
	}

	/**
	 * Método que modifica el nivel de abastecimiento de una sucursal
	 * @return Cantidad de tuplas alteradas. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public long cambiarNivelAbastecimiento(long id, int nivelAbastecimiento)
	{
		log.info ("Modificar nivel de abastecimiento:  " + id);
		return pp.cambiarNivelAbastecimiento(id, nivelAbastecimiento);
	}
	/**
	 * Encuentra todos los Sucursals en SuperAndes y los devuelce como VO
	 * Adiciona entradas al log de la aplicación
	 * @return Una lista de objetos Sucursal con todos las Sucursals que conoce la aplicación, llenos con su información básica
	 */
	public List<VOSucursal> darVOSucursales()
	{
		log.info ("Generando los VO de Sucursal");
		List<VOSucursal> voSucursalSumi = new LinkedList<VOSucursal> ();
		for (Sucursal produProve: pp.darSucursales())
		{
			voSucursalSumi.add (produProve);
		}
		log.info ("Generando los VO de Sucursals: " + voSucursalSumi.size () + " Sucursals existentes");
		return voSucursalSumi;
	}
	/* ****************************************************************
	 * 			Métodos para manejar las TIPO PRODUCTO
	 *****************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla TIPOPRODUCTO
	 * @return El objeto TIPOPRODUCTO adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public TipoProducto adicionarTipoProducto(String nombre, String categoria) 
	{
		log.info ("Adicionando TipoProducto :  " + nombre);
		TipoProducto tipPro = pp.adicionarTipoProducto(nombre, categoria);
		log.info ("Adicionando TipoProducto :  " + tipPro);
		return tipPro;
	}

	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla TIPOPRODUCTO
	 * @return el numero de tuplas eliminadas. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public long eliminarTipoProducto(String nombre) 
	{
		log.info ("Eliminando TipoProducto :  " + nombre);
		long ll = pp.eliminarTipoProducto(nombre);
		log.info ("Eliminando TipoProducto :  " + ll + "tuplas eliminadas");
		return ll;
	}

	/**
	 * Método que consulta todas las tuplas en la tabla TipoProducto
	 * @return La lista de objetos TipoProducto, construidos con base en las tuplas de la tabla TipoProducto
	 */
	public List<TipoProducto> darTiposProducto()
	{
		log.info ("Dando TiposProducto");
		List<TipoProducto> liTipProdu = pp.darTiposProducto();
		log.info ("Dando TiposProducto" + liTipProdu.size() +  " número de TiposProducto");
		return liTipProdu;
	}
	/**
	 * Encuentra todos los TipoProductos en SuperAndes y los devuelce como VO
	 * Adiciona entradas al log de la aplicación
	 * @return Una lista de objetos TipoProducto con todos las TipoProductos que conoce la aplicación, llenos con su información básica
	 */
	public List<VOTipoProducto> darVOTiposProducto()
	{
		log.info ("Generando los VO de TipoProducto");
		List<VOTipoProducto> voTipoProductoSumi = new LinkedList<VOTipoProducto> ();
		for (TipoProducto produProve: pp.darTiposProducto())
		{
			voTipoProductoSumi.add (produProve);
		}
		log.info ("Generando los VO de TipoProductos: " + voTipoProductoSumi.size () + " TipoProductos existentes");
		return voTipoProductoSumi;
	}
	/* ****************************************************************
	 * 			Métodos para manejar las TRANSACCION_CLIENTE
	 *****************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla TRANSACCIONCLIENTE
	 * @return El objeto TRANSACCIONCLIENTE adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public TransaccionCliente adicionarTransaccionCliente(Double monto, Timestamp fecha, long idCliente) 
	{
		log.info ("Adicionando TransaccionCliente :  " + idCliente );
		TransaccionCliente transClien = pp.adicionarTransaccionCliente(monto, fecha, idCliente);
		log.info ("Adicionando TransaccionCliente :  " + transClien);
		return transClien;
	}

	/**
	 * Método que elimina, de manera transaccional, una tupla en la tabla TRANSACCION por nombre
	 * @return El numero de tuplas eliminadas. null si ocurre alguna Excepción o si no se pudo eliminar.
	 */
	public long eliminarTransaccion(long id) 
	{
		log.info ("Eliminando Transaccion :  " + id);
		long ll = pp.eliminarTransaccion(id);
		log.info ("Eliminando Transaccion :  " + ll + "tuplas eliminadas");
		return ll;
	}

	/**
	 *  * Método que obtiene, de manera transaccional, una tupla en la tabla TRANSACCIONCLIENTE por id
	 * @return La transaccion deseada. null si ocurre alguna Excepción o si no se pudo eliminar.
	 */
	public TransaccionCliente darTransaccion(long id) 
	{
		log.info ("Dando Transaccion :  " + id);
		TransaccionCliente prod = pp.darTransaccion(id);
		log.info ("Dando Transaccion :  " + prod);
		return prod;
	}

	/**
	 * Método que obtiene, de manera transaccional, las transacciones hechas por un cliente de idCliente
	 * @return Las transacciones deseadas. null si ocurre alguna Excepción o si no se pudo eliminar.
	 */
	public List<TransaccionCliente> darTransaccionesPorCliente(long idCliente)
	{
		log.info ("Dando transacciones del cliente :  " + idCliente);
		List<TransaccionCliente> liTrans = pp.darTransaccionesPorCliente(idCliente);
		log.info ("Dando transacciones del cliente  :  " + liTrans.size() +  " número de transacciones");
		return liTrans;
	}

	/**
	 * Método que obtiene, de manera transaccional, todas las transacciones hechas
	 * @return Las transacciones deseadas. null si ocurre alguna Excepción o si no se pudo eliminar.
	 */
	public List<TransaccionCliente> darTransacciones()
	{
		log.info ("Dando Transacciones");
		List<TransaccionCliente> liTransacc = pp.darTransacciones();
		log.info ("Dando Transacciones" + liTransacc.size() +  " número de Transacciones");
		return liTransacc;
	}
	/**
	 * Encuentra todos los Transaccions en SuperAndes y los devuelce como VO
	 * Adiciona entradas al log de la aplicación
	 * @return Una lista de objetos Transaccion con todos las Transaccions que conoce la aplicación, llenos con su información básica
	 */
	public List<VOTransaccionCliente> darVOTransacciones()
	{
		log.info ("Generando los VO de Transaccion");
		List<VOTransaccionCliente> voTransaccionSumi = new LinkedList<VOTransaccionCliente> ();
		for (TransaccionCliente produProve: pp.darTransacciones())
		{
			voTransaccionSumi.add (produProve);
		}
		log.info ("Generando los VO de Transaccions: " + voTransaccionSumi.size () + " Transaccions existentes");
		return voTransaccionSumi;
	}
	/* ****************************************************************
	 * 			Métodos para manejar las USUARIO
	 *****************************************************************/
	/**
	 * Método que inserta, de manera transaccional, una tupla en la tabla USUARIO
	 * @return El objeto USUARIO adicionado. null si ocurre alguna Excepción o si no se pudo agregar.
	 */
	public UsuarioPersonal adicionarUsuario(String user, String pw, long idSucursal) 
	{
		log.info ("Adicionando UsuarioPersonal :  " + user );
		UsuarioPersonal transClien = pp.adicionarUsuario(user, pw, idSucursal);
		log.info ("Adicionando UsuarioPersonal :  " + transClien);
		return transClien;
	}

	/**
	 * Método que elimina, de manera transaccional, una tupla de la tabla USUARIO
	 * @return El objeto USUARIO eliminado. null si ocurre alguna Excepción o si no se pudo eliminar.
	 */
	public long eliminarUsuario(String user) 
	{
		log.info ("Eliminando Usuario :  " + user);
		long ll = pp.eliminarUsuario(user);
		log.info ("Eliminando Usuario :  " + ll + "tuplas eliminadas");
		return ll;
	}

	/**
	 * Método que retorna, de manera transaccional, todas las tuplas de la tabla USUARIO
	 * @return Lista de usuarios. null si ocurre alguna Excepción.
	 */
	public List<UsuarioPersonal> darUsuarios()
	{
		log.info ("Dando Usuarios");
		List<UsuarioPersonal> liUsuarios = pp.darUsuarios();
		log.info ("Dando Usuarios" + liUsuarios.size() +  " número de Usuarios");
		return liUsuarios;
	}

	/**
	 * Método que retorna, de manera transaccional, el USUARIO de la tabla que tenga el user dado por parámetro
	 * @return Lista de usuarios. null si ocurre alguna Excepción.
	 */
	public UsuarioPersonal darUsuario(String user)
	{
		log.info ("Dando Usuario :  " + user);
		UsuarioPersonal usu = pp.darUsuario(user);
		log.info ("Dando Usuario :  " + usu);
		return usu;
	}

	/**
	 * Método que modifica la contraseña de un USUARIO
	 * @return Cantidad de tuplas alteradas. null si ocurre alguna Excepción o si no se pudo cambiar.
	 */
	public long cambiarContrasena(String user, String newpw)
	{
		log.info ("Modificar contraseña del usuario:  " + user);
		return pp.cambiarContrasena(user, newpw);
	}
	/**
	 * Encuentra todos los Transaccions en SuperAndes y los devuelce como VO
	 * Adiciona entradas al log de la aplicación
	 * @return Una lista de objetos Transaccion con todos las Transaccions que conoce la aplicación, llenos con su información básica
	 */
	public List<VOUsuarioPersonal> darVOUsuarios()
	{
		log.info ("Generando los VO de Transaccion");
		List<VOUsuarioPersonal> voUsuarios = new LinkedList<VOUsuarioPersonal> ();
		for (UsuarioPersonal usuarios: pp.darUsuarios())
		{
			voUsuarios.add (usuarios);
		}
		log.info ("Generando los VO de Transaccions: " + voUsuarios.size () + " Transaccions existentes");
		return voUsuarios;
	}
	/**
	 * Elimina todas las tuplas de todas las tablas de la base de datos de SuperAndes
	 * Crea y ejecuta las sentencias SQL para cada tabla de la base de datos - EL ORDEN ES IMPORTANTE 
	 * @return Un arreglo con 7 números que indican el número de tuplas borradas en las tablas GUSTAN, SIRVEN, VISITAN, BEBIDA,
	 * TIPOBEBIDA, BEBEDOR y BAR, respectivamente
	 */
	public long [] limpiarSuperAndes ()
	{
		log.info ("Limpiando la BD de SuperAndes");
		long [] borrrados = pp.limpiarSuperAndes();
		log.info ("Limpiando la BD de Parranderos: Listo!");
		return borrrados;

	}
	/* ****************************************************************
	 * 			Métodos para manejar los CARRITOS
	 *****************************************************************/
	public Carrito adicionarProductoAlCliente( long idCliente, String idProducto, int cantidad, long idSucursal) 
	{
		log.info ("Adicionando Producto al CLiente :  " + idCliente +" , " + idProducto +" , cant "+ cantidad );
		Carrito carri = pp.adicionarProductoAlCliente(idCliente, idProducto, cantidad, idSucursal);
		log.info ("Adicionando Producto al CLiente :  " + carri);
		return carri;
	}
	public long eliminarProductoAlCliente(long idCliente, String idProducto, long idSucursal)
	{
		log.info ("Eliminando producto del cliente :  " + idCliente +" , " +idProducto);
		long ll = pp.eliminarProductoAlCliente(idCliente, idProducto,idSucursal);
		log.info ("Eliminando producto del cliente :  " + ll + "tuplas eliminadas");
		return ll;
	}
	public long vaciarCarro(long idCliente)
	{
		log.info ("Eliminando productos del cliente :  " + idCliente );
		long ll = pp.vaciarCarro(idCliente);
		log.info ("Eliminando productos del cliente :  " + ll + "tuplas eliminadas");
		return ll;
	}
	public List<Carrito> darProductosDelCliente(long idCliente)
	{
		log.info ("Dando productos del cliente");
		List<Carrito> liCarrito = pp.darProductosDelCliente(idCliente);
		log.info ("Dando productos del cliente" + liCarrito.size() );
		return liCarrito;
	}

	public void pagar(long idCliente, Timestamp fecha, long idSucursal)
	{
		List<Carrito> productosAPagar = darProductosDelCliente(idCliente);
		double monto = 0.0;

		TransaccionCliente tc = adicionarTransaccionCliente(monto, fecha, idCliente);
		for (int i = 0; i < productosAPagar.size(); i++)
		{
			Carrito c = productosAPagar.get(i);
			ProductoSuministrado ps = darProdutoSuministrado(c.getIdProducto(), idSucursal);
			Object[] ob = adicionarCompra(ps.getCodigoDeBarras(), tc.getId(), c.getCantidad(), idSucursal);
			System.out.println(((double)ob[4]));
			monto += (double)ob[4];

		}
		pp.modificarMonto(tc.getId(), monto);
		pp.vaciarCarro(idCliente);
	}
	public void abandonarCarro(long idCliente)
	{
		log.info ("Abandonado el carro del cliente :  " + idCliente );
		pp.abandonarCarro(idCliente);
		log.info ("Abandonado el carro del cliente :  " + idCliente );
	}

	public void devolverProductosDeCarrosAbandonados(long idSucursal)
	{
		List<Cliente> listaCliente = pp.darClientesCarroAbandonado();
		for (int i = 0; i < listaCliente.size(); i++) 
		{
			Cliente c = listaCliente.get(i);
			if(c.getCarroAbandonado().equals("V"))
			{
				List<Carrito> lista = darProductosDelCliente(c.getId());
				for (int j = 0; j < lista.size(); j++) 
				{
					ProductoSuministrado ps = darProdutoSuministrado(lista.get(i).getIdProducto(), idSucursal);
					if(ps != null)
					{
						pp.modificarCantidadEstante(ps.getCodigoDeBarras(), ps.getIdEstante(), (ps.getCantidadEnEstante() + lista.get(i).getCantidad()), idSucursal);
					}
				}
				vaciarCarro(c.getId());
				pp.desabandonarCarro(c.getId());
			}
		}
	}

	public void consolidarOrdenesPedido(long idSucursal, Timestamp fecha) throws Exception
	{
		List<Proveedor> listaProveedores = darProveedores();
		String enProceso = "EN_PROCESO";
		
		int contador = 0;

		for (Proveedor proveedor : listaProveedores) 
		{
			List<OrdenPedido> listaOrdenesPedidoEnProceso = pp.darOrdenesPedidoEstadoDeProveedor(proveedor.getNIT(), enProceso, idSucursal);
			if(listaOrdenesPedidoEnProceso.size() > 0 )
			{
				contador++;
				OrdenPedido op = adicionarOrdenPedido(fecha, "CONSOLIDADA", 1, proveedor.getNIT(), idSucursal, fecha);

				for (OrdenPedido ordenPedido : listaOrdenesPedidoEnProceso) 
				{
					List<InfoProducto> li = pp.darProductosDeUnaOrden(ordenPedido.getId());
					for (InfoProducto infoProducto : li)
					{
						adicionarInfoProducto(infoProducto.getPrecioUnitario(), infoProducto.getCantidad(), infoProducto.getPrecioUnidadMedida(), infoProducto.getCalidad(), op.getId(), infoProducto.getCodigoProducto(), "");
						pp.eliminarInfoProducto(ordenPedido.getId(), infoProducto.getCodigoProducto());
					}
					pp.eliminarOrdenPedido(ordenPedido.getId());
				}
			}
			else
			{
				continue;
			}
			
		}
		if(contador == 0)
		{
			throw new Exception("No hay ordenes por consolidar");
		}
	}

	public void registrarSalidaOrdenPedidoConsolidada(String nit )
	{
		List<OrdenPedido> ordenesPedido = pp.darOrdenesPedidoEstadoDeProveedor(nit, "CONSOLIDADA");

		for (OrdenPedido ordenPedido : ordenesPedido) 
		{
			pp.modificarEstado(ordenPedido.getId(), "EN_CAMINO");
		}
	}
	public void registrarLLegadaOrdenPedidoConsolidadaProveedor(String nit, int calificacion)
	{
		List<OrdenPedido> orden = pp.darOrdenesPedidoEstadoDeProveedor(nit, "EN_CAMINO");
		for (OrdenPedido ordenPedido : orden) 
		{
			List<InfoProducto> listaInfos = pp.darProductosDeUnaOrden(ordenPedido.getId());
			for (InfoProducto info : listaInfos) 
			{
				ProductoSuministrado ps = darProdutoSuministrado(info.getCodigoProducto(), ordenPedido.getIdSucursal());
				pp.modificarCantidadBodega(info.getCodigoProducto(), ps.getIdBodega(), (info.getCantidad()+ps.getCantidadEnBodega()), ordenPedido.getIdSucursal());
			}

			if(ordenPedido.getFechaEntregaEstipulada().getTime() - System.currentTimeMillis() >=  0)
			{
				pp.modificarEstado(ordenPedido.getId(), "ENTREGADA");
			}
			else
			{
				pp.modificarEstado(ordenPedido.getId(), "ENTREGADA_TARDE");
			}
			pp.finalizarFechaReal(ordenPedido.getId());
			pp.calificarOrden(ordenPedido.getId(), calificacion);
		}
	}
	/* **********************************************************
	 * 			M�todos para manejar los REQUERMIENTOS DE CONSULTA
	 *****************************************************************/

	//rfc 7
	public List<Object[]>[] analisisOperacionSuperAndes(String tipo, String unidad , String determinado)
	{
		List<Object[]>[] l = new List[3];

		Object[] o = pp.analisisOperacionSuperAndes(tipo, unidad, determinado);

		Object[] demand = (Object[]) o[0];
		List<Object[]> maxDemand = (List<Object[]>) demand[0];
		List<Object[]> minDemand = (List<Object[]>) demand[1];

		List<Object[]> ingr = (List<Object[]>) o[1];

		l[0] = maxDemand; 
		l[1] = minDemand;
		l[2] = ingr;

		return l;
	}

	//RFC8
	public List<Cliente>  darClientesFrecuentes(long idSucursal)
	{
		List<BigDecimal> lista = pp.clientesFrecuentes(idSucursal);
		ArrayList<Cliente> listaClientes = new ArrayList<>();
		System.out.println(lista.size());
		for (BigDecimal objects : lista) 
		{
			Cliente c = pp.darCliente(objects.longValue());
			listaClientes.add(c);
		}
		return listaClientes;
	}

	//RFC9
	public ArrayList<String> productosBajaDemanda()
	{
		return pp.productosBajaDemanda();
	}

	public List<Object[]> ventasFrecuentes(String inicio, String fin)
	{
		return pp.ventasPorSucursal(inicio, fin);
	}
	public List<Producto> darProductos()
	{
		return pp.darProductos();
	}
	public List<Object[]> top20promos()
	{
		return pp.top20Promociones();
	}
	
	public void cargaMasiva()
	{
		pp.cargaMasiva();
	}
	
	public List<Object[]> req1Ite3(String codigo, String fechaInicio, String fechaFinal, String tipoCliente, ArrayList<String> seleccion, String ordenamiento)
	{
		return pp.req1Itera3(codigo, fechaInicio, fechaFinal, tipoCliente, seleccion, ordenamiento);
	}
	
	public List<Object[]> req2Ite3(String codigo, String fechaInicio, String fechaFinal, String tipoCliente, boolean seleccion, String ordenamiento)
	{
		return pp.req2Itera3(codigo, fechaInicio, fechaFinal, tipoCliente, seleccion, ordenamiento);
	}
	
	public List<List<Object[]>> req3Ite3(){
		return pp.req3Itera3();
	}
	
	public List<List<Object[]>> req4Ite3(){
		return pp.req4Iter3();
	}
	
	public List<String> tama�oTablas(){
		return pp.tama�oTablas();
	}
}

