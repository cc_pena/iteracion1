package uniandes.isis2304.superAndes.negocio;
/**
 * Interfaz para los métodos get de PromocionDosProductos.
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz. 
 * 
 * @author fj.gonzalez
 */
public interface VOPromocionDosProductos 
{
	/**
	 * @return El monto total.
	 */
	public Double getMontoTotal();
	/**
	 * @return Una cadena de caracteres con la información básica de la promoción de dos productos.
	 */
	@Override
	public String toString();
}
