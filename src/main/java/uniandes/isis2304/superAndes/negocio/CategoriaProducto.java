package uniandes.isis2304.superAndes.negocio;

/**
 * Clase para modelar el concepto CATEGORIA del negocio SuperAndes
 * 
 * @author cc.pena
 */
public class CategoriaProducto implements VOCategoria
{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	
	/**
	 * Nombre UNICO de la categoria
	 */
	private String nombre;
	
	/* ****************************************************************
	 * 			Constructores
	 *****************************************************************/
	
	/**
	 * Constructor por defecto
	 */
	public CategoriaProducto()
	{
		this.nombre = "";
	}
	
	/**
	 * Constructor con valores
	 * @param nombre - el nombre de la categoria
	 */
	public CategoriaProducto(String nombre)
	{
		this.nombre = nombre;
	}
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre - el nuevo nombre de la categoria
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	/**
	 * @return cadena con la informacion básica de la categoria
	 */
	@Override
	public String toString() {
		return "Categoria [nombre=" + nombre + "]";
	}
}
