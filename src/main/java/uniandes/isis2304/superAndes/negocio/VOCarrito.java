package uniandes.isis2304.superAndes.negocio;
/**
 * Interfaz para los métodos get de CARRITO.
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz. 
 * 
 * @author fj.gonzalez
 */
public interface VOCarrito 
{
	/**
	 * @return El id del cliente asociado a un producto en el momento.
	 */
	public long getIdCliente();
	/**
	 * @return El id del producto asociado al cliente en el momento.
	 */
	public String getIdProducto();
	/**
	 * @return La cantidad del producto.
	 */
	public int getCantidad();
}
