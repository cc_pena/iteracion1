package uniandes.isis2304.superAndes.negocio;
/**
 * Interfaz para los métodos get de TIPOPRODUCTO.
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz. 
 * 
 * @author fj.gonzalez
 */
public interface VOTipoProducto 
{
	/**
	 * @return El nombre del tipo de producto.
	 */
	public String getNombre();
	/**
	 * @return La categoría del tipo de producto.
	 */
	public String getCategoria();
	/**
	 * @return Una cadena de caracteres con la información básica del tipo de producto.
	 */
	@Override
	public String toString();

}
