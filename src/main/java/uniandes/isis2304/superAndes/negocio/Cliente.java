package uniandes.isis2304.superAndes.negocio;

/**
 * Clase para modelar el concepto CLIENTE del negocio SuperAndes
 * 
 * @author cc.pena
 */
public class Cliente implements VOCliente
{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	
	/**
	 * Identificador unico del cliente
	 */
	protected long id;
	
	/**
	 * Cantidad de puntos del cliente
	 */
	protected int puntos;
	
	
	private String carroAbandonado;
	
	/* ****************************************************************
	 * 			Constructores
	 *****************************************************************/
	
	/**
	 * Constructor por defecto
	 */
	public Cliente()
	{
		this.id = 0;
		this.puntos = 0;
		this.carroAbandonado = "F";
	}
	
	/**
	 * Constructor con valores
	 * @param id - identificador unico
	 * @param puntos - puntos del cliente
	 */
	public Cliente(long id, int puntos, String carroAbandonado)
	{
		this.id = id;
		this.puntos = puntos;
		this.carroAbandonado = carroAbandonado;
	}
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/

	public long getId() {
		return id;
	}

	/**
	 * @param id - el nuevo id del cliente
	 */
	public void setId(long id) {
		this.id = id;
	}

	public int getPuntos() {
		return puntos;
	}
	
	public String getCarroAbandonado()
	{
		return carroAbandonado;
	}

	/**
	 * @param puntos - la nueva cantidad de puntos del cliente
	 */
	public void setPuntos(int puntos) {
		this.puntos = puntos;
	}
	
	/**
	 * @param puntos - la nueva cantidad de puntos del cliente
	 */
	public void setCarroAbandonado(String carroabandonado) {
		this.carroAbandonado = carroabandonado;
	}
	
	/**
	 * @return cadena con la informacion básica del cliente
	 */
	@Override
	public String toString() {
		return "[id=" + id + ", puntos=" + puntos+ ", carroAbandonado = "+carroAbandonado;
	}
}
