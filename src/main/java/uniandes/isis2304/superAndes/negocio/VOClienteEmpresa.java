package uniandes.isis2304.superAndes.negocio;
/**
 * Interfaz para los métodos get de CLIENTE_EMPRESA.
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz. 
 * 
 * @author fj.gonzalez
 */
public interface VOClienteEmpresa 
{
	/**
	 * @return El NIT de la empresa.
	 */
	public String getNit();
	/**
	 * @return La dirección de la empresa.
	 */
	public String getDireccion();
	/**
	 * @return Una cadena de caracteres con la información básica del clienteEmpresa.
	 */
	@Override
	public String toString();
	
	

}
