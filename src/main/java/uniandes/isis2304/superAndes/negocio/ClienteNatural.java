package uniandes.isis2304.superAndes.negocio;

/**
 * Clase para modelar el concepto CLIENTENATURAL del negocio SuperAndes
 * 
 * @author cc.pena
 */
public class ClienteNatural extends Cliente implements VOClienteNatural
{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	
	/**
	 * Cedula del cliente natural
	 */
	private String cedula;
	
	/**
	 * nombre del cliente natural
	 */
	private String nombre;
	
	/**
	 * email del cliente natural
	 */
	private String email;
	
	/* ****************************************************************
	 * 			Constructores
	 *****************************************************************/
	
	/**
	 * Constructor por defecto
	 */
	public ClienteNatural()
	{
		super();
		this.cedula = "";
		this.nombre = "";
		this.email = "";
	}
	
	/**
	 * Constructor con valores
	 * @param id - identificador unico
	 * @param puntos - puntos del cliente
	 * @param cedula - cedula unica del cliente natural
	 * @param nombre - nombre del cliente natural
	 * @param email - email del cliente natural
	 */
	public ClienteNatural(long id, int puntos, String cedula, String nombre, String email)
	{
		super(id, puntos,"F");
		this.cedula = cedula;
		this.nombre = nombre;
		this.email = email;
	}
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/

	public long getId(){
		return id;
	}
	
	public void setId(long id){
		this.id = id;
	}
	
	public String getCedula() {
		return cedula;
	}

	/**
	 * @param cedula - nueva cedula del cliente natural
	 */
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getNombre() {
		return nombre;
	}

	/**
	 * param nombre - nuevo nombre del cliente natural
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEmail() {
		return email;
	}

	/**
	 * @param email - nuevo email del cliente natural
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * @return cadena con la informacion básica del cliente natural
	 */
	@Override
	public String toString() {
		return "ClienteNatural " + super.toString() + ", cedula=" + cedula + ", nombre=" + nombre + ", email=" + email + "]";
	}
}
