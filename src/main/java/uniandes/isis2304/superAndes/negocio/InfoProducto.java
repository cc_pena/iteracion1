package uniandes.isis2304.superAndes.negocio;

/**
 * Clase para modelar la relación INFOPRODUCTO del negocio SuperAndes:
 * Cada objeto de esta clase representa el hecho que un producto esta asociado a una orden de pedido y viceversa.
 * Se modela mediante los identificadores del producto y la orden respectivamente.
 * Debe existir un producto con el codigo dado
 * Debe existir una orden con el identificador dado 
 * 
 * @author cc.pena
 */
public class InfoProducto implements VOInfoProducto
{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	
	/**
	 * orden de pedido asociada
	 */
	private long idOrden;
	
	/**
	 * codigo del producto asociado
	 */
	private String codigoProducto;
	
	/**
	 * precio del producto asociado
	 */
	private Double precioUnitario;
	
	/**
	 * cantidad de productos del producto asociado
	 */
	private Integer cantidad;
	
	/**
	 * precio PUDM del producto asociado
	 */
	private String precioUnidadMedida;
	
	/**
	 * calidad del producto asociado
	 */
	private Double calidad;
	

	
	/* ****************************************************************
	 * 			Constructores
	 *****************************************************************/

	/**
	 * Constructor por defecto
	 */
	public InfoProducto()
	{
		this.idOrden = 0;
		this.codigoProducto = "";
		this.precioUnitario = 0.0;
		this.cantidad = 0;
		this.precioUnidadMedida = "";
		this.calidad = 0.0;
	}
	
	/**
	 * Constructor con valores
	 * @param idOrdenPedido - orden de pedido asociada
	 * @param codigoProducto - codigo del producto asociado
	 * @param precio - precio del producto asociado
	 * @param cantidad - cantidad de productos del producto asociado
	 * @param precioPorUnidadDeMedida - precio PUDM del producto asociado
	 * @param calidadProducto - calidad del producto asociado ( >=1 && <= 5 )
	 */
	public InfoProducto(long idOrdenPedido, String codigoProducto, Double precio, Integer cantidad,
			String precioPorUnidadDeMedida, Double calidadProducto) {
		this.idOrden = idOrdenPedido;
		this.codigoProducto = codigoProducto;
		this.precioUnitario = precio;
		this.cantidad = cantidad;
		this.precioUnidadMedida = precioPorUnidadDeMedida;
		this.calidad = calidadProducto;
	}
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/

	public long getIdOrden() {
		return idOrden;
	}

	/**
	 * @param idOrdenPedido - nuevo orden de pedido asociado
	 */
	public void setIdOrden(long idOrdenPedido) {
		this.idOrden = idOrdenPedido;
	}

	public String getCodigoProducto() {
		return codigoProducto;
	}

	/**
	 * @param codigoProducto - nuevo producto asociado
	 */
	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}

	public Double getPrecioUnitario() {
		return precioUnitario;
	}

	/**
	 * @param precio - nuevo precio del producto asociado
	 */
	public void setPrecioUnitario(Double precio) {
		this.precioUnitario = precio;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	/**
	 * @param cantidad - nueva cantidad de productos del producto asociado
	 */
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public String getPrecioUnidadMedida() {
		return precioUnidadMedida;
	}

	/**
	 * @param precioPorUnidadDeMedida - nuevo precio PUDM del producto asociado
	 */
	public void setPrecioUnidadMedida(String precioPorUnidadDeMedida) {
		this.precioUnidadMedida = precioPorUnidadDeMedida;
	}

	public Double getCalidad() {
		return calidad;
	}

	/**
	 * @param calidadProducto - nueva calidad del producto asociado ( >=1 && <= 5 )
	 */
	public void setCalidad(Double calidadProducto) {
		this.calidad = calidadProducto;
	}
	
	/**
	 * @return Una cadena de caracteres con la información básica del infoProducto.
	 */
	@Override
	public String toString() {
		return "InfoProducto [idOrdenPedido=" + idOrden+", codigoProducto="+codigoProducto+", precio="+precioUnitario+", cantidad"+ cantidad +", precioPorUnidadDeMedida="+precioUnidadMedida+", calidadProducto="+calidad+"]";
	}
}
