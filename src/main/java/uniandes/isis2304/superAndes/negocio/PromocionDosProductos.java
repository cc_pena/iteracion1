package uniandes.isis2304.superAndes.negocio;

import java.sql.Timestamp;

/**
 * Clase para modelar el concepto PROMOCION DE PAQUETE DE DOS PRODUCTOS del negocio de SuperAndes
 *
 * @author fj.gonzalez
 */
public class PromocionDosProductos extends Promocion implements VOPromocionDosProductos
{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El monto total que es equivalente a los dos productos.
	 */
	private Double montoTotal;
	/* ****************************************************************
	 * 			Constructores
	 *****************************************************************/
	/**
	 * Constructor vacio.
	 */
	public PromocionDosProductos() 
	{
		super();
		montoTotal = 0.0;
		this.setTipo(5);

	}
	/**
	 * Constructor con valores.
	 */
	public PromocionDosProductos(long pId, Timestamp pFechaExpedicion,Timestamp pFechaFinalizacion,Timestamp pFechaFinExistencias, Integer pUnidades , String pCodigoBarrasProducto1 , String pCodigoBarrasProducto2 , Integer pTipo, Double pMont, String pEstado) 
	{
		super(pId, pFechaExpedicion, pFechaFinalizacion, pFechaFinExistencias, pUnidades, pCodigoBarrasProducto1, pCodigoBarrasProducto2, pTipo,pEstado);
		montoTotal = pMont;
		this.setTipo(5);
	}
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	
	public long getId()
	{

		return id;
	}
	/**
	 * @param id - El nuevo id de la promoción.
	 */
	public void setId(long id) 
	{
		this.id = id;
	}
	
	@Override
	public Double getMontoTotal() 
	{
		return montoTotal;
	}
	/**
	 * @param montoTotal - El nuevo monto de la promoción.
	 */
	public void setMontoTotal(Double montoTotal) 
	{
		this.montoTotal = montoTotal;
	}
	/**
	 * @return Una cadena de caracteres con la información básica de la promoción de dos productos.
	 */
	public String toString()
	{
		return "PromocionDosProductos : " + super.toString() + "monto total" + montoTotal;
		
	}
	

}
