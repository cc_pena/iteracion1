package uniandes.isis2304.superAndes.negocio;

import java.sql.Timestamp;

/**
 * Clase para modelar el concepto PROMOCION PAGUE N LLEVE M UNIDADES del negocio de SuperAndes
 *
 * @author fj.gonzalez
 */
public class PromocionPagueNLleveMUnidades extends Promocion implements VOPromocionPagueNLleveMUnidades
{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/	
	/**
	 * Las unidades ofrecidas en la promoción.
	 */
	private Integer unidadesOfrecidas;
	/**
	 * Las unidades pagadas en la promoción.
	 */
	private Integer unidadesPagadas;

	/* ****************************************************************
	 * 			Constructores
	 *****************************************************************/	
	/**
	 * Constructor vacio.
	 */
	public PromocionPagueNLleveMUnidades()
	{
		super();
		unidadesPagadas = 0;
		unidadesOfrecidas = 0;
		this.setTipo(1);
	}
	/**
	 * Constructor con valores.
	 */
	public PromocionPagueNLleveMUnidades(long pId, Timestamp pFechaExpedicion,Timestamp pFechaFinalizacion,Timestamp pFechaFinExistencias, Integer pUnidades , String pCodigoBarrasProducto1 , String pCodigoBarrasProducto2 , Integer pTipo, Integer pUniPag , Integer pUniOfrec,String pEstado)
	{
		
		super(pId, pFechaExpedicion, pFechaFinalizacion, pFechaFinExistencias, pUnidades, pCodigoBarrasProducto1, pCodigoBarrasProducto2, pTipo,pEstado);
		unidadesPagadas = pUniPag;
		unidadesOfrecidas = pUniOfrec;
		this.setTipo(1);

	}
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	
	public long getId()
	{

		return id;
	}
	/**
	 * @param id - El nuevo id de la promoción.
	 */
	public void setId(long id) 
	{
		this.id = id;
	}
	
	@Override
	public Integer getUnidadesPagadas() 
	{
		return unidadesPagadas;
	}

	@Override
	public Integer getUnidadesOfrecidas() 
	{
		return unidadesOfrecidas;
	}
	/**
	 * @param unidadesPagadas - Las nuevas unidades que se desean pagar.
	 */
	public void setUnidadesPagadas(Integer unidadesPagadas) 
	{
		this.unidadesPagadas = unidadesPagadas;
	}
	/**
	 * @param unidadesOfrecidas - Las nuevas unidades que se desean ofrecer.
	 */
	public void setUnidadesOfrecidas(Integer unidadesOfrecidas) 
	{
		this.unidadesOfrecidas = unidadesOfrecidas;
	}
	/**
	 * @return Una cadena de caracteres con la información básica de la promoción pague n lleve m unidades.
	 */
	public String toString()
	{
		return "PromocionPagueNLleveMUnidades : " + super.toString() + "unidades que tiene que pagar : " + unidadesPagadas + ", unidades que ofrece : " + unidadesOfrecidas;

	}
	

}
