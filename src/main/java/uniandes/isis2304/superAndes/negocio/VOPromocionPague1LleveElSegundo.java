package uniandes.isis2304.superAndes.negocio;
/**
 * Interfaz para los métodos get de PromocionPague1LleveElSegundo .
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz. 
 * 
 * @author fj.gonzalez
 */
public interface VOPromocionPague1LleveElSegundo 
{
	/**
	 * @return El porcentaje de descuento de la segunda unidad del producto.
	 */
	public Double getporctDescSegProducto();
	/**
	 * @return Una cadena de caracteres con la información básica de la promoción pague 1 lleve el segundo con descuento.
	 */
	@Override
	public String toString();

}
