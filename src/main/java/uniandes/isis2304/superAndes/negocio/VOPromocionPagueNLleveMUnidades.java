package uniandes.isis2304.superAndes.negocio;
/**
 * Interfaz para los métodos get de PromocionPagueNLleveMUnidades .
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz. 
 * 
 * @author fj.gonzalez
 */
public interface VOPromocionPagueNLleveMUnidades 
{
	/**
	 * @return Las unidades que se pagarán del producto.
	 */
	public Integer getUnidadesPagadas();
	/**
	 * @return Las unidades ofrecidas del producto.
	 */
	public Integer getUnidadesOfrecidas();
	/**
	 * @return Una cadena de caracteres con la información básica de la promoción pague n lleve m unidades.
	 */
	@Override
	public String toString();

}
