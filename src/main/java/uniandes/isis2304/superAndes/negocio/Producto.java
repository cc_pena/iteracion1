package uniandes.isis2304.superAndes.negocio;

/**
 * Clase para modelar el concepto PRODUCTO del negocio SuperAndes
 * 
 * @author cc.pena
 */
public class Producto implements VOProducto
{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	
	/**
	 * codigo de barras UNICO del producto
	 */
	protected String codigoDeBarras;
	
	/**
	 * Marca del producto
	 */
	private String marca;
	
	/**
	 * Nombre del producto
	 */
	private String nombre;
	
	/**
	 * Presentacion del producto
	 */
	private String presentacion;
	
	/**
	 * Unidad de medida del producto
	 */
	private String unidadDeMedida;
	
	/**
	 * Volumen del producto
	 */
	private String volumen;
	
	/**
	 * Peso del producto
	 */
	private String peso;
	
	/**
	 * Cantidad en la presentacion
	 */
	private Integer cantidadPresentacion;
	
	/**
	 * Tipo del producto
	 */
	private String tipo;

	/* ****************************************************************
	 * 			Constructores
	 *****************************************************************/
	
	/**
	 * Constructor por defecto
	 */
	public Producto()
	{
		this.codigoDeBarras = "";
		this.marca = "";
		this.nombre = "";
		this.presentacion = "";
		this.unidadDeMedida = "";
		this.volumen = "";
		this.peso = "";
		this.cantidadPresentacion = 0;
		this.tipo = "";
	}
	
	/**
	 * Constructor con valores
	 * @param codigoBarras - codigo de barras UNICO del producto
	 * @param marca - Marca del producto
	 * @param nombre - Nombre del producto
	 * @param presentacion - Presentacion del producto
	 * @param unidadMedida - Unidad de medida del producto
	 * @param volumen - Volumen del producto
	 * @param peso - Peso del producto
	 * @param cantidadEnLaPresentacion - Cantidad en la presentacion
	 * @param tipoProducto - Tipo del producto
	 */
	public Producto(String codigoBarras, String marca, String nombre, String presentacion, String unidadMedida,
			String volumen, String peso, Integer cantidadEnLaPresentacion, String tipoProducto) {
		this.codigoDeBarras = codigoBarras;
		this.marca = marca;
		this.nombre = nombre;
		this.presentacion = presentacion;
		this.unidadDeMedida = unidadMedida;
		this.volumen = volumen;
		this.peso = peso;
		this.cantidadPresentacion = cantidadEnLaPresentacion;
		this.tipo = tipoProducto;
	}
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/

	public String getCodigoDeBarras() {
		return codigoDeBarras;
	}

	/**
	 * @param codigoBarras - nuevo codigo de barras UNICO del producto
	 */
	public void setCodigoDeBarras(String codigoBarras) {
		this.codigoDeBarras = codigoBarras;
	}

	public String getMarca() {
		return marca;
	}

	/**
	 * @param marca - nueva marca del producto
	 */
	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre - nuevo nombre del producto
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPresentacion() {
		return presentacion;
	}

	/**
	 * @param presentacion - la nueva presentacion del producto
	 */
	public void setPresentacion(String presentacion) {
		this.presentacion = presentacion;
	}

	public String getUnidadDeMedida() {
		return unidadDeMedida;
	}

	/**
	 * @param unidadMedida - la nueva unidad de medida 
	 */
	public void setUnidadDeMedida(String unidadMedida) {
		this.unidadDeMedida = unidadMedida;
	}

	public String getVolumen() {
		return volumen;
	}

	/**
	 * @param volumen - el nuevo volumen del producto
	 */
	public void setVolumen(String volumen) {
		this.volumen = volumen;
	}

	public String getPeso() {
		return peso;
	}

	/**
	 * @param peso - el nuevo peso del producto
	 */
	public void setPeso(String peso) {
		this.peso = peso;
	}

	public Integer getCantidadPresentacion() {
		return cantidadPresentacion;
	}

	/**
	 * @param cantidadEnLaPresentacion - la nueva CELP del producto
	 */
	public void setCantidadPresentacion(Integer cantidadEnLaPresentacion) {
		this.cantidadPresentacion = cantidadEnLaPresentacion;
	}

	public String getTipo() {
		return tipo;
	}

	/**
	 * @param tipoProducto - el nuevo tipo de producto
	 */
	public void setTipo(String tipoProducto) {
		this.tipo = tipoProducto;
	}
	
	/**
	 * @return Una cadena de caracteres con la información básica del producto.
	 */
	public String toString() {
		return "Producto [codigoBarras="+codigoDeBarras+", marca="+marca+", nombre="+nombre+", presentacion="+presentacion+", unidadMedida"+unidadDeMedida+", volumen="+volumen+", peso="+peso+", cantidadEnLaPresentacion="+cantidadPresentacion+", tipoProducto="+tipo+"]";
	}
	
	/**
	 * @return Cadena de caracteres para simplificar su uso en la herencia
	 */
	public String toStringTemporal() { 
		return "[codigoBarras="+codigoDeBarras+", marca="+marca+", nombre="+nombre+", presentacion="+presentacion+", unidadMedida"+unidadDeMedida+", volumen="+volumen+", peso="+peso+", cantidadEnLaPresentacion="+cantidadPresentacion+", tipoProducto="+tipo;
	}
}
