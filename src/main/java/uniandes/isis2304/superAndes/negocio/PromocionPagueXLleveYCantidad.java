package uniandes.isis2304.superAndes.negocio;

import java.sql.Timestamp;

/**
 * Clase para modelar el concepto PROMOCION PAGUE X LLEVE Y EN CANTIDAD del negocio de SuperAndes
 *
 * @author fj.gonzalez
 */
public class PromocionPagueXLleveYCantidad extends Promocion implements VOPromocionPagueXLleveYCantidad 
{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/	
	/**
	 * La cantidad pagada en la promoción.
	 */
	private Integer cantidadPagada;
	/**
	 * La cantidad ofrecida en la promoción.
	 */
	private Integer cantidadOfrecida;
	/* ****************************************************************
	 * 			Constructores
	 *****************************************************************/	
	/**
	 * Constructor vacio.
	 */
	public PromocionPagueXLleveYCantidad() 
	{
		super();
		cantidadPagada = 0;
		cantidadOfrecida = 0;
		this.setTipo(3);
		// TODO Auto-generated constructor stub
	}
	/**
	 * Constructor con valores.
	 */
	public PromocionPagueXLleveYCantidad(long pId, Timestamp pFechaExpedicion,Timestamp pFechaFinalizacion,Timestamp pFechaFinExistencias, Integer pUnidades , String pCodigoBarrasProducto1 , String pCodigoBarrasProducto2 , Integer pTipo, Integer pCantPag , Integer pCantOfrec,String pEstado)
	{
		super(pId, pFechaExpedicion, pFechaFinalizacion, pFechaFinExistencias, pUnidades, pCodigoBarrasProducto1, pCodigoBarrasProducto2, pTipo,pEstado);
		cantidadPagada = pCantPag;
		cantidadOfrecida = pCantOfrec;
		this.setTipo(3);
	}

	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/	
	
	public long getId()
	{

		return id;
	}
	/**
	 * @param id - El nuevo id de la promoción.
	 */
	public void setId(long id) 
	{
		this.id = id;
	}
	
	@Override
	public Integer getCantidadPagadas() 
	{
		return cantidadPagada;
	}

	@Override
	public Integer getCantidadOfrecidas() 
	{
		return cantidadOfrecida;
	}
	/**
	 * @return Una cadena de caracteres con la información básica de la promoción pague x lleve y cantidad.
	 */
	public String toString()
	{
		return "PromocionPagueXLleveYCantidad : " + super.toString() + "cantidad por la que paga : " + cantidadPagada + ", cantidad que ofrece : " + cantidadOfrecida;
	}

}
