package uniandes.isis2304.superAndes.negocio;
/**
 * Interfaz para los métodos get de ORDEN_PEDIDO.
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz. 
 * 
 * @author fj.gonzalez
 */
import java.sql.Timestamp;

public interface VOOrdenPedido 
{

	/**
	 * @return El id de la orden pedido.
	 */
	public long getId();

	/**
	 * @return El id de la sucursal que hace la orden pedido.
	 */
	public long getIdSucursal();

	/**
	 * @return El NIT del proveedor al que se le hace la orden.
	 */
	public String getNITProveedor();

	/**
	 * @return La fecha estipulada de entrega del pedido.
	 */
	public Timestamp getFechaEntregaEstipulada();

	/**
	 * @return La fecha real de entrega del pedido.
	 */
	public Timestamp getFechaEntregaReal();

	/**
	 * @return El estado actual del pedido. (EN CAMINO, ENTREGADO A TIEMPO , ENTREGADO TARDE, REGISTRADO )
	 */
	public String getEstado();

	/**
	 * @return La calificación del servicio prestado por el proveedor en dicho pedido.
	 */
	public Double getCalificacionServicio();
	/**
	 * @return Una cadena de caracteres con la información básica de la orden pedido.
	 */
	@Override
	public String toString();
		
	
	
	

}
