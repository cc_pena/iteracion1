package uniandes.isis2304.superAndes.negocio;

/**
 * Clase para modelar la relación PRODUCTOSDELPROVEEDOR del negocio SuperAndes:
 * Cada objeto de esta clase representa el hecho que un producto esta asociado a un proveedor y viceversa.
 * Se modela mediante los identificadores del producto y el proveedor respectivamente.
 * Debe existir un producto con el codigo dado
 * Debe existir un proveedor con el identificador dado 
 * 
 * @author cc.pena
 */
public class ProductosDelProveedor implements VOProductosDelProveedor
{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	
	/**
	 * NIT del proveedor asociado
	 */
	private String NITProveedor;
	
	/**
	 * identificador del producto asociado
	 */
	private String codigoproducto;

	/* ****************************************************************
	 * 			Constructores
	 *****************************************************************/
	
	/**
	 * Constructor por defecto
	 */
	public ProductosDelProveedor()
	{
		this.NITProveedor = "";
		this.codigoproducto = "";
	}
	
	/**
	 * Constructor con valores
	 * @param nITProveedor - NIT del proveedor asociado
	 * @param codigoproducto - identificador del producto asociado
	 */
	public ProductosDelProveedor(String NITProveedor, String codigoproducto) {
		this.NITProveedor = NITProveedor;
		this.codigoproducto = codigoproducto;
	}
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/

	public String getNITProveedor() {
		return NITProveedor;
	}

	/**
	 * @param NITProveedor - el nuevo NIT del proveedor (debe existir)
	 */
	public void setNITProveedor(String NITProveedor) {
		this.NITProveedor = NITProveedor;
	}

	public String getCodigoProducto() {
		return codigoproducto;
	}

	/**
	 * @param codigoproducto - nuevo producto asociado (debe existir)
	 */
	public void setCodigoProducto(String codigoproducto) {
		this.codigoproducto = codigoproducto;
	}
	
	/**
	 * @return Una cadena de caracteres con la información básica de los productos de los proveedores.
	 */
	@Override
	public String toString() {
		return "ProductosDelProveedor [NIT="+ NITProveedor+", codigoProducto="+codigoproducto+"]";
	}
}
