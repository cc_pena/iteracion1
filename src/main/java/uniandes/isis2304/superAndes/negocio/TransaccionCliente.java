package uniandes.isis2304.superAndes.negocio;

import java.sql.Timestamp;
/**
 * Clase para modelar la relación TRANSACCION_CLIENTE del negocio SuperAndes
 * Debe existir un cliente con el identificador dado
 * 
 * @author fj.gonzalez
 */
public class TransaccionCliente implements VOTransaccionCliente 
{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El identificador de la transacción.
	 */
	private long id;
	/**
	 * El monto de la transacción.
	 */
	private Double monto;
	/**
	 * La fecha de la transacción.
	 */
	private Timestamp fecha;
	/**
	 * El identificador del bebedor que gusta de la bebida
	 */
	private long idCliente;
	/* ****************************************************************
	 * 			Constructores
	 *****************************************************************/
	/**
	 * Constructor vacío.
	 */
	public TransaccionCliente() 
	{
		id =0;
		monto=0.0;
		fecha= new Timestamp(0);
		idCliente = 0;
	}
	/**
	 * Constructor por defecto
	 */
	public TransaccionCliente(long pId, Double pMont, Timestamp pFech , long pIdCli) 
	{
		id =pId;
		monto= pMont;
		fecha= pFech;
		idCliente = pIdCli;
	}
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	public long getId()
	{

		return id;
	}

	
	public Double getMonto() 
	{
		return monto;
	}

	
	public Timestamp getFecha()
	{
		return fecha;
	}

	
	public long getIdCliente() 
	{
		return idCliente;
	}

	/**
	 * @param id - El nuevo id de la transacción.
	 */
	public void setId(long id)
	{
		this.id = id;
	}
	/**
	 * @param monto - El nuevo monto de la transacción.
	 */
	public void setMonto(Double monto)
	{
		this.monto = monto;
	}
	/**
	 * @param fecha - La nueva fecha de la transacción.
	 */
	public void setFecha(Timestamp fecha)
	{
		this.fecha = fecha;
	}
	/**
	 * @param idCliente - El nuevo identificador del cliente. Este id tiene que haber existido previamente.
	 */
	public void setIdCliente(long idCliente)
	{
		this.idCliente = idCliente;
	}

	@Override
	public String toString()
	{
		return "Transaccion [id="+id+", monto="+monto+", fecha="+fecha+", idCliente="+idCliente+"]";
	}
}
