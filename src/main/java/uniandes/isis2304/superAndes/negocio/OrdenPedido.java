package uniandes.isis2304.superAndes.negocio;

import java.sql.Timestamp;

/**
 * Clase para modelar la relación ORDENPEDIDO del negocio SuperAndes:
 * Cada objeto de esta clase representa el hecho que un proovedor esta asociado a una sucursal y viceversa.
 * Se modela mediante los identificadores del proveedor y la sucursal respectivamente.
 * Debe existir un proovedor con el NIT dado
 * Debe existir una sucursal con el identificador dado 
 * 
 * @author cc.pena
 */
public class OrdenPedido implements VOOrdenPedido
{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	
	/**
	 * Identificador de la orden
	 */
	private long id;
	
	/**
	 * Identificador de la sucursal asociada
	 */
	private long idSucursal;
	
	/**
	 * NIT del proveedor asociado
	 */
	private String NITProveedor;
	
	/**
	 * fecha estipulada para completar la orden
	 */
	private Timestamp fechaEntregaEstipulada;
	
	/**
	 * fecha real que se completo la orden
	 */
	private Timestamp fechaEntregaReal;
	
	/**
	 * estado en el que se encuentra el pedido
	 */
	private String estado;
	
	/**
	 * Calificacion del servicio recibido por el proovedor en esa orden
	 */
	private Double calificacionServicio;
	
	/* ****************************************************************
	 * 			Constructores
	 *****************************************************************/
	
	/**
	 * Constructor por defecto
	 */
	public OrdenPedido()
	{
		this.id = 0;
		this.idSucursal = 0;
		this.NITProveedor = "";
		this.fechaEntregaEstipulada = new Timestamp(0);
		this.fechaEntregaReal = new Timestamp(0);
		this.estado = "";
		this.calificacionServicio = 0.0;
	}
	
	/**
	 * Constructor con valores
	 * @param id - Identificador de la orden
	 * @param idSucursal - Identificador de la sucursal asociada
	 * @param NITProveedor - NIT del proveedor asociado
	 * @param fechaEntregaEstipulada - fecha estipulada para completar la orden
	 * @param fechaEntregaReal - fecha real que se completo la orden
	 * @param estado - estado en el que se encuentra el pedido
	 * @param calificacionServicio - Calificacion del servicio recibido por el proovedor en esa orden
	 */
	public OrdenPedido(long id, long idSucursal, String NITProveedor, Timestamp fechaEntregaEstipulada,
			Timestamp fechaEntregaReal, String estado, Double calificacionServicio) {
		this.id = id;
		this.idSucursal = idSucursal;
		this.NITProveedor = NITProveedor;
		this.fechaEntregaEstipulada = fechaEntregaEstipulada;
		this.fechaEntregaReal = fechaEntregaReal;
		this.estado = estado;
		this.calificacionServicio = calificacionServicio;
	}
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/

	public long getId() {
		return id;
	}

	/**
	 * @param id - el nuevo id de la orden
	 */
	public void setId(long id) {
		this.id = id;
	}

	public long getIdSucursal() {
		return idSucursal;
	}

	/**
	 * @param idSucursal - la nueva sucursal asociada a la orden
	 */
	public void setIdSucursal(long idSucursal) {
		this.idSucursal = idSucursal;
	}

	public String getNITProveedor() {
		return NITProveedor;
	}

	/**
	 * @param NITProveedor - el nuevo NIT del proveedor
	 */
	public void setNITProveedor(String NITProveedor) {
		this.NITProveedor = NITProveedor;
	}

	public Timestamp getFechaEntregaEstipulada() {
		return fechaEntregaEstipulada;
	}

	/**
	 * @param fechaEntregaEstipulada - la nueva fecha estipulada
	 */
	public void setFechaEntregaEstipulada(Timestamp fechaEntregaEstipulada) {
		this.fechaEntregaEstipulada = fechaEntregaEstipulada;
	}

	public Timestamp getFechaEntregaReal() {
		return fechaEntregaReal;
	}

	/**
	 * @param fechaEntregaReal - la nueva fecha real
	 */
	public void setFechaEntregaReal(Timestamp fechaEntregaReal) {
		this.fechaEntregaReal = fechaEntregaReal;
	}

	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado - el nuevo estado de la orden
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Double getCalificacionServicio() {
		return calificacionServicio;
	}

	/**
	 * @param calificacionServicio - la nueva calificacion del servicio
	 */
	public void setCalificacionServicio(Double calificacionServicio) {
		this.calificacionServicio = calificacionServicio;
	}
	
	public String toString() {
		return "OrdenPedido [id="+id+", idSucursal="+idSucursal+", NITProveedor="+NITProveedor+", fechaEntregaEstipulada"+fechaEntregaEstipulada+", fechaEntregaReal="+fechaEntregaReal+", estado="+estado+", calificacionServicio="+calificacionServicio+"]";
	}
}
