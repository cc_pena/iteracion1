package uniandes.isis2304.superAndes.negocio;
/**
 * Clase para modelar el concepto SUCURSAL del negocio de SuperAndes
 *
 * @author fj.gonzalez
 */
public class Sucursal implements VOSucursal
{

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El identificador ÚNICO de la sucursal.
	 */
	private long id;
	/**
	 * El nombre de la sucursal.
	 */
	private String nombre;
	/**
	 * La dirección de la sucursal.
	 */
	private String direccion;
	/**
	 * La ciudad de la sucursal.
	 */
	private String ciudad;
	/**
	 * El nivel de reorden de la sucursal que se asocia a cada bodega.
	 */
	private Integer nivelReorden;
	/**
	 * El nivel de abastecimiento de la sucursal que se asocia a cada estante.
	 */
	private Integer nivelAbastecimiento;
	/**
	 * El nombre del super al que pertenece la sucursal.
	 */
	private String nombreSupermercado;
	
	/* ****************************************************************
	 * 			Constructores
	 *****************************************************************/
	/**
	 * Constructor vacio.
	 */
	public Sucursal() 
	{
		 id=0;
		 nombre="";
		 direccion="";
		 ciudad="";
		 nivelReorden=0;
		 nivelAbastecimiento=0;
	}
	/**
	 * Constructor con valores.
	 */
	public Sucursal(long pId, String pNom , String pDir , String pCiud , Integer pNRO , Integer pNA) 
	{
		 id=pId;
		 nombre=pNom;
		 direccion=pDir;
		 ciudad=pCiud;
		 nivelReorden=pNRO;
		 nivelAbastecimiento=pNA;
	}
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	public long getId() 
	{
		return id;
	}	
	public String getNombre() 
	{
		return nombre;
	}

	
	public String getDireccion() 
	{
		return direccion;
	}

	
	public String getCiudad() 
	{
		return ciudad;
	}
	
	public Integer getNivelReOrden() 
	{
		return nivelReorden;
	}

	
	public Integer getNivelDeAbastecimiento() 
	{
		return nivelAbastecimiento;
	}
	public String getNombreSupermercado()
	{
		return nombreSupermercado;
	}

	public void setId(long id) 
	{
		this.id = id;
	}
	/**
	 * @param nombre - El nuevo nombre de la sucursal.
	 */
	public void setNombre(String nombre)
	{
		this.nombre = nombre;
	}
	/**
	 * @param direccion - La nueva dirección de la sucursal.
	 */
	public void setDireccion(String direccion) 
	{
		this.direccion = direccion;
	}
	/**
	 * @param ciudad - La nueva ciudad de la sucursal.
	 */
	public void setCiudad(String ciudad)
	{
		this.ciudad = ciudad;
	}
	/**
	 * @param nivelReorden - El nuevo nivel de reorden de la sucursal.
	 */
	public void setNivelReorden(Integer nivelReorden)
	{
		this.nivelReorden = nivelReorden;
	}
	/**
	 * @param nivelAbastecimiento - El nuevo nivel de abastecimiento de la sucursal.
	 */
	public void setNivelAbastecimiento(Integer nivelAbastecimiento) 
	{
		this.nivelAbastecimiento = nivelAbastecimiento;
	}
	/**
	 * @param nombreSupermercado - El nuevo nombre del supermercado de la sucursal.
	 */
	public void setNombreSupermercado(String nombreSupermercado) 
	{
		this.nombreSupermercado = nombreSupermercado;
	}

	@Override
	public String toString()
	{
		return "Sucursal [id="+id+", nombre="+nombre+", direccion="+direccion+", ciudad="+ciudad+", nivelReorden="+nivelReorden+", nivelAbas="+nivelAbastecimiento+", supermercado="+nombreSupermercado+"]";
	}
}
