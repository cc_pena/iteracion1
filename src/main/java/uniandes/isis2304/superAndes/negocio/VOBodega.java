package uniandes.isis2304.superAndes.negocio;
/**
 * Interfaz para los métodos get de BODEGA.
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz. 
 * 
 * @author fj.gonzalez
 */
public interface VOBodega 
{
	/**
	 * @return El id de la bodega.
	 */
	public long getId();
	/**
	 * @return El id de la sucursal
	 */
	public long getIdSucursal();
	/**
	 * @return La capacidad en masa de la bodega.
	 */
	public Integer getCapacidadEnMasa();
	/**
	 * @return La capacidad en volumen de la bodega.
	 */
	public Integer getCapacidadVolumetrica();
	/**
	 * @return La cantidad en volumen de la bodega.
	 */
	public Integer getCantidadVolumetrica();
	/**
	 * @return La capacidad en volumen de la bodega.
	 */
	public Integer getCantidadEnMasa();
	/**
	 * @return El tipo de producto que almacena la bodega.
	 */
	public String getTipoProducto();
	/**
	 * @return Una cadena de caracteres con la información básica de la bodega.
	 */
	@Override
	public String toString();
	
	

}
