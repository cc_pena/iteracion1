package uniandes.isis2304.superAndes.negocio;
/**
 * Interfaz para los métodos get de PROVEEDOR.
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz. 
 * 
 * @author fj.gonzalez
 */
public interface VOProveedor
{
	/**
	 * @return El NIT del proveedor.
	 */
	public String getNIT();
	
	/**
	 * @return El nombre del bebedor.
	 */
	public String getNombre();
	
	/**
	 * @return La calificación que tiene el proveedor.
	 */
	public Double getCalificacion();
	/**
	 * @return Una cadena de caracteres con la información básica del proveedor.
	 */
	@Override
	public String toString();

}
