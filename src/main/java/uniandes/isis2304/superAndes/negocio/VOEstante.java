package uniandes.isis2304.superAndes.negocio;
/**
 * Interfaz para los métodos get de ESTANTE.
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz. 
 * 
 * @author fj.gonzalez
 */
public interface VOEstante 
{
	/**
	 * @return El id del estante.
	 */
	public long getId();
	/**
	 * @return El id de la sucursal.
	 */
	public long getIdSucursal();
	/**
	 * @return La capacidad en masa del estante.
	 */
	public Integer getCapacidadEnMasa();
	/**
	 * @return La capacidad volumetrica del estante.
	 */
	public Integer getCapacidadVolumetrica();
	/**
	 * @return El tipo de producto que almacena el estante.
	 */
	public String getTipoProducto();
	/**
	 * @return La cantidad en masa del estante.
	 */
	public Integer getCantidadEnMasa();
	/**
	 * @return La cantidad actual volumetrica del estante.
	 */
	public Integer getCantidadVolumetrica();
	/**
	 * @return Una cadena de caracteres con la información básica del estante.
	 */
	@Override
	public String toString();
	
}
