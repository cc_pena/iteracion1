package uniandes.isis2304.superAndes.negocio;
/**
 * Interfaz para los métodos get de PromocionDescuento .
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz. 
 * 
 * @author fj.gonzalez
 */
public interface VOPromocionDescuento
{
	/**
	 * @return El porcentaje de descuento
	 */
	public Double getPorcentajeDescuento();
	/**
	 * @return Una cadena de caracteres con la información básica de la promoción de descuento.
	 */
	@Override
	public String toString();
}
