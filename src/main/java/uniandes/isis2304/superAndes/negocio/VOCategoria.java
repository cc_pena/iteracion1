package uniandes.isis2304.superAndes.negocio;
/**
 * Interfaz para los métodos get de CATEGORIA.
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz. 
 * 
 * @author fj.gonzalez
 */
public interface VOCategoria 
{
	/**
	 * @return El nombre de la categoría.
	 */
	public String getNombre();
	/**
	 * @return Una cadena de caracteres con la información básica de la categoria.
	 */
	@Override
	public String toString();
}
