package uniandes.isis2304.superAndes.negocio;

/**
 * Clase para modelar el concepto CLIENTEEMPRESA del negocio SuperAndes
 * 
 * @author cc.pena
 */
public class ClienteEmpresa extends Cliente implements VOClienteEmpresa
{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	
	/**
	 * nit UNICO del cliente de una empresa
	 */
	private String nit;
	
	/**
	 * direccion del cliente de una empresa
	 */
	private String direccion;
	
	/* ****************************************************************
	 * 			Constructores
	 *****************************************************************/
	
	/**
	 * Constructor por defecto
	 */
	public ClienteEmpresa()
	{
		super();
		this.nit = "";
		this.direccion = "";
	}
	
	/**
	 * Constructor con valores
	 * @param id - identificador unico
	 * @param puntos - puntos del cliente
	 * @param nit - nit unico del cliente de una empresa
	 * @param direccion - direccion del cliente de una empresa
	 */
	public ClienteEmpresa(long id, Integer puntos, String nit, String direccion)
	{
		super(id, puntos, "F");
		this.nit = nit;
		this.direccion = direccion;
	}
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/
	public long getId(){
		return id;
	}
	
	public void setId(long id){
		this.id = id;
	}
	
	

	public String getNit() {
		return nit;
	}

	/**
	 * @param nit - nuevo nit unico del cliente de una empresa
	 */
	public void setNit(String nit) {
		this.nit = nit;
	}

	public String getDireccion() {
		return direccion;
	}
	
	/**
	 * @param direccion - nueva direccion del cliente de una empresa
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	/**
	 * @return cadena con la informacion básica del cliente de una empresa
	 */
	@Override
	public String toString() {
		return "ClienteEmpresa " + super.toString() + ", nit=" + nit + ", direccion=" + direccion + "]";
	}
}
