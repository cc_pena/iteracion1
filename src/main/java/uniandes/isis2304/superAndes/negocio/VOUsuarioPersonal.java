package uniandes.isis2304.superAndes.negocio;
/**
 * Interfaz para los métodos get de UsuarioPersonal .
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz. 
 * 
 * @author fj.gonzalez
 */
public interface VOUsuarioPersonal 
{
	/**
	 * @return El usuario de ingreso de un personal
	 */
	public String getUsername();
	/**
	 * @return La contraseña de ingreso de un personal
	 */
	public String getPassword();
	/**
	 * @return La sucursal asociada
	 */
	public long getIdSucursal();
	/**
	 * @return Una cadena de caracteres con la información básica del usuario de tipo personal.
	 */
	@Override
	public String toString();
}
