package uniandes.isis2304.superAndes.negocio;

/**
 * Clase para modelar el concepto BODEGA del negocio SuperAndes
 * 
 * @author cc.pena
 */
public class Bodega implements VOBodega
{
	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	
	/** 
	 * Identificador unico de la bodega
	 */
	private long id;
	
	/** 
	 * Identificador de la sucursal asociada
	 */
	private long idSucursal;
	
	/** 
	 * Capacidad en masa de la bodega
	 */
	private Integer capacidadEnMasa;
	
	/** 
	 * Capacidad en volumen de la bodega
	 */
	private Integer capacidadVolumetrica;
	
	/** 
	 * Tipo de producto que almacena la bodega
	 */
	private String tipoProducto;
	
	/**
	 * cantidad en masa del estante
	 */
	private Integer cantidadEnMasa;
	
	/**
	 * cantidad volumetrica del estante
	 */
	private Integer cantidadVolumetrica;
	
	/* ****************************************************************
	 * 			Constructores
	 *****************************************************************/
	
	/**
	 * Constructor por defecto
	 */
	public Bodega()
	{
		this.id = 0;
		this.idSucursal = 0;
		this.capacidadEnMasa = 0;
		this.capacidadVolumetrica = 0;
		this.tipoProducto = "";
		this.cantidadEnMasa = 0;
		this.cantidadVolumetrica = 0;
	}
	
	/**
	 * Constructor con valores
	 * @param id - el id de la bodega
	 * @param idSucursal - el id de la sucursal asociada
	 * @param capacidadEnMasa - su capacidad en masa
	 * @param capacidadVolumetrica - su capacidad en volumen
	 * @param tipoProducto - el tipo del producto que almacena
	 */
	public Bodega(long id, long idSucursal, Integer capacidadEnMasa, Integer capacidadVolumetrica, String tipoProducto, Integer cantidadEnMasa, Integer cantidadVolumetrica) 
	{
		this.id = id;
		this.idSucursal = idSucursal;
		this.capacidadEnMasa = capacidadEnMasa;
		this.capacidadVolumetrica = capacidadVolumetrica;
		this.tipoProducto = tipoProducto;
		this.cantidadEnMasa = cantidadEnMasa;
		this.cantidadVolumetrica = cantidadVolumetrica;
	}
	
	/* ****************************************************************
	 * 			Métodos
	 *****************************************************************/

	@Override
	public long getId() {
		// TODO Auto-generated method stub
		return id;
	}
	
	/**
	 * @param id - el nuevo id de la bodega
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	@Override
	public long getIdSucursal() {
		// TODO Auto-generated method stub
		return id;
	}

	/**
	 * @param idSucursal - el nuevo id de la sucursal asociada
	 */
	public void setIdSucursal(long idSucursal) {
		this.idSucursal = idSucursal;
	}
	
	@Override
	public Integer getCapacidadEnMasa() {
		// TODO Auto-generated method stub
		return capacidadEnMasa;
	}

	/**
	 * @param capacidadEnMasa - la nueva capacidad en masa
	 */
	public void setCapacidadEnMasa(Integer capacidadEnMasa) {
		this.capacidadEnMasa = capacidadEnMasa;
	}
	
	@Override
	public Integer getCapacidadVolumetrica() {
		// TODO Auto-generated method stub
		return capacidadVolumetrica;
	}

	/**
	 * @param capacidadVolumetrica - la nueva capacidad volumétrica
	 */
	public void setCapacidadVolumetrica(Integer capacidadVolumetrica) {
		this.capacidadVolumetrica = capacidadVolumetrica;
	}
	
	@Override
	public String getTipoProducto() {
		// TODO Auto-generated method stub
		return tipoProducto;
	}

	/**
	 * @param tipoProducto - el nuevo tipo de producto
	 */
	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}
	
	/**
	 * @return cadena con la informacion básica de la bodega
	 */
	@Override
	public String toString() {
		return "Bodega [id=" + id + ", idSucursal=" + idSucursal + ", capacidadEnMasa=" + capacidadEnMasa+ ", capacidadVolumetrica=" + capacidadVolumetrica + ", tipoProducto=" + tipoProducto + ", cantidadActualEnMasa =" + cantidadEnMasa+", cantidadVolumetrica=" + cantidadVolumetrica+"]";
	}

	public Integer getCantidadVolumetrica()
	{
		return cantidadVolumetrica;
	}

	public Integer getCantidadEnMasa() 
	{
		return cantidadEnMasa;
	}

	/**
	 * @param cantidadEnMasa la cantidadEnMasa a cambiar.
	 */
	public void setCantidadEnMasa(Integer cantidadEnMasa) 
	{
		this.cantidadEnMasa = cantidadEnMasa;
	}

	/**
	 * @param cantidadVolumetrica la cantidadVolumetrica a cambiar.
	 */
	public void setCantidadVolumetrica(Integer cantidadVolumetrica)
	{
		this.cantidadVolumetrica = cantidadVolumetrica;
	}
}
