package uniandes.isis2304.superAndes.negocio;
/**
 * Interfaz para los métodos get de COMPRAN.
 * Sirve para proteger la información del negocio de posibles manipulaciones desde la interfaz. 
 * 
 * @author fj.gonzalez
 */
public interface VOCompran 
{
	/**
	 * @return El codigo del producto suministrado.
	 */
	public String getCodigoProductoSuministrado();
	/**
	 * @return El id de la transacción asociada.
	 */
	public long getIdTransaccionCliente();
	/**
	 * @return La cantidad de unidades de dicho producto.
	 */
	public Integer getCantidad();
	/**
	 * @return Una cadena de caracteres con la información básica de la compra.
	 */
	@Override
	public String toString();
	
}
