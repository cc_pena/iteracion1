package uniandes.isis2304.superAndes.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

import javax.swing.JOptionPane;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;

import uniandes.isis2304.superAndes.negocio.Bodega;
import uniandes.isis2304.superAndes.negocio.ClienteEmpresa;
import uniandes.isis2304.superAndes.negocio.ClienteNatural;
import uniandes.isis2304.superAndes.negocio.Estante;
import uniandes.isis2304.superAndes.negocio.OrdenPedido;
import uniandes.isis2304.superAndes.negocio.Producto;
import uniandes.isis2304.superAndes.negocio.ProductoSuministrado;
import uniandes.isis2304.superAndes.negocio.Promocion;
import uniandes.isis2304.superAndes.negocio.PromocionDescuento;
import uniandes.isis2304.superAndes.negocio.PromocionDosProductos;
import uniandes.isis2304.superAndes.negocio.PromocionPague1LleveElSegundo;
import uniandes.isis2304.superAndes.negocio.Proveedor;
import uniandes.isis2304.superAndes.negocio.Sucursal;
import uniandes.isis2304.superAndes.negocio.SuperAndes;
/**
 * Clase con los métdos de prueba de funcionalidad sobre TIPOBEBIDA
 * @author Germán Bravo
 *
 */
public class SuperAndesTest
{
	/* **********************
	 * 			Constantes
	 ***********************/
	/**
	 * Logger para escribir la traza de la ejecución
	 */
	private static Logger log = Logger.getLogger(SuperAndesTest.class.getName());

	/**
	 * Ruta al archivo de configuración de los nombres de tablas de la base de datos: La unidad de persistencia existe y el esquema de la BD también
	 */
	private static final String CONFIG_TABLAS_T = "./src/main/resources/config/TablasBDT.json"; 


	private static final String INICIALIZACION = "./src/main/resources/config/inicializacion.txt"; 

	/* **********************
	 * 			Atributos
	 ***********************/

	/**
	 * La clase que se quiere probar
	 */
	private SuperAndes superAndes;

	@Before
	public void conectar()
	{
		try
		{
			log.info ("Probando las operaciones CRD");
			superAndes = new SuperAndes (openConfig (CONFIG_TABLAS_T));

			crearTiposyCategorias();
		}
		catch (Exception e)
		{
			//			e.printStackTrace();
			log.info ("Prueba de CRD de  incompleta. No se pudo conectar a la base de datos !!. La excepción generada es: " + e.getClass ().getName ());
			log.info ("La causa es: " + e.getCause ().toString ());

			String msg = "Prueba de CRD de  incompleta. No se pudo conectar a la base de datos !!.\n";
			msg += "Revise el log de parranderos y el de datanucleus para conocer el detalle de la excepción";
			System.out.println (msg);
			fail (msg);
		}
	}
	private void crearTiposyCategorias()
	{
		superAndes.adicionarCategoriaProducto("Perecedero");
		superAndes.adicionarCategoriaProducto("Tecnologia");
		superAndes.adicionarTipoProducto("Carnes", "Perecedero");
		superAndes.adicionarTipoProducto("Lacteos", "Perecedero");
		superAndes.adicionarTipoProducto("Electrodomesticos", "Tecnologia");
		superAndes.adicionarTipoProducto("Videojuegos", "Tecnologia");

	}
	@Before
	public void inicializacion()
	{

		try 
		{
			FileReader f = new FileReader(new File(INICIALIZACION));
			BufferedReader bf = new BufferedReader(f);

			String linea = bf.readLine();
			if(linea.equals("sucursal"))
			{
				linea = bf.readLine();
				while (!linea.equals("") && !linea.equals("proveedor")) 
				{
					String[] sucur = linea.split("-");
					superAndes.adicionarSucursal(sucur[0], sucur[1], sucur[2], Integer.parseInt(sucur[3]), Integer.parseInt(sucur[4]), sucur[5]);
					linea = bf.readLine();
				}

				linea = bf.readLine();
				while(!linea.equals("") && !linea.equals("clientenatural"))
				{
					String[] prov = linea.split("-");
					superAndes.adicionarProveedor(prov[0], prov[1]);
					linea = bf.readLine();
				}

				linea = bf.readLine();
				while(!linea.equals("") && !linea.equals("cliente empresa"))
				{
					String[] cli = linea.split("-");
					superAndes.adicionarClienteNatural(Integer.parseInt(cli[3]) ,cli[0], cli[1], cli[2]);
					linea = bf.readLine();
				}

				linea = bf.readLine();
				while(!linea.equals("") && !linea.equals("producto"))
				{
					String[] cli = linea.split("-");
					superAndes.adicionarClienteEmpresa(Integer.parseInt(cli[2]), cli[0], cli[1]);
					linea = bf.readLine();
				}

				linea = bf.readLine();
				while(!linea.equals("") )
				{
					String[] prod = linea.split("-");
					superAndes.adicionarProducto(prod[0], prod[1], prod[2], prod[3], prod[4], prod[5], prod[6],Integer.parseInt( prod[7]));
					linea = bf.readLine();
				}


			}
			bf.close();
			f.close();

		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/* **********************
	 * 			Métodos de prueba para las tablas - Creación y borrado
	 ***********************/
	/**
	 * Método que prueba las operaciones sobre la tabla Bodega.
	 */
	@Test
	public void CRDSucursalTest() 
	{
		try
		{
			//Sucursales que deber�an haber inicialmente
			assertEquals(5, superAndes.darSucursales().size());
			Sucursal s = superAndes.adicionarSucursal("B/Manga", "Clla 153", "Paji", 50, 50, "Jumbo");
			//Sucursales despu�s de a�adir una m�s.
			assertEquals(6, superAndes.darSucursales().size());
			long tuplasEl = superAndes.eliminarSucursal(s.getId());
			//Solo elimino una sucursal.
			assertEquals(1, tuplasEl);
			//Efectivamente la elimino.
			assertEquals(5, superAndes.darSucursales().size());


			//Ahora intentamos eliminar una sucursal inexistente.
			assertEquals(0, superAndes.eliminarSucursal(s.getId()));

			s = superAndes.adicionarSucursal("B/Manga", "Clla 153", "Paji", 50, 50, "Jumbo");
			//prueba que elimine por nombre
			tuplasEl = superAndes.eliminarSucursalPorNombre(s.getNombre());
			//Solo elimino una sucursal.
			assertEquals(1, tuplasEl);

			s = superAndes.adicionarSucursal("B/Manga", "Clla 153", "Paji", 50, 50, "Jumbo");
			Sucursal noE  = superAndes.adicionarSucursal("B/Manga", "Clla 153", "Paji", 50, 50, "Jumbo");

			//La debe agregar.
			assertNotNull(s);
			//No la deber�a agregar ya que el supermercado cuenta con una con el mismo nombre
			assertNull(noE);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			String msg = "Error en la ejecución de las pruebas de operaciones sobre la tabla TipoBebida.\n";
			msg += "Revise el log de superAndes y el de datanucleus para conocer el detalle de la excepción";
			System.out.println (msg);

			fail ("Error en las pruebas sobre la tabla TipoBebida");
		}
		finally
		{
			superAndes.limpiarSuperAndes ();
			superAndes.cerrarUnidadPersistencia ();    		
		}
	}
	@Test
	public void CRDBodegaEstanteTest()
	{
		try
		{
			//Bodegas y estantes que deber�an haber inicialmente
			assertEquals(0, superAndes.darBodegas().size());
			assertEquals(0, superAndes.darEstantes().size());

			Sucursal ejemplo = superAndes.darSucursales().get(2);

			Bodega b = superAndes.adicionarBodega(150, ejemplo.getId(), 1000, "Carnes");
			Estante e = superAndes.adicionarEstante(200, ejemplo.getId(), 1300, "Carnes");
			//verificar que se hayan agregado

			assertNotNull(b);
			assertNotNull(e);

			assertEquals(1, superAndes.darBodegas().size());
			assertEquals(1, superAndes.darEstantes().size());


			long tuplElBg = superAndes.eliminarBodegaPorId(b.getId());
			long tuplElEst = superAndes.eliminarEstantePorId(e.getId());

			//Verificamos que haya eliminado a los dos

			assertEquals(0, tuplElBg-tuplElEst);

			//La sucursal no deber�a tener bodegas ni estante

			assertEquals(0, superAndes.darBodegasSucursal(ejemplo.getId()).size());
			assertEquals(0, superAndes.darEstantesSucursal(ejemplo.getId()).size());

			//Verificaci�n de que el estante y bodega se asignen a una sucursal correcta

			Sucursal ejemplo2 = superAndes.darSucursales().get(0);

			b = superAndes.adicionarBodega(150, ejemplo2.getId(), 1000, "Lacteos");
			e = superAndes.adicionarEstante(200, ejemplo2.getId(), 1300, "Lacteos");

			//La sucursal ejemplo no debe tener
			assertEquals(0, superAndes.darBodegasSucursal(ejemplo.getId()).size());
			assertEquals(0, superAndes.darEstantesSucursal(ejemplo.getId()).size());
			//La sucursal ejemplo2 deber�a tener una bodega y un estante
			assertEquals(1, superAndes.darBodegasSucursal(ejemplo2.getId()).size());
			assertEquals(1, superAndes.darEstantesSucursal(ejemplo2.getId()).size());			

		}
		catch (Exception e)
		{
			e.printStackTrace();
			String msg = "Error en la ejecución de las pruebas de operaciones sobre la tabla TipoBebida.\n";
			msg += "Revise el log de superAndes y el de datanucleus para conocer el detalle de la excepción";
			System.out.println (msg);

			fail ("Error en las pruebas sobre la tabla TipoBebida");
		}
		finally
		{
			superAndes.limpiarSuperAndes ();
			superAndes.cerrarUnidadPersistencia ();    		
		}
	}

	@Test
	public void CRDUsuarioTest()
	{
		try
		{
			//Bodegas y estantes que deber�an haber inicialmente
			assertEquals(0, superAndes.darUsuarios().size());

			Sucursal ejemplo = superAndes.darSucursales().get(4);

			superAndes.adicionarUsuario("lll", "laura", ejemplo.getId());
			//Deber�a agregar al usuario fj.gonzalez
			assertEquals(1, superAndes.darUsuarios().size());

			superAndes.adicionarUsuario("lll", "laura", ejemplo.getId());
			//No deber�a agregarlo ya que ya existe.
			assertEquals(1, superAndes.darUsuarios().size());


			//verificamos que el metodo darUsuario funcione
			assertNotNull(superAndes.darUsuario("lll"));

			long t = superAndes.eliminarUsuario("lll");

			//lo elimino

			assertEquals(1, t);

			t = superAndes.eliminarUsuario("fj.gonzalez");

			//No elimina
			assertEquals(0, t);

		}
		catch (Exception e)
		{

			String msg = "Error en la ejecución de las pruebas de operaciones sobre la tabla TipoBebida.\n";
			msg += "Revise el log de superAndes y el de datanucleus para conocer el detalle de la excepción";
			System.out.println (msg);

			fail ("Error en las pruebas sobre la tabla Usuario");
		}
		finally
		{
			superAndes.limpiarSuperAndes ();
			superAndes.cerrarUnidadPersistencia ();    		
		}
	}

	@Test
	public void CRDClienteTest()
	{
		try
		{
			//verificacion de que  hay 5 clientes inicialmente en la base de datos
			assertEquals(5, superAndes.darClientes().size());

			ClienteEmpresa ce = superAndes.darClienteEmpresa("84335");

			ClienteNatural cn = superAndes.darClienteNatural("222");

			assertNotNull(ce);
			assertNotNull(cn);

			//No deber�a obtener nada
			assertNull(superAndes.darClienteNatural("34"));

			assertEquals (1, superAndes.eliminarClienteEmpresaPorId(ce.getId())[1]);
			assertEquals (1, superAndes.eliminarClientePorCedula(cn.getCedula()));

			ce = superAndes.darClienteEmpresa("90002");

			superAndes.sumarPuntosACliente(ce.getId(), 500);

			ce = superAndes.darClienteEmpresa("90002");

			assertEquals(534, ce.getPuntos());




		}
		catch (Exception e)
		{

			String msg = "Error en la ejecución de las pruebas de operaciones sobre la tabla TipoBebida.\n";
			msg += "Revise el log de superAndes y el de datanucleus para conocer el detalle de la excepción";
			System.out.println (msg);

			fail ("Error en las pruebas sobre la tabla Usuario");
		}
		finally
		{
			superAndes.limpiarSuperAndes ();
			superAndes.cerrarUnidadPersistencia ();    		
		}
	}

	@Test
	public void CRDCarritoTest()
	{
		try
		{
			ClienteNatural c = superAndes.darClienteNatural("111");
			Sucursal s = superAndes.darSucursales().get(1);
			Bodega b = superAndes.adicionarBodega(150, s.getId(), 1000, "Lacteos");
			Producto pr = superAndes.darProductoPorNombre("Lechita").get(0);
			Estante e = superAndes.adicionarEstante(200, s.getId(), 1300, "Lacteos");
			ProductoSuministrado ps = superAndes.adicionarProductoSum(4000, 150, "40gr/", pr.getCodigoDeBarras(), 150, e.getId(), b.getId(), s.getId());
			assertEquals(1,superAndes.darProductosSuministrado().size());
			//efectivamente agrego el producto al carro
			superAndes.adicionarProductoAlCliente(c.getId(), ps.getCodigoDeBarras(), 10, s.getId());
			assertEquals(1, superAndes.darProductosDelCliente(c.getId()).size());

			//eliminar el producto al carro
			superAndes.eliminarProductoAlCliente(c.getId(), ps.getCodigoDeBarras(), s.getId());
			assertEquals(0, superAndes.darProductosDelCliente(c.getId()).size());

			//documentamos una falla.
			superAndes.adicionarProductoAlCliente(c.getId(), ps.getCodigoDeBarras(), 6, s.getId());
			assertNull(superAndes.adicionarProductoAlCliente(c.getId(), ps.getCodigoDeBarras(), 6, s.getId()));


			//abandona el carro
			superAndes.abandonarCarro(c.getId());

			c = superAndes.darClienteNatural("111");

			//Efectivamente se abandono el carro.
			assertEquals("V", c.getCarroAbandonado());
			//A�n est� el producto en el carro.
			assertEquals(1, superAndes.darProductosDelCliente(c.getId()).size());

			superAndes.devolverProductosDeCarrosAbandonados(s.getId());
			//Ya no est� el producto en el carro.
			assertEquals(0, superAndes.darProductosDelCliente(c.getId()).size());

			c = superAndes.darClienteNatural("111");


			assertEquals("F", c.getCarroAbandonado());


		}
		catch (Exception e)
		{
			e.printStackTrace();
			String msg = "Error en la ejecución de las pruebas de operaciones sobre la tabla TipoBebida.\n";
			msg += "Revise el log de superAndes y el de datanucleus para conocer el detalle de la excepción";
			System.out.println (msg);

			fail ("Error en las pruebas sobre la tabla Carrito");
		}
		finally
		{
			superAndes.limpiarSuperAndes ();
			superAndes.cerrarUnidadPersistencia ();    		
		}
	}

	@Test
	public void CRDConsolidarPedidoTest()
	{
		try
		{
			Proveedor p1 = superAndes.darProveedores().get(0);
			Proveedor p2 = superAndes.darProveedores().get(1);
			Proveedor p3 = superAndes.darProveedores().get(2);

			Sucursal s = superAndes.darSucursales().get(1);

			superAndes.adicionarOrdenPedido(new Timestamp(System.currentTimeMillis()), "EN_PROCESO", 2, p2.getNIT(), s.getId(), new Timestamp(System.currentTimeMillis()));
			superAndes.adicionarOrdenPedido(new Timestamp(System.currentTimeMillis()), "EN_PROCESO", 3, p1.getNIT(), s.getId(), new Timestamp(System.currentTimeMillis()));
			superAndes.adicionarOrdenPedido(new Timestamp(System.currentTimeMillis()), "EN_PROCESO", 4, p2.getNIT(), s.getId(), new Timestamp(System.currentTimeMillis()));
			superAndes.adicionarOrdenPedido(new Timestamp(System.currentTimeMillis()), "EN_PROCESO", 5, p3.getNIT(), s.getId(), new Timestamp(System.currentTimeMillis()));
			superAndes.adicionarOrdenPedido(new Timestamp(System.currentTimeMillis()), "EN_PROCESO", 1, p1.getNIT(), s.getId(), new Timestamp(System.currentTimeMillis()));
			superAndes.adicionarOrdenPedido(new Timestamp(System.currentTimeMillis()), "EN_PROCESO", 2, p2.getNIT(), s.getId(), new Timestamp(System.currentTimeMillis()));
			superAndes.adicionarOrdenPedido(new Timestamp(System.currentTimeMillis()), "EN_PROCESO", 3, p3.getNIT(), s.getId(), new Timestamp(System.currentTimeMillis()));
			superAndes.adicionarOrdenPedido(new Timestamp(System.currentTimeMillis()), "EN_PROCESO", 4, p1.getNIT(), s.getId(), new Timestamp(System.currentTimeMillis()));
			superAndes.adicionarOrdenPedido(new Timestamp(System.currentTimeMillis()), "EN_PROCESO", 4, p1.getNIT(), s.getId(), new Timestamp(System.currentTimeMillis()));
			superAndes.adicionarOrdenPedido(new Timestamp(System.currentTimeMillis()), "EN_PROCESO", 4, p1.getNIT(), s.getId(), new Timestamp(System.currentTimeMillis()));
			superAndes.adicionarOrdenPedido(new Timestamp(System.currentTimeMillis()), "ENTREGADA", 4, p2.getNIT(), s.getId(), new Timestamp(System.currentTimeMillis()));
			superAndes.adicionarOrdenPedido(new Timestamp(System.currentTimeMillis()), "EN_PROCESO", 4, p3.getNIT(), s.getId(), new Timestamp(System.currentTimeMillis()));
			superAndes.adicionarOrdenPedido(new Timestamp(System.currentTimeMillis()), "EN_PROCESO", 4, p3.getNIT(), s.getId(), new Timestamp(System.currentTimeMillis()));

			//Agrego las 13 ordenes.
			assertEquals(13, superAndes.darOrdenesPedido().size());

			//El proveedor 1 si tiene las 5 ordenes.
			assertEquals(5,superAndes.darOrdenesPedidoProveedor(p1.getNIT()).size());

			superAndes.consolidarOrdenesPedido(s.getId(), new Timestamp(System.currentTimeMillis()));

			List<OrdenPedido> ordenesPedido =  superAndes.darOrdenesPedido();
			assertEquals(4, ordenesPedido.size());
			int contadorConsolida = 0;
			int contadorDemas = 0;
			for (OrdenPedido ordenPedido : ordenesPedido) 
			{
				if(ordenPedido.getEstado().contains("CONSO"))
				{
					contadorConsolida++;
				}
				else
				{
					contadorDemas++;
				}

			}
			assertEquals(3, contadorConsolida);
			assertEquals(1, contadorDemas);

			superAndes.registrarSalidaOrdenPedidoConsolidada(p2.getNIT());
			superAndes.registrarLLegadaOrdenPedidoConsolidadaProveedor(p2.getNIT(), 4);

			List<OrdenPedido> op = 	superAndes.darOrdenesPedidoProveedor(p2.getNIT());

			for (OrdenPedido ordenPedido : op)
			{
				if(!ordenPedido.getEstado().contains("ENTR"))
				{
					fail("No registro la llegada de las ordenes");
				}
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			String msg = "Error en la ejecución de las pruebas de operaciones sobre la tabla TipoBebida.\n";
			msg += "Revise el log de superAndes y el de datanucleus para conocer el detalle de la excepción";
			System.out.println (msg);

			fail ("Error en las pruebas sobre la tabla Carrito");
		}
		finally
		{
			superAndes.limpiarSuperAndes ();
			superAndes.cerrarUnidadPersistencia ();    		
		}


	}
	@Test
	public void CRDProveedorTest()
	{
		try
		{
			assertEquals(3, superAndes.darProveedores().size());

			Proveedor p = superAndes.darProveedor("18");

			assertNotNull(p);

			assertNull(superAndes.darProveedor("16"));

			assertNull(superAndes.adicionarProveedor("17", "Nestle"));

			assertEquals (1, superAndes.eliminarProveedor("21"));

			assertEquals(2, superAndes.darProveedores().size());

			assertEquals(0, superAndes.eliminarProveedor("21"));


			p = superAndes.darProveedor("17");

			String pr = superAndes.darProductos().get(0).getCodigoDeBarras();

			superAndes.adicionarProductoProveedor(pr, p.getNIT());

			assertEquals(1,superAndes.darProductosProveedor().size());

			superAndes.modificarCalificacionDelProveedor(p.getNIT(), 5);

			p = superAndes.darProveedor("17");

			assertEquals("5.0", p.getCalificacion().toString());
		}
		catch (Exception e)
		{
			e.printStackTrace();
			String msg = "Error en la ejecución de las pruebas de operaciones sobre la tabla TipoBebida.\n";
			msg += "Revise el log de superAndes y el de datanucleus para conocer el detalle de la excepción";
			System.out.println (msg);

			fail ("Error en las pruebas sobre la tabla Carrito");
		}
		finally
		{
			superAndes.limpiarSuperAndes ();
			superAndes.cerrarUnidadPersistencia ();    		
		}
	}
	@SuppressWarnings("deprecation")
	@Test
	public void CRDPromocionTest()
	{
		try
		{

			Timestamp fechaExp = new Timestamp(2018,9, 27, 0, 0, 0, 0);
			List<Producto> lis =  superAndes.darProductos();
			Sucursal s = superAndes.darSucursales().get(3);
			Estante e = superAndes.adicionarEstante(200, s.getId(), 1300, "Carnes");
			Bodega b = superAndes.adicionarBodega(150, s.getId(), 1000, "Carnes");
			for (int i = 0; i < lis.size(); i++) 
			{
				superAndes.adicionarProductoSum(6500, 100, "safdg", lis.get(i).getCodigoDeBarras(), 100, e.getId(), b.getId(), s.getId());

			}
			assertEquals(0, superAndes.darPromociones().size());

			System.out.println(lis.size());

			PromocionDescuento pd=	superAndes.adicionarPromocionDescuento(fechaExp, fechaExp, lis.get(0).getCodigoDeBarras(), null, 1, 2, "Prom1", 50, -1);
			PromocionDosProductos p2p = 	superAndes.adicionarPromocionDosProductos(fechaExp, fechaExp, lis.get(1).getCodigoDeBarras(),lis.get(2).getCodigoDeBarras() , 1, 5, "Prom4", 20000);
			PromocionPague1LleveElSegundo p12 = 	superAndes.adicionarPromocionPague1LleveElSegundo(fechaExp, fechaExp, lis.get(3).getCodigoDeBarras(), null, 1, 4, "Prom2", 25);

			assertEquals(3, superAndes.darPromociones().size());


			superAndes.acabarPromocion(new Timestamp(20000000), pd.getId());
			
			Promocion p = superAndes.darPromocion(pd.getId());
			assertEquals("I", p.getEstado());
			
			assertEquals(1,superAndes.eliminarPromocion(p12.getId())[5]);


		}
		catch (Exception e)
		{
			e.printStackTrace();
			String msg = "Error en la ejecución de las pruebas de operaciones sobre la tabla TipoBebida.\n";
			msg += "Revise el log de superAndes y el de datanucleus para conocer el detalle de la excepción";
			System.out.println (msg);

			fail ("Error en las pruebas sobre la tabla Carrito");
		}
		finally
		{
			superAndes.limpiarSuperAndes ();
			superAndes.cerrarUnidadPersistencia ();    		
		}
	}
	/* **********************
	 * 			Métodos de configuración
	 ***********************/
	/**
	 * Lee datos de configuración para la aplicación, a partir de un archivo JSON o con valores por defecto si hay errores.
	 * @param tipo - El tipo de configuración deseada
	 * @param archConfig - Archivo Json que contiene la configuración
	 * @return Un objeto JSON con la configuración del tipo especificado
	 * 			NULL si hay un error en el archivo.
	 */
	private JsonObject openConfig (String archConfig)
	{
		JsonObject config = null;
		try 
		{
			Gson gson = new Gson( );
			FileReader file = new FileReader (archConfig);
			JsonReader reader = new JsonReader ( file );
			config = gson.fromJson(reader, JsonObject.class);
			log.info ("Se encontró un archivo de configuración de tablas válido");
		} 
		catch (Exception e)
		{
			//			e.printStackTrace ();
			log.info ("NO se encontró un archivo de configuración válido");			
			JOptionPane.showMessageDialog(null, "No se encontró un archivo de configuración de tablas válido: ", "TipoBebidaTest", JOptionPane.ERROR_MESSAGE);
		}	
		return config;
	}	
}